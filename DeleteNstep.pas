unit DeleteNstep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,Unit0_globalVariant, sEdit, sSpinEdit;

type
  TFormDeleteN = class(TForm)
    lbl1: TLabel;
    lbl2: TLabel;
    btn1: TButton;
    edt1: TsDecimalSpinEdit;
    edt2: TsDecimalSpinEdit;
    procedure btn1Click(Sender: TObject);
    procedure edt2Change(Sender: TObject);
    procedure edt1Change(Sender: TObject);
    procedure btn1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormDeleteN: TFormDeleteN;

implementation

uses main;

{$R *.dfm}

procedure TFormDeleteN.btn1Click(Sender: TObject);
begin
 if MessageDlg('删除从步骤'+edt1.text+'到步骤'+edt2.text+'之间的所有步骤'
          +',LCD显示屏显示删除过程',  mtWarning, mbOKCancel, 0) = mrOk then
  begin
    modbusfun16int:=$2F23;     //
    modbusfun16len:=$0004;
    //sbuf[1]:=1;
    sbuf[2]:=$10;

    sbuf[8+2*0]:=StrToInt('0'+edt1.Text) div 256;
    sbuf[9+2*0]:=StrToInt('0'+edt1.Text) mod 256;
    sbuf[8+2*1]:=StrToInt('0'+edt2.Text) div 256;
    sbuf[9+2*1]:=StrToInt('0'+edt2.Text) mod 256;
    addToLog('删除第'+edt1.Text+'~'+edt2.Text+'步骤' );

    close;
  end;

end;

procedure TFormDeleteN.edt2Change(Sender: TObject);
var ii:SmallInt;
begin

end;

procedure TFormDeleteN.edt1Change(Sender: TObject);
var ii,lineInsert,lineInsert2:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持
    for ii:=1 to 50 do begin        //颜色先还原
      zu1edit[ii].Color:=clWhite;
      zu2edit[ii].Color:=clWhite;
      zu3edit[ii].Color:=clWhite;

      zu5edit[ii].Color:=clWhite;
      zu6edit[ii].Color:=clWhite;
      zu7edit[ii].Color:=clWhite;
      zu8edit[ii].Color:=clWhite;
      zu9edit[ii].Color:=clWhite;
      zu10edit[ii].Color:=clWhite;
      zu11edit[ii].Color:=clWhite;
      zu12edit[ii].Color:=clWhite;
      zu13edit[ii].Color:=clWhite;
      zu14edit[ii].Color:=clWhite;
      zu15edit[ii].Color:=clWhite;
      zu16edit[ii].Color:=clWhite;
    end;
    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                             //位置大于1，
          and (StrToInt(edt1.Text)>=((StrToInt(form_line.edt45.text)div 50)*50) ) //大于当前页起始步骤
          and (StrToInt(edt2.Text)>=StrToInt(edt1.Text)) )        //且步骤数
      //  and ((StrToInt(edt2.Text)div 50)=(StrToInt(edt1.Text)div 50))
    then begin

      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      lineInsert2:= ( StrToInt(edt2.Text)-StrToInt(edt1.Text) )+1; //步骤数
      for ii:=1 to lineInsert2 do begin
        if(lineInsert+ii)>50 then Break;

        zu1edit [lineInsert+ii].Color:=clMoneyGreen;
        zu2edit [lineInsert+ii].Color:=clMoneyGreen;
        zu3edit [lineInsert+ii].Color:=clMoneyGreen;

        zu5edit [lineInsert+ii].Color:=clMoneyGreen;
        zu6edit [lineInsert+ii].Color:=clMoneyGreen;
        zu7edit [lineInsert+ii].Color:=clMoneyGreen;
        zu8edit [lineInsert+ii].Color:=clMoneyGreen;
        zu9edit [lineInsert+ii].Color:=clMoneyGreen;
        zu10edit[lineInsert+ii].Color:=clSkyBlue;
        zu11edit[lineInsert+ii].Color:=clMoneyGreen;
        zu12edit[lineInsert+ii].Color:=clMoneyGreen;
        zu13edit[lineInsert+ii].Color:=clMoneyGreen;
        zu14edit[lineInsert+ii].Color:=clMoneyGreen;
        zu15edit[lineInsert+ii].Color:=clMoneyGreen;
        zu16edit[lineInsert+ii].Color:=clMoneyGreen;
      end;
    end;
  end;
end;

procedure TFormDeleteN.btn1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_ESCAPE then begin
    Close;
  end;
//会连击？？  if Key=VK_DELETE then begin
//    btn1Click(Self);
//  end;

end;

end.
