{  描述：     main主窗口生成及显示专用函数文件；
  文件名：	  Unit_MaincreateSHowCode...
  应用语言:   delphi
  版本：			V1.0
  苏州周至电子科技有限公司http://zhzhi.cn
  Copyright(C) Jin 2013   All rights reserved
  作者：Jin
  建立日期：2019-05-09
//=======重点===================================================================
//1.主窗口生成cReate函数，
//2.主窗口显示函数Show函数；
}

 

unit Unit1_MainCreateShowlCode;
interface

uses
  Windows, Messages, SysUtils,Variants, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ActnList, NB30, registry, shellapi, dbclient, inifiles, db, adodb,

  iComponent, iVCLComponent, iCustomComponent, iPlotComponent, iPlot,
  IdUDPBase, IdUDPServer, IdBaseComponent, IdComponent, IdIPWatch, ScktComp,
  IdAntiFreezeBase, IdAntiFreeze,
  IdTCPConnection, IdTCPClient,
  IdSocketHandle,IdUDPClient,
  Menus, ToolWin, sScrollBox, iLed, iLedRound, sMemo, iLabel,
  iEditCustom, iEdit, iLedArrow, sLabel, iLedMatrix , RzLaunch,

   FileCtrl, StdCtrls;

const
  tttttt:SmallInt =256;

Var

  tttt:SmallInt;
 /////////////--------------------函数------------------------------------
  procedure MatrixLed_createProcess(len:integer);         //矩阵LED指示灯生成
  procedure ReadConfig_iniProcess(len:integer);         //读取Ini文件，并配置PC软件
  procedure PNbarcode_fileCreateProcess(len:integer);
  procedure sysParaKey(Sender:TObject;key:Word;hh:SmallInt);
  procedure stepKeyEdit(key:Word;hh:SmallInt);
  procedure bakToServerDisk();
  
implementation
uses StepCopy,main,OSstudy,FindPoint,PNenter,LoadFile,Unit0_globalVariant,Unit4_logFileCode,InsertNstep;


procedure ReadConfig_iniProcess(len:integer);         //读取Ini文件，并配置PC软件
var temp:byte;
    i:SmallInt;
    nrf_dat_config: Tinifile;
    save_file_name,ini_path,sst: string;

begin

  with Form_line do begin
    /////////////读配置文件---------------------------------------------------
    ini_path := ExtractFilePath(application.ExeName) + 'config.ini';
    try
      nrf_dat_config := Tinifile.Create(ini_path);    //关联INI文件

    //-----------------------须提前读取的[Comm Para]组机种参数---------------------------------------------------------------------
      if nrf_dat_config.Readstring('Comm Para', 'Path', '')<>'' then      //不为空
        begin
        edt2.Text:=nrf_dat_config.Readstring('Comm Para', 'Path', '');
        if not DirectoryExists(edt2.Text) then                            //FileExists
          edt2.Text:=ExtractFilePath(application.ExeName) ;
        end
      else
        edt2.Text:=ExtractFilePath(application.ExeName) ;

      if nrf_dat_config.Readstring('Comm Para', 'Uplocal', '')<>'' then
        edt38.Text:=nrf_dat_config.Readstring('Comm Para', 'Uplocal', '');     //是否保存在本地磁盘
      if nrf_dat_config.Readstring('Comm Para', 'barcodeConfig', '')<>'' then begin
        Label7.hint:=nrf_dat_config.Readstring('Comm Para', 'barcodeConfig', '');     //是否清barcode
      end else begin
        if nrf_dat_config.Readstring('Comm Para', 'clearbarcode', '')<>'' then
          Label7.hint:=nrf_dat_config.Readstring('Comm Para', 'clearbarcode', '');     //是否清barcode
      end;
      if not IsNumberic(Label7.hint) then
        Label7.hint:='2'
      else begin
        if StrToInt(Label7.hint)>=1000 then begin
          grp36.Visible:=false;
          Label7.hint:=IntToStr(StrToInt(Label7.hint)mod 1000)
        end;
      end;
      if Label7.hint='2' then begin      //不使能条码
        Label7.Color:=clGray;
      end else begin
        Label7.Color:=clGreen;
      end;
      if Label7.hint='11' then begin     //双工位使能
        medit1.Visible:=True;
        iLedRound1.Visible:=true;
        iLedRound2.Visible:=true;
        iLabel1.Visible:=True;
        iLabel2.Visible:=true;
        if(Label7.hint='11') then begin          //位置对齐
           iledround1.Top:= Rleds[0].Top;
           iledround1.Left:= Rleds[0].Left;
           ilabel1.Top:=  iledround1.Top+70;
           ilabel1.Left:= iledround1.Left+50;

           iledround2.Top:= Rleds[1].Top;
           iledround2.Left:= Rleds[1].Left;
           ilabel2.Top:=  iledround2.Top+70;
           ilabel2.Left:= iledround2.Left+50;
        end;
      end else begin
        medit1.Visible:=False;
        edit1.Width:=800;
      end;

      if nrf_dat_config.Readstring('Comm Para', 'barcodeMinLen', '')<>'' then      //barcodeMinlen 最小长度
        edt55.Text:=nrf_dat_config.Readstring('Comm Para', 'barcodeMinLen', '')
      else
        edt55.Text:='1';
      if nrf_dat_config.Readstring('Comm Para', 'barcodeSelT', '')<>'' then begin     //barcode长度
        edt76.Text:=nrf_dat_config.Readstring('Comm Para', 'barcodeSelT', '');
        edt76.Visible:=true;
        lbl139.Visible:=True;
      end;
    //------------------------------------[Result]组机种参数---------------------------------------------------------------------

      if nrf_dat_config.Readstring('result', 'PassOverT', '')<>''then
        edt40.Text:= nrf_dat_config.Readstring('result', 'PassOverT', '');
      if nrf_dat_config.Readstring('result', 'NGOverT', '')<>''then
        edt42.Text:= nrf_dat_config.Readstring('result', 'NGOverT', '');

      if nrf_dat_config.Readstring('result', 'StopMaxCount', '')<>''then     //计数到，停止工作
      begin
        edt111.Text:= nrf_dat_config.Readstring('result', 'StopMaxCount', '');
        Form_line.grp46.Visible:=True;
      end else begin
        btn75.Visible:=false;
        edt111.Visible:=False;
      end;
      if nrf_dat_config.Readstring('result', 'StopYield', '')<>''then     //良率低于，停止工作
      begin
        if IsNumberic(nrf_dat_config.Readstring('result', 'StopYield', '')) then
          lbl82.Tag:=StrToInt(nrf_dat_config.Readstring('result', 'StopYield', ''));
      end else begin
        if nrf_dat_config.Readstring('Comm Para', 'CSVen', '')='2' then
          lbl82.Tag:=90;
      end;
      if lbl82.Tag>0 then begin
        lbl82.Color:=clGreen;
        lbl82.hint:='>='+inttostr(lbl82.Tag)+'%';
      end;

      if nrf_dat_config.Readstring('result', 'StopYieldTotal', '')<>''then     //启用良率的OK数量，停止工作
      begin
        if IsNumberic(nrf_dat_config.Readstring('result', 'StopYieldTotal', '')) then
          lbl80.Tag:= StrToInt(nrf_dat_config.Readstring('result', 'StopYieldTotal', ''));
      end else begin
        if nrf_dat_config.Readstring('Comm Para', 'CSVen', '')='2' then
          lbl80.Tag:=20;
      end;
      if lbl80.Tag>0 then begin
        lbl80.Color:=clGreen;
        lbl80.hint:='>='+inttostr(lbl80.Tag);
      end;

      if nrf_dat_config.Readstring('result', 'CloseReport', '')='1'then  begin
        grp17.Visible:=False;lbl169.Left:=0;Label11.Left:=8;
      end else begin
        grp17.Visible:=true; lbl169.Left:=120;Label11.Left:=128;
      end;
      if(Readchis('result','BarcodeStart')='1' ) then begin
        grp32.Caption:=grp32.Caption+'---注意：启用了<接收到条码[回车符]立即 启动测试>！';
      end else if( (Readchis('result','UDPBarcodeStart')='1' )or(Readchis('result','UDPBarcodeStart')='ON' ) ) then begin
        grp32.Caption:=grp32.Caption+'---注意：启用了<接收到[UDP]条码立即 启动测试>！';
      end;
    //------------------------------------[Server]组机种参数---------------------------------------------------------------------

    //------------------------------------[Comm Para]组机种参数---------------------------------------------------------------------
      if nrf_dat_config.Readstring('Comm Para', 'UploadServerT', '')<>'' then begin     //不为空 ，>0上传映射服务器间隔分钟数
        edt6.Text:=nrf_dat_config.Readstring('Comm Para', 'UploadServerT', '');
        edt6.Hint:=nrf_dat_config.Readstring('Comm Para', 'UploadServerHint', '');      //早期：=UpSinglePCS 两种文件剪切到映射盘 ；否则升级1：生成临时文件夹temp\并剪切到映射盘  //jaguar连片启用 单PCS也上传
        if nrf_dat_config.Readstring('Comm Para', 'UploadServerNoBarcode', '')<>'' then
          lbl2.Hint:=nrf_dat_config.Readstring('Comm Para', 'UploadServerNoBarcode', '')      //UploadServer(minute) 标签；
        else
          lbl2.Hint:='ERROR';
      end else
        edt6.Text:='0';                         //不传映射盘    '1440';   //24小时
      if(StrToInt(edt6.Text)>0)  then begin     //显示服务器文件路径

        grp1.Visible:=True;
        grp1.Caption:='Server Path 网盘路径';
        if nrf_dat_config.Readstring('Comm Para', 'PathServer', '')<>'' then      //不为空
        begin
          edt1.Text:=nrf_dat_config.Readstring('Comm Para', 'PathServer', '');
          if not DirectoryExists(edt1.Text) and (StrToInt(edt6.Text)>0) then
            begin
              edt1.Color:=clRed;
              ShowMessage('Server Path not exist!网盘地址不存在！');
            end;
        end else
          edt1.Text:=ExtractFilePath(application.ExeName) ;
      end  else
        grp1.Visible:=False;      //不显示服务器文件路径

      if nrf_dat_config.Readstring('Comm Para', 'HIDdelay', '')<>'' then      //允hid通信延时
        edt15.Text:= nrf_dat_config.Readstring('Comm Para', 'HIDdelay', '');
      if nrf_dat_config.Readstring('Comm Para', 'COMMdelay', '')<>'' then      //允hid通信延时
        edt14.Text:= nrf_dat_config.Readstring('Comm Para', 'COMMdelay', '');

            /////////////////是否生产csv记录文件
      if nrf_dat_config.Readstring('Comm Para', 'OneDayOneFile', '')<>'' then begin
        label10.Hint:=nrf_dat_config.Readstring('Comm Para', 'OneDayOneFile', '');
        if IsNumberic(label10.Hint)  then
          label10.Tag:=StrToInt(label10.Hint);
      end;
      for i:=1 to UpStepMax do
      begin
        ScaleZu[i]:='1';
        if nrf_dat_config.Readstring('Model Para', 'Scale'+inttostr(i), '')<>'' then      //不为空
          begin
            ScaleZu[i]:= nrf_dat_config.Readstring('Model Para', 'Scale'+inttostr(i), '');
          end;
      end;
      if (nrf_dat_config.Readstring('Comm Para', 'CSVen', '')='2')or(nrf_dat_config.Readstring('Comm Para', 'CSVen', '')='Tesla') then      //tesla格式
      begin
        edt47.Text:='2'; grp1.Tag:=2;
        grp18.Visible:=True;
        if Readchis('Model Para', 'LoginItem4')=''  then lbl93.Caption:='1号夹具';           //ts9.Visible:=False;
      end else begin      //其他CSV格式文件或者tab格式文本文件
        if nrf_dat_config.Readstring('Comm Para', 'CSVen', '')<>'' then begin
          edt47.Text:=nrf_dat_config.Readstring('Comm Para', 'CSVen', '');
        end;

      end;
      /////////////////是否网口连接，-----------------------------------------------------

      if nrf_dat_config.Readstring('Comm Para', 'CommOpen', '')<>'' then begin
        groupbox1.hint:=nrf_dat_config.Readstring('Comm Para', 'CommOpen', '');     //是否启动软件打开串口
        //触摸屏翻页命令 btn77.Visible:=true;
        //btn78.Visible:=true;
        if groupbox1.hint='5' then begin       //定义FCT5主板，不不能从主板读取步骤名，则必须从config读取；
          NameCount:=GetNameUnitFromConfig($33);  //PowerOnCreatNewFile:=0;         //Comm1.ReadIntervalTimeout:=400;
          edt14.Text:='600';               //因早期FCT5主板翻页有500mS的延时；
        end;
      end;
      if groupbox1.hint='0' then begin    //非串口，不显示页内翻页
        btn77.Visible:=false;
        btn78.Visible:=false;
      end;
    
      if ComboBox1.Items.IndexOf(nrf_dat_config.Readstring('Comm Para', 'Port', ''))<0 then
        //messagedlg('No data! Pls check .ini!', mterror, [mbyes], 0)
      else begin
        ComboBox1.Text := nrf_dat_config.Readstring('Comm Para', 'Port', '');
        cbb3.Text:= ComboBox1.Text;
      end;

      if ComboBox2.Items.IndexOf(nrf_dat_config.Readstring('Comm Para', 'Baud', ''))<0 then
        //messagedlg('No data! Pls check .ini!', mterror, [mbyes], 0)
      else begin
        ComboBox2.Text :=nrf_dat_config.Readstring('Comm Para', 'Baud', '') ;
        cbb2.Text:= ComboBox2.Text;
      end;
      /////////////////是否显示调试界面,DebugMode有更高优先级

      if nrf_dat_config.Readstring('Comm Para', 'DebugMode', '')='1' then begin     //调试 模式
        Ts3.TabVisible:=True;
        rb5.Checked:=True;             //数据监控解析     允许调试助手界面
        lbl79.Color:=clGray;
      end else begin
        Ts3.TabVisible:=False;
        rb4.Checked:=True;             //仅显示串口数据；
        lbl79.Color:=clGreen;
      end;

      if nrf_dat_config.Readstring('Comm Para', 'ChildFolder', '')<>'' then
      begin
        edt62.Text:=nrf_dat_config.Readstring('Comm Para', 'ChildFolder', '');
        if (edt62.Text='25') then  begin

          if nrf_dat_config.Readstring('Comm Para', 'ReTest', '')<>'' then begin     //不为空
             edt169.Text:=nrf_dat_config.Readstring('Comm Para', 'ReTest', '');
             grp43.Visible:=true;
          end;
          btn101.Enabled:=False;
          btn130.Visible:=True;
          edt137.Text:='FAIL';
          if nrf_dat_config.Readstring('server', 'StandardIP', '')<>'' then
            edt141.Text:= nrf_dat_config.Readstring('server', 'StandardIP', '')
          else
            edt141.Text:='mycsmtet.mflex.com.cn/';

          btn118.Caption:='ET测试前校验[ABP2]';
          if nrf_dat_config.Readstring('server', 'BPCommand', '')<>'' then
            edt153.Text:= nrf_dat_config.Readstring('server', 'BPCommand', '')
          else
            edt153.Text:='/prevalidation?';

          btn114.Caption:='ET测试结果上传[2BU0]';
          if nrf_dat_config.Readstring('server', 'BUCommand', '')<>'' then
            edt154.Text:= nrf_dat_config.Readstring('server', 'BUCommand', '')
          else
            edt154.Text:='';  //'batches';
          if nrf_dat_config.Readstring('server', 'DoneCommand', '')<>'' then
            edt155.Text:= nrf_dat_config.Readstring('server', 'DoneCommand', '')
          else
            edt155.Text:='---';
          if nrf_dat_config.Readstring('server', 'DirCommand', '')<>'' then
            edt156.Text:= nrf_dat_config.Readstring('server', 'DirCommand', '')
          else
            edt156.Text:='api/ettestrecords';          //取消 /

        //  btn119.Enabled:=false;
          if (Readchis('Server','MESfun')<>'') then begin
            chk25.Visible:=true;
            chk26.Visible:=true;
            chk27.Visible:=true;
            chk28.Visible:=true;

            btn130.Caption:='MES联机';
            btn130.Font.Color:=clGreen;
            btn130.Hint:=Readchis('Server','MESfun');
            WriteChis('Server','MESdefault',btn130.Hint);        //把设置的MES代码填入默认值；
          end else begin
            btn130.Caption:='MES脱机';
            btn130.Font.Color:=clDefault;
          end;

          //Frrr:=GetDateFormat(DateDelta);                              //获取当前系统日期分隔符
          //Dat:=StrToDate(Format('2016%s4%s11',[Fr,Fr]));  //这样始终都会跟当前系统日期分隔符同步，这样不管你把当前系统日期分隔
          SetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SSHORTDATE, 'yyyy-MM-dd');
          Sleep(100);

         
          grp41.Visible:=True;
        end else if  (Readchis('Server','MESprogram')<>'')or(Readchis('Server','MESprogramdefault')<>'') then begin    //联机启用和脱机启用
          btn56.Caption:='up上传'+char($0A)+'文件';

          grp41.Visible:=True;
          btn101.Enabled:=False;
          btn130.Visible:=True;
          if (Readchis('Server','MESprogramdefault')='auto') then begin   //开机进入联机状态
             WriteChis('Server','MESprogram','auto');
          end;
          if (Readchis('Server','MESprogram')<>'') then begin
            btn130.Caption:='MES联机';
            btn130.Font.Color:=clGreen;
            edt155.Enabled:=true;
            btn130.Hint:=Readchis('Server','MESprogram');
            WriteChis('Server','MESprogramdefault',btn130.Hint);        //把设置的MES代码填入默认值；
          end else begin
            btn130.Caption:='MES脱机';
            btn130.Font.Color:=clDefault;
            edt155.Enabled:=False;
          end;

          if nrf_dat_config.Readstring('server', 'StandardIP', '')<>'' then
            edt141.Text:= nrf_dat_config.Readstring('server', 'StandardIP', '')
          else
            edt141.Text:='180.166.252.70:17000/';

          btn118.Caption:='从MES加载测试程序';
          if nrf_dat_config.Readstring('server', 'BPCommand', '')<>'' then
            edt153.Text:= nrf_dat_config.Readstring('server', 'BPCommand', '')
          else
            edt153.Text:='/program/dwnfilestojson';

          btn114.Caption:='测试程序上传MES';
          if nrf_dat_config.Readstring('server', 'BUCommand', '')<>'' then
            edt154.Text:= nrf_dat_config.Readstring('server', 'BUCommand', '')
          else
            edt154.Text:='/program/upfilestojson';
        //  btn119.Caption:='MCU校验';
          if nrf_dat_config.Readstring('server', 'DoneCommand', '')<>'' then
            edt155.Text:= nrf_dat_config.Readstring('server', 'DoneCommand', '')
          else
            edt155.Text:='/fct/validfctbymcu';
          if nrf_dat_config.Readstring('server', 'UpCommand', '')<>'' then
            edt180.Text:= nrf_dat_config.Readstring('server', 'UpCommand', '')
          else
            edt180.Text:='/fct/validfctbyledadc';

          edt136.Text:='P00091729H0013S00114T1100010L030W03';
          edt73.Text:='pn-23447700';
          edt83.Text:='rl-AB';
          edt165.Text:='llc-W1';
          edt177.Text:='hv-5.0';               //上海丽清319的硬件版本
          edt178.Text:='sv-S010';
          edt179.Text:='ld-10102019';
          edt142.Text:='cs';//-0x86498332';
          edt177.Visible:=true;
          edt178.Visible:=true;
          edt179.Visible:=true;

          if nrf_dat_config.Readstring('server', 'DirCommand', '')<>'' then
            edt156.Text:= nrf_dat_config.Readstring('server', 'DirCommand', '')
          else
            edt156.Text:='api';

        end else begin

        end;


        if(StrToInt(edt62.Text)>=1)  then      //文件夹参数        //读取FCT主板的机种名；依机种分文件夹生成log，[高级应用]，
          grp18.Visible:=true;                                                  //UMT
 
        if(Form_line.edt62.text='25')  then begin      //jaguar第一/二次扩展

            if nrf_dat_config.Readstring('Comm Para', 'UploadServerT', '')='' then begin     //不启用网盘；
              edt1.Visible:=False;
              btn4.Visible:=False;
              lbl2.Visible:=false;
              edt6.Visible:=False;
              btn2.Visible:=False;
              grp1.Visible:=false;
            end else
              grp1.Visible:=true;

            begin                           //二次升级
              edt157.Visible:=true;
              btn125.Visible:=true;
              lbl180.Visible:=True;
              lbl180.Caption:='SNfilename';
              if nrf_dat_config.Readstring('Comm Para', 'TeslaSNPath', '')<>'' then      //不为空
                begin
                edt157.Text:=nrf_dat_config.Readstring('Comm Para', 'TeslaSNPath', '');
                if not DirectoryExists(edt157.Text) then                            //FileExists
                  edt157.Text:=ExtractFilePath(application.ExeName) ;
                end
              else
                edt157.Text:=ExtractFilePath(application.ExeName) ;

              if nrf_dat_config.Readstring('Comm Para', 'BakPathName', '')<>'' then      //备份文件夹名称，不为空
              begin
                lbl183.Caption:=nrf_dat_config.Readstring('Comm Para', 'BakPathName', '');
              end;
            end;
        end;
        if(edt62.Text='24')  then begin
            nrf_dat_config.WriteString('Server','MESfun','');      //强制脱机
   
            edt157.Visible:=true;
            btn125.Visible:=true;
            lbl180.Visible:=True;
            lbl180.Caption:='SNfilename';
            if nrf_dat_config.Readstring('Comm Para', 'TeslaSNPath', '')<>'' then      //不为空
              begin
              edt157.Text:=nrf_dat_config.Readstring('Comm Para', 'TeslaSNPath', '');
              if not DirectoryExists(edt157.Text) then                            //FileExists
                edt157.Text:=ExtractFilePath(application.ExeName) ;
              end
            else
              edt157.Text:=ExtractFilePath(application.ExeName) ;

            if nrf_dat_config.Readstring('Comm Para', 'BakPathName', '')<>'' then      //备份文件夹名称，不为空
            begin
              lbl183.Caption:=nrf_dat_config.Readstring('Comm Para', 'BakPathName', '');
            end;
        end;
        if nrf_dat_config.Readstring('Comm Para', 'closeFixBarcodeID', '')<>'' then
          edt163.Visible:=false;
      end;

    //------------------------------------[Barcode Check]组机种参数---------------------------------------------------------------------
      edt48.Text:=nrf_dat_config.Readstring('Barcode Check', 'BarCheckEn', '');       //专业：核对2个条码是否匹配！
      if edt48.text='1' then begin
        if(nrf_dat_config.Readstring('Barcode Check', 'Bar1Pos', '')<>'') then
          edt123.Text:=nrf_dat_config.Readstring('Barcode Check', 'Bar1Pos', '');
        if(nrf_dat_config.Readstring('Barcode Check', 'Bar1Len', '')<>'') then
          edt124.Text:=nrf_dat_config.Readstring('Barcode Check', 'Bar1Len', '');
        if(nrf_dat_config.Readstring('Barcode Check', 'Bar2Pos', '')<>'') then
          edt125.Text:=nrf_dat_config.Readstring('Barcode Check', 'Bar2Pos', '');
        if(nrf_dat_config.Readstring('Barcode Check', 'Bar2Len', '')<>'') then
          edt126.Text:=nrf_dat_config.Readstring('Barcode Check', 'Bar2Len', '');
        if(nrf_dat_config.Readstring('Barcode Check', 'Bar1Name', '')<>'') then
          lbl155.Caption:=nrf_dat_config.Readstring('Barcode Check', 'Bar1Name', '');
        if(nrf_dat_config.Readstring('Barcode Check', 'Bar2Name', '')<>'') then
          lbl156.Caption:=nrf_dat_config.Readstring('Barcode Check', 'Bar2Name', '');
      end else begin
        edt48.Visible:=false;
        lbl151.Visible:=false;
        edt123.Visible:=false;
        edt124.Visible:=false;
        edt125.Visible:=false;
        edt126.Visible:=false;
        lbl155.Visible:=false;
        lbl156.Visible:=false;
      end;
      
    //------------------------------------[Model Para]组机种参数---------------------------------------------------------------------
      Lbl114.Caption:=nrf_dat_config.Readstring('Model Para', 'prefix', '');
      if (lbl90.hint='model')and (StrToInt(edt62.Text)=0 ) then     //淘汰？用config.ini配置机种名称
        lbl90.hint:=nrf_dat_config.Readstring('Model Para', 'Model', '');   //测试界面里的辅助机种名

      if Readchis('Model Para','BarCodeEdit')<>'' then begin
        if StrToInt(Readchis('Model Para','BarCodeEdit'))>=1000 then
          edit1.Enabled:=false;
          
        if (StrToInt(Readchis('Model Para','BarCodeEdit'))mod 1000) >70 then begin
          edit1.EditMask:='';
          edit1.Width:=1000;   
          medit1.EditMask:=edit1.EditMask;
          
          edit1.MaxLength:=StrToInt(Readchis('Model Para','BarCodeEdit'))mod 1000;
        end;
      end;
      if (nrf_dat_config.Readstring('Model Para', 'enPN', '')<>'')              //使能机种选择
      then begin
        edt82.Text:=nrf_dat_config.Readstring('Model Para', 'enPN', '') ;  //料号索引,与下面读取不能分开 ,否则会0出现-1报错
        if( edt82.Text='-1' ) then edt82.Text:='1';                //-1 会出错
        edt82.Tag:=StrToInt(edt82.Text);

        if (nrf_dat_config.Readstring('Model Para', 'PNsize', '')<>'')  then    //用于tesla对Order和Operator统一最大长度的限制，通过下面各个机种参数可取消
        begin
          FormPN.edt1.Text:=nrf_dat_config.Readstring('Model Para', 'PNsize', '') ;

          FormPN.edt2.MaxLength:=StrToInt(FormPN.edt1.Text) ;
          FormPN.edt3.MaxLength:=StrToInt(FormPN.edt1.Text) ;
          FormLoad.edt5.MaxLength:=StrToInt(FormPN.edt1.Text) ;
          FormLoad.edt6.MaxLength:=StrToInt(FormPN.edt1.Text) ;
        end;
        FormPN.edt2.Hint:=nrf_dat_config.Readstring('Model Para', 'OrderSize', '');
        if IsNumberic(FormPN.edt2.Hint) then begin
          FormPN.edt2.MaxLength:=StrToInt(FormPN.edt2.Hint);
          FormLoad.edt5.MaxLength:=StrToInt(FormPN.edt2.Hint);
        end;
        FormPN.edt3.Hint:=nrf_dat_config.Readstring('Model Para', 'OperatorSize', '');
        if IsNumberic(FormPN.edt3.Hint) then begin
          FormPN.edt3.MaxLength:=StrToInt(FormPN.edt3.Hint);
          FormLoad.edt6.MaxLength:=StrToInt(FormPN.edt3.Hint);
        end;

        if IsNumberic(FormPN.edt4.Text) then begin
          if Readchis('Model Para', 'LoginItem1En')='1' then
            FormPN.edt4.Text:=IntToStr(StrToInt(FormPN.edt4.Text)and $0E);
          if Readchis('Model Para', 'LoginItem2En')='1' then
            FormPN.edt4.Text:=IntToStr(StrToInt(FormPN.edt4.Text)and $0D);
          if Readchis('Model Para', 'LoginItem3En')='1' then
            FormPN.edt4.Text:=IntToStr(StrToInt(FormPN.edt4.Text)and $0B);
          if Readchis('Model Para', 'LoginItem4En')='1' then
            FormPN.edt4.Text:=IntToStr(StrToInt(FormPN.edt4.Text)and $07);
          if Readchis('Model Para', 'LoginItem1Dis')='1' then
            FormPN.edt4.Text:=IntToStr(StrToInt(FormPN.edt4.Text)or $01);
          if Readchis('Model Para', 'LoginItem2Dis')='1' then
            FormPN.edt4.Text:=IntToStr(StrToInt(FormPN.edt4.Text)or $02);
          if Readchis('Model Para', 'LoginItem3Dis')='1' then
            FormPN.edt4.Text:=IntToStr(StrToInt(FormPN.edt4.Text)or $04);
          if Readchis('Model Para', 'LoginItem4Dis')='1' then
            FormPN.edt4.Text:=IntToStr(StrToInt(FormPN.edt4.Text)or $08);
        end;

        for i:=1 to 50 do begin           //遍历最大10个机种及治具
          if nrf_dat_config.Readstring('Model Para', 'PN'+inttostr(i), '')<>'' then      //不为空
          begin
            FormPN.cbb1.Items.Strings[i]:=nrf_dat_config.Readstring('Model Para', 'PN'+inttostr(i), '') ;
          end;
          if nrf_dat_config.Readstring('Model Para', 'Fix'+inttostr(i), '')<>'' then      //不为空
          begin
            FormPN.cbb2.Items.Strings[i]:=nrf_dat_config.Readstring('Model Para', 'Fix'+inttostr(i), '') ;
            FormLoad.cbb2.Items.Strings[i]:=nrf_dat_config.Readstring('Model Para', 'Fix'+inttostr(i), '') ;
          end;
          if nrf_dat_config.Readstring('Model Para', 'ST'+inttostr(i), '')<>'' then      //不为空
          begin
            FormPN.cbb3.Items.Strings[i]:=nrf_dat_config.Readstring('Model Para', 'ST'+inttostr(i), '') ;
          end;
        end;
        for i:=51 to 200 do begin           //遍历最大200个机种及治具
          if nrf_dat_config.Readstring('Model Para', 'Fix'+inttostr(i), '')<>'' then      //不为空
          begin
            FormPN.cbb2.Items.Add(nrf_dat_config.Readstring('Model Para', 'Fix'+inttostr(i), '') );
            FormLoad.cbb2.Items.Add(nrf_dat_config.Readstring('Model Para', 'Fix'+inttostr(i), '') );
          end;
        end;
        if StrToInt(edt82.Text)>=99 then                     //PN
          FormPN.cbb1.ItemIndex:=99
        else
          FormPN.cbb1.ItemIndex:=StrToInt(edt82.Text);
        if  (Readchis('Server','MESprogram')<>'')or(Readchis('Server','MESprogramdefault')<>'')  then begin    //丽清可选站别
          if (nrf_dat_config.Readstring('Model Para', 'enFix', '')<>'') then begin       //备用；选择治具名称     //edt83.Text:= nrf_dat_config.Readstring('Model Para', 'enFix', '');
            FormPN.cbb2.ItemIndex:=StrToInt(nrf_dat_config.Readstring('Model Para', 'enFix', '')) ;
            FormLoad.cbb2.ItemIndex:=StrToInt(nrf_dat_config.Readstring('Model Para', 'enFix', '')) ;
          end;
          if (nrf_dat_config.Readstring('Model Para', 'enST', '')<>'') then        //备用；选择治具名称
            FormPN.cbb3.ItemIndex:=StrToInt(nrf_dat_config.Readstring('Model Para', 'enST', ''));
        end else begin         //  if (edt62.text='25')  then begin    //jaguar
          if (nrf_dat_config.Readstring('Model Para', 'enFix', '')<>'') then begin       //备用；选择治具名称    //edt83.Text:= nrf_dat_config.Readstring('Model Para', 'enFix', '');
            FormPN.cbb2.ItemIndex:=StrToInt(nrf_dat_config.Readstring('Model Para', 'enFix', '')) ;
            FormLoad.cbb2.ItemIndex:=StrToInt(nrf_dat_config.Readstring('Model Para', 'enFix', '')) ;
          end;
        end;
      end;


    finally
      nrf_dat_config.free;
    end;
  end;
end;
procedure PNbarcode_fileCreateProcess(len:integer);         //读取Ini文件的PN配置，barcode配置，并初始化文件生成
var temp:byte;
    i:SmallInt;
    nrf_dat_config: Tinifile;
    save_file_name,ini_path,sst: string;
begin

  with Form_line do begin
    if strtoint(edt82.Text)>=100 then begin                     //jaguar等 启用PC存放程序  and(Readchis('Model Para','PNformBarcode')='1')
       edt82.Text:='99';
       grp18.Caption:='';
       edt82.Visible:=false;
                                        //=0会出错！

       btn26Click(Form_line);              //加载之前先复位？
       tmr1Timer(Form_line);
       Application.ProcessMessages;       //定时发送复位命令
       Sleep(100);
       Application.ProcessMessages;       //接收复位命令回传

       formLOad.btn3.Visible:=false;    //加载按钮禁止！
       formLOad.edt4.Visible:=true;     //扫工单条码，加载程序；
       formLOad.grp1.Visible:=True;
       

       formLOad.ShowModal;

       
       begin                              //jaguar    //if (edt62.text='25') then
        if StrToInt(edt82.Text)<99 then begin                    //使用基本登录窗口参数
          writechis('Model Para', 'enFix', IntToStr(FormPN.cbb2.ItemIndex))
        end else
          writechis('Model Para', 'enFix', IntToStr(FormLoad.cbb2.ItemIndex));              //扩展登录参数
       end;
       sbtbtn1.Enabled:=False;       //loading...之后才使能
       sbtbtn1.Visible:=false;
    end else if StrToInt('0'+trim(Form_line.edt82.Text) ) >0 then  begin    //enPN=1..99 ,通用！ 读取配置字段的出处不同。    // tesla，jiahe专用

      if form_line.Close_1>1 then form_line.Close_1:=1;
      FormPN.showmodal;
      If form_line.Close_1 = 0 Then begin
        Application.Terminate;   // LOGIN按了Close键;       //  close;
        Exit;
      end;

      try
        ini_path := ExtractFilePath(application.ExeName) + 'config.ini';
        nrf_dat_config := Tinifile.Create(ini_path);

        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'conductorNum', '')<>'' then      //默认1，不为空
        begin
          edt81.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'conductorNum', '') ;
        end;
        if edt81.Text='0' then ts9.TabVisible:=False else ts9.TabVisible:=True;         //导线卡关闭

        for i:=1 to UpStepMax do
        begin
          ScaleZu[i]:='1';
          if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'Scale'+inttostr(i), '')<>'' then      //不为空
            begin
              ScaleZu[i]:= nrf_dat_config.Readstring(FormPN.cbb1.Text, 'Scale'+inttostr(i), '');
            end;
        end;
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarCodeLen', '')<>'' then      //设置条码长度规则
        begin
          edt7.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarCodeLen', '') ;
        end;

        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarPos', '')<>'' then      //下面3个参数组合来判断条码含有固定字符
        begin
          edt8.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarPos', '') ;
        end;
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarPos2', '')<>'' then      //第2位置条码规则
        begin
          edt182.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarPos2', '') ;
        end;
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarPos3', '')<>'' then      //第3位置条码规则
        begin
          edt183.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarPos3', '') ;
        end;
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarPos4', '')<>'' then      //第4位置条码规则
        begin
          edt184.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarPos4', '') ;
        end;
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'MaskBarPos', '')<>'' then   //通配符*？位置条码规则
        begin
          edt181.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'MaskBarPos', '') ;
        end;

        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarChars', '')<>'' then      //
        begin
          edt10.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarChars', '') ;
        end;
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarLen', '')<>'' then      //
        begin
          edt9.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'BarLen', '') ;
        end else begin
          edt9.Text:=IntToStr(Length(edt10.Text));
        end;

      //////////////-------------------PN料号-----------------------------------
        if StrToInt(edt82.Text)<99 then       //启用PN登录窗口！否则：大于99为程序存放测控板，不可变
           edt82.Text:= IntToStr(FormPN.cbb1.ItemIndex);
        nrf_dat_config.WriteString('Model Para', 'enPN', edt82.Text);
        if (Readchis('Server','MESprogram')<>'')or(Readchis('Server','MESprogramdefault')<>'')   then begin    //丽清可选站别
          //edt83.Text:= IntToStr(FormPN.cbb2.ItemIndex);        //无实际意义
          nrf_dat_config.WriteString('Model Para', 'enFix', IntToStr(FormPN.cbb2.ItemIndex));
          //edt83.Text:= IntToStr(FormPN.cbb3.ItemIndex);        //无实际意义
          nrf_dat_config.WriteString('Model Para', 'enST',  IntToStr(FormPN.cbb3.ItemIndex));
        end else begin                //  if (edt62.text='25') then begin    //jaguar     //edt83.Text:= IntToStr(FormPN.cbb2.ItemIndex);        //无实际意义
          if StrToInt(edt82.Text)<99 then                     //使用基本登录窗口参数
            nrf_dat_config.WriteString('Model Para', 'enFix', IntToStr(FormPN.cbb2.ItemIndex))
          else
            nrf_dat_config.WriteString('Model Para', 'enFix', IntToStr(FormLoad.cbb2.ItemIndex));              //扩展登录参数
        end;
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'enPrint', '')<>'' then  begin      //自动打印使能 ,并设置大小
           edt75.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'enPrint', '');
           if edt75.Text='0' then begin
             btn55.Visible:=false;
             btn28.Visible:=false;
             btn29.Visible:=false;
             btn27.Enabled:=false;
           end else begin
             btn55.Visible:=true;
             btn28.Visible:=true;
             //不能用 btn29.Visible:=true;
             btn27.Enabled:=true;
           end;
        end;
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'PrintVHdir', '')<>'' then        //设置 打印机方向+ + 空行数+信息长短
            edt11.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'PrintVHdir', '');
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'PrintRow', '')<>'' then           //此两个参数影响软件启动时间
            edt159.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'PrintRow', '');
        if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'PrintMaxRows', '')<>'' then        //打印最大行数，高优先级；
            edt159.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'PrintMaxRows', '');
        if edt159.Text<>'0' then  begin
          edt159.Visible:=True;
          lbl184.Visible:=true;
        end;
      finally
        nrf_dat_config.free;
      end;
    end else begin          //读取机种下面关于条码的规则
      if Readchis('Model Para','Process')<>'' then begin   //淘汰！ 早期数据库专用   //初始化上传参数
        if Readchis('Model Para', 'LoginItem4')=''  then lbl93.Caption:='Process';
        stat1.Font.Size:=15;

        datetimetostring(sst,'yyyymmddhhnnss',Now);
        btn1.Font.Color:=clDefault;//clWindow;

        if (Readchis('Model Para', 'PNbitDis', '')<>'')  then    //输入选项使能，=0的关闭输入
        begin
          FormPN.edt4.Text:=Readchis('Model Para', 'PNbitDis', '') ;
        end;
  
        edt80.Text:=Readchis('Model Para','Process');           //制程<--治具名称
        if form_line.Close_1>1 then form_line.Close_1:=1;
        FormPN.showmodal;
        If form_line.Close_1 = 0 Then begin Application.Terminate; Exit;end;  // LOGIN按了Close键;
      end;
    end;
    
    if (StrToInt('0'+trim(Form_line.edt82.Text) )=0)or(StrToInt('0'+trim(Form_line.edt82.Text) ) >=99) then begin   //enPN>0,通用！
      try
        ini_path := ExtractFilePath(application.ExeName) + 'config.ini';
        nrf_dat_config := Tinifile.Create(ini_path);


        if nrf_dat_config.Readstring('Model Para', 'BarCodeLen', '')<>'' then      //默认1，不为空
        begin
          edt7.Text:=nrf_dat_config.Readstring('Model Para', 'BarCodeLen', '') ;
        end;

        if nrf_dat_config.Readstring('Model Para', 'BarPos', '')<>'' then         //下面3个参数组合来判断条码含有固定字符
        begin
          edt8.Text:=nrf_dat_config.Readstring('Model Para', 'BarPos', '') ;
        end;
        if nrf_dat_config.Readstring('Model Para', 'BarPos2', '')<>'' then      //第2位置条码规则
        begin
          edt182.Text:=nrf_dat_config.Readstring('Model Para', 'BarPos2', '') ;
        end;
        if nrf_dat_config.Readstring('Model Para', 'BarPos3', '')<>'' then      //第3位置条码规则
        begin
          edt183.Text:=nrf_dat_config.Readstring('Model Para', 'BarPos3', '') ;
        end;
        if nrf_dat_config.Readstring('Model Para', 'BarPos4', '')<>'' then      //第4位置条码规则
        begin
          edt184.Text:=nrf_dat_config.Readstring('Model Para', 'BarPos4', '') ;
        end;
        if nrf_dat_config.Readstring('Model Para', 'MaskBarPos', '')<>'' then   //通配符*？位置条码规则
        begin
          edt181.Text:=nrf_dat_config.Readstring('Model Para', 'MaskBarPos', '') ;
        end;
       if nrf_dat_config.Readstring('Model Para', 'BarChars', '')<>'' then      //
        begin
          edt10.Text:=nrf_dat_config.Readstring('Model Para', 'BarChars', '') ;
        end;

        if nrf_dat_config.Readstring('Model Para', 'BarLen', '')<>'' then begin
          edt9.Text:=nrf_dat_config.Readstring('Model Para', 'BarLen', '') ;
        end else begin
          edt9.Text:=IntToStr(Length(edt10.Text));
        end;

      finally
        nrf_dat_config.free;
      end;
    end;
    if Readchis('Model Para','BarCodeEdit')='' then begin     //默认初始化上传参数
      edt134.Visible:=False;
      lbl160.Visible:=False;
      btn98.Visible:=False;
    end else begin                    //超宽条码使能，并使能Load文件
      edt14.Visible:=false;          //通讯延时参数不显示；
      edt15.Visible:=false;

      L6.Enabled:=true;             //菜单里 加载功能
      sbtbtn1.Enabled:=False;       //loading...之后才使能
      sbtbtn1.Visible:=true;

      if (StrToInt('0'+trim(Form_line.edt82.Text) ) >0)and(StrToInt('0'+trim(Form_line.edt82.Text) ) <99) then begin   //enPN>0,通用！
        if Readchis(FormPN.cbb1.Text, 'BarCodeLen', '')<>'' then      //默认1，不为空
        begin
          edt134.Text:=Readchis(FormPN.cbb1.Text, 'BarCodeLen', '') ;
        end;
      end else  begin
        if Readchis('Model Para','BarCodeLen')<>'' then begin
          edt134.Text:= Readchis('Model Para','BarCodeLen');
        end;
      end;
      btn1.Font.Color:=clDefault;//clWindow;
      edit1.Width:=700;             //超宽条码
    end;
    if Readchis('Model Para','MonitorResetKey')<>'' then         //默认："复位"按钮不显示
      btn98.Visible:=True;
      
    for i:=1 to UpStepMax do
    begin
      SpecValZu[i]:='0';
      StdValZu[i]:='0';
      HpointZu[i]:='0';
      LpointZu[i]:='0';
      ModeZu[i]  :='0';
    end;
  //------------ 设定程序本身所使用的日期时间格式，文件保存用-------------------

    LongDateFormat := 'yyyy-MM-dd';
    ShortDateFormat := 'yyyy-MM-dd';

    LongTimeFormat := 'HH:nn:ss';
    ShortTimeFormat := 'HH:nn:ss';
    DateSeparator := '-';
    TimeSeparator := ':';
  
    //-------------生产一个带日期的文本文件---------------------------------------
    TestNum:=0;
    if StrToInt(edt62.Text) =0 then               //child=0:用config.ini设置的机种名生产数据文件
    begin
      if StrToInt(edt38.text)mod 10 >0 then begin    //uplocal>0
          CreatAnewCsv(0,0)
      end;

    end;
    mmo2String[10]:=''; //记录设备 数据上传字符串
    mmo2String[17]:='';
  end;
end;
procedure MatrixLed_createProcess(len:integer);         //矩阵LED指示灯生成
var
    temp16,i,j,k,onewide:SmallInt;
begin

  with Form_line do begin
    if (strtoint(edt59.Text)>0) and (strtoint(edt60.Text)>0) then
    begin
     { for I := 0 to High(Rleds) do    //200个 竟然能花费4秒的启动时间？？？
      begin
        Rlabels[i].Free;     Rlabels[i]:=TiLabel.Create(Self);
        Redits[i].free;     Redits[i]:=Tiedit.create(Self);
        Rleds[I].Free;     Rleds[I] := Tiledround.Create(Self);
      end;  }

      if (strtoint(edt145.Text)mod 1000)>0 then begin
        onewide:=(scrlbx2.Width-2*lbl73.Width) div ( StrToInt(edt60.Text)*(StrToInt(edt145.Text)mod 1000) ) ;//单位宽度
        for k := 0 to (StrToInt(edt145.Text)mod 1000)-1 do      //遍历所有组
        begin
          for J := 0 to StrToInt(edt59.Text)-1 do      //遍历所有行
          begin
              for I := 0 to StrToInt(edt60.Text)-1 do    //遍历所有列
              begin
                 // Rlabels[J*StrToInt(edt60.Text) + I].Free;
                 // Rlabels[J*StrToInt(edt60.Text) + I]:=TiLabel.Create(Application);

                  Redits[K*StrToInt(edt59.Text)*StrToInt(edt60.Text) + J*StrToInt(edt60.Text) + I].free;
                  Redits[K*StrToInt(edt59.Text)*StrToInt(edt60.Text) + J*StrToInt(edt60.Text) + I]:=Tiedit.create(Application);

                  Rleds[K*StrToInt(edt59.Text)*StrToInt(edt60.Text) + J*StrToInt(edt60.Text) + I].Free;
                  Rleds[K*StrToInt(edt59.Text)*StrToInt(edt60.Text) + J*StrToInt(edt60.Text) + I] := Tiledround.Create(Application);


                  with Rleds[K*StrToInt(edt59.Text)*StrToInt(edt60.Text) + J*StrToInt(edt60.Text) + I] do
                  begin
                    //Visible  := False;
                    if (j mod 2)=1 then      //奇数行位置 像右缩进，或者向下偏移
                      begin
                      //if ildrw1.Style=ilasLeft then    //右到左顺序              (Rled1.Width+10)*16
                      //  Left     := Rled1.Left + (StrToInt(edt60.Text)-1-I )*StrToInt(edt120.Text) + ( (StrToInt(edt60.Text)-1-I )*(scrlbx2.Width-2*lbl73.Width) div (StrToInt(edt60.Text)+1))
                      //              +Strtoint(edt69.Text)  //整行右偏移
                      //else
                      begin
                        if (i mod 2)=1 then       //奇数列左移动
                          Left   := Rled1.Left + I*StrToInt(edt120.Text) + ((I+k*(StrToInt(edt60.Text)))*onewide) +Strtoint(edt69.Text) +strtoint(edt112.Text)   //  (Rled1.Width+10)*16
                        else
                          Left   := Rled1.Left + I*StrToInt(edt120.Text) + ((I+k*(StrToInt(edt60.Text)))*onewide) +Strtoint(edt69.Text);  //(Rled1.Width+10)*16整行右偏移
                      end;
                      if (i mod 2)=1 then              //列数为偶数，下偏位
                        Top      := Rled1.Top  + J*(Rled1.Height+30)  *5 div (2+strtoint(edt59.Text))  //由行数控制间距
                                    +Strtoint(edt70.Text)  //偶数下偏移
                                    +Strtoint(edt71.Text)                  //整行下偏移
                      else
                        Top      := Rled1.Top  + J*(Rled1.Height+30) *5 div (2+strtoint(edt59.Text))  //由行数控制间距
                                    +Strtoint(edt71.Text);                //整行下偏移
                      end
                    else begin
                      //if ildrw1.Style=ilasLeft then   //右到左顺序      (Rled1.Width+10)*16
                      //  Left     := Rled1.Left + (StrToInt(edt60.Text)-1-I )*StrToInt(edt120.Text) + (StrToInt(edt60.Text)-1-I )*(scrlbx2.Width-2*lbl73.Width) div ( StrToInt(edt60.Text)+1 )
                      //else
                      begin
                        if (i mod 2)=1 then    //奇数列左移动
                          Left   := Rled1.Left + I*StrToInt(edt120.Text) + ((I+k*(StrToInt(edt60.Text)))*onewide) +strtoint(edt112.Text)   //  (Rled1.Width+10)*16
                        else
                          Left   := Rled1.Left + I*StrToInt(edt120.Text) + ((I+k*(StrToInt(edt60.Text)))*onewide);   //  (Rled1.Width+10)*16
                      end;
                      if (i mod 2)=1 then              //列数为偶数，下偏位
                        Top      := Rled1.Top  + J*(Rled1.Height+30)  *5 div (2+strtoint(edt59.Text))  //由行数控制间距
                                    +Strtoint(edt70.Text) //偶数下偏移
                      else
                        Top      := Rled1.Top  + J*(Rled1.Height+30)*5 div (2+strtoint(edt59.Text))  //由行数控制间距;
                    end;

                    Width    := Rled1.Width;
                    Anchors  := Rled1.Anchors;
                    ActiveColor:=Rled1.ActiveColor;
                    InactiveColor:=Rled1.InactiveColor;
                    AutoInactiveColor:=Rled1.AutoInactiveColor;
                    //if not Assigned(Parent) then
                    Parent := Rled1.Parent;
                    TabOrder := Rled1.TabOrder +K*StrToInt(edt59.Text)*StrToInt(edt60.Text) + J*StrToInt(edt60.Text) + I;
                  end;
           
                  with Redits[K*StrToInt(edt59.Text)*StrToInt(edt60.Text) + J*StrToInt(edt60.Text) + I] do
                  begin
                    Visible:=true;
                    Top     := Rleds[K*StrToInt(edt59.Text)*StrToInt(edt60.Text) + J*StrToInt(edt60.Text) + I].Top;
                    Left    := Rleds[K*StrToInt(edt59.Text)*StrToInt(edt60.Text) + J*StrToInt(edt60.Text) + I].Left;
                    Alignment:=Redit1.Alignment;
                    Width   := Redit1.Width;

                    Parent := Redit1.Parent;
                    Value:=inttostr(K*StrToInt(edt59.Text)*StrToInt(edt60.Text) + J*StrToInt(edt60.Text) + I+1);
                    Enabled:=Redit1.Enabled;
                  end;
               // end;
              end;
          end;
        end;
      end else begin
        for J := 0 to StrToInt(edt59.Text)-1 do      //遍历所有行
        begin
            for I := 0 to StrToInt(edt60.Text)-1 do    //遍历所有列
            begin
               // Rlabels[J*StrToInt(edt60.Text) + I].Free;
               // Rlabels[J*StrToInt(edt60.Text) + I]:=TiLabel.Create(Application);

                Redits[J*StrToInt(edt60.Text) + I].free;
                Redits[J*StrToInt(edt60.Text) + I]:=Tiedit.create(Application);

                Rleds[J*StrToInt(edt60.Text) + I].Free;
                Rleds[J*StrToInt(edt60.Text) + I] := Tiledround.Create(Application);


                with Rleds[J*StrToInt(edt60.Text) + I] do
                begin
                  //Visible  := False;
                  if (j mod 2)=1 then      //奇数行位置 像右缩进，或者向下偏移
                    begin
                    if ildrw1.Style=ilasLeft then    //右到左顺序              (Rled1.Width+10)*16
                      Left     := Rled1.Left + (StrToInt(edt60.Text)-1-I )*StrToInt(edt120.Text) + ( (StrToInt(edt60.Text)-1-I )*(scrlbx2.Width-2*lbl73.Width) div (StrToInt(edt60.Text)+1))
                                  +Strtoint(edt69.Text)  //整行右偏移
                    else  begin
                      if (i mod 2)=1 then       //奇数列左移动
                        Left   := Rled1.Left + I*StrToInt(edt120.Text) + (I*(scrlbx2.Width-2*lbl73.Width) div (StrToInt(edt60.Text)+1) ) +Strtoint(edt69.Text) +strtoint(edt112.Text)   //  (Rled1.Width+10)*16
                      else
                        Left   := Rled1.Left + I*StrToInt(edt120.Text) + (I*(scrlbx2.Width-2*lbl73.Width) div (StrToInt(edt60.Text)+1) ) +Strtoint(edt69.Text);  //(Rled1.Width+10)*16整行右偏移
                    end;
                    if (i mod 2)=1 then              //列数为偶数，下偏位
                      Top      := Rled1.Top  + J*(Rled1.Height+30)  *5 div (2+strtoint(edt59.Text))  //由行数控制间距
                                  +Strtoint(edt70.Text)  //偶数下偏移
                                  +Strtoint(edt71.Text)                  //整行下偏移
                    else
                      Top      := Rled1.Top  + J*(Rled1.Height+30) *5 div (2+strtoint(edt59.Text))  //由行数控制间距
                                  +Strtoint(edt71.Text);                //整行下偏移
                    end
                  else begin
                    if ildrw1.Style=ilasLeft then   //右到左顺序      (Rled1.Width+10)*16
                      Left     := Rled1.Left + (StrToInt(edt60.Text)-1-I )*StrToInt(edt120.Text) + (StrToInt(edt60.Text)-1-I )*(scrlbx2.Width-2*lbl73.Width) div ( StrToInt(edt60.Text)+1 )
                    else  begin
                      if (i mod 2)=1 then    //奇数列左移动
                        Left   := Rled1.Left + I*StrToInt(edt120.Text) + (I*(scrlbx2.Width-2*lbl73.Width) div (StrToInt(edt60.Text)+1) ) +strtoint(edt112.Text)   //  (Rled1.Width+10)*16
                      else
                        Left   := Rled1.Left + I*StrToInt(edt120.Text) +  I*(scrlbx2.Width-2*lbl73.Width) div (StrToInt(edt60.Text)+1);   //  (Rled1.Width+10)*16
                    end;
                    if (i mod 2)=1 then              //列数为偶数，下偏位
                      Top      := Rled1.Top  + J*(Rled1.Height+30)  *5 div (2+strtoint(edt59.Text))  //由行数控制间距
                                  +Strtoint(edt70.Text) //偶数下偏移
                    else
                      Top      := Rled1.Top  + J*(Rled1.Height+30)*5 div (2+strtoint(edt59.Text))  //由行数控制间距;
                  end;

                  Width    := Rled1.Width;
                  Anchors  := Rled1.Anchors;
                  ActiveColor:=Rled1.ActiveColor;
                  InactiveColor:=Rled1.InactiveColor;
                  AutoInactiveColor:=Rled1.AutoInactiveColor;
                  //if not Assigned(Parent) then
                  Parent := Rled1.Parent;
                  TabOrder := Rled1.TabOrder + J*StrToInt(edt60.Text) + I;
                  hint:='ErrCode';
                end;
           
                with Redits[J*StrToInt(edt60.Text) + I] do
                begin
                  Visible:=true;
                  Top     := Rleds[J*StrToInt(edt60.Text) + I].Top;
                  Left     := Rleds[J*StrToInt(edt60.Text) + I].Left;
                  Width    := Redit1.Width;
                  Alignment:=Redit1.Alignment;
                  Parent := Redit1.Parent;
                  Value:=inttostr(J*StrToInt(edt60.Text) + I+1);
                  Enabled:=Redit1.Enabled;
                end;
             // end;
            end;
        end;
      end;
    end;
  end;
end;

procedure stepKeyEdit(key:Word;hh:SmallInt);
  procedure zuSetFocus(row:SmallInt;nn:SmallInt);     //选中行、列位置的edit框；
  begin
    case  row of
      1:   zu1edit[nn-0].SetFocus;
      2:   zu2edit[nn-0].SetFocus;
      3:   zu3edit[nn-0].SetFocus;
      4:   zu4edit[nn-0].SetFocus;
      5:   zu5edit[nn-0].SetFocus;
      6:   zu6edit[nn-0].SetFocus;
      7:   zu7edit[nn-0].SetFocus;
      8:   zu8edit[nn-0].SetFocus;
      9:   zu9edit[nn-0].SetFocus;
      10:   zu10edit[nn-0].SetFocus;
      11:   zu11edit[nn-0].SetFocus;
      12:   zu12edit[nn-0].SetFocus;
      13:   zu13edit[nn-0].SetFocus;
      14:   zu14edit[nn-0].SetFocus;
      15:   zu15edit[nn-0].SetFocus;
      16:   zu16edit[nn-0].SetFocus;
      17:   zu17edit[nn-0].SetFocus;
    end;

  end;
begin
   with Form_line do begin
      if (Key=VK_HOME) then begin     //程序最小步骤
        modbusfun06dat:=1;
        modbusfun06:=$10B0;
        edt45.Text:='1';
        StepNowFresh:=0;
        zuSetfocus((hh div 60)+1,StepNowFresh+1);
      end;
      if (Key=VK_END)  then begin      //最大
        modbusfun06dat:=1900;
        modbusfun06:=$10B0;
        edt45.Text:='900';
        StepNowFresh:=VIRLCROWS-1;
        zuSetfocus((hh div 60)+1,StepNowFresh+1);
      end;
      if (GetKeyState(VK_CONTROL) and $8000)=$8000 then  begin
        if Key=VK_SPACE then 
          N60Click(Form_line);
        Exit;
      end;
      if (Key=Byte('Q'))  then begin       //页顶
        modbusfun06dat:=0;
        if IsNumberic(edt45.Text)and(StrToInt(edt45.Text)>0) then
          modbusfun06dat:=((StrToInt(edt45.Text)-1)div 50)*50+1;
        modbusfun06:=$10B0;
        edt45.Text:=IntToStr(modbusfun06dat);
        StepNowFresh:=0;
        zuSetfocus((hh div 60)+1,StepNowFresh+1);
      end;
      if (Key=Byte('E'))  then begin       //页底
        modbusfun06dat:=0;
        if IsNumberic(edt45.Text)and(StrToInt(edt45.Text)>0) then
          modbusfun06dat:=((StrToInt(edt45.Text)-1)div 50)*50+50;
        modbusfun06:=$10B0;
        edt45.Text:=IntToStr(modbusfun06dat);
        StepNowFresh:=VIRLCROWS-1; //等同
        zuSetfocus((hh div 60)+1,StepNowFresh+1);
      end;
      if (Key=VK_PRIOR)or(Key=Byte('R'))  then begin     //上翻页
        btn12Click(Form_line);
        if IsNumberic(edt45.Text)and(StrToInt(edt45.Text)>50) then
          edt45.Text:=IntToStr(StrToInt(edt45.Text)-50);
      end;
      if (Key=VK_NEXT)or(Key=Byte('F'))  then begin      //下翻页
        btn13Click(Form_line);
        if IsNumberic(edt45.Text)and(StrToInt(edt45.Text)+50<=1900) then
          edt45.Text:=IntToStr(StrToInt(edt45.Text)+50);
      end;

      if (Key=VK_LEFT)or(Key=Byte('A'))  then begin    
        zuSetfocus((hh div 60)+0,StepNowFresh+1);
       
      end;
      if (Key=VK_RIGHT)or(Key=Byte('D'))  then begin
        zuSetfocus((hh div 60)+2,StepNowFresh+1);

      end;
      if (Key=VK_UP)or(Key=Byte('W'))  then begin
        if(StepNowFresh>0)then begin
          zuSetfocus((hh div 60)+1,StepNowFresh);
        end else if StrToInt(edt45.Text)>1 then begin
          zuSetfocus((hh div 60)+1,VIRLCROWS);

        end;
        if StrToInt(edt45.Text)>1 then begin
          modbusfun06dat:=((StrToInt(edt45.Text)-1)div 50)*50+1+StepNowFresh-1;
          modbusfun06:=$10B0;   //等同          //btn18Click(Self);
          edt45.Text:=IntToStr(modbusfun06dat);
        //  edt45.Text:=IntToStr(StrToInt(edt45.Text)-1);
        end;
      end;
      if (Key=VK_DOWN)or(Key=Byte('S'))  then begin
        if(StepNowFresh+1<=VIRLCROWS)then begin
          zuSetfocus((hh div 60)+1,(StepNowFresh+1)mod VIRLCROWS+1);

        end;
        modbusfun06dat:=((StrToInt(edt45.Text)-1)div 50)*50+1+StepNowFresh+1;
        modbusfun06:=$10B0;   //等同         //btn17Click(Self);
        edt45.Text:=IntToStr(modbusfun06dat);
      //  edt45.Text:=IntToStr(StrToInt(edt45.Text)+1);
      end;


      if (Key=VK_BACK) then begin     //删除
        D5Click(Form_line);
      end;
      if (Key=VK_INSERT)or(Key=Byte('T'))  then begin
        if IsNumberic(Form_line.edt45.Text) then begin
          FormInsertN.edt1.Text:=Form_line.edt45.Text;
        end;
         I2Click(Form_line);
      end;
      if (Key=Byte('G'))  then begin          //copy步骤
         I3Click(Form_line);
      end;
   end;
end;
procedure sysParaKey(Sender:TObject;key:Word;hh:SmallInt);
var ii:SmallInt;
begin
  with Form_line do begin
     if (hh div 60)=17-1 then begin                //
        if (Key=VK_ADD)or(Key=187) then begin           //微调不记录Log!
          if IsNumberic(zu17edit[StepNowFresh+1].text[1]) then begin
            ii:=StrToInt(zu17edit[StepNowFresh+1].text[1]);
            if ii<9 then begin
               modbusfun16:=$3012+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10) div strtoint(Form_line.edt121.Text) ) *32 ;
               modbusfun16str:=IntToStr(ii+1)+Copy(zu17edit[StepNowFresh+1].text,2,15);
                modbusfun16len:=8;
            end;
          end;
        end;
        if (Key=VK_SUBTRACT)or(Key=189) then begin       //大键盘‘-‘
          if IsNumberic(zu17edit[StepNowFresh+1].text[1]) then begin
            ii:=StrToInt(zu17edit[StepNowFresh+1].text[1]);
            if ii>0 then begin
               modbusfun16:=$3012+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10) div strtoint(Form_line.edt121.Text) ) *32 ;
               modbusfun16str:=IntToStr(ii-1)+Copy(zu17edit[StepNowFresh+1].text,2,15);
               modbusfun16len:=8;
            end;
          end;
        end;
     {  if Key=VK_MULTIPLY then begin
          if IsNumberic(zu17edit[StepNowFresh+1].text[4]) then begin
            ii:=StrToInt(zu17edit[StepNowFresh+1].text[4]);
            if ii<9 then begin
               modbusfun16:=$3012+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10) div strtoint(Form_line.edt121.Text) ) *32 ;
               modbusfun16str:=Copy(zu17edit[StepNowFresh+1].text,1,3)+IntToStr(ii+1);
                modbusfun16len:=2;
            end;
          end;
       end;
       if Key=VK_DIVIDE then begin
          if IsNumberic(zu17edit[StepNowFresh+1].text[4]) then begin
            ii:=StrToInt(zu17edit[StepNowFresh+1].text[4]);
            if ii>0 then begin
               modbusfun16:=$3012+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10) div strtoint(Form_line.edt121.Text) ) *32 ;
               modbusfun16str:=Copy(zu17edit[StepNowFresh+1].text,1,3)+IntToStr(ii-1);
                modbusfun16len:=2;
            end;

          end;
       end;  }
     end else if (hh div 60)=2-1 then begin                //
        if (Key=VK_ADD)or(Key=187) then begin           //微调不记录Log!
          if IsNumberic(zu2edit[StepNowFresh+1].text[1]) then begin
            ii:=StrToInt(zu2edit[StepNowFresh+1].text[1]);
            if ii<9 then begin
               modbusfun16:=$3000+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(Form_line.edt121.Text) )*32+((hh+10) div 60)+0;
               modbusfun16str:=IntToStr(ii+1)+Copy(zu2edit[StepNowFresh+1].text,2,3);
                modbusfun16len:=2;
            end;
          end;
        end;
        if (Key=VK_SUBTRACT)or(Key=189) then begin       //大键盘‘-‘
          if IsNumberic(zu2edit[StepNowFresh+1].text[1]) then begin
            ii:=StrToInt(zu2edit[StepNowFresh+1].text[1]);
            if ii>0 then begin
               modbusfun16:=$3000+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(Form_line.edt121.Text) )*32+((hh+10) div 60)+0;
               modbusfun16str:=IntToStr(ii-1)+Copy(zu2edit[StepNowFresh+1].text,2,3);
               modbusfun16len:=2;
            end;
          end;
        end;
       if Key=VK_MULTIPLY then begin
          if IsNumberic(zu2edit[StepNowFresh+1].text[4]) then begin
            ii:=StrToInt(zu2edit[StepNowFresh+1].text[4]);
            if ii<9 then begin
               modbusfun16:=$3000+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(Form_line.edt121.Text) )*32+((hh+10) div 60)+0;
               modbusfun16str:=Copy(zu2edit[StepNowFresh+1].text,1,3)+IntToStr(ii+1);
                modbusfun16len:=2;
            end;
          end;
       end;
       if Key=VK_DIVIDE then begin
          if IsNumberic(zu2edit[StepNowFresh+1].text[4]) then begin
            ii:=StrToInt(zu2edit[StepNowFresh+1].text[4]);
            if ii>0 then begin
               modbusfun16:=$3000+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(Form_line.edt121.Text) )*32+((hh+10) div 60)+0;
               modbusfun16str:=Copy(zu2edit[StepNowFresh+1].text,1,3)+IntToStr(ii-1);
                modbusfun16len:=2;
            end;

          end;
       end;
     end else if (hh div 60)=10-1 then begin                //
        if (Key=VK_ADD)or(Key=187) then begin           //微调不记录Log!
          if IsNumberic(zu10edit[StepNowFresh+1].text[1]) then begin
            ii:=StrToInt(zu10edit[StepNowFresh+1].text[1]);
            if ii<9 then begin
               modbusfun16:=$3000+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(Form_line.edt121.Text) )*32+((hh+10) div 60)+1;
               modbusfun16str:=IntToStr(ii+1)+Copy(zu10edit[StepNowFresh+1].text,2,3);
                modbusfun16len:=2;
            end;
          end;
        end;
        if (Key=VK_SUBTRACT)or(Key=189) then begin       //大键盘‘-‘
          if IsNumberic(zu10edit[StepNowFresh+1].text[1]) then begin
            ii:=StrToInt(zu10edit[StepNowFresh+1].text[1]);
            if ii>0 then begin
               modbusfun16:=$3000+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(Form_line.edt121.Text) )*32+((hh+10) div 60)+1;
               modbusfun16str:=IntToStr(ii-1)+Copy(zu10edit[StepNowFresh+1].text,2,3);
               modbusfun16len:=2;
            end;
          end;
        end;
       if Key=VK_MULTIPLY then begin
          if IsNumberic(zu10edit[StepNowFresh+1].text[4]) then begin
            ii:=StrToInt(zu10edit[StepNowFresh+1].text[4]);
            if ii<9 then begin
               modbusfun16:=$3000+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(Form_line.edt121.Text) )*32+((hh+10) div 60)+1;
               modbusfun16str:=Copy(zu10edit[StepNowFresh+1].text,1,3)+IntToStr(ii+1);
                modbusfun16len:=2;
            end;
          end;
       end;
       if Key=VK_DIVIDE then begin
          if IsNumberic(zu10edit[StepNowFresh+1].text[4]) then begin
            ii:=StrToInt(zu10edit[StepNowFresh+1].text[4]);
            if ii>0 then begin
               modbusfun16:=$3000+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(Form_line.edt121.Text) )*32+((hh+10) div 60)+1;
               modbusfun16str:=Copy(zu10edit[StepNowFresh+1].text,1,3)+IntToStr(ii-1);
                modbusfun16len:=2;
            end;

          end;
       end;
     end else if ( (hh div 60)>1)and( (hh div 60)<16 ) then begin                //
        if (Key=VK_ADD)or(Key=187) then begin           //微调不记录Log!
          case  (hh div 60)+1 of                          
            1:  if IsSignedNumbericHex(zu1edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+0;
                   modbusfun06dat:=StrToInt(zu1edit[StepNowFresh+1].text)+1;
                end;
            2:  if IsSignedNumbericHex(zu2edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+0;
                   modbusfun06dat:=StrToInt(zu2edit[StepNowFresh+1].text)+1;
                end;

            3:   if IsSignedNumbericHex(zu3edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+1;
                   modbusfun06dat:=StrToInt(zu3edit[StepNowFresh+1].text)+1;
                end;
            4:   if IsSignedNumbericHex(zu4edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+1;
                   modbusfun06dat:=StrToInt(zu4edit[StepNowFresh+1].text)+1;
                end;
            5:   if IsSignedNumbericHex(zu5edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+1;
                   modbusfun06dat:=StrToInt(zu5edit[StepNowFresh+1].text)+1;
                end;
            6:   if IsSignedNumbericHex(zu6edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+1;
                   modbusfun06dat:=StrToInt(zu6edit[StepNowFresh+1].text)+1;
                end;
            7:   if IsSignedNumbericHex(zu7edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+1;
                   modbusfun06dat:=StrToInt(zu7edit[StepNowFresh+1].text)+1;
                end;
            8:   if IsSignedNumbericHex(zu8edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+1;
                   modbusfun06dat:=StrToInt(zu8edit[StepNowFresh+1].text)+1;
                end;
            9:   if IsSignedNumbericHex(zu9edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+1;
                   modbusfun06dat:=StrToInt(zu9edit[StepNowFresh+1].text)+1;
                end;

            11:   if IsSignedNumbericHex(zu11edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+2;
                   modbusfun06dat:=StrToInt(zu11edit[StepNowFresh+1].text)+1;
                end;
            12:   if IsSignedNumbericHex(zu12edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+2;
                   modbusfun06dat:=StrToInt(zu12edit[StepNowFresh+1].text)+1;
                end;
            13:   if IsSignedNumbericHex(zu13edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+2;
                   modbusfun06dat:=StrToInt(zu13edit[StepNowFresh+1].text)+1;
                end;
            14:   if IsSignedNumbericHex(zu14edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+2;
                   modbusfun06dat:=StrToInt(zu14edit[StepNowFresh+1].text)+1;
                end;
            15:   if IsSignedNumbericHex(zu15edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+2;
                   modbusfun06dat:=StrToInt(zu15edit[StepNowFresh+1].text)+1;
                end;
            16:   if IsSignedNumbericHex(zu16edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+2;
                   modbusfun06dat:=StrToInt(zu16edit[StepNowFresh+1].text)+1;
                end;
            17:   if IsSignedNumbericHex(zu17edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+2;
                   modbusfun06dat:=StrToInt(zu17edit[StepNowFresh+1].text)+1;
                end;
          end;

        end;
        if (Key=VK_SUBTRACT)or(Key=189) then begin       //大键盘‘-‘
          case  (hh div 60)+1 of
            1:  if IsSignedNumbericHex(zu1edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+0;
                   modbusfun06dat:=StrToInt(zu1edit[StepNowFresh+1].text)-1;
                end;
            2:  if IsSignedNumbericHex(zu2edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+0;
                   modbusfun06dat:=StrToInt(zu2edit[StepNowFresh+1].text)-1;
                end;
            3:   if IsSignedNumbericHex(zu3edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+1;
                   modbusfun06dat:=StrToInt(zu3edit[StepNowFresh+1].text)-1;
                end;
            4:   if IsSignedNumbericHex(zu4edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+1;
                   modbusfun06dat:=StrToInt(zu4edit[StepNowFresh+1].text)-1;
                end;
            5:   if IsSignedNumbericHex(zu5edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+1;
                   modbusfun06dat:=StrToInt(zu5edit[StepNowFresh+1].text)-1;
                end;
            6:   if IsSignedNumbericHex(zu6edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+1;
                   modbusfun06dat:=StrToInt(zu6edit[StepNowFresh+1].text)-1;
                end;
            7:   if IsSignedNumbericHex(zu7edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+1;
                   modbusfun06dat:=StrToInt(zu7edit[StepNowFresh+1].text)-1;
                end;
            8:   if IsSignedNumbericHex(zu8edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+1;
                   modbusfun06dat:=StrToInt(zu8edit[StepNowFresh+1].text)-1;
                end;
            9:   if IsSignedNumbericHex(zu9edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+1;
                   modbusfun06dat:=StrToInt(zu9edit[StepNowFresh+1].text)-1;
                end;

            11:   if IsSignedNumbericHex(zu11edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+2;
                   modbusfun06dat:=StrToInt(zu11edit[StepNowFresh+1].text)-1;
                end;
            12:   if IsSignedNumbericHex(zu12edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+2;
                   modbusfun06dat:=StrToInt(zu12edit[StepNowFresh+1].text)-1;
                end;
            13:   if IsSignedNumbericHex(zu13edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+2;
                   modbusfun06dat:=StrToInt(zu13edit[StepNowFresh+1].text)-1;
                end;
            14:   if IsSignedNumbericHex(zu14edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+2;
                   modbusfun06dat:=StrToInt(zu14edit[StepNowFresh+1].text)-1;
                end;
            15:   if IsSignedNumbericHex(zu15edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+2;
                   modbusfun06dat:=StrToInt(zu15edit[StepNowFresh+1].text)-1;
                end;
            16:   if IsSignedNumbericHex(zu16edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+2;
                   modbusfun06dat:=StrToInt(zu16edit[StepNowFresh+1].text)-1;
                end;
            17:   if IsSignedNumbericHex(zu17edit[StepNowFresh+1].text) then begin
                   modbusfun06:=$3000+StepNowFresh*32+(hh div 60)+2;
                   modbusfun06dat:=StrToInt(zu17edit[StepNowFresh+1].text)-1;
                end;
          end;
        end;
       if Key=VK_MULTIPLY then begin
          if IsNumberic(zu10edit[StepNowFresh+1].text[4]) then begin
            ii:=StrToInt(zu10edit[StepNowFresh+1].text[4]);
            if ii<9 then begin
               modbusfun16:=$3000+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(Form_line.edt121.Text) )*32+((hh+10) div 60)+1;
               modbusfun16str:=Copy(zu10edit[StepNowFresh+1].text,1,3)+IntToStr(ii+1);
                modbusfun16len:=2;
            end;
          end;
       end;
       if Key=VK_DIVIDE then begin
          if IsNumberic(zu10edit[StepNowFresh+1].text[4]) then begin
            ii:=StrToInt(zu10edit[StepNowFresh+1].text[4]);
            if ii>0 then begin
               modbusfun16:=$3000+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(Form_line.edt121.Text) )*32+((hh+10) div 60)+1;
               modbusfun16str:=Copy(zu10edit[StepNowFresh+1].text,1,3)+IntToStr(ii-1);
                modbusfun16len:=2;
            end;

          end;
       end;
     end;
  end;
end;
procedure bakToServerDisk();
  var
    f: TextFile;
    Edt2temp,save_file_name,ss,save_file_name_Sever: string;
  procedure  MoveBarcodefile(barfiletype:SmallInt);        //定时条码文件复制到网盘；
  var
    SearchRec: TSearchRec;
    found,fCopynum: integer;
    F:TextFile;
    ss,strc,PNnameTemp:string;
  begin
    with Form_line do begin                 //仅找一个txt文件 Result := TStringList.Create;
      if(grp1.Tag=2)and(edt62.text='25')and(edt6.hint<>'UpSinglePCS') then  begin
        Edt2temp:='temp\';                                       //SMT默认： 生成临时文件夹temp\并剪切到映射盘
      end else
        Edt2temp:='';

      //-------1.tela用！--当前机种订单文件优先上传---------------------------------------
      if (edt62.Text<>'25')or(edt6.hint='UpSinglePCS')  then begin             //tesla默认：当前机种文件不优先执行；统一改由轮询执行；
        found := FindFirst(edt2.Text +Edt2temp +lbl90.hint+'\' + '*.csv', faAnyFile, SearchRec); //第一文件
        while found = 0 do  begin             //找到文件              //AssignFile(F,edt2.Text+lbl90.hint+'\' +SearchRec.Name);
          if (lbl181.Caption<>'1') then              //轮询时：状态改变，退出轮询上传
            Exit;
          save_file_name :=lbl90.hint+'\' +SearchRec.Name;
          save_file_name_Sever:=SearchRec.Name;
          if(Label10.caption<>save_file_name) then                    //非当前被占用文件
          try
            if not DirectoryExists(edt1.Text) then begin
               edt1.Color:=clRed;
               stat1.Panels[3].Text:='SerVer Folder not exist!';
               Exit;
            end else begin
               if not CopyFile(PChar(edt2.Text  +Edt2temp + save_file_name), PChar(edt1.Text + save_file_name_Sever), True) then begin //如果文件已存在则返回错误
                  stat1.Panels[4].Text:='Copy File Failed!';
                  //Exit;
               end else begin
                  if not DirectoryExists(edt2.Text+lbl183.Caption ) then                 //备份路径
                  try
                    begin
                      CreateDir(edt2.Text+lbl183.Caption );  //创建目录
                    end;
                  except  //finally
                      stat1.Panels[4].Text:='Cannot Create '+edt2.Text+lbl183.Caption;
                      Exit;
                  end;
                  if not DirectoryExists(edt2.Text+lbl183.Caption+lbl90.hint+'\' ) then
                  try
                    begin
                      CreateDir(edt2.Text+lbl183.Caption+lbl90.hint+'\' );  //创建目录
                    end;
                  except  //finally
                      stat1.Panels[4].Text:='Cannot Create '+edt2.Text+lbl183.Caption+lbl90.hint+'\' ;
                      Exit;
                  end;                                                          //重复copy，防止遗漏！
                  CopyFile(PChar(edt2.Text  +Edt2temp + save_file_name), PChar(edt2.Text+lbl183.Caption + save_file_name), True);//
                  DeleteFile(edt2.Text  +Edt2temp + save_file_name);
                  stat1.Panels[4].Text:='Copy File Succeed!';
               end;
                fCopynum:=fCopynum+1;
                if fCopynum>5*strtoint(edt6.Text) then
                  Exit;
            end;
          finally
            //Closefile(f);
          end;
          found := FindNext(SearchRec);
        end;
        FindClose(SearchRec);
      end;

      if(edt62.Text='24')or( (edt62.Text='25')and(edt6.hint='UpSinglePCS') )then begin    //早期（没有专用TEMP文件夹）：当前机种的单PCS也上传  两种文件剪切到映射盘
        found := FindFirst(edt157.Text+lbl90.hint+'\' + '*.csv', faAnyFile, SearchRec);
        while found = 0 do  begin             //找到文件             //AssignFile(F,edt2.Text+lbl90.hint+'\' +SearchRec.Name);
          if lbl181.Caption<>'1' then         //状态改变，退出定时上传
            Exit;
          save_file_name :=lbl90.hint+'\' +SearchRec.Name;
          save_file_name_Sever:=SearchRec.Name;
          if(Label10.caption<>save_file_name) then             //非当前被占用文件
          try
            if not DirectoryExists(edt1.Text) then begin
                edt1.Color:=clRed;
                stat1.Panels[3].Text:='SerVer Folder not exist!';
                Exit;
            end else begin
                 if not CopyFile(PChar(edt157.Text + save_file_name), PChar(edt1.Text + save_file_name_Sever), True) then begin //如果文件已存在则返回错误
                    stat1.Panels[4].Text:='Copy File Failed!';
                    //Exit;
                 end else begin
                    if not DirectoryExists(edt157.Text+lbl183.Caption ) then                 //备份路径
                    try
                      begin
                        CreateDir(edt157.Text+lbl183.Caption );  //创建目录
                      end;
                    except  //finally
                        stat1.Panels[4].Text:='Cannot Create '+edt157.Text+lbl183.Caption;
                        Exit;
                    end;
                    if not DirectoryExists(edt157.Text+lbl183.Caption+lbl90.hint+'\' ) then
                    try
                      begin
                        CreateDir(edt157.Text+lbl183.Caption+lbl90.hint+'\' );  //创建目录
                      end;
                    except  //finally
                        stat1.Panels[4].Text:='Cannot Create '+edt157.Text+lbl183.Caption+lbl90.hint+'\' ;
                        Exit;
                    end;
                    CopyFile(PChar(edt157.Text + save_file_name), PChar(edt157.Text+lbl183.Caption + save_file_name), True);////RenameFile(edt2.Text + save_file_name, edt2.Text +'bak'+ save_file_name);
                    DeleteFile(edt157.Text + save_file_name);
                    stat1.Panels[4].Text:='Copy File Succeed!';
                 end;
                  fCopynum:=fCopynum+1;
                  if fCopynum>5*strtoint(edt6.Text) then
                    Exit;
            end;
          finally
            //Closefile(f);
          end;
          found := FindNext(SearchRec);
        end;
        FindClose(SearchRec);
      end;

      //---2.tesla和SMT共用！------轮询 遍历其它机种文件---------------------------------------

      fCopynum:=0;                                                              //单次上传计数；
      if not IsNumberic(lbl182.Caption)  then
        lbl182.Caption:='0';
      while true do  begin
        lbl182.Caption:=IntToStr(StrToInt(lbl182.Caption)+1);                     //依次加1，从PN1开始搜索文件；
        if Readchis('Model Para','PN'+lbl182.Caption)<>''  then begin
          PNnameTemp:=Readchis('Model Para','PN'+lbl182.Caption);
          if DirectoryExists(edt2.Text  +Edt2temp +PNnameTemp+'\' ) then begin
             break;
          end;
        end else if StrToInt(lbl182.Caption)>48 then begin
          lbl182.Caption:='0';                                                  //读取到最后一个PN,退出并下次循环上传
          exit;
        end;
      end;

      found := FindFirst(edt2.Text  +Edt2temp +PNnameTemp+'\' + '*.csv', faAnyFile, SearchRec); //第一文件
      while found = 0 do  begin             //找到文件             //AssignFile(F,edt2.Text+lbl90.hint+'\' +SearchRec.Name);
        if lbl181.Caption<>'1' then         //状态改变，退出定时上传
          Exit;
        save_file_name :=PNnameTemp+'\' +SearchRec.Name;
        save_file_name_Sever:=SearchRec.Name;
        if(Label10.caption<>save_file_name) then             //非当前被占用文件
        try
          if not DirectoryExists(edt1.Text) then begin
              edt1.Color:=clRed;
              stat1.Panels[3].Text:='SerVer Folder not exist!';
              Exit;
          end else begin
               if not CopyFile(PChar(edt2.Text  +Edt2temp + save_file_name), PChar(edt1.Text + save_file_name_Sever), True) then begin //如果文件已存在则返回错误
                  stat1.Panels[4].Text:='Copy File Failed!';
                  //Exit;
               end else begin
                  if not DirectoryExists(edt2.Text+lbl183.Caption ) then                 //备份路径
                  try
                    begin
                      CreateDir(edt2.Text+lbl183.Caption );  //创建目录
                    end;
                  except  //finally
                      stat1.Panels[4].Text:='Cannot Create '+edt2.Text+lbl183.Caption;
                      Exit;
                  end;
                  if not DirectoryExists(edt2.Text+lbl183.Caption+PNnameTemp+'\' ) then   //PN路径
                  try
                    begin
                      CreateDir(edt2.Text+lbl183.Caption+PNnameTemp+'\' );  //创建目录
                    end;
                  except  //finally
                      stat1.Panels[4].Text:='Cannot Create '+edt2.Text+lbl183.Caption+PNnameTemp+'\' ;
                      Exit;
                  end;                                                          //重复copy不会成功，防止遗漏！
                  CopyFile(PChar(edt2.Text  +Edt2temp + save_file_name), PChar(edt2.Text+lbl183.Caption + save_file_name), True);////RenameFile(edt2.Text + save_file_name, edt2.Text +'bak'+ save_file_name);
                  DeleteFile(edt2.Text  +Edt2temp + save_file_name);
                  stat1.Panels[4].Text:='Copy File Succeed!';
               end;
                fCopynum:=fCopynum+1;
                if fCopynum>10*strtoint(edt6.Text) then
                  Exit;
          end;
        finally
          //Closefile(f);
        end;
        found := FindNext(SearchRec);
      end;
      FindClose(SearchRec);
      
      if (edt62.Text='25')and(edt2.Text<>edt157.Text) then begin                //单PCS路径不同，则分别轮询；不用备份
        fCopynum:=0;                                                              //单次上传计数；
        while true do  begin
          lbl182.tag:=lbl182.tag+1;                     //依次加1，从PN1开始搜索文件；
          if Readchis('Model Para','PN'+inttostr(lbl182.tag) )<>''  then begin
            PNnameTemp:=Readchis('Model Para','PN'+inttostr(lbl182.tag));
            if DirectoryExists(edt157.Text  +Edt2temp +PNnameTemp+'\' ) then begin
               break;
            end;
          end else if(lbl182.tag>48) then begin
            lbl182.tag:=0;                                                  //读取到最后一个PN,退出并下次循环上传
            exit;
          end;
        end;

        found := FindFirst(edt157.Text  +Edt2temp +PNnameTemp+'\' + '*.csv', faAnyFile, SearchRec); //第一文件
        while found = 0 do  begin             //找到文件             //AssignFile(F,edt2.Text+lbl90.hint+'\' +SearchRec.Name);
          if lbl181.Caption<>'1' then         //状态改变，退出定时上传
            Exit;
          save_file_name :=PNnameTemp+'\' +SearchRec.Name;
          save_file_name_Sever:=SearchRec.Name;               //一次保存完，不存在占用！if(Label10.caption<>save_file_name) then             //非当前被占用文件
          try
            if not DirectoryExists(edt1.Text) then begin
                edt1.Color:=clRed;
                stat1.Panels[3].Text:='SerVer Folder not exist!';
                Exit;
            end else begin
                if not CopyFile(PChar(edt157.Text  +Edt2temp + save_file_name), PChar(edt1.Text + save_file_name_Sever), True) then begin //如果文件已存在则返回错误
                    stat1.Panels[4].Text:='Copy File Failed!';
                    //Exit;
                end else begin
                    DeleteFile(edt157.Text  +Edt2temp + save_file_name);
                    stat1.Panels[4].Text:='Copy File Succeed!';
                end;
                fCopynum:=fCopynum+1;
                if fCopynum>10*strtoint(edt6.Text) then
                  Exit;
            end;
          finally             //Closefile(f);
          end;
          found := FindNext(SearchRec);
        end;
        FindClose(SearchRec);
      end;
    //----3.tesla用！--------------------------------------------  
      if(edt62.Text='24')or( (edt62.Text='25')and(edt6.hint='UpSinglePCS') )then begin    //早期（没有专用TEMP文件夹）：轮询机种文件夹里的单PCS也上传   两种文件剪切到映射盘
        found := FindFirst(edt157.Text+PNnameTemp+'\' + '*.csv', faAnyFile, SearchRec);
        while found = 0 do  begin             //找到文件             //AssignFile(F,edt2.Text+lbl90.hint+'\' +SearchRec.Name);
          if lbl181.Caption<>'1' then         //状态改变，退出定时上传
            Exit;
          save_file_name :=PNnameTemp+'\' +SearchRec.Name;
          save_file_name_Sever:=SearchRec.Name;
          if(Label10.caption<>save_file_name) then             //非当前被占用文件
          try
            if not DirectoryExists(edt1.Text) then begin
                edt1.Color:=clRed;
                stat1.Panels[3].Text:='SerVer Folder not exist!';
                Exit;
            end else begin
                 if not CopyFile(PChar(edt157.Text + save_file_name), PChar(edt1.Text + save_file_name_Sever), True) then begin //如果文件已存在则返回错误
                    stat1.Panels[4].Text:='Copy File Failed!';
                    //Exit;
                 end else begin
                    if not DirectoryExists(edt157.Text+lbl183.Caption ) then                 //备份路径
                    try
                      begin
                        CreateDir(edt157.Text+lbl183.Caption );  //创建目录
                      end;
                    except  //finally
                        stat1.Panels[4].Text:='Cannot Create '+edt157.Text+lbl183.Caption;
                        Exit;
                    end;
                    if not DirectoryExists(edt157.Text+lbl183.Caption+PNnameTemp+'\' ) then
                    try
                      begin
                        CreateDir(edt157.Text+lbl183.Caption+PNnameTemp+'\' );  //创建目录
                      end;
                    except  //finally
                        stat1.Panels[4].Text:='Cannot Create '+edt157.Text+lbl183.Caption+PNnameTemp+'\' ;
                        Exit;
                    end;
                    CopyFile(PChar(edt157.Text + save_file_name), PChar(edt157.Text+lbl183.Caption + save_file_name), True);////RenameFile(edt2.Text + save_file_name, edt2.Text +'bak'+ save_file_name);
                    DeleteFile(edt157.Text + save_file_name);
                    stat1.Panels[4].Text:='Copy File Succeed!';
                 end;
                  fCopynum:=fCopynum+1;
                  if fCopynum>10*strtoint(edt6.Text) then
                    Exit;
            end;
          finally
            //Closefile(f);
          end;
          found := FindNext(SearchRec);
        end;
        FindClose(SearchRec);
      end;
    end;
  end;
begin
  with Form_line do begin
    if (edt62.Text='24')or(edt62.Text='25') then begin                            //定时上传网盘
       if ( (Label10.caption<>'Label10')and (lbl181.Caption='10') )then    //1.关闭窗口；仅一次，对其它程序无影响；第一文件子路径+文件名 已生成！
       begin                                                                                                                       
          if(grp1.Tag=2)and(edt62.text='25')and(edt6.hint<>'UpSinglePCS') then
            Edt2temp:='temp\'                                              //SMT默认：复制集总文件到临时文件夹temp\并剪切到映射盘
          else
            Edt2temp:='';
          save_file_name :=Label10.caption;
          if Pos('\',Label10.caption)>0 then
            save_file_name_Sever:=copy(Label10.caption,pos('\',Label10.caption)+1,length(Label10.caption))
          else
            save_file_name_Sever:=Label10.caption;
          try
            if not DirectoryExists(edt1.Text) then begin
                edt1.Color:=clRed;
                stat1.Panels[3].Text:='SerVer Folder not exist!';
            end else begin
                 if not CopyFile(PChar(edt2.Text  +Edt2temp + save_file_name), PChar(edt1.Text + save_file_name_Sever), True) then  //如果文件已存在则返回错误
                    stat1.Panels[4].Text:='Copy File Failed!'
                 else begin
                    DeleteFile(edt2.Text  +Edt2temp + save_file_name);
                    stat1.Panels[4].Text:='Copy File Succeed!';
                 end;
            end;
          finally
            //Closefile(f);
          end;
       end;
       if ( (lbl180.caption<>'lbl180')and (lbl181.Caption='25') )and(edt62.text='25')and(edt6.hint<>'UpSinglePCS')then    //生成单PCS文件时，触发进入剪切单PCS文件
       begin
                                                                                  //升级1：生成临时文件夹temp\并剪切到映射盘
          Edt2temp:='temp\';
          save_file_name :=lbl180.caption;
          if Pos('\',lbl180.caption)>0 then
            save_file_name_Sever:=copy(lbl180.caption,pos('\',lbl180.caption)+1,length(lbl180.caption))
          else
            save_file_name_Sever:=lbl180.caption;
          try
            if not DirectoryExists(edt1.Text) then begin
                edt1.Color:=clRed;
                stat1.Panels[3].Text:='SerVer Folder not exist!';
            end else begin
                 if not CopyFile(PChar(edt157.Text  +Edt2temp + save_file_name), PChar(edt1.Text + save_file_name_Sever), True) then  //如果文件已存在则返回错误
                    stat1.Panels[4].Text:='Copy File Failed!'
                 else begin
                    DeleteFile(edt157.Text  +Edt2temp + save_file_name);
                    stat1.Panels[4].Text:='Copy File Succeed!';
                 end;
            end;
          finally
            //Closefile(f);
          end;
       end;

       if lbl181.Caption='1' then begin                                           //2.轮询时间到，一次结束； //if stat1.Panels[4].Text='Copy File Succeed!' then
         MoveBarcodefile(0);
         lbl181.Caption:='0';
       end else if(lbl181.Caption='24')or( (lbl181.Caption='25')and(edt6.hint='UpSinglePCS') )then begin   //3.刚生成条码文件（早期jaguar剪切单PCS文件到映射盘）
         if (Lbl180.caption<>'lbl180') then       //子路径+文件名 已生成！
         begin
            save_file_name :=Lbl180.caption;
            if Pos('\',Lbl180.caption)>0 then     //是否需要子目录
              save_file_name_Sever:=copy(Lbl180.caption,pos('\',Lbl180.caption)+1,length(Lbl180.caption))
            else
              save_file_name_Sever:=Lbl180.caption;
            try
            //--1------------------条码文件先备份起来 -----------------------------
              if not DirectoryExists(edt157.Text+lbl183.Caption ) then
              try
                begin
                  CreateDir(edt157.Text+lbl183.Caption );  //创建备份目录
                end;
              except  //finally
                  stat1.Panels[4].Text:='Cannot Create '+edt157.Text+lbl183.Caption;
                  Exit;
              end;
              if not DirectoryExists(edt157.Text+lbl183.Caption+lbl90.hint+'\' ) then                 //本地备份路径
              try
                begin
                  CreateDir(edt157.Text+lbl183.Caption+lbl90.hint+'\' );  //创建备份子目录
                end;
              except  //finally
                  stat1.Panels[4].Text:='Cannot Create '+edt157.Text+lbl183.Caption+lbl90.hint+'\' ;
                  Exit;
              end;
              CopyFile(PChar(edt157.Text + save_file_name), PChar(edt157.Text+lbl183.Caption + save_file_name), True);////RenameFile(edt2.Text + save_file_name, edt2.Text +'bak'+ save_file_name);
            //-2-------------------条码文件上传网盘 -----------------------------
              if not DirectoryExists(edt1.Text) then begin
                  edt1.Color:=clRed;
                  stat1.Panels[3].Text:='SerVer Folder not exist!';
              end else begin
                  edt1.Color:=clWindow;
                  stat1.Panels[3].Text:='SerVer Folder existed!';
                   if not CopyFile(PChar(edt157.Text + save_file_name), PChar(edt1.Text + save_file_name_Sever), True) then  //如果文件已存在则返回错误
                      stat1.Panels[4].Text:='Copy File Failed!'
                   else begin
                   {与前面重复了
                      if not DirectoryExists(edt157.Text+lbl183.Caption ) then                 //网盘路径
                      try
                        begin
                          CreateDir(edt157.Text+lbl183.Caption );  //创建目录
                        end;
                      except  //finally
                          stat1.Panels[4].Text:='Cannot Create '+edt157.Text+lbl183.Caption;
                          Exit;
                      end;
                      if not DirectoryExists(edt157.Text+lbl183.Caption+lbl90.hint+'\' ) then                 //网盘路径
                      try
                        begin
                          CreateDir(edt157.Text+lbl183.Caption+lbl90.hint+'\' );  //创建目录
                        end;
                      except  //finally
                          stat1.Panels[4].Text:='Cannot Create '+edt157.Text+lbl183.Caption+lbl90.hint+'\' ;
                          Exit;
                      end;
                      CopyFile(PChar(edt157.Text + save_file_name), PChar(edt157.Text+lbl183.Caption + save_file_name), True);////RenameFile(edt2.Text + save_file_name, edt2.Text +'bak'+ save_file_name);
                   }
                      DeleteFile(edt157.Text + save_file_name);
                      stat1.Panels[4].Text:='Copy File Succeed!';
                   end;
              end;
            finally
              //Closefile(f);
            end;
         end;
       end;
    end;
  end;
end;

end.
