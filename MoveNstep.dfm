object FormMoveN: TFormMoveN
  Left = 329
  Top = 262
  Width = 843
  Height = 157
  Caption = 'MoveNstep '#31227#21160#22810#20010#27493#39588
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 40
    Top = 24
    Width = 160
    Height = 16
    Caption = 'StepStart '#24320#22987#27493#39588#65306
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #26032#23435#20307
    Font.Style = []
    ParentFont = False
  end
  object lbl2: TLabel
    Left = 40
    Top = 64
    Width = 144
    Height = 16
    Caption = 'StepEnd '#32467#26463#27493#39588' :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #26032#23435#20307
    Font.Style = []
    ParentFont = False
  end
  object lbl3: TLabel
    Left = 376
    Top = 24
    Width = 160
    Height = 16
    Caption = 'Move to '#31227#21160#21040#27493#39588#65306
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #26032#23435#20307
    Font.Style = []
    ParentFont = False
  end
  object spltr1: TsSplitter
    Left = 280
    Top = 0
    Height = 101
    Align = alNone
    SkinData.SkinSection = 'SPLITTER'
  end
  object btn1: TButton
    Left = 376
    Top = 55
    Width = 177
    Height = 41
    Caption = 'StartMove '#24320#22987#31227#21160
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = btn1Click
    OnKeyDown = btn1KeyDown
  end
  object btn2: TsButton
    Left = 592
    Top = 56
    Width = 129
    Height = 41
    Caption = 'Cancle '#21462#28040
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnClick = btn2Click
    SkinData.SkinSection = 'BUTTON'
  end
  object ildrw1: TiLedArrow
    Left = 296
    Top = 24
    Width = 73
    ArrowBodyLength = 53
  end
  object edt1: TsDecimalSpinEdit
    Left = 192
    Top = 16
    Width = 89
    Height = 28
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Text = '10'
    OnChange = edt1Change
    SkinData.SkinSection = 'EDIT'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    Increment = -1.000000000000000000
    MinValue = 1.000000000000000000
    Value = 10.000000000000000000
    DecimalPlaces = 0
  end
  object edt2: TsDecimalSpinEdit
    Left = 192
    Top = 56
    Width = 89
    Height = 28
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    Text = '10'
    OnChange = edt1Change
    SkinData.SkinSection = 'EDIT'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    Increment = -1.000000000000000000
    MinValue = 1.000000000000000000
    Value = 10.000000000000000000
    DecimalPlaces = 0
  end
  object edt3: TsDecimalSpinEdit
    Left = 552
    Top = 16
    Width = 89
    Height = 28
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Text = '1'
    SkinData.SkinSection = 'EDIT'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    Increment = -1.000000000000000000
    MinValue = 1.000000000000000000
    Value = 1.000000000000000000
    DecimalPlaces = 0
  end
end
