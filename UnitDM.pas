unit UnitDM;

interface

uses
  SysUtils, Classes,SyncObjs;

type
  TDMLJK = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
     FCsWriteLogFile:TCriticalSection;
     exepath:string;
     procedure MyAddlog(s:string);
     procedure strToList(src: string; var ts: TStringList);
    { Public declarations }
  end;

var
  DMLJK: TDMLJK;

implementation

{$R *.dfm}

procedure TDMLJK.strToList(src: string; var ts: TStringList);
var
  dec: string;
  i: integer;
  str: string;
begin
  dec := ']';
  if src = '' then
  begin
    ts.Add('');
    exit;
  end;
  repeat
    i := pos(dec, src);
    str := copy(src, 1, i - 1);
    if (str = '') and (i > 0) then
    begin
      delete(src, 1, length(dec));
      continue;
    end;
    if i > 0 then
    begin
      ts.Add(str);
      delete(src, 1, i + length(dec) - 1);
    end;
  until i <= 0;
  if src <> '' then
    ts.Add(src);
end;

procedure TDMLJK.MyAddlog(s:string);
var
  tFile: TextFile;
  filename: string;
  b:Boolean;
begin
//  if FormatDateTime('yyyyMMdd',now)>'20231230' then
//  Abort;
  filename:=exepath+'log';
  b:=False;
  if not(DirectoryExists(filename)) then
  begin
    try
       ForceDirectories(filename);
       b:=True;
    except
    end;
  end
  else
  begin
       b:=True;
  end;
  if b=False then exit;
  filename:=filename+'\'+formatdatetime('yyyyMMdd',Now)+'.txt';
    FCsWriteLogFile.Enter();
    AssignFile(tFile,filename);
      try
       if FileExists(filename) then
         Append(tFile)
       else
         Rewrite(tFile);
         Writeln(tFile,'<'+FormatDateTime('hh:nn:ss', Now)+'>'+s);
      except
      end;
    CloseFile(tFile);
    FCsWriteLogFile.Leave;
end;

procedure TDMLJK.DataModuleCreate(Sender: TObject);
begin
  exepath:=ExtractFilePath(ParamStr(0));
  FCsWriteLogFile:=TCriticalSection.Create;
end;

procedure TDMLJK.DataModuleDestroy(Sender: TObject);
begin
 FCsWriteLogFile.free;
end;

end.
