unit FindPoint;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,Unit0_globalVariant, ExtCtrls;

type
  TFormFind = class(TForm)
    btn1: TButton;
    edt1: TEdit;
    edt2: TEdit;
    edt3: TEdit;
    mmo1: TMemo;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    tmr1: TTimer;
    btn2: TButton;
    btn3: TButton;
    btn4: TButton;
    mmo2: TMemo;
    procedure btn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edt1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tmr1Timer(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormFind: TFormFind;

implementation

uses main;

{$R *.dfm}

procedure TFormFind.btn1Click(Sender: TObject);
begin
  close;
end;

procedure TFormFind.FormShow(Sender: TObject);
begin
//  modbusfun06dat:=$00F1;    //读找点结果
//  modbusfun06:=$0001;
  btn1.SetFocus;
  tmr1.Enabled:=True;
  
end;

procedure TFormFind.edt1Click(Sender: TObject);
var
  str:string;
begin
  if InputQuery('Pls Enter Command!', '111', str) then
    begin
    // str := InputBox('输入整数', '123', '000000');
     if str='' then str:='0';
     edt1.Text:=str;
     modbusfun06dat:=StrToInt(str);
     modbusfun06:=$1080;
    end;
end;

procedure TFormFind.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 modbusfun05:=3; modbusfun05dat:=$FF;
 tmr1.Enabled:=False;
end;

procedure TFormFind.tmr1Timer(Sender: TObject);
begin
  if modbusfun06=0 then
  begin
      modbusfun06dat:=$00F1;    //读找点结果
      modbusfun06:=$0001;
  end;
end;

procedure TFormFind.btn2Click(Sender: TObject);
begin
  btn4.Caption:='继续找点';
  tmr1.Enabled:=False;
  modbusfun06dat:=114;
  modbusfun06:=$1080;
  form_line.ts3.TabVisible:=True;
  form_line.PageControl1.TabIndex:=5-1;
  //formwave.showmodal;
end;

procedure TFormFind.btn3Click(Sender: TObject);
begin
  btn4.Caption:='继续找点';
   tmr1.Enabled:=False;
  form_line.ts3.TabVisible:=True;
  form_line.PageControl1.TabIndex:=5-1;
  //formwave.showmodal;
end;

procedure TFormFind.btn4Click(Sender: TObject);
begin
 // if tmr1.Enabled then begin
  //  btn4.Caption:='继续找点';
 //   tmr1.Enabled:=False;
 // end else begin
    btn4.Caption:='停止找点';
    tmr1.Enabled:=True;
 // end;
end;

end.
