object FormStepCopy: TFormStepCopy
  Left = 217
  Top = 179
  Width = 658
  Height = 295
  Caption = 'Copy N '#22810#32852#26495#25335#36125
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 16
    Top = 16
    Width = 104
    Height = 13
    Caption = 'CodeVer '#20195#30721#29256#26412#65306
    Enabled = False
  end
  object lbl2: TLabel
    Left = 16
    Top = 40
    Width = 99
    Height = 13
    Caption = 'Resive '#20462#35746#29256#26412' '#65306
    Enabled = False
  end
  object lbl3: TLabel
    Left = 16
    Top = 149
    Width = 150
    Height = 13
    Caption = 'TotalBoards '#24635#32852#29255#25968#37327#65306'    --'
  end
  object lbl4: TLabel
    Left = 14
    Top = 64
    Width = 195
    Height = 13
    Caption = 'GreenOffset '#32852#29255#32511#33394#25351#31034#28783#20559#31227#65306'   --'
    Enabled = False
  end
  object lbl5: TLabel
    Left = 16
    Top = 176
    Width = 185
    Height = 13
    Caption = 'O/S-n '#24320#30701#36335#27979#35797#27425#25968'('#40664#35748':0)         -'
  end
  object lbl7: TLabel
    Left = 296
    Top = 146
    Width = 153
    Height = 13
    Caption = '>=2  '#35774#23450#20026'2'#21017#25335#36125' '#19968#27425'        '
  end
  object lbl8: TLabel
    Left = 296
    Top = 168
    Width = 342
    Height = 13
    Caption = '21:'#26681#25454#20004'PCS'#30701#36335#32676#26234#33021#22797#21046#65292#40664#35748#65306'0('#20165#22797#21046#19968#27425#30701#36335#32676')          '
  end
  object lbl6: TLabel
    Left = 16
    Top = 117
    Width = 149
    Height = 13
    Caption = 'CopyPins '#25335#36125#28857#20559#31227#25968#65306'    --'
  end
  object edt1: TEdit
    Left = 216
    Top = 8
    Width = 73
    Height = 21
    Enabled = False
    TabOrder = 0
    Text = '10'
  end
  object edt2: TEdit
    Left = 216
    Top = 32
    Width = 73
    Height = 21
    Enabled = False
    TabOrder = 1
    Text = '32'
  end
  object edt3: TEdit
    Left = 216
    Top = 141
    Width = 73
    Height = 21
    TabOrder = 2
    Text = '2'
  end
  object btn1: TButton
    Left = 16
    Top = 197
    Width = 217
    Height = 41
    Caption = 'StartCopy '#24320#22987#25152#26377#25335#36125
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
    OnClick = btn1Click
  end
  object edt4: TEdit
    Left = 216
    Top = 61
    Width = 73
    Height = 21
    Enabled = False
    TabOrder = 4
    Text = '32'
  end
  object edt5: TEdit
    Left = 216
    Top = 168
    Width = 73
    Height = 21
    TabOrder = 5
    Text = '0'
  end
  object btn2: TButton
    Left = 280
    Top = 198
    Width = 209
    Height = 41
    Caption = 'CopyO/S '#20165#25335#36125#30701#36335#32676
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    OnClick = btn2Click
  end
  object edt6: TEdit
    Left = 216
    Top = 112
    Width = 73
    Height = 21
    TabOrder = 7
    Text = '32'
  end
end
