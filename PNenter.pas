unit PNenter;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  IniFiles,FileCtrl,strUtils,Dialogs, StdCtrls, ExtCtrls,Unit0_globalVariant;

type
  TFormPN = class(TForm)
    btn1: TButton;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    edt2: TEdit;
    edt3: TEdit;
    tmr1: TTimer;
    cbb1: TComboBox;
    cbb2: TComboBox;
    edt13: TEdit;
    lbl11: TLabel;
    edt15: TEdit;
    cbb3: TComboBox;
    Panel1: TPanel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    edt1: TEdit;
    edt4: TEdit;
    edt5: TEdit;
    edt6: TEdit;
    edt7: TEdit;
    edt8: TEdit;
    edt9: TEdit;
    edt10: TEdit;
    edt11: TEdit;
    edt12: TEdit;
    edt14: TEdit;
    procedure btn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edt1Enter(Sender: TObject);
    procedure edt2Enter(Sender: TObject);
    procedure edt3Enter(Sender: TObject);
    procedure edt4Enter(Sender: TObject);
    procedure cbb1Change(Sender: TObject);
    procedure edt3KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormPN: TFormPN;
  tmr3_statue:Boolean;
implementation

uses main;

{$R *.dfm}

procedure TFormPN.btn1Click(Sender: TObject);
begin
  begin
    if( Trim(cbb1.Text)='')or(FormPN.cbb1.ItemIndex<1) then cbb1.Color:=clRed else cbb1.Color:=clWindow;

    if Trim(edt2.Text)='' then begin edt2.Color:=clRed ;exit; end;
    if (edt6.Text<>'0') and (Length(Trim(edt2.Text))>StrToInt(edt6.Text)) then begin edt2.Color:=clRed ;exit; end;
    if (edt7.Text<>'0') and (Length(Trim(edt2.Text))<StrToInt(edt7.Text)) then begin edt2.Color:=clRed ;exit; end;
    edt2.Color:=clWindow;
    if ( Readchis(FormPN.cbb1.Text, 'OrderCharPos', '')='999' )and (edt13.Text='') then begin edt13.Color:=clRed ;exit; end;
    edt13.Color:=clWindow;
    
    if Trim(edt3.Text)='' then begin edt3.Color:=clRed ;exit; end;
    if (edt8.Text<>'0') and (Length(Trim(edt3.Text))>StrToInt(edt8.Text)) then begin edt3.Color:=clRed ;exit; end;
    if (edt9.Text<>'0') and (Length(Trim(edt3.Text))<StrToInt(edt9.Text)) then begin edt3.Color:=clRed ;exit; end;
    edt3.Color:=clWindow;

   //无意义 if( Trim(cbb2.Text)='')or(FormPN.cbb2.ItemIndex<1) then cbb2.Color:=clRed else cbb2.Color:=clWindow;
    if (Readchis('Server','MESprogram')<>'')or(Readchis('Server','MESprogramdefault')<>'')  then begin     //丽清可选站别
      if( Trim(cbb2.Text)='')or(FormPN.cbb2.ItemIndex<1) then begin cbb2.Color:=clRed;exit; end else cbb2.Color:=clWindow;
      if( Trim(cbb3.Text)='')or(FormPN.cbb3.ItemIndex<1) then begin cbb3.Color:=clRed;exit; end else cbb3.Color:=clWindow;
    end;
    if (Trim(cbb1.Text)<>'')and (FormPN.cbb1.ItemIndex>0)
      //and (Trim(edt2.Text)<>'')and (Trim(edt3.Text)<>'')
      //无意义 and (Trim(cbb2.Text)<>'') and (FormPN.cbb2.ItemIndex>0)                   //无实际意义
    then begin
      form_line.edt77.text:=cbb1.Text;               //PC软件定义的机种名，->主板含有VZV0则有更高优先级
      if (StrToInt(form_line.edt62.Text) <1) then    //childfoler=0: PC直接生成文件 ,则显示文件名
        form_line.lbl90.hint:=cbb1.Text;

      form_line.edt78.text:=edt2.Text;
      form_line.edt79.text:=edt3.Text;
      form_line.edt80.text:=cbb2.Text;               //治具编号
      form_line.edt164.text:=edt15.Text;             //扩展项目
      if (Readchis('Server','MESprogram')<>'')or(Readchis('Server','MESprogramdefault')<>'')  then     //丽清可选站别
        form_line.edt164.text:=cbb3.Text;               //station编号

      if Readchis(FormPN.cbb1.Text, 'OrderCharPos', '')='999' then    //订单规则字符起始位置
        form_line.edt10.text:=edt13.Text;            //与fixture重合位置
      Form_line.CLose_1:=10;

      //enPN=99时设置edt12.text个位为0不执行下面选机种！
      if (Form_line.grp1.tag=2) and (FormPN.cbb1.ItemIndex>0)and (FormPN.cbb1.ItemIndex<=240) then begin    //选治具程序使能，且PC软件有正确选择  FIXPNSEL
        modbusfun06:=$0041;    //选择机种
        modbusfun06dat:=FormPN.cbb1.ItemIndex-1+strtoint(edt14.Text)*256;       //低8位设置机种编号，从0开始；高8位保留（设置机种分组数edt14.text）
      end;
     
      Close;
    end;
  end;
end;

procedure TFormPN.FormShow(Sender: TObject);
var
    nrf_dat_config: Tinifile;
    ini_path: string;
begin


  tmr1.Enabled:=true;
  tmr3_statue:=Form_line.tmr3.Enabled;
  Form_line.tmr3.Enabled:=False;     //暂停测试界面读取主板信息的指令；

  if (StrToInt(edt4.Text)and $01)=$01 then begin
    if Form_line.edt82.Text='99' then begin                     //jaguar启用PC存放程序，手动加载！    enPN

    end else if (Readchis('Model Para', 'PN1')<>'')  then    //有定义PN1
      begin
        cbb1.Text:=Readchis('Model Para', 'PN1')
      end
    else cbb1.Text:='0';

    cbb1.Enabled:=False;
  end;
  if (StrToInt(edt4.Text)and $02)=$02 then begin        //输入订单被禁止
    edt2.Text:='0';
    edt2.Enabled:=False;
    if (StrToInt(edt4.Text)and $04)<>$04 then begin     //员工号输入开启   if Readchis('Model Para','Process')<>'' then begin      //jiashida专用！
      edt3.SetFocus;
    end else
      btn1.setfocus;
  end else
    edt2.SetFocus;

  if (StrToInt(edt4.Text)and $04)=$04 then begin
    edt3.Text:='0';
    edt3.Enabled:=False;
  end;

  if (StrToInt(edt4.Text)and $08)=$08 then begin
    cbb2.Text:='0';
    cbb2.Enabled:=False;
  end;
  if Readchis('Model Para','Process')<>'' then begin      //jiashida专用！
    edt3.SetFocus;
    if ( Readchis('Model Para', 'OperatorMaxLen', '')<>'')  then
    begin
      FormPN.edt8.Text:= Readchis('Model Para', 'OperatorMaxLen', '') ;
      FormPN.edt3.MaxLength:=0;
    end;
    if ( Readchis('Model Para', 'OperatorMinLen', '')<>'')  then
    begin
      FormPN.edt9.Text:= Readchis('Model Para', 'OperatorMinLen', '') ;
    end;
  end else begin
    try
      ini_path := ExtractFilePath(application.ExeName) + 'config.ini';
      nrf_dat_config := Tinifile.Create(ini_path);

      if (nrf_dat_config.Readstring(FormPN.cbb1.Text, 'OrderMaxLen', '')<>'')  then
      begin
        FormPN.edt6.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'OrderMaxLen', '') ;
        FormPN.edt2.MaxLength:=0;
      end;
      if (nrf_dat_config.Readstring(FormPN.cbb1.Text, 'OrderMinLen', '')<>'')  then
      begin
        FormPN.edt7.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'OrderMinLen', '') ;
      end;
      if (nrf_dat_config.Readstring(FormPN.cbb1.Text, 'OperatorMaxLen', '')<>'')  then
      begin
        FormPN.edt8.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'OperatorMaxLen', '') ;
        FormPN.edt3.MaxLength:=0;
      end;
      if (nrf_dat_config.Readstring(FormPN.cbb1.Text, 'OperatorMinLen', '')<>'')  then
      begin
        FormPN.edt9.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'OperatorMinLen', '') ;
      end;
    finally
      nrf_dat_config.free;
    end;
  end;

  if(Form_line.edt62.text='25') then begin
    lbl1.Caption:='Product';
    lbl2.Caption:='Lot No';
    lbl4.Caption:='Station:';
    edt13.Enabled:=False;
    edt13.Visible:=False;
    cbb2.Visible:=true;
  end else if Form_line.edit1.MaxLength>70 then begin         //车灯机箱专用
    cbb2.Visible:=true;
    edt13.Enabled:=False;
    edt13.Visible:=False;
    FormPN.lbl4.Caption:='Line';
    Form_line.lbl93.Caption:='Line';
    if (Readchis('Server','MESprogram')<>'')or(Readchis('Server','MESprogramdefault')<>'') then begin    //丽清可选站别    //xx jaguar和
      FormPN.lbl11.Caption:='Station';
      Form_line.lbl188.Caption:='Station';
      lbl11.Visible:=True;      //扩展项目
      edt13.Visible:=false;
      FormPN.cbb2.Items.Strings[0]:='';
      FormPN.cbb3.Items.Strings[0]:='';
      cbb3.Visible:=true;
      Form_line.lbl188.Visible:=True;
      Form_line.edt164.Visible:=true;
    end;
  end else if Readchis(FormPN.cbb1.Text, 'OrderCharPos', '')='999' then  begin
    edt13.Enabled:=True;
    edt13.Visible:=true;
    lbl4.Caption:='Date+Lot';
    cbb2.Visible:=False;
  end else begin
    edt13.Enabled:=False;
    edt13.Visible:=False;
    lbl4.Caption:='Fixture';
    cbb2.Visible:=true;
  end;
  if (Readchis('Model Para', 'LoginItem1')<>'')  then begin
    FormPN.lbl1.Caption:=Readchis('Model Para', 'LoginItem1');
    Form_line.lbl90.Caption:=Readchis('Model Para', 'LoginItem1');
  end;
  if (Readchis('Model Para', 'LoginItem2')<>'')  then begin
    FormPN.lbl2.Caption:=Readchis('Model Para', 'LoginItem2');
    Form_line.lbl91.Caption:=Readchis('Model Para', 'LoginItem2');
  end;
  if (Readchis('Model Para', 'LoginItem3')<>'')  then begin
    FormPN.lbl3.Caption:=Readchis('Model Para', 'LoginItem3');
    Form_line.lbl92.Caption:=Readchis('Model Para', 'LoginItem3');
  end;
  if (Readchis('Model Para', 'LoginItem4')<>'')  then begin
    FormPN.lbl4.Caption:=Readchis('Model Para', 'LoginItem4');
    Form_line.lbl93.Caption:=Readchis('Model Para', 'LoginItem4');
  end;
end;

procedure TFormPN.tmr1Timer(Sender: TObject);
begin
  try
    if (StrToInt(edt4.Text)and $02)<>$02 then
     // FormPN.Active:=True;
      edt2.SetFocus
    else begin
      if (StrToInt(edt4.Text)and $04)<>$04 then begin //员工输入使能 改成通用  //if Readchis('Model Para','Process')<>'' then begin      //jiashida专用！
        edt3.SetFocus;
      end else
        btn1.setfocus;     //取得光标，否则可能因为HID而得不到。
    end;
  finally
  end;
  tmr1.Enabled:=false;
end;

procedure TFormPN.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Form_line.tmr3.Enabled:=tmr3_statue;                  //恢复定时器3状态
  tmr1.Enabled:=False;
  if Form_line.CLose_1<=1 then Form_line.CLose_1:=0;     //没有输入而直接关闭，则退出

end;

procedure TFormPN.edt1Enter(Sender: TObject);
begin
 // edt2.SetFocus;
end;

procedure TFormPN.edt2Enter(Sender: TObject);
begin
 // edt3.SetFocus;
end;

procedure TFormPN.edt3Enter(Sender: TObject);
begin
  //edt4.SetFocus;
end;

procedure TFormPN.edt4Enter(Sender: TObject);
begin
  //btn1.SetFocus;
end;

procedure TFormPN.cbb1Change(Sender: TObject);
var
    nrf_dat_config: Tinifile;
    ini_path: string;
begin
  try
    ini_path := ExtractFilePath(application.ExeName) + 'config.ini';
    nrf_dat_config := Tinifile.Create(ini_path);

    if (nrf_dat_config.Readstring(FormPN.cbb1.Text, 'OrderMaxLen', '')<>'')  then
    begin
      FormPN.edt6.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'OrderMaxLen', '') ;
      FormPN.edt2.MaxLength:=0;
    end;
    if (nrf_dat_config.Readstring(FormPN.cbb1.Text, 'OrderMinLen', '')<>'')  then
    begin
      FormPN.edt7.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'OrderMinLen', '') ;
    end;

    if nrf_dat_config.Readstring(FormPN.cbb1.Text, 'OrderCharPos', '')='999' then  begin
      edt13.Enabled:=True;
      edt13.Visible:=true;
      if Readchis('Model Para', 'LoginItem4')=''  then lbl4.Caption:='Date+Lot';
      cbb2.Visible:=False;
    end else begin
      edt13.Enabled:=False;
      edt13.Visible:=False;
      if Readchis('Model Para', 'LoginItem4')=''  then lbl4.Caption:='Fixture';
      cbb2.Visible:=true;
    end;


    if (nrf_dat_config.Readstring(FormPN.cbb1.Text, 'OperatorMaxLen', '')<>'')  then
    begin
      FormPN.edt8.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'OperatorMaxLen', '') ;
      FormPN.edt3.MaxLength:=0;
    end;
    if (nrf_dat_config.Readstring(FormPN.cbb1.Text, 'OperatorMinLen', '')<>'')  then
    begin
      FormPN.edt9.Text:=nrf_dat_config.Readstring(FormPN.cbb1.Text, 'OperatorMinLen', '') ;
    end;
  finally
    nrf_dat_config.free;
  end;
end;

procedure TFormPN.edt3KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=$0D then begin
    edt3.SelectAll;
   {数据库没有开启，此处有问题！ if (Readchis('server','EmployeeSelect')<>'') and ( StrToInt(Form_line.edt31.Text)=2 ) then    begin
       Form_line.unqryJSD.SQL.Text:=Readchis('server','EmployeeSelect')+'='+ QuotedStr(edt3.Text);

       Form_line.unqryJSD.open;
       Form_line.unqryJSD.Refresh;
       if Form_line.unqryJSD.IsEmpty  then MessageDlg('Employee ID is not exist!',mterror,[mbyes],0) else btn1Click(self);
    end else }
    btn1Click(self);
  end;
end;

procedure TFormPN.FormCreate(Sender: TObject);
begin
 self.Panel1.Visible:=False;
end;

procedure TFormPN.FormDblClick(Sender: TObject);
begin
  self.Panel1.Visible:=not( self.Panel1.Visible );
end;

end.
