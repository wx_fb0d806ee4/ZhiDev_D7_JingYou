{  描述：     交互通信专用函数文件；相当于数据链路层；
  文件名：	  Unit2_communication
  应用语言:   delphi
  版本：			V1.0
  苏州周至电子科技有限公司http://zhzhi.cn
  Copyright(C) Jin 2013   All rights reserved
  作者：Jin
  建立日期：2019-05-09
//=======重点===================================================================
//1.TIM1中断之 发送数据处理，；
//2.rxdprocess函数，汇总各类接口，处理接收的数据
//3.rxdMonitorUppLow含有freshDataLogFromDemo功能，头文件字符串需与文件生成CreateFile同步；
}

unit Unit2_Communication;

interface

uses
  Windows, Messages, SysUtils,Variants, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ActnList, NB30, registry, shellapi, dbclient, inifiles, db, adodb,

  iComponent, iVCLComponent, iCustomComponent, iPlotComponent, iPlot,
  IdUDPBase, IdUDPServer, IdBaseComponent, IdComponent, IdIPWatch, ScktComp,
  IdAntiFreezeBase, IdAntiFreeze,
  IdTCPConnection, IdTCPClient,
  IdSocketHandle,IdUDPClient,
  Menus, ToolWin, sScrollBox, iLed, iLedRound, sMemo, iLabel,
  iEditCustom, iEdit, iLedArrow, sLabel, iLedMatrix , RzLaunch,

  FileCtrl, StdCtrls,DateUtils,masks;

const
  tttttt:SmallInt =256;

Var

  tttt:SmallInt;
 //--------------按模式列表处理函数------------------------------------
  procedure DispUnitErrHint(tt:SmallInt;dispHint:SmallInt);
 //--------------debug交互 函数------------------------------------
  procedure rxdprocess(tt:Word;Port:SmallInt);
  procedure DebugWin_txdProcess(len:integer);         //在debug界面发送数据
  procedure AutoImportNextPN();
  procedure AutoExportNextPN(ss:SmallInt);
implementation

uses
  StepCopy,main,OSstudy,FindPoint,PNenter,Unit0_globalVariant
  ,Unit4_logFileCode,LoadFile,LogHeadTips;

procedure DispUnitErrHint(tt:SmallInt;dispHint:SmallInt);
begin
  Zu1edit[row_num].Hint:='';                             //row_num从1开始！
  Zu2edit[row_num].Hint:='';
  Zu3edit[row_num].Hint:='';
  Zu4edit[row_num].Hint:='';
  Zu5edit[row_num].Hint:='';
  Zu6edit[row_num].Hint:='';
  Zu7edit[row_num].Hint:='';
  Zu8edit[row_num].Hint:='';
  Zu19edit[row_num].Hint:='';
  Zu10edit[row_num].Hint:='';
  Zu11edit[row_num].Hint:='';
  Zu12edit[row_num].Hint:='';
  Zu13edit[row_num].Hint:='';
  Zu14edit[row_num].Hint:='';
  Zu15edit[row_num].Hint:='';
  Zu16edit[row_num].Hint:='';
  Zu17edit[row_num].Hint:='';
  Zu18edit[row_num].Hint:='';
  Zu19edit[row_num].Hint:='';

  if zu1edit[row_num].Text='' then begin
    Zu1edit[row_num].Hint:='没有连接上测控主板，无法在线调试！';
    Exit;
  end else if (StrToInt('0'+sysedit[5].Text)<600)and(zu1edit[row_num].Text='99') then begin       //FCT5固件V5xx固件的page只存放10个步骤
    Zu1edit[row_num].Hint:='测试程序结束！';
    Exit;
  end else if zu1edit[row_num].Text='9999' then begin                           //v600~800为过渡固件，v800以上为成熟固件！
    Zu1edit[row_num].Hint:='测试程序结束！';
    Exit;
  end else if (zu1edit[row_num].Text='0')or not IsNumberic(zu1edit[row_num].Text) then begin
    Zu1edit[row_num].Hint:='步骤关闭！';
  end else if (zu1edit[row_num].Text='1')and(row_num<>1) then begin
    Zu1edit[row_num].Hint:='表格步骤，选择执行其中一个！';
  end else if (StrToInt('0'+sysedit[5].Text)<600)and( ((StrToInt(zu1edit[row_num].Text)-1) mod 10)<>(row_num-1) mod 10 ) then begin   //FCT5固件page只存放10个步骤
    Zu1edit[row_num].Hint:='步骤【序号】异常，不执行！';
  end else if (StrToInt('0'+sysedit[5].Text)>600)and( ((StrToInt(zu1edit[row_num].Text)-1) mod VIRLCROWS)<>(row_num-1) mod VIRLCROWS ) then begin
    Zu1edit[row_num].Hint:='步骤【序号】异常，不执行！';
  end else begin
    Zu1edit[row_num].Hint:='步骤【序号】正常，可执行！';
  end;
  if Char(rbuf[7])='#' then begin
    Zu2edit[row_num].Hint:='【备注Notes】用作步骤名称，设定测量值单位='+Char(rbuf[6]);
  end else if Char(rbuf[7])='$' then begin
    Zu2edit[row_num].Hint:='执行到该步骤时，LCD屏显示【备注Notes】的内容，提示下一步操作！';
  end else if Char(rbuf[7])='@' then begin
    Zu2edit[row_num].Hint:='执行到该步骤时，上传PC（提示当前操作！）';
  end else begin
    Zu2edit[row_num].Hint:='步骤名称';
  end;
  if(zu3edit[row_num].Text='30002') then begin
    Zu3edit[row_num].Hint:='【上限%】【下限%】为有符号区间值！';
  end else if(zu3edit[row_num].Text='30005') then begin
    Zu3edit[row_num].Hint:='【上限%】【下限%】为无符号区间值！';
  end else if(zu3edit[row_num].Text='30004') then begin
    Zu3edit[row_num].Hint:='根据【测控值】跳转！';
  end else if(zu3edit[row_num].Text='30008') then begin
    Zu3edit[row_num].Hint:='判断结果始终OK！';
  end else if(zu3edit[row_num].Text='30009') then begin
    Zu3edit[row_num].Hint:='取前面步骤的【测控值】做判断标准！';
  end else if(zu3edit[row_num].Text='30010') then begin
    Zu3edit[row_num].Hint:='（测控值）为【上限】~【下限】之间的随机数！';
  end else if IsNumberic(zu3edit[row_num].Text)and(StrToInt(zu3edit[row_num].Text)>=30011) then begin
    Zu3edit[row_num].Hint:='取系统全局参数！';
  end else begin
    Zu3edit[row_num].Hint:='【上限%】【下限%】为百分数！';
  end;
  if(zu4edit[row_num].Text='30000') then begin
    Zu4edit[row_num].Hint:='测控值无穷大！';
  end else if(zu4edit[row_num].Text='30001') then begin
    Zu4edit[row_num].Hint:='执行失败！';
  end else if(zu4edit[row_num].Text='30003') then begin
    Zu4edit[row_num].Hint:='无效的步骤参数！';
  end else if(zu4edit[row_num].Text='30007') then begin
    Zu4edit[row_num].Hint:='没有执行校准步骤！';
  end else begin
    Zu4edit[row_num].Hint:='测控值';
  end;

  if Char(rbuf[24])='Z' then begin
    Zu9edit[row_num].Hint:='此步骤执行之前无延时!';
  end else begin
    Zu9edit[row_num].Hint:='此步骤执行之前的延时='+Zu9edit[row_num].Text+'mS';
  end;
//=======1.0===========K=？ 执行步骤解析---------------------------------------------------------
  if(zu11edit[row_num].Text='0') then begin
    Zu11edit[row_num].Hint:='单步调试时，不执行该步骤！';
  end else if(zu11edit[row_num].Text='1') then begin
    Zu11edit[row_num].Hint:='上电时执行该步骤！';
  end else if(zu11edit[row_num].Text='2') then begin
    Zu11edit[row_num].Hint:='复位时执行该步骤！';
  end else if(zu11edit[row_num].Text='3') then begin
    Zu11edit[row_num].Hint:='ready时执行该步骤！';
  end else if(zu11edit[row_num].Text='4') then begin
    Zu11edit[row_num].Hint:='测试异常时执行该步骤！';
  end else if(zu11edit[row_num].Text='5')or(zu11edit[1+ i mod VIRLCROWS ].Text='6') then begin
    if Char(rbuf[24])='Q' then
      Zu11edit[row_num].Hint:='仅使用DI5,DI6做启动信号时，执行该步骤';  //<Q模式>步骤，否则不执行该<Q模式>步骤！';
  end else begin
    Zu11edit[row_num].Hint:='微调比例系数！';
  end;
  if Char(rbuf[24])='Q' then begin
    if Char(rbuf[27])='N' then  begin
      if (Zu12edit[row_num].Text='0')or(Zu12edit[row_num].Text='1')then
        Zu12edit[row_num].Hint:='每块切换板支持的继电器个数=16'
      else
        Zu12edit[row_num].Hint:='每块切换板支持的继电器个数='+Zu12edit[row_num].Text;
    end;
  end else begin
    Zu12edit[row_num].Hint:='微调偏移！';
  end;

  Zu13edit[row_num].Hint:='多次测量取平均值，最大999次';

  if(zu4edit[row_num].Text='5000') then begin
    Zu14edit[row_num].Hint:='无条件跳转！';
  end else if(zu14edit[row_num].Text='8000') then begin
    Zu14edit[row_num].Hint:='任意步骤NG时跳转！';
  end else if(zu14edit[row_num].Text='9000') then begin
    Zu14edit[row_num].Hint:='开关板自检NG时跳转！';
  end else if IsNumberic(zu14edit[row_num].Text)and(StrToInt(zu14edit[row_num].Text)>8001)  then begin
    Zu14edit[row_num].Hint:='工位防呆！';
  end else if IsNumberic(zu14edit[row_num].Text)and(StrToInt(zu14edit[row_num].Text)>0)then  begin
    Zu14edit[row_num].Hint:='区分连片测试的单元号，最大999';
  end;
  Zu17edit[row_num].Hint:='自定义步骤备注';
  case Char(rbuf[24]) of       //模式主字符，显示出单位符号；

    'A','N':
        begin
          Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':电表模块通信功能';
          case Char(rbuf[27]) of
            'C':
                  begin
                    case Char(rbuf[25]) of
                      'I'..'J': begin
                            Zu10edit[row_num].Hint:=Char(rbuf[25])+':初始化'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':CAN初始化';
                           end;
                      'C'..'D': begin
                            Zu10edit[row_num].Hint:=Char(rbuf[25])+':发送'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':CAN发送'+Zu17edit[row_num].Text;
                           end;
                      'R': begin
                            Zu11edit[row_num].Hint:='超时时间='+Zu9edit[row_num].Text+'*'+Zu11edit[row_num].Text+'mS';
                            if Zu15edit[row_num].Text='0' then
                              Zu10edit[row_num].Hint:=Char(rbuf[25])+':接收'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':CAN接收数据！'
                            else
                              Zu10edit[row_num].Hint:=Char(rbuf[25])+':接收'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':CAN先发送'+Zu17edit[row_num].Text+',然后接收数据！';
                           end;
                    else
                       Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':CAN通信功能';
                    end;
                  end;
            'S':
                  begin
                    Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':SIM功能测试';
                  end;
            'L':
                  begin
                    Zu11edit[row_num].Hint:='超时时间='+Zu9edit[row_num].Text+'*'+Zu11edit[row_num].Text+'mS';
                    Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':LIN通信功能测试';
                  end;
            'H':
                  begin
                    Zu11edit[row_num].Hint:='超时时间='+Zu9edit[row_num].Text+'*'+Zu11edit[row_num].Text+'mS';
                    Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':专用tesla磁场强度测量';
                  end;
            'I','J':
                 case Char(rbuf[25]) of
                    '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uA';
                    '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mA';
                    '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' A';
                    '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kA';
                    '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MA';
                 end;
            'V':
                 case Char(rbuf[25]) of
                    '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uV';
                    '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mV';
                    '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' V';
                    '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kV';
                    '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MV';
                 end;
            'P':
                 case Char(rbuf[25]) of
                    '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'pA';
                    '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'nA';
                    '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uA';
                    '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mA';
                    '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' A';
                 end;
            'Q':
                 case Char(rbuf[25]) of
                    '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'pV';
                    '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'nV';
                    '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uV';
                    '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mV';
                    '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' V';
                 end;
            'T':
                 case Char(rbuf[25]) of
                    '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'u^C';
                    '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'m^C';
                    '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' ^C';
                    '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'k^C';
                    '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'M^C';
                 end;

          else  //u数量级起始
              {   case Char(rbuf[25]) of
                    '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'u';
                    '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'m';
                    '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' ';
                    '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'k';
                    '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'M';
                 end;   }
          end;
        end;
    'B':
        begin
          Zu11edit[row_num].Hint:='超时时间='+Zu9edit[row_num].Text+'*'+Zu11edit[row_num].Text+'mS';
          Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':与PC软件交互功能';
          case Char(rbuf[27]) of
            'R':begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':功能码'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':读取modbus协议仪表数据';
                  case Char(rbuf[25]) of
                    '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'n';
                    '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'u';
                    '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'m';
                    '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' ';
                    '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'k';
                  end;

                  if char(rbuf[7])='#' then begin
                    if(char(rbuf[6])>'!')and(char(rbuf[6])<='~') then begin
                      Zu18edit[row_num].Text:=Zu18edit[row_num].Text+char(rbuf[6]);
                      if(char(rbuf[9])>'!')and(char(rbuf[9])<='~') then begin
                        Zu18edit[row_num].Text:=Zu18edit[row_num].Text+char(rbuf[9]);
                        if(char(rbuf[8])>'!')and(char(rbuf[8])<='~') then begin
                          Zu18edit[row_num].Text:=Zu18edit[row_num].Text+char(rbuf[8]);
                        end
                      end
                    end
                  end;
                end;
            'P','Q','T':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':与PC软件交互';
                end;
            'A':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':通过串口输出定制数据';
                end;
            'U','V':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':上传测控数据给PC记录保存！';
                end;
            'G','F','2':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':配置条码设备、读取条码数据！';
                end;
            'N':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':专用-通过串口3读取拉力计数据';
                end;
            'E':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':专用-读取单线EEPROM数据';
                end;
            'S':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':专用-HR SENSOR传感器读写';
                end;
            'K','L':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':专用-开关板状态上传！';
                end;
            'W':            //专用！
                 case Char(rbuf[25]) of
                    '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'um';
                    '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mm';
                    '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' m';
                    '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'km';
                    '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'Mm';
                 end;

          else  //u数量级起始

          end;
        end;
    'C':
        begin
          Zu9edit[row_num].Hint:='充放电时间='+Zu9edit[row_num].Text+'mS';
          Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':大于50nF电容的测量';
          if Zu13edit[row_num].Text<>'0' then
            Zu13edit[row_num].Hint:='求平均次数='+inttostr(Word(rbuf[32]*256+rbuf[33])mod 1000);
          case Char(rbuf[25]) of
              'E':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'*10nF';
              'F':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'*100nF';
              '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'pF';
              '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'nF';
              '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uF';
              '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mF';
              '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' F';
          else
              Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mR';
          end;
        end;
    'D':
        begin
          Zu9edit[row_num].Hint:='充放电时间='+Zu9edit[row_num].Text+'mS';
          Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':压降、电压测量';
          if Char(rbuf[27])='0' then  Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+'(1mA恒流)';
          if Char(rbuf[27])='D' then  Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+'(0.1mA恒流)';
          case Char(rbuf[27]) of
            'W': begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':采集一段电压波形，求峰值电压';
                end;
            'S'..'T': begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':四端元件的电压测量';
                end;
            '1','2','D','F','G':
                 case Char(rbuf[25]) of
                    '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uV';
                    '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mV';
                    '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' V';
                    '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kV';
                    '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MV';
                 end;

          else  //u数量级起始
               Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mV';
          end;
        end;
    'F': begin
          Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A);
          case Char(rbuf[27]) of
            'A':
                begin
                  case Char(rbuf[25]) of
                     'C':
                        Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':A开关板放电';
                  end;
                end;
            'U':
                begin
                  Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':切换电压测量点（适用D型开关板）';
                end;
            'Q','V':
                begin
                  Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':D型开关板功能切换';
                end;
            '1'..'5':
                begin
                  Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':定时器输出PWM信号';
                end;
            'F':
                begin
                  Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':频率测量';
                  case Char(rbuf[25]) of
                    '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'Hz';
                    '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kHz';
                    '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MHz';
                    '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'GHz';
                    '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'THz';
                  end;
                end;
            'W':
                begin
                  Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':占空比测量';
                  case Char(rbuf[25]) of
                    '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'m%';
                    '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'%';
                    '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'k%';
                    '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'M%';
                    '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'G%';
                  end;
                end;
          else  //u数量级起始

          end;
        end;
   'G','2'..'6':
      begin
        if Char(rbuf[25])='C' then
          Zu10edit[row_num].Hint:=Char(rbuf[25])+':配置串口、仪器参数'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':仪器仪表交互'
        else
          Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':仪器仪表交互';
        if IsNumberic(zu13edit[row_num].Text)and(StrToInt(zu13edit[row_num].Text)div 10000 =1)then  begin
            Zu13edit[row_num].Hint:='单PCS机台专用';
        end;
        if IsNumberic(zu13edit[row_num].Text)and( (StrToInt(zu13edit[row_num].Text)div 1000)mod 10 =4)then  begin
            Zu13edit[row_num].Hint:=Zu13edit[row_num].Hint+'；测控板模拟数据';
        end;
        if StrToInt(Zu16edit[row_num].Text)>=10000 then
          Zu16edit[row_num].Hint:='选择通信端口='+Zu16edit[row_num].Text;
        Zu17edit[row_num].Hint:='SCPI仪表命令='+Zu17edit[row_num].Text;
        if (Char(rbuf[25])<'A')or(Char(rbuf[25])>'F') then
          Zu11edit[row_num].Hint:='超时时间='+Zu9edit[row_num].Text+'*'+Zu11edit[row_num].Text+'mS';
        case Char(rbuf[27]) of
          'G':
              begin
                Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':读取仪表数据，并缓存';
              end;
          'H','0'..'8':
              begin
                Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':高压线材测试仪器交互，可查看原始数据';
              end;
          'R':
               case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' R';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kR';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MR';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'GR';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'-';
               end;
          'F':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mR';
          'B','V':
               case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uV';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mV';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' V';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kV';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MV';
               end;
          'C':
               case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uA';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mA';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' A';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kA';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MA';
               end;
          'D':
               case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uW';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mW';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' W';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kW';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MW';
               end;
          'E':
               case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uJ';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mJ';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' J';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kJ';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MJ';
               end;
          'K':
               case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'pR';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'nR';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uR';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mR';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' R';
               end;
          'L':
               case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'pH';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'nH';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uH';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mH';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' H';
               end;
          'M':
               case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'pF';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'nF';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uF';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mF';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' F';
               end;
          'X':
               case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'p';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'n';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'u';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'m';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' ';
               end;
          'Y':
               case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'u';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'m';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' ';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'k';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'M';
               end;
          'Z':
               case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' ';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'k';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'M';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'G';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'T';
               end;

        else  //u数量级起始
            {   case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'u';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'m';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' ';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'k';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'M';
               end;   }
        end;
      end;
   'H'..'J':
      begin
        Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A);
        case Char(rbuf[27]) of
          'B':
              begin
                Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':单总线1wire芯片功能测试';
              end;
          'H':
              begin
                Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':定制功能';
              end;
          'P'..'W':
              begin
                Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':SPI芯片功能测试';
              end;
          'D':
              begin
                Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':定制测距芯片功能';
              end;
          'X':
              begin
                Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':I2C芯片功能测试';
              end;
        else
            Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':芯片通信功能';
        end;
      end;
    'K','O','S':
      begin
        Zu10edit[row_num].Hint:=Char(rbuf[25])+':阻值的数量级'+char($0A);
        case Char(rbuf[25]) of          //第1字符决定单位；
            '0':begin
                  if Zu4edit[row_num].Text='0' then
                    Zu18edit[row_num].Text:='ok *R'
                  else
                    Zu18edit[row_num].Text:='ng *R';
                  if Char(rbuf[24])='S' then begin
                    Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':短路测试';
                    Zu3edit[row_num].Hint:='任意两点之间阻值小于'+Zu3edit[row_num].text+'R(欧姆)时，测试ng!'
                  end else begin
                    Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':开路测试';
                    Zu3edit[row_num].Hint:='任意两点之间阻值大于'+Zu3edit[row_num].text+'R(欧姆)时，测试ng!'
                  end;
               end;
            '1': begin
                  if Zu4edit[row_num].Text='0' then
                    Zu18edit[row_num].Text:='ok *0.01kR'
                  else
                    Zu18edit[row_num].Text:='ng *0.01kR';
                  if Char(rbuf[24])='S' then begin
                    Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':短路测试';
                    Zu3edit[row_num].Hint:='任意两点之间阻值小于'+Zu3edit[row_num].text+'*0.01kR(欧姆)时，测试ng!'
                  end else begin
                    Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':开路测试';
                    Zu3edit[row_num].Hint:='任意两点之间阻值大于'+Zu3edit[row_num].text+'*0.01kR(欧姆)时，测试ng!'
                  end;
               end;
            '2': begin
                  if Zu4edit[row_num].Text='0' then
                    Zu18edit[row_num].Text:='ok *0.1kR'
                  else
                    Zu18edit[row_num].Text:='ng *0.1kR';
                  if Char(rbuf[24])='S' then begin
                    Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':短路测试';
                    Zu3edit[row_num].Hint:='任意两点之间阻值小于'+Zu3edit[row_num].text+'0.1kR(欧姆)时，测试ng!'
                  end else begin
                    Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':开路测试';
                    Zu3edit[row_num].Hint:='任意两点之间阻值大于'+Zu3edit[row_num].text+'0.1kR(欧姆)时，测试ng!'
                  end;
               end;
            '3': begin
                  if Zu4edit[row_num].Text='0' then
                    Zu18edit[row_num].Text:='ok *0.001MR'
                  else
                    Zu18edit[row_num].Text:='ng *0.001MR';
                  if Char(rbuf[24])='S' then begin
                    Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':短路测试';
                    Zu3edit[row_num].Hint:='任意两点之间阻值小于'+Zu3edit[row_num].text+'MR(欧姆)时，测试ng!'
                  end else begin
                    Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':开路测试';
                    Zu3edit[row_num].Hint:='任意两点之间阻值大于'+Zu3edit[row_num].text+'MR(欧姆)时，测试ng!'
                  end;
               end;
        else
               if Zu4edit[row_num].Text='0' then
                  Zu18edit[row_num].Text:='ok *R'
               else
                  Zu18edit[row_num].Text:='ng *R';
        end;
      end;

    'L':
        case Char(rbuf[27]) of
          'R','G','B','W':
              begin
                Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':LED灯颜色、亮度测量';
                case Char(rbuf[25]) of
                  'K':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'%';
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uLux';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mLux';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' Lux';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kLux';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MLux';
                end;
              end;
           '1','2':
              begin
                Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':三极管、光耦等多端被动元件测量';
                case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' R';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kR';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MR';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'GR';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'TR';
                end;
              end;
        else  //u数量级起始

        end;
    'P':
        case Char(rbuf[27]) of
          'F':
              begin
                Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':光栅尺数据读取，最小单位词头1';
                case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'k';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'M';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'G';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'T';
                end;
                if char(rbuf[7])='#' then begin
                  Zu18edit[row_num].Text:=Zu18edit[row_num].Text+char(rbuf[6]);
                end;
              end;
          'G':
              begin
                Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':光栅尺数据读取，最小单位词头u';
                case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'u';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'m';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' ';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'k';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'M';
                end;
                if char(rbuf[7])='#' then begin
                  Zu18edit[row_num].Text:=Zu18edit[row_num].Text+char(rbuf[6]);
                end else
                  Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'m';
              end;
        else begin //1数量级起始
              Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':马达转速测量';
               case Char(rbuf[25]) of

                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'RPM';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kRPM';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MRPM';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'GRPM';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'TRMP';
               end;
            end;
        end;
   'Q':
      begin
          if (Char(rbuf[25])='1') then
            Zu10edit[row_num].Hint:=Char(rbuf[25])+':开启'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':输出控制'
          else if (Char(rbuf[25])='0') then
            Zu10edit[row_num].Hint:=Char(rbuf[25])+':关闭'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':输出控制'
          else
            Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':输出控制';
        case Char(rbuf[27]) of
          'G':  begin
                  Zu7edit[row_num].Hint:='结果NG时,开启输出DO'+Zu7edit[row_num].text;
                  Zu8edit[row_num].Hint:='结果OK时,开启输出DO'+Zu8edit[row_num].text;
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':根据【连片序号】有选择输出控制';
                end;
          'P':  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':步进马达控制';
          'N':  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':（可级联）继电器切换板控制';
        end;
      end;
    'R':
      begin
        Zu10edit[row_num].Hint:=Char(rbuf[25])+':测量值单位的数量级'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':电阻阻值测量';
        case Char(rbuf[27]) of
          'V':
            begin
              Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':桥式压力传感器输出电压测量';
               case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uV';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mV';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' V';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kV';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MV';
               end;
            end;
          'E','R':
               case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uR';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mR';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' R';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kR';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MR';
               end;
          'H':
            begin
              Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':四线电阻阻值测量';
               case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mR';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' R';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kR';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MR';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'GR';
               else
                          
                  if IsNumberic(Zu18edit[row_num].Text) then  begin
                    if strtoint(Zu18edit[row_num].Text)<30000 then
                      Zu18edit[row_num].Text:=copy(FormatFloat('0.000',strtofloat(Zu18edit[row_num].Text)/1000),0,7)+' R'
                    else
                      Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' R';
                  end else
                    Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' R';
                  //Zu18edit[row_num].Text:=copy(FormatFloat('0.000',strtofloat(Zu18edit[row_num].Text)/1000),0,7)+' R';
               end;
            end;
        else  //u数量级起始
               case Char(rbuf[25]) of
                  'F','H':if IsNumberic(Zu18edit[row_num].Text) then  begin
                            if strtoint(Zu18edit[row_num].Text)<30000 then
                              Zu18edit[row_num].Text:=copy(FormatFloat('0.000',strtofloat(Zu18edit[row_num].Text)/1000),0,7)+' R'
                            else
                              Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' R';
                          end else
                            Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' R';
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' R';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kR';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MR';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'GR';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'TR';
               else
                  Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'R';
               end;

        end;
      end;
   'T':
      begin
        Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':MIC灵敏度校准';
        case Char(rbuf[27]) of
          'D':
              begin
                Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':MIC的THD值测量';
              end;
          'M','N','H','Q':
              begin
                Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':MIC灵敏度测量';
                Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'dB';
              end;
          //,'K','P'   校准，不显示
          'C':
              begin
                Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':定制pF电容测量';
                case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'fF';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'pF';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'nF';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uF';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mF';
                end;
              end;
          'R':
              begin
                Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':定制阻抗测量';
                case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mR';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' R';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kR';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MR';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'GR';
                end;
              end;
        else  //u数量级起始
                     
        end;
      end;
   'U'..'V':
        case Char(rbuf[27]) of
          'B','C':
              begin
                Zu10edit[row_num].Hint:=Char(rbuf[25])+':测量值单位数量级'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':小于50nF电容测量';
                case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'fF';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'pF';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'nF';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uF';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mF';
                end;
              end;
          'T':
              begin
                Zu10edit[row_num].Hint:=Char(rbuf[25])+':测量值单位数量级'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':时序测量';
                case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uS';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mS';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' S';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kS';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MS';
                end;
              end;
          'I',   //逐渐不用
          'J':
              begin
                Zu10edit[row_num].Hint:=Char(rbuf[25])+':测量值单位数量级'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':使用采样电阻测电流';
                case Char(rbuf[25]) of
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uA';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mA';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' A';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kA';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MA';
                end;
              end;
          'V','U','Z':
              begin
                Zu10edit[row_num].Hint:=Char(rbuf[25])+':测量值单位数量级'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':电阻分压的测量';
                case Char(rbuf[25]) of
                  'G','D':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'*0.1o';
                  'E':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'*10mR';    //不能写成ohm
                  'H','F':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mR';
                  '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uV';
                  '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'mV';
                  '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' V';
                  '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'kV';
                  '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'MV';
                else
                  Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'uV';
                end;
             end;
          'D','F','G','L':
              begin
                Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':多端测量时，辅助电高低电平设置';
              end;
          'X','Y':
              begin
                Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':多通道电压波形采集';
              end;
        else  //u数量级起始

        end;
   'W':
        begin

          Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':输入信号满足条件时等待';
          if (Char(rbuf[25])='1')or(Char(rbuf[25])='H') then
            Zu10edit[row_num].Hint:=Char(rbuf[25])+':高电平、开路时等待'+char($0A)+ Zu10edit[row_num].Hint;
          if (Char(rbuf[25])='0')or(Char(rbuf[25])='L') then
            Zu10edit[row_num].Hint:=Char(rbuf[25])+':低电平、短路时等待'+char($0A)+ Zu10edit[row_num].Hint;
          if Zu9edit[row_num].Text<>'0' then
            Zu11edit[row_num].Hint:='超时时间='+Zu9edit[row_num].Text+'*'+Zu11edit[row_num].Text+'mS'
          else
            Zu11edit[row_num].Hint:='死等待';
          Zu16edit[row_num].Hint:='子功能';
          case Char(rbuf[27]) of
            'J':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':输入信号满足条件跳转！';
                  if (Char(rbuf[25])='1')or(Char(rbuf[25])='H') then
                    Zu10edit[row_num].Hint:=Char(rbuf[25])+':高电平、开路时跳转'+char($0A)+ Zu10edit[row_num].Hint;
                  if (Char(rbuf[25])='0')or(Char(rbuf[25])='L') then
                    Zu10edit[row_num].Hint:=Char(rbuf[25])+':低电平、短路时跳转'+char($0A)+ Zu10edit[row_num].Hint;
                  Zu11edit[row_num].Hint:='';
                  Zu3edit[row_num].hint:='';
                  Zu5edit[row_num].hint:='';
                  Zu6edit[row_num].hint:='';
                  if Zu3edit[row_num].Text<>'0' then
                    Zu3edit[row_num].hint:='跳转到第'+Zu3edit[row_num].Text+'步骤'
                  else  if Zu5edit[row_num].Text<>'0' then
                    Zu5edit[row_num].hint:='向后跳转'+Zu5edit[row_num].Text+'个步骤'
                  else  if Zu6edit[row_num].Text<>'0' then
                    Zu6edit[row_num].hint:='向前跳转'+Zu6edit[row_num].Text+'个步骤';

                end;
            'P':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':到位信号等待，配合QP步进电机控制模式使用！';
                end;
          else
          end;
        end;
    'X'..'Y':               //IJ读寄存器，没有单位
        begin
          Zu10edit[row_num].Hint:=Char(rbuf[25])+':测量值单位数量级'+char($0A);
          case Char(rbuf[27]) of
            'T':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':待机时，增加延时时间';
                end;
            'H':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':空'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':根据指定(测控值)做运算判断,按16进制运算显示';
                  Zu18edit[row_num].Text:='0x'+inttohex((Word(rbuf[12]*256+rbuf[13])),2);
                end;
            'P':
                begin
                  Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':根据指定(测控值)做运算判断,单位最小词头p';
                  case Char(rbuf[25]) of
                    '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'p';
                    '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'n';
                    '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'u';
                    '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'m';
                    '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'';
                  end;
                end;
            'Y':
                begin
                  Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':根据指定(测控值)做运算判断,单位最小词头1';
                  case Char(rbuf[25]) of
                       '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' ';
                       '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'k';
                       '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'M';
                       '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'G';
                       '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'T';
                  end;
                end;
            'Z':

                if Char(rbuf[24])='Y' then begin
                  Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':根据指定(测控值)做运算判断,单位最小词头1';
                   case Char(rbuf[25]) of
                      '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' ';
                      '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'k';
                      '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'M';
                      '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'G';
                      '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'T';
                   end
                end else begin
                  Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':根据指定(测控值)做运算判断,单位最小词头m';
                  case Char(rbuf[25]) of
                    '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'m';
                    '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' ';
                    '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'k';
                    '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'M';
                    '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'G';
                  end;
                end;
            'M':
                begin
                   Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':根据指定(测控值)做运算判断,单位最小词头M';
                   case Char(rbuf[25]) of
                      '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'M';
                      '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'G';
                      '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'T';
                      '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'P';
                      '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'E';
                   end;
                end;
            'G','J':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':用于NTC自适应温度列表的测量！';
                  Zu11edit[row_num].Hint:='';
                end;
            '1':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':组编号'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':设置全局参数组的参数！';
                end;
            'X':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':组编号'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':读取全局参数组的参数！';
                end;
            'L':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':根据指定(测控值)做逻辑运算并判断！';
                end;
            'A':
                begin
                  Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':指定两步骤的测控值，相加运算！,单位最小词头u';
                  case Char(rbuf[25]) of
                    '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'u';
                    '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'m';
                    '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' ';
                    '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'k';
                    '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'M';
                  end;
                end;
            'S':
                begin
                  Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':指定两步骤的测控值，相减运算！,单位最小词头u';
                  case Char(rbuf[25]) of
                    '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'u';
                    '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'m';
                    '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' ';
                    '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'k';
                    '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'M';
                  end;
                end;
          else   //u数量级起始
                if char(rbuf[7])='#' then begin
                  Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':根据指定(测控值)做运算判断,单位最小词头:u';
                  case Char(rbuf[25]) of
                    '0':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'u';
                    '1'..'2':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'m';
                    '3'..'5':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+' ';
                    '6'..'8':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'k';
                    '9':Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'M';
                  end;
                end else begin
                  Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+Char(rbuf[24])+Char(rbuf[27])+':根据指定(测控值)做运算判断';
                end;
          end;
          if char(rbuf[7])='#' then begin
            if(char(rbuf[6])>'!')and(char(rbuf[6])<='~') then begin
              Zu18edit[row_num].Text:=Zu18edit[row_num].Text+char(rbuf[6]);
              if(char(rbuf[9])>'!')and(char(rbuf[9])<='~') then begin
                Zu18edit[row_num].Text:=Zu18edit[row_num].Text+char(rbuf[9]);
                if(char(rbuf[8])>'!')and(char(rbuf[8])<='~') then begin
                  Zu18edit[row_num].Text:=Zu18edit[row_num].Text+char(rbuf[8]);
                end
              end
            end
          end;
        end;
   'Z':
        begin
          Zu10edit[row_num].Hint:=Char(rbuf[24])+Char(rbuf[27])+':系统通用模式';
          Zu11edit[row_num].Hint:='无';
          case Char(rbuf[27]) of
            '1':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':组编号'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':系统全局变量组';
                end;
            'Z':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':主动初始化变量！';
                end;
            'H':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':系统注册！';
                end;
            'U':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':硬件串口编号'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':测控板串口参数配置！';
                end;
            'A':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':自检、找点参数配置！';
                end;
            'B':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':蜂鸣器、程控电压配置！';
                end;
            'C':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':LCD显示方式配置！';
                end;
            'D':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':组编号'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':辅助参数组配置！';
                end;
            'O'..'P':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':开短路底层参数配置！';
                end;
            'E':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':急停、光栅启用配置！';
                end;
            'V':
                begin
                  case Char(rbuf[25]) of
                    'A','V':begin
                              if row_num<>1 then begin
                                Zu10edit[row_num].Hint:=Char(rbuf[25])+':无效！请移动到第1步骤'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':测控系统参数配置！';
                              end else begin
                                Zu10edit[row_num].Hint:=Char(rbuf[25])+':基础功能！'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':测控系统参数配置！';
                                Zu13edit[row_num].Hint:='系统功能分支';
                                case Word(rbuf[32]*256+rbuf[33]) of
                                  0,10: begin
                                          Zu13edit[row_num].Hint:=Zu13edit[row_num].Hint+':设置LCD显示方式和单启动信号';
                                          Zu12edit[row_num].Hint:='设置LCD显示方式';
                                          Zu15edit[row_num].Hint:='设置启动后延时时间';
                                          Zu16edit[row_num].Hint:='设置启动信号滤波时间';
                                          Zu17edit[row_num].Hint:='设置机种名PN='+Zu17edit[row_num].Text;
                                        end;
                                  22,23: begin
                                          Zu13edit[row_num].Hint:=Zu13edit[row_num].Hint+':设置DX1\DX2做双启动信号';
                                          Zu16edit[row_num].Hint:='双启动信号允许最大时间差（mS）';
                                        end;
                                  81: begin
                                          Zu13edit[row_num].Hint:=Zu13edit[row_num].Hint+':设置DX1..DX6,9985,9986都可做启动信号';
                                          Zu16edit[row_num].Hint:='双启动信号允许最大时间差（mS）';
                                      end;
                                  133: begin
                                          Zu13edit[row_num].Hint:=Zu13edit[row_num].Hint+':设置9999，9998，9985，9986信号做上电启动信号';
                                      end;
                                end;
                              end;
                            end;
                    'B':begin
                            if row_num=2 then begin
                              Zu10edit[row_num].Hint:=Char(rbuf[25])+':设置条码规则！'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':测控系统参数配置！';
                              Zu14edit[row_num].Hint:='条码规则1:条码长度';
                              Zu15edit[row_num].Hint:='条码规则2:起始字符位置';
                              Zu17edit[row_num].Hint:='条码规则2:匹配字符';

                            end else if (row_num>2)and(row_num<9) then begin
                              Zu10edit[row_num].Hint:=Char(rbuf[25])+':标准板条码！'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':测控系统参数配置！';
                              Zu17edit[row_num].Hint:='标准板条码='+Zu17edit[row_num].Text;
                            end else begin
                                Zu10edit[row_num].Hint:=Char(rbuf[25])+':无效！请移动到第2~8步骤'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':测控系统参数配置！';
                            end;
                        END;
                    '0','1':begin
                              Zu10edit[row_num].Hint:=Char(rbuf[25])+':信号、连片指示功能！'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':测控系统参数配置！';
                        end;
                    '2','3':begin
                              Zu10edit[row_num].Hint:=Char(rbuf[25])+':从机连片指示灯功能！'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':测控系统参数配置！';
                        END;
                  else
                    Zu10edit[row_num].Hint:=Char(rbuf[25])+':无！'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':测控系统其它参数配置！';
                  end;
                end;
            'J'..'K':
                begin
                  Zu10edit[row_num].Hint:=Char(rbuf[25])+':子功能'+char($0A)+Char(rbuf[24])+Char(rbuf[27])+':根据系统状态跳转！';
                  Zu3edit[row_num].hint:='';
                  Zu5edit[row_num].hint:='';
                  Zu6edit[row_num].hint:='';
                  if Zu3edit[row_num].Text<>'0' then
                    Zu3edit[row_num].hint:='跳转到第'+Zu3edit[row_num].Text+'步骤'
                  else  if Zu5edit[row_num].Text<>'0' then
                    Zu5edit[row_num].hint:='向后跳转'+Zu5edit[row_num].Text+'个步骤'
                  else  if Zu6edit[row_num].Text<>'0' then
                    Zu6edit[row_num].hint:='向前跳转'+Zu6edit[row_num].Text+'个步骤';
                end;
          else
          end;
        end;

  else
  end;
 //   if (StrToInt('0'+sysedit[5].Text)>600)and(StrToInt(zu13edit[row_num)div 1000=6) then begin
 //     mmo27.Text:=mmo27.Text+('使用{系统参数组[6]}修正（测控值）！');
 //   end;
 //  case Char(rbuf[25]) of
 //     '0'..'9': Zu10edit[row_num].Hint:=Char(rbuf[25])+':根据系统状态跳转！'+char($0A)+Zu10edit[row_num].Hint:;
 //  end;

  Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+char($0A)+Char(rbuf[26])+':';
  if(Char(rbuf[26])='0') then  begin
     Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+'空！'+char($0A);
  end;
  if(Char(rbuf[26])='1')or(Char(rbuf[26])='3')or(Char(rbuf[26])='5')or(Char(rbuf[26])='7')or(Char(rbuf[26])='9') then  begin
    Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+'测控值可显示、查看、上传!'+char($0A);
  end;
  if(Char(rbuf[26])='2')or(Char(rbuf[26])='3')or(Char(rbuf[26])='6')or(Char(rbuf[26])='7') then  begin
     Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+'执行步骤前，LCD有显示参数！'+char($0A);
  end;
  if(Char(rbuf[26])='4')or(Char(rbuf[26])='5')or(Char(rbuf[26])='6')or(Char(rbuf[26])='7') then  begin
     Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+'把（测量值）在LCD结果界面上直接显示（透显）！'+char($0A);
  end;
  if(Char(rbuf[26])='8')or(Char(rbuf[26])='9') then  begin
    if(Char(rbuf[24])='O')OR(Char(rbuf[24])='K')OR(Char(rbuf[24])='S') then begin
      Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+'O/S不计入良率！'+char($0A);
    end else
      Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+'波形数据(ADC数据或者I2C)上传一次！'+char($0A);
    Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+'所有连片结果通过U4输出一次！'+char($0A);
  end;
  if(Char(rbuf[26])='A') then  begin
    if(Char(rbuf[27])='T')and( (Char(rbuf[24])='U')or(Char(rbuf[24])='V') ) then
     Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+'利用已采集到的波形计算时序差值！执行步骤时：不显示参数（节省时间），可查看、上传测量数据！'+char($0A)
    else
     Zu10edit[row_num].Hint:=Zu10edit[row_num].Hint+'执行步骤时：不显示参数（节省时间），可查看、上传测量数据！'+char($0A);
  end;

end;


procedure AutoImportNextPN();          //自动导入下一个机种的程序文件；
var
  f: TextFile;
  save_file_name: string;
  TxtFile:TStringList;
begin
  with Form_line do begin
    lbl28.Caption:='Start Import Data to MCU';
    lbl28.Font.Color:=clRed;
    sprgrsbr1.Position:=0;

 
    if fileexists(spthdlg1.Path+'\' +inttostr(a1.tag+1)+'.txt') then
      save_file_name :=spthdlg1.Path+'\' +inttostr(a1.tag+1)+'.txt'
    else if fileexists(spthdlg1.Path+'\' +'0.txt') then begin
      save_file_name :=spthdlg1.Path+'\' +'0.txt';
    end else begin
      lbl29.Caption:='Import error!文件不存在！';
      Exit;
    end;
    TxtFile:=TStringList.Create;
    TxtFile.LoadFromFile(save_file_name);
    sprgrsbr1.Max:=1+TxtFile.Count;
    sprgrsbr1.Position:=0;
    FormLoad.sprgrsbr1.Max:=1+TxtFile.Count;
    FormLoad.sprgrsbr1.Position:=0;
    TxtFile.Destroy;
    lbl29.Caption:=save_file_name;
    try
      assignfile(f, save_file_name);
      //rewrite(f);
      Reset(f);
      //application.ProcessMessages;
      modbusfuntion:=$4F;
    finally
      Closefile(f);
    end;
  end;
end;
procedure AutoExportNextPN(ss:SmallInt);
var
  f: TextFile;
  save_file_name,SLINE_head,s: string;
begin
  with Form_line do begin
    if (StrToInt('0'+sysedit[5].Text)=0) or (working >0) then      //没有读到版本号，不执行
      begin
        if(working >0) then  lbl28.Caption:='MCU is busying, Pls press RESET!请按复位再导出'
        else lbl28.Caption:='Pls retry to click!请重新上电，重启软件试一次！';
      end
    else begin
      lbl28.Caption:='Start Export Data to PC';
      lbl28.Font.Color:=clRed;
      if btn24.Tag >100 then begin                    //读取到最大步骤数
        sprgrsbr1.Max:= (btn24.Tag*100) div 90;
      end else
        sprgrsbr1.Max:=100;
      sprgrsbr1.Position:=0;
      if ss=1 then begin
        save_file_name :=ExtractFilePath(application.ExeName) +edt77.text+'-'+edt78.text+'-'+edt79.text+'-'+edt80.text+'.txt';
      end else
        save_file_name :=spthdlg1.Path+'\' +inttostr(a1.tag+1)+'.txt';
      lbl29.Caption:=save_file_name;
      try
        assignfile(f, save_file_name);
        rewrite(f); Sleep(50);
        //application.ProcessMessages;
        SLINE_head:='';
        s := '序号N';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '0'+sysedit[5].Text;//改为版本号<-'名称'  ,导入文件时会判断，不能含字母
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
         s := '标准值V';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '测控值S';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '上限Up%';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '下限Lo%';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '高点H';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '低点L';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '前延时T';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '模式M';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
         s := '比例K%';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '偏移B';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '平均P次';//上限%2';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '联板号L';//下限%2';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
         s := '高点H2';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '低点L2';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := 'Notes备注 >>>';    //zu17edit
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;


        writeln(f, SLINE_head);
        modbusfuntion:=$3F;
      finally
        Closefile(f);
      end;
    end;
  end
end;

procedure DebugWin_txdProcess(len:integer);         //在debug界面发送数据
var temp8 :byte;
    save_file_name,SLINE_head,str:string;  //,s
    Ansi_s,AnsiSLINE_head:String;
    strs:TStrings;
    i,j,temp16,line:Integer;


  procedure txdExportProcess();
  //var
   // i,j,temp16,line:Integer;
  begin
    with Form_line do begin
       if row_num=(VIRLCROWS+1) then row_num:=0;
       if (StrToInt('0'+sysedit[5].Text)<630) then
       begin
          if (row_num>=10) and (row_num<VIRLCROWS) then row_num:=VIRLCROWS;
       end;
       //sbuf[1]:=1;
       if (row_num<VIRLCROWS) then begin
          tmr1.Interval:=200;
          if (StrToInt('0'+sysedit[5].Text)>=600) then tmr1.Interval:=150+strtoint(edt14.Text);
       //   if Assigned(CurrentDevice) then   tmr1.Interval:=50+strtoint(edt15.Text);
          sbuf[2]:=3;
          sbuf[3]:=$30+($20*row_num)div 256;
          sbuf[4]:=($20*row_num)mod 256;
          sbuf[5]:=$00;
          sbuf[6]:=$1A;     ////ZU17EDIT  2017-2-14之前为$12;
          send_len:=6;
          lbl28.Caption:='Export Steps '+inttostr(row_num)+'......';
          if sprgrsbr1.Position<(sprgrsbr1.Max*90 div 100) then
            sprgrsbr1.Position:=sprgrsbr1.Position+1  ;
       end else if(row_num=VIRLCROWS) then    //翻页
          begin
          tmr1.Interval:=400;      //早期V6.30以前处理EEPROM大于500mS，此处小于500是只有eeprom读，可用读步骤和系统参数补偿。
          if (StrToInt('0'+sysedit[5].Text)>=630)  then
          begin
            if (StrToInt('0'+sysedit[5].Text)>=600) then tmr1.Interval:=150+strtoint(edt14.Text);
            //if Assigned(CurrentDevice) then   tmr1.Interval:=50+strtoint(edt15.Text);
          end;
          sbuf[2]:=5;
          sbuf[3]:=$00;
          sbuf[4]:=$0E;
          sbuf[5]:=$FF;
          sbuf[6]:=$00;
          send_len:=6;
          lbl28.Caption:='Next Page......';
        //  row_num:=0;
          end
       else if(row_num<(11+VIRLCOFFSET)) then    //空闲
          begin

          end
       else if(row_num<(20+VIRLCOFFSET)) then    //19 导出 系统参数
          begin
          sbuf[2]:=3;
          sbuf[3]:=$10;
          sbuf[4]:=$A0;
          sbuf[5]:=$00;
          sbuf[6]:=$18;
          send_len:=6;
          end
       else if(row_num<(SHORTPINS+20+VIRLCOFFSET)) then    //20 短路群 52对应255点二次开短路
       begin
        tmr1.Interval:=200;
        if (StrToInt('0'+sysedit[5].Text)>=600) then tmr1.Interval:=150+strtoint(edt14.Text);
        //if Assigned(CurrentDevice) then   tmr1.Interval:=50+strtoint(edt15.Text);
        sbuf[2]:=3;
        if IsNumberic(sysedit[1].Text) and (StrToInt(sysedit[1].Text)>=768+1+768) then begin     //16倍密度
          sbuf[3]:=$24+((row_num-20-VIRLCOFFSET)) div 256;
          sbuf[4]:=$00+((row_num-20-VIRLCOFFSET)) mod 256;
        end else begin
          sbuf[3]:=$18+((row_num-20-VIRLCOFFSET)*16) div 256;
          sbuf[4]:=$00+((row_num-20-VIRLCOFFSET)*16) mod 256;
        end;
        sbuf[5]:=$00;
        sbuf[6]:=$10;
        send_len:=6;
        lbl28.Caption:='Export OS Lines-'+inttostr(row_num-20-VIRLCOFFSET);
        sprgrsbr1.Position:=sprgrsbr1.max*95 div 100;
       end;
       try
          send_crcdata(send_len,2);
      //          nsend:=nsend+1;
      //          fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
       except
          messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
       end;
      tmr2.Tag:=1;  //接收到数据再加1//row_num:=row_num+1;
    end;
  end;
  procedure txdMonitorProcess();
  var
    i,j,temp16,line:Integer;

  begin
    with Form_line do begin
      if(modbusfun16int=0) and (modbusfun02=0)  then for i:=2 to 64 do  sbuf[i] :=0;    //16int和fun02功能码不能清0
      tmr1.Interval:=200;
      if (StrToInt('0'+sysedit[5].Text)>=600) then tmr1.Interval:=150+strtoint(edt14.Text);
      //if Assigned(CurrentDevice) then       //usbb通信
      begin
        usb_busyT:=500 div 5;
        tmr1.Interval:=10+strtoint(edt15.Text);
      end;
      if(modbusfun16=$FFFF) then begin      //设置USB调试空闲命令<--
         tmr1.Interval:=5;                 //验证值越大反应越慢！
         usb_busyT:=50 div 5;              //验证：测试板透传要保证间隔20mS以上超时；
         modbusfun16:=0;
      end else if(modbusfun16>0) then       //字符串下载     //addToLog('修改参数：0x'+inttohex(modbusfun16,4 )+':'+modbusfun16str );
          begin
              
              //sbuf[1]:=1;
              sbuf[2]:=16;
              sbuf[3]:=modbusfun16 div 256;
              sbuf[4]:=modbusfun16 mod 256;
              sbuf[5]:=modbusfun16len div 256;
              sbuf[6]:=modbusfun16len mod 256;
              sbuf[7]:=modbusfun16len*2;
              for i:=8 to 64 do sbuf[i]:=0;
              if (modbusfun16len=2)or(modbusfun16len=3) then      //Name或者Mode,高低字节对调
              begin
                sbuf[8]:=byte(' '); sbuf[9]:=byte(' ');sbuf[10]:=byte(' ');
                sbuf[11]:=byte(' ');sbuf[12]:=byte(' ');sbuf[13]:=byte(' ');
              end;
              Move(modbusfun16str[1],sbuf[8],Length(modbusfun16str));
              if modbusfun16=$C9 then begin   //pc透传数据
                sbuf[7]:=Length(modbusfun16str);
                send_len:=7+Length(modbusfun16str);
              end else begin
                if modbusfun16str='' then sbuf[8]:=byte('0');
                if(modbusfun16len=2)or(modbusfun16len=3)  then      //Name或者Mode,高低字节对调
                begin
                  temp8:=sbuf[8]; sbuf[8]:=sbuf[9];sbuf[9]:=temp8;
                  temp8:=sbuf[10]; sbuf[10]:=sbuf[11];sbuf[11]:=temp8;
                  temp8:=sbuf[12]; sbuf[12]:=sbuf[13];sbuf[13]:=temp8;
                end ;
             //   CopyMemory(PChar(sbuf^),modbusfun16str,Length(modbusfun16str));
                send_len:=7+modbusfun16len*2;
              end;
              try
                  send_crcdata(send_len,2);
            //    nsend:=nsend+1;
            //    fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
              except
                  messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
              end;
              row_num:=VIRLCROWS-2;
            //  if(StepNowFresh>0) then row_num:=StepNowFresh else row_num:=0;
            //  StepNowFresh:=0;
              btn16.Tag:=5;
              modbusfun16:=0;
              if N34.Checked then  modbusfun16:=$FFFF;
          end
      else if(modbusfun16int>0) then       //整数下载.sbuf内容已清零，可复用别的功能码
          begin
              //addToLog('修改参数：0x'+inttohex(modbusfun16int,4 )+':0x'+inttohex(modbusfun16len,4) );
              //sbuf[2]:=16;            //指令字节事先定义好
              sbuf[3]:=modbusfun16int div 256;
              sbuf[4]:=modbusfun16int mod 256;
              sbuf[5]:=modbusfun16len div 256;
              sbuf[6]:=modbusfun16len mod 256;
              sbuf[7]:=modbusfun16len*2;
              send_len:=7+modbusfun16len*2;
              if sbuf[2]=$03 then send_len:=6;   //查询 ,开机初始化
              try
                      send_crcdata(send_len,2);
            //          nsend:=nsend+1;
            //          fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
              except
                      messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
              end;
              row_num:=0;modbusfun16int:=0;
              if N34.Checked then  modbusfun16:=$FFFF;
          end
      else    if(modbusfun02>0) then       //02读状态
          begin
              //sbuf[1]:=1;
              sbuf[2]:=2;
              send_len:=6;
              try
                      send_crcdata(send_len,2);
            //          nsend:=nsend+1;
            //          fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
              except
                      messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
              end;
              row_num:=0;modbusfun02:=0;
              if N34.Checked then  modbusfun16:=$FFFF;
          end
      else if(modbusfun03>0) then       //03读取步骤
          begin
              //sbuf[1]:=1;
              sbuf[2]:=3;
              sbuf[3]:=modbusfun03 shr 8;
              sbuf[4]:=modbusfun03;
              sbuf[5]:=modbusfun03dat shr 8;      //编辑模式也是测试模式
              sbuf[6]:=modbusfun03dat;
              send_len:=6;
              try
                  send_crcdata(send_len,2);
            //          nsend:=nsend+1;
            //     fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
              except
                  messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
              end;
              row_num:=1;     //03功能吗接收数据可以处理！
              modbusfun03:=0;
              if N34.Checked then  modbusfun16:=$FFFF;
          end
      else if(modbusfun05>0) then       //05下载
          begin
              //addToLog('修改参数：0x'+inttohex(modbusfun05,4 )+':0x'+inttohex(modbusfun05dat) );
              //sbuf[1]:=1;
              sbuf[2]:=5;
              sbuf[3]:=$00;
              sbuf[4]:=modbusfun05;
              if(modbusfun05=3)then sbuf[4]:=0;      //编辑模式也是测试模式
              sbuf[5]:=modbusfun05dat;
              sbuf[6]:=$00;
              send_len:=6;
              try
                      send_crcdata(send_len,2);
            //          nsend:=nsend+1;
            //          fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
              except
                      messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
              end;
              row_num:=VIRLCROWS-2;//row_num:=0;
              modbusfun05:=0;
              if N34.Checked then  modbusfun16:=$FFFF;
          end
      else if(modbusfun06>0) then       //地址大于0，整数下载
          begin

              //sbuf[1]:=1;
              sbuf[2]:=6;
              sbuf[3]:=modbusfun06 div 256;
              sbuf[4]:=modbusfun06 mod 256;
              sbuf[5]:=modbusfun06dat shr 8;
              sbuf[6]:=modbusfun06dat mod 256;
              send_len:=6;
              try
                      send_crcdata(send_len,2);
            //          nsend:=nsend+1;
            //          fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
              except
                      messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
              end;

              if (Form_line.groupbox1.hint<>'5')and(modbusfun06=$01)and(modbusfun06dat=$00F6) then begin         //FCT5不支持加载和读取长文件名
                Label11.Font.Color:=clWindowText;Form_line.label11.Caption:='Loading...';  Form_line.Label11.Font.Size:=150;
              end;

              if (modbusfun06=$0041)and not A1.Checked and not n51.Checked and not n50.Checked then begin   //选机种，需要重新读取步骤名称，否则造成读取步骤名出错
                tmr3.Enabled:=true;                   //通过定时器执行不同的开机读取指令 ，以及Result界面显示
                if groupbox1.hint<>'5' then begin         //FCT5不支持加载和读取长文件名
                  // Label11.Font.Color:=clWindowText;label11.Caption:='Loading...'; Label11.Font.Size:=150;
                   Lbl169.Caption:='';lbl169.Font.Size:=10;
                end;
              end;
              row_num:=VIRLCROWS-2;
            //  if(StepNowFresh>0) then row_num:=StepNowFresh else row_num:=0;
            //  StepNowFresh:=0;
              btn16.Tag:=5;
              modbusfun06:=0;
              if N34.Checked then  modbusfun16:=$FFFF;
          end
      else   if not FormLoad.Showing then  begin           // 步骤更新显示
          if row_num>(VIRLCROWS+2) then begin
            row_num:=0;
          end;
          if (StrToInt('0'+sysedit[5].Text)<630) then      //老式主板
          begin
             if (row_num>=10) and (row_num<VIRLCROWS) then row_num:=VIRLCROWS;
          end;
          //sbuf[1]:=1;
          if btn16.Tag=5 then begin     //手动修改或 保存参数
              sbuf[2]:=1;
              sbuf[3]:=$00;
              sbuf[4]:=$00;
              sbuf[5]:=$00;
              sbuf[6]:=$80;   //2017-2-11之前为$10;
              send_len:=6;

              btn16.Tag:=10;        //通知接收函数处理；
          end else begin
            if (row_num<VIRLCROWS) then
              begin
              sbuf[2]:=3;
              sbuf[3]:=$30+($20*row_num)div 256;
              sbuf[4]:=($20*row_num)mod 256;
              sbuf[5]:=$00;
              sbuf[6]:=$1A;   //zu17edit最大1C  2017-2-14之前为$12;//0C;
              send_len:=6;
              end
            else if(row_num=VIRLCROWS) then    //系统参数
              begin
              sbuf[2]:=3;
              sbuf[3]:=$10;
              sbuf[4]:=$A0;
              sbuf[5]:=$00;
              sbuf[6]:=$1A;  ////ZU17EDIT最大1C  2017-2-14之前为$18
              send_len:=6;

              end
            else if(row_num=VIRLCROWS+1) then    //读状态标志，保存标志等。。。
              begin
              sbuf[2]:=1;
              sbuf[3]:=$00;
              sbuf[4]:=$00;
              sbuf[5]:=$00;
              sbuf[6]:=$80;   //2017-2-11之前为$10;
              send_len:=6;

              end
            else if(row_num=VIRLCROWS+2) then    //系统参数选择项
            begin
              sbuf[2]:=1;
              sbuf[3]:=$00;
              sbuf[4]:=$20;
              sbuf[5]:=$00;
              sbuf[6]:=$20;  //2017-2-14之前为$10;
              send_len:=6;

            end;
            tmr2.Tag:=1;  //接收到数据再加1//row_num:=row_num+1;        //接收用变量 row_num比发送+1
          end;
          //if btn16.Tag=5 then btn16.Tag:=btn16.Tag-1;

          try
            send_crcdata(send_len,2);
          except
            
              messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
          end;
      end;
    end;
  end;

  procedure txdImportProcess();
  var
    i,j,temp16,line:Integer;

    procedure txdReadSteps();
    var
      i,j,temp16,line:Integer;
    begin
      with Form_line do begin
        tmr1.Interval:=200;
        if (StrToInt('0'+sysedit[5].Text)>=600) then tmr1.Interval:=150+strtoint(edt14.Text);    //读取+通信时间
        //if Assigned(CurrentDevice) then   tmr1.Interval:=100+strtoint(edt15.Text);  //50
        try
          readln(inportF,AnsiSLINE_head);      //读取一行
          //j s:=AnsiSLINE_head;//StringReplace(SLINE_head, Char($00), '0', [rfReplaceAll]);; 00是行结束符，替换不了。
          //j SLINE_head:=s;//StringReplace(s, ' ', '0', [rfReplaceAll])  //空格要替代才可以用字符串分解
                      //+'0'+#9+'0'+#9+'0'+#9+'0'+#9+'0'+#9+'0'+#9+'0'+#9+'0'+#9+'0'+#9+'0'+#9 ;

          if not rb5.Checked then   begin
            if form_line.chk38.Checked then
              memo1.Lines.Add(GetTimemS+AnsiSLINE_head)
            else
              memo1.Lines.Add(AnsiSLINE_head);
          end;
          strs:=TStringList.Create;
          //strs.Delimiter:=#9;               //  strs.QuoteChar := ' ';
          //strs.DelimitedText:=AnsiSLINE_head;   //拆分不完全正确。连续的空格认识是TAB

          //以下函数没有上述的BUG   //SplitColumns(AnsiSLINE_head, strs, #9);           
          Ansi_s:=Char(#9);
          //SplitString(AnsiSLINE_head, Ansi_s , strs);
          //if  strs = nil then exit;
          strs.Clear;
          while Pos(Ansi_s, AnsiSLINE_head)>0 do
          begin
              i := Pos(Ansi_s, AnsiSLINE_head);
              strs.add(Copy(AnsiSLINE_head, 1, i - 1));
              AnsiSLINE_head := Copy(AnsiSLINE_head, i + length(Ansi_s), length(AnsiSLINE_head) - i);
          end;
          strs.Add(AnsiSLINE_head);

          sbuf[2]:=$10;
          sbuf[3]:=$30+($20*row_num)div 256;
          sbuf[4]:=($20*row_num)mod 256;
          sbuf[5]:=$00;
          sbuf[6]:=$0C;
          sbuf[7]:=2*sbuf[6];
          sbuf[8]:=0;sbuf[9]:=0;
          if length(strs[0])>0 then
          begin
            if IsNumberic(strs[0])   then begin
              sbuf[8]:=StrToInt(trim(strs[0])) div 256;
              sbuf[9]:=StrToInt(trim(strs[0])) mod 256;
            end else begin
              Application.MessageBox('File format error! 程序文件步骤格式不对，请用记事本打开检查格式或者是否被加密；',
                '错误', MB_OK + MB_ICONSTOP);
              modbusfuntion:=$00;    //退出！
              lbl28.Caption:='Import Error,File format error! ';
              CloseFile(inportF);
              exit;
            end;
          end;
          sbuf[10]:=byte(' ');sbuf[11]:=byte(' ');sbuf[12]:=byte(' ');sbuf[13]:=byte(' ');

          if strs.Count>1 then       //名称
          begin
            if length(strs[1])>0 then
              begin
                                
              Move(strs[1][1],sbuf[10],Length(strs[1]));
              temp8:=sbuf[10]; sbuf[10]:=sbuf[11];sbuf[11]:=temp8;
              temp8:=sbuf[12]; sbuf[12]:=sbuf[13];sbuf[13]:=temp8;
              for i:=0 to 3 do
                begin
                  if(sbuf[10+i]>=$7F) then sbuf[10+i]:=byte('*');       //无效
                end;
              end;
          end;
          for i:=2 to 8 do begin
              if strs.Count>9 then
                begin
                sbuf[10+2*i]:=0;sbuf[11+2*i]:=0;
                if length(strs[i])>0 then
                  begin
                  sbuf[10+2*i]:=StrToInt(trim(strs[i])) shr 8;//div 256;
                  sbuf[11+2*i]:=StrToInt(trim(strs[i])) mod 256;
                  end;
                end;
          end;
          sbuf[28]:=byte(' ');sbuf[29]:=byte(' ');sbuf[30]:=byte(' ');sbuf[31]:=byte(' ');
          if strs.Count>9 then     //模式
            begin
              if (length(strs[9])>0) then
              begin
                Move(strs[9][1],sbuf[28],Length(strs[9]));
                temp8:=sbuf[28]; sbuf[28]:=sbuf[29];sbuf[29]:=temp8;
                temp8:=sbuf[30]; sbuf[30]:=sbuf[31];sbuf[31]:=temp8;
                for i:=0 to 3 do
                  begin
                    if(sbuf[28+i]>=$80) then sbuf[28+i]:=byte('*');    //2018-10-27不支持中文和空格
                  end;
              end;
            end;
          for i:=10 to 15 do  begin
              if strs.Count>15 then          //有16项参数
              begin
                sbuf[12+2*i]:=0;sbuf[13+2*i]:=0;
                if length(strs[i])>0 then
                  begin
                  sbuf[12+2*i]:=StrToInt(trim(strs[i]))shr 8;// div 256;
                  sbuf[13+2*i]:=StrToInt(trim(strs[i])) mod 256;
                  end;
                sbuf[6]:=sbuf[6]+1;
                sbuf[7]:=sbuf[7]+2;
              end;
          end;
          if ImportVer>=654 then        //高于6.54才支持备注
          begin
            sbuf[44]:=byte('?');
            for i:=1 to 15 do sbuf[44+i]:=0;    //结束  byte(' ');
            if (lbl28.Tag=1) then begin           //最后导入PN ，使用第一行写PN；
                sbuf[6]:=$1A;
                sbuf[7]:=2*sbuf[6];
                lbl28.Tag:=1000;             //不能=2提前保存，必须整页保存！后面再检验是否’？‘
                lbl28.Hint:= strs[16];
            end else if (strs.Count>16) then begin
                if (length(strs[16])>0) then    //有数据
                begin
                  Move(strs[16][1],sbuf[44],Length(strs[16]));
                end;
                sbuf[6]:=$1A;      //ZU17EDIT
                sbuf[7]:=2*sbuf[6];
            end;
          end;
          if ImportVer>=630 then   //读取到版本号 V630
          begin
            if(StrToInt(trim(strs[0]))>VER630_MAXSTEP) then row_num:=VIRLCROWS+8;
          end else
          if ImportVer>=600 then   //读取到版本号V6
          begin
            if(StrToInt(trim(strs[0]))>(990+5)) then row_num:=VIRLCROWS+8;
          end else
          if ImportVer>=500 then   //读取到版本号V5
          begin
            if(StrToInt(trim(strs[0]))>(90+5)) then row_num:=VIRLCROWS+8;
          end else     //=0,没有版本号 程序都不超90
          begin
            if(StrToInt(trim(strs[0]))>90) then row_num:=VIRLCROWS+8;
          end;
          strs.Destroy;
          if  (working >0) then      //MCU在忙，不执行
          begin
             lbl28.Caption:='MCU is busying, Pls press RESET!';
             modbusfuntion:=$53;
             row_num:=0;               //Break;
          end;
        finally
          //CloseFile(inportF);
        end;
        send_len:=7+sbuf[7];
      end;
    end;
    procedure txdReadOSgroups();
    var
      i,j,temp16,line:Integer;
    begin
      with Form_line do begin
        tmr1.Interval:=500;       //文件可能很多，占时间
        shortptr:=0;
        try
          SLINE_head:='';
          readln(inportF,SLINE_head); //丢掉一行
          if Length(SLINE_head)>0 then begin      //有短路群，否则结束！

            for line:=0 to 2*MAXPINS do  begin    //MCU支持最大行数：所以点都是单点
              SLINE_head:='';
              readln(inportF,SLINE_head);         //读取一行
              if Length(SLINE_head)>0 then  begin
                  ansi_s:=StringReplace(SLINE_head, ')', '', [rfReplaceAll]);
                  SLINE_head:=StringReplace(ansi_s, '(', '', [rfReplaceAll]);  //空格要替代才可以用字符串分解
                  ansi_s:=StringReplace(SLINE_head, ']', '', [rfReplaceAll]);
                  SLINE_head:=StringReplace(ansi_s, '[', '', [rfReplaceAll]);  //空格要替代才可以用字符串分解
                  if not rb5.Checked then  begin
                    if form_line.chk38.Checked then
                      memo1.Lines.Add(GetTimemS+SLINE_head)
                    else
                      memo1.Lines.Add(SLINE_head);
                  end;
               // SLINE_head:=s+'0'+#9+'0'+#9+'0'+#9+'0'+#9+'0'+#9+'0'+#9;   //补充长度
                  strs:=TStringList.Create;   //strs.Delimiter:=#9;
                  strs.CommaText:=SLINE_head; //strs.DelimitedText:=SLINE_head;
                  if not IsSignedNumberic(strs[0])   then begin
                    Application.MessageBox('File format error! 程序文件之短路群参数格式不对，请用记事本打开检查格式或者是否被加密；',
                      '错误', MB_OK + MB_ICONSTOP);
                    modbusfuntion:=$00;    //退出！
                    lbl28.Caption:='Import Error,File format error! ';
                    CloseFile(inportF);
                    exit;
                  end;
                  for i:=0 to strs.Count-1 do begin
                      temp16:=strtoint(strs[i]);
                      if IsNumberic(sysedit[1].Text) and (StrToInt(sysedit[1].Text)>=768+1+1280) then begin      //大于2048，屏蔽标志位前移动
                        if temp16<0 then begin                                //负数代表屏蔽点；
                          temp16:=-temp16;
                          if (temp16 and $1FFF)>0 then
                            temp16:=temp16-1;       //保存值从0开始 ，0代表1；
                          temp16:=temp16+$2000;
                        end else begin
                          if (temp16 and $1FFF)>0 then
                            temp16:=temp16-1;       //保存值从0开始 ，0代表1；
                        end;
                        if(temp16 and $C000)<>$0000 then    //<-$C000       //群标志被占用：实际点号大于最大值，则结束标志；
                          temp16:=$DFFF             //结束非负数：<-$FFFF
                        else begin
                          if i=0 then
                              temp16:=temp16+$8000;                     //群开始
                          if strs.Count=1 then begin                    //单点
                              temp16:=temp16+$4000;
                          end;
                        end;
                        shortbuff[shortptr]:=temp16;
                        shortptr:=shortptr+1;             //buff数组加一，总短路群点数。
                        shortbuff[shortptr]:=MAXPINS*2 ;  //为OS下载退出做准备，以防意外

                      end else begin
                        if temp16<0 then begin                                //负数代表屏蔽点；
                          temp16:=-temp16;
                          if (temp16 and $0FFF)>0 then
                            temp16:=temp16-1;       //保存值从0开始 ，0代表1；
                          temp16:=temp16+$1000;
                        end else begin
                          if (temp16 and $0FFF)>0 then
                            temp16:=temp16-1;       //保存值从0开始 ，0代表1；
                        end;
                        if(temp16 and $E000)<>$0000 then    //<-$C000         //实际点号大于最大值，则结束标志；
                          temp16:=$CFFF    //<-$FFFF
                        else begin
                          if i=0 then temp16:=temp16+$8000;             //群开始
                          if strs.Count=1 then begin                    //单点
                              temp16:=temp16+$4000;
                          end;
                        end;
                        shortbuff[shortptr]:=temp16;
                        shortptr:=shortptr+1;   //buff数组加一，总短路群点数。
                        shortbuff[shortptr]:=MAXPINS ; //为OS下载退出做准备，以防意外
                      end;
                  end;
                  strs.Destroy;
              end  else  begin
                  if IsNumberic(sysedit[1].Text) and (StrToInt(sysedit[1].Text)>=768+1+1280) then begin      //大于2048，屏蔽标志位前移动
                    shortbuff[shortptr]:=MAXPINS*2;
                  end else
                    shortbuff[shortptr]:=MAXPINS ; //为OS下载退出做准备，以防意外
                  break;
              end;
            end;                    //设置短路群
            send_len:=0;            //不发送数据! 与其它命令有区别！

          end else begin
            send_len:=0;
            modbusfuntion:=$53;   //复位--结束
          end;
        finally
              //CloseFile(inportF);
        end;
      end;
    end;
    procedure txdInportSysPara();
    var
      i,j,temp16,line:Integer;
    begin
      with Form_line do begin
        try
          readln(inportF,AnsiSLINE_head); AnsiSLINE_head:='';                 //丢掉一行

          readln(inportF,Ansi_s);      //读取一行
          AnsiSLINE_head:=Ansi_s+'0'+#9+'0'+#9+'0'+#9+'0'+#9+'0'+#9+'0'+#9;   //补充长度
          if not rb5.Checked then begin
            if form_line.chk38.Checked then
              memo1.Lines.Add(GetTimemS+AnsiSLINE_head)
            else
              memo1.Lines.Add(AnsiSLINE_head);
          end;
          strs:=TStringList.Create;
          strs.Delimiter:=#9;  //strs.CommaText:=constr ;
          strs.DelimitedText:=AnsiSLINE_head;

          sbuf[2]:=$10;
          sbuf[3]:=$10;
          sbuf[4]:=$A0;
          sbuf[5]:=$00;
          sbuf[6]:=$14;        //限制zz5系列最大数。接收缓冲为50个字节。
          sbuf[7]:=2*sbuf[6];
          if not IsNumberic(strs[0])   then begin

            Application.MessageBox('File format error! 程序文件之系统参数格式不对，请用记事本打开检查格式或者是否被加密；',
              '错误', MB_OK + MB_ICONSTOP);
            modbusfuntion:=$00;    //退出！
            lbl28.Caption:='Import Error,File format error! ';
            CloseFile(inportF);
            exit;
          end;
          for i:=0 to 12 do
          begin
              sbuf[8+2*i]:=0;sbuf[9+2*i]:=0;
              if length(strs[i])>0 then
              begin
                sbuf[8+2*i]:=StrToInt(strs[i]) div 256;
                sbuf[9+2*i]:=StrToInt(strs[i]) mod 256;
              end;
          end;
          TxtOSPins:= StrToInt(strs[3]);     //提取OS点数

          sbuf[34]:=byte(' ');sbuf[35]:=byte(' ');sbuf[36]:=byte(' ');sbuf[37]:=byte(' ');
          sbuf[38]:=byte(' ');sbuf[39]:=byte(' ');
          if length(strs[13])>0 then
          begin
            Move(strs[13][1],sbuf[34],Length(strs[13]));
            temp8:=sbuf[34]; sbuf[34]:=sbuf[35];sbuf[35]:=temp8;
            temp8:=sbuf[36]; sbuf[36]:=sbuf[37];sbuf[37]:=temp8;
            temp8:=sbuf[38]; sbuf[38]:=sbuf[39];sbuf[39]:=temp8;
            //2018-10-27改成支持小写和中文 for i:=0 to 5 do
            //  begin
            //    if(sbuf[34+i]>=$80) then sbuf[34+i]:=byte(' ');
            //  end;
          end;
          if length(strs[14])>0 then     //OKenable
          begin
            sbuf[8]:=StrToInt(strs[14]);
          end;
          strs.Destroy;
        finally
          //CloseFile(inportF);
        end;
      end;
    end;
  begin
    with Form_line do begin
         if row_num=VIRLCROWS+2 then row_num:=0;
         if (StrToInt('0'+sysedit[5].Text)<630) then     //早期版本最大10步骤
         begin
           if (row_num>=10) and (row_num<VIRLCROWS) then row_num:=VIRLCROWS;
         end;            //sbuf[1]:=1;
         if(row_num=VIRLCROWS)then begin   //保存 ；    //有bug<---强制保存一次！or(lbl28.Tag=2) 机种名第一次写0’
              tmr1.Interval:=600;            //早期版本不能再小，因为固件有500延时读写EEPROM
              if (StrToInt('0'+sysedit[5].Text)>=630)    then
              begin
                if (StrToInt('0'+sysedit[5].Text)>=600) then tmr1.Interval:=300+strtoint(edt14.Text);  //+100
                //if Assigned(CurrentDevice) then   tmr1.Interval:=150+strtoint(edt15.Text);
              end;
              sbuf[2]:=5;
              sbuf[3]:=$00;
              sbuf[4]:=$0B;
              sbuf[5]:=$FF;
              sbuf[6]:=$00;
              send_len:=6;                    // row_num:=0;
              lbl28.Caption:='Save Steps......';
           //   if(lbl28.Tag=2)then begin       //意义不大，只能提示后面有没有写入PN结束！不能保证后面导入（复位）不出错
           //     row_num:=0;
           //     lbl28.Tag:=1000;
           //   end;
         end  else if (row_num<VIRLCROWS) then   begin
              txdReadSteps();
              lbl28.Caption:='Inport Steps-'+inttostr(sbuf[8]*256+sbuf[9])+'-......';
              sprgrsbr1.Position:=sprgrsbr1.Position+1;
         end  else if(row_num=VIRLCROWS+1) then begin   //翻页

              tmr1.Interval:=600;             //早期版本不能再小，因为固件有500延时读写EEPROM
              if (StrToInt('0'+sysedit[5].Text)>=630)  then
              begin
                if (StrToInt('0'+sysedit[5].Text)>=600) then tmr1.Interval:=250+strtoint(edt14.Text);   //+100
                //if Assigned(CurrentDevice) then   tmr1.Interval:=120+strtoint(edt15.Text);     //2018-7-30增加20mS，因MCUflash擦除时间过长
              end;
              sbuf[2]:=5;
              sbuf[3]:=$00;
              sbuf[4]:=$0E;
              sbuf[5]:=$FF;
              sbuf[6]:=$00;
              send_len:=6;                   //  row_num:=0;
              lbl28.Caption:='Page Down......';
         end else if(row_num<(10+VIRLCROWS)) then begin   //2~9+VIRLCROWS保存步骤，并跳转到系统参数

              tmr1.Interval:=600;            //早期V6.30以前处理EEPROM大于500mS
              if (StrToInt('0'+sysedit[5].Text)>=630)    then
              begin
                if (StrToInt('0'+sysedit[5].Text)>=600) then tmr1.Interval:=300+strtoint(edt14.Text);     //+100
                //if Assigned(CurrentDevice) then   tmr1.Interval:=150+strtoint(edt15.Text);
              end;
              sbuf[2]:=5;
              sbuf[3]:=$00;
              sbuf[4]:=$0B;
              sbuf[5]:=$FF;
              sbuf[6]:=$00;
              send_len:=6;
              row_num:=20+VIRLCOFFSET;
              lbl28.Caption:='Save Steps......';

         end else if(row_num<(11+VIRLCOFFSET)) then begin   //空闲
            
         end else if(row_num<=(21+VIRLCOFFSET)) then begin   //导入系统参数
            
              txdInportSysPara();

              send_len:=7+sbuf[7];
              row_num:=22+VIRLCOFFSET;
              lbl28.Caption:='Inport Parameters......';
              sprgrsbr1.Position:=sprgrsbr1.Max*90 div 100;
         end  else if(row_num<=(23+VIRLCOFFSET)) then begin   //保存系统参数

              tmr1.Interval:=600;            //早期V6.30以前处理EEPROM大于500mS
              if (StrToInt('0'+sysedit[5].Text)>=630)    then
              begin
                if (StrToInt('0'+sysedit[5].Text)>=600) then tmr1.Interval:=300+strtoint(edt14.Text);     //+100
                //if Assigned(CurrentDevice) then   tmr1.Interval:=150+strtoint(edt15.Text);
              end;
            sbuf[2]:=5;              //早期V6.30以前处理EEPROM大于500mS
            sbuf[3]:=$00;
            sbuf[4]:=$08;
            sbuf[5]:=$FF;
            sbuf[6]:=$00;
            send_len:=6;
            row_num:=30+VIRLCOFFSET;
            lbl28.Caption:='Save System Parameter......';

         end else if(row_num<(31+VIRLCOFFSET)) then  begin  //空闲

            
         end else if(row_num=(31+VIRLCOFFSET)) then    //只会执行一次！读取文件短路群 ,并导入，31开始；没有短路群立即退出；
            begin
              txdReadOSgroups();
              lbl28.Caption:='Read DataFiles......';
            end
         else if(row_num<(SHORTPINS+32+VIRLCOFFSET)) then    //----31开始 改为32----开始 31-63是导入短路群
            begin
              tmr1.Interval:=200;
              if N34.Checked then begin                //透传导入短路群，与保存短路群的命令间隔要加大，否则 importerr
                if (StrToInt('0'+sysedit[5].Text)>=600) then tmr1.Interval:=200+strtoint(edt14.Text);
                //if Assigned(CurrentDevice) then      tmr1.Interval:=300+strtoint(edt15.Text);
              end else begin
                if (StrToInt('0'+sysedit[5].Text)>=600) then tmr1.Interval:=150+strtoint(edt14.Text);
                //if Assigned(CurrentDevice) then      tmr1.Interval:=50+strtoint(edt15.Text);
              end;

              sbuf[2]:=$10;
              if IsNumberic(sysedit[1].Text) and (StrToInt(sysedit[1].Text)>=768+1+768) then begin                 //16倍密度
                sbuf[3]:=$24+((row_num-32-VIRLCOFFSET))div 256;      
                sbuf[4]:=((row_num-32-VIRLCOFFSET))mod 256;
              end else begin
                sbuf[3]:=$18+($10*(row_num-32-VIRLCOFFSET))div 256;
                sbuf[4]:=($10*(row_num-32-VIRLCOFFSET))mod 256;
              end;
              sbuf[5]:=$00;
              sbuf[6]:=$10;
              sbuf[7]:=2*sbuf[6];
              for i:=0 to 15 do begin
                  sbuf[8+2*i]:=shortbuff[i+16*(row_num-32-VIRLCOFFSET)] shr 8;//有符号数，会出错 div 256;
                  sbuf[9+2*i]:=shortbuff[i+16*(row_num-32-VIRLCOFFSET)] ;//mod 256;
                  if ((shortbuff[i+16*(row_num-32-VIRLCOFFSET)] and $0FFF)>= TxtOSPins) then  begin   //大于文件中OS点数  
                    row_num:=32+VIRLCOFFSET+SHORTPINS;     //进入保存短路群 200 SHORTPINS+40;   要对应上短路群保存态
                    break;
                  end;
              end;
              send_len:=7+sbuf[7];   //if( {16*(row_num-30) >shortptr}) then row_num:=68;   //页数超过最大点所需页数
              lbl28.Caption:='Inport OS Lines-'+inttostr(row_num-31-VIRLCOFFSET);
              sprgrsbr1.Position:=sprgrsbr1.Max*95 div 100;
            end
         else if(row_num<=(SHORTPINS+41+VIRLCOFFSET)) then    //210 69 保存短路群
         begin
            tmr1.Interval:=600;                     //设定下次定时到间隔；
            if (StrToInt('0'+sysedit[5].Text)>=630)      then
            begin
              if (StrToInt('0'+sysedit[5].Text)>=600) then tmr1.Interval:=200+strtoint(edt14.Text);
              //if Assigned(CurrentDevice) then   tmr1.Interval:=150+strtoint(edt15.Text);
            end;
            sbuf[2]:=5;
            sbuf[3]:=$00;
            sbuf[4]:=$09;
            sbuf[5]:=$FF;
            sbuf[6]:=$00;
            send_len:=6;
            row_num:=0;
            lbl28.Caption:='Save OS......';
            modbusfuntion:=$53;
            try
                CloseFile(inportF);
            finally
            end;
         end;

        try
            if  send_len>0 then send_crcdata(send_len,2);
      //          nsend:=nsend+1;
      //          fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
        except
            messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
        end;
        if inportflag>0 then
        begin
            modbusfuntion:=$00;                 //退出！
            lbl28.Caption:='Import Error,No response！';
        end;
        if(row_num=(31+VIRLCOFFSET)) then      //读取文件短路群但不导入MCU，所以不用导入标志
            row_num:=row_num+1                 //必须执行加1
        else
            inportflag:=1;                    //没有执行则报错！
        tmr2.Tag:=1;  //接收到数据再加1//row_num:=row_num+1;
    end;
  end;
  procedure txdPreImportProcess();
  begin
    with Form_line do begin
      tmr1.Interval:=300;
      if (StrToInt('0'+sysedit[5].Text)>=600) then tmr1.Interval:=200+strtoint(edt14.Text);  //复位：读取+通信时间+考虑未知事项
      //if Assigned(CurrentDevice) then   tmr1.Interval:=150+strtoint(edt15.Text);
      save_file_name :=Lbl29.caption;
      try
        assignfile(inportF, save_file_name);      //ExtractFilePath(application.ExeName)
        Reset(inportF);                           //application.ProcessMessages;
        readln(inportF,SLINE_head);             //读取第一行，丢掉

        if not rb5.Checked then begin           //串口全解析
          if form_line.chk38.Checked then
            memo1.Lines.Add(GetTimemS+SLINE_head)
          else
            memo1.Lines.Add(SLINE_head);
        end;
        strs:=TStringList.Create;
        strs.Delimiter:=#9;               //  strs.QuoteChar := ' ';
        strs.DelimitedText:=SLINE_head;   //查分不完全正确。连续的空格认识是TAB
        ImportVer:=0;
        if (strs.Count>17)and(Pos('v',strs[17])=6)and(IsFloatNum(Copy(strs[17],7,Length(strs[17])))) then begin    //升级：测试程序含有“固件v:"字符
          ImportVer:=Trunc(strtofloat(Copy(strs[17],7,Length(strs[17]))));
          if ImportVer<100 then begin             //无效版本
              if strs.Count>1 then begin                                        //兼容早期：第2列存放纯数字固件大版本；
                if(IsNumberic(strs[1])) then
                begin
                  ImportVer:=StrToInt(strs[1]);
                end;
              end;
          end;
        end else if strs.Count>1 then begin                                     //兼容早期：第2列存放纯数字固件大版本；
          if(IsNumberic(strs[1])) then
          begin
            ImportVer:=StrToInt(strs[1]);
          end;
        end;

      finally
        //CloseFile(inportF);
      end;
      //sbuf[1]:=1;
      sbuf[2]:=5; sbuf[3]:=$00;
      sbuf[4]:=0;sbuf[5]:=$FF; sbuf[6]:=$00;
      send_len:=6;
      try
          send_crcdata(send_len,2);
      except
          messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
      end;
      modbusfuntion:=$51;row_num:=0;
    end;
  end;
begin
   with Form_line do begin
       sbuf[1]:=1;

      tmr1.Interval:=300;   //防止下面有分支漏失，为edt15.text=0加上调试速度准备；

      case modbusfuntion of // 第一个字符都不同，所以取出第一个进行比较
          $4E..$4F:              //开始inport 导入 ---复位
              begin
                if modbusfuntion=$4E then lbl28.Tag :=1 else lbl28.Tag :=0;           //是否后写PN
                addToLog('开始导入文件：'+Lbl29.caption);
                txdPreImportProcess();
              end;
          $51:
              begin
                txdImportProcess();             //导入文件处理过程；
              end;

          $53:
              begin
                 case lbl28.tag of          //最后把PN写入！
                    1000:begin
                            sbuf[2]:=6; sbuf[3]:=$10;  sbuf[4]:=$B0;            //跳到步骤1
                            sbuf[5]:=$00; sbuf[6]:=$01;
                            send_len:=6;
                            try
                               send_crcdata(send_len,2);
                            except
                               messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
                            end;
                            lbl28.tag:=1001;

                            if edt82.tag>=100 then begin
                              sleep(20);
                              modbusfuntion:=$68;
                              modbusfun03:=$3000;              //读取机种PN，判断是否‘？’
                              modbusfun03dat:=$1A;
                            end;
                        end;

                    1001:begin
                            Sleep(50);
                            sbuf[2]:=16;
                            sbuf[3]:=$30;
                            sbuf[4]:=$12;
                            sbuf[5]:=$00;
                            sbuf[6]:=$08;
                            sbuf[7]:=$10;
                            send_len:=7+sbuf[7];
                            for i:=8 to 64 do sbuf[i]:=0;
                            Move(lbl28.Hint[1],sbuf[8],Length(lbl28.Hint));     //最新料号
                            if lbl28.Hint='' then sbuf[8]:=byte('0');
                            try
                                send_crcdata(send_len,2);
                            except
                                messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
                            end;
                            lbl28.tag:=1002;
                         end;
                    1002:begin
                            sbuf[2]:=5;
                            sbuf[3]:=$00;
                            sbuf[4]:=$0B;
                            sbuf[5]:=$FF;                                       //保存
                            sbuf[6]:=$00;
                            send_len:=6;                    
                            try
                               send_crcdata(send_len,2);
                            except
                               messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
                            end;
                            lbl28.Caption:='Save PN......';

                            lbl28.tag:=0;
                         end;
                 else
                    //sbuf[1]:=1;
                    sbuf[2]:=5; sbuf[3]:=$00;
                    sbuf[4]:=0;sbuf[5]:=$FF; sbuf[6]:=$00;
                    send_len:=6;
                    try
                            send_crcdata(send_len,2);
                    except
                            messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
                    end;
                    modbusfuntion:=$00;
                    if inportflag>0 then
                    begin
                        addToLog('导入失败');
                        lbl28.Caption:='Import Error！';
                      //  lbl28.Font.Color:=clWindowtext;
                    end  else  begin
                        if (a1.Checked or n50.Checked) and ((1+A1.Tag)<n50.tag) then begin
                          modbusfun06:=$0041;    //选择机种
                          A1.Tag:=A1.Tag+1;
                          modbusfun06dat:=A1.Tag+$2000;
                        end else begin
                          N50.Checked:=false;
                          a1.checked:=false;
                        end;
                        addToLog('成功导入');
                        lbl28.Caption:='Import Finish！';
                        lbl28.Font.Color:=clWindowtext;
                        sprgrsbr1.Position:=sprgrsbr1.Max;                    //进度条满格
                        if FormLoad.Visible  then begin
                          if edt82.Text='99' then begin
                            FormLoad.btn5.Tag:=1;FormLoad.Close;
                            if edt82.Tag=99 then
                              Form_line.tmr3Timer(Form_line);
                          end else begin
                            case
                              Application.MessageBox('Load Sussessful!加载完成！关闭加载窗口',
                              '提示', MB_OK + MB_ICONQUESTION) of
                              IDOK:
                                begin
                                  FormLoad.btn5.Tag:=1;FormLoad.Close;
                                end;
                            end;
                          end;
                        end;
                    end;
                    inportflag:=1;   inportNewFileflag:=1;     //允许收到新数据后加载文件头；
                 end;
              end;
          $54:           //导入error
              begin
                modbusfuntion:=$00;

                lbl28.Caption:='Import USB ERROR！通信错误！';
                lbl28.Font.Color:=clred;
                addToLog('USB通信错误，加载失败！');
                if FormLoad.Visible  then begin
                  case
                    Application.MessageBox('upfile Fail!USB通信错误！请重新加载',
                    '提示', MB_OK + MB_ICONQUESTION) of
                    IDOK:
                      begin
                         FormLoad.btn5.Tag:=2;FormLoad.Close;
                      end;
                  end;
                end;
              end;
              ///////////////////////////////////////导出文件
          $3F: begin
                //ShowMessage('First');
                addToLog('开始导出文件：'+Lbl29.caption);
                tmr1.Interval:=300;
                if (StrToInt('0'+sysedit[5].Text)>=600) then tmr1.Interval:=150+strtoint(edt14.Text);
                //if Assigned(CurrentDevice) then   tmr1.Interval:=100+strtoint(edt15.Text);
                save_file_name :=Lbl29.caption;
                try
                  assignfile(exportF, save_file_name);      //ExtractFilePath(application.ExeName)
                  Append(exportF);
                  //application.ProcessMessages;
                finally
                end;
                //sbuf[1]:=1;
                sbuf[2]:=5; sbuf[3]:=$00;
                sbuf[4]:=0;sbuf[5]:=$FF; sbuf[6]:=$00;
                  send_len:=6;
                  try
                          send_crcdata(send_len,2);
                  except
                          messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
                  end;
                modbusfuntion:=$41;row_num:=0;

              end;
         { $40:
              sbuf[3]:=$10;
              sbuf[4]:=$A0;              }
          $41:
              begin                      //export 导出数据
                if btn24.Tag >100 then begin         //读取到最大步骤数
                  sprgrsbr1.Max:= (btn24.Tag*100) div 90;
                end;
                tmr1.Interval:=300;   //2019-7-26增加
                txdExportProcess();
              end;
       // $42:
          $43:
              begin
                tmr1.Interval:=300;
                if (StrToInt('0'+sysedit[5].Text)>=600) then tmr1.Interval:=200+strtoint(edt14.Text);
                //if Assigned(CurrentDevice) then   tmr1.Interval:=150+strtoint(edt15.Text);
                //sbuf[1]:=1;
                sbuf[2]:=5; sbuf[3]:=$00;
                sbuf[4]:=0;sbuf[5]:=$FF; sbuf[6]:=$00;
                send_len:=6;
                try
                        send_crcdata(send_len,2);
                except
                        messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
                end;
                modbusfuntion:=$00;
                try CloseFile(exportF);  finally end;

                if n51.Checked and ( (1+A1.Tag)<n51.tag ) then begin
                  modbusfun06:=$0041;    //选择机种
                  A1.Tag:=A1.Tag+1;
                  modbusfun06dat:=A1.Tag+$2000;
                end else
                  N51.Checked:=false;
                addToLog('导出完成');
                lbl28.Caption:='Export Finish！';
                lbl28.Font.Color:=clWindowtext;

                sprgrsbr1.Position:=sprgrsbr1.Max;
                if FormLoad.Visible  then begin
                  if (Readchis('Server','MESprogram')<>'') then begin
                    addToLog('开始上传 '+Lbl29.caption+' 给MES系统');
                    btn114Click(Form_line);
                    if lbl164.Caption='OK' then begin
                      addToLog('上传完成');
                      case
                        Application.MessageBox('upfile Sussessful!上传MES系统完成！关闭上传窗口',
                        '提示', MB_OK + MB_ICONQUESTION) of
                        IDOK:
                          begin
                            FormLoad.btn5.Tag:=3;FormLoad.Close;
                          end;
                      end;
                    end else begin
                      addToLog('上传失败');
                      case
                        Application.MessageBox('upfile Fail!上传MES系统失败！请重新上传',
                        '提示', MB_OK + MB_ICONQUESTION) of
                        IDOK:
                          begin

                          end;
                      end;
                    end;
                  end else begin
                    case
                      Application.MessageBox('export Sussessful!导出测试程序完成！关闭上传窗口',
                      '提示', MB_OK + MB_ICONQUESTION) of
                      IDOK:
                        begin
                          FormLoad.btn5.Tag:=4;FormLoad.Close;
                        end;
                    end;
                  end;
                end;
              end;
          $44:           //导出error
              begin
                modbusfuntion:=$00;
                try CloseFile(exportF);  finally end;

                lbl28.Caption:='Export USB ERROR！通信错误！';
                lbl28.Font.Color:=clred;
                addToLog('USB通信错误，上传失败！');
                if FormLoad.Visible  then begin
                  
                  case
                    Application.MessageBox('upfile Fail!USB通信错误！请重新上传',
                    '提示', MB_OK + MB_ICONQUESTION) of
                    IDOK:
                      begin
                        FormLoad.btn5.Tag:=5;FormLoad.Close;
                      end;
                  end;
                end;
              end;
              ///////////////////////////////////////写入规格参数------------------
          $70: begin          //先复位
                tmr1.Interval:=300;
                if (StrToInt('0'+sysedit[5].Text)>=600) then tmr1.Interval:=150+strtoint(edt14.Text);
                //if Assigned(CurrentDevice) then   tmr1.Interval:=100+strtoint(edt15.Text);
                //sbuf[1]:=1;
                sbuf[2]:=5; sbuf[3]:=$00;
                sbuf[4]:=0;sbuf[5]:=$FF; sbuf[6]:=$00;
                send_len:=6;
                try
                        send_crcdata(send_len,2);
                except
                        messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
                end;
                modbusfuntion:=$71;row_num:=0;
              end;
           $77: begin
                  modbusfun06dat:=$00C3;            //广播写电子负载
                  modbusfun06:=$0001;
                  //sbuf[1]:=1;
                  sbuf[2]:=6;
                  sbuf[3]:=modbusfun06 shr 8;
                  sbuf[4]:=modbusfun06;
                  sbuf[5]:=modbusfun06dat shr 8;      //编辑模式也是测试模式
                  sbuf[6]:=modbusfun06dat;
                  send_len:=6;
                  try
                      send_crcdata(send_len,2);
                //          nsend:=nsend+1;
                //     fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
                  except
                      messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
                  end;
               // btn68click(self);
                  modbusfuntion:=$78;
                  row_num:=0;
               end;
          $78: begin
                  //sbuf[1]:=1;
                  sbuf[2]:=5;              //早期V6.30以前处理EEPROM大于500mS
                  sbuf[3]:=$00;
                  sbuf[4]:=$08;
                  sbuf[5]:=$FF;
                  sbuf[6]:=$00;
                  send_len:=6;
                  try
                      send_crcdata(send_len,2);
                //          nsend:=nsend+1;
                //     fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
                  except
                      messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
                  end;
                  row_num:=0;
                  modbusfuntion:=$00;
                  modbusfun16int:=0;
                  modbusfun06:=0;
               end;


      else           //非数据导入-导出。则是数据监控
              begin
                txdMonitorProcess();
              end;
      end;
   end;
end;

procedure rxdprocess(tt:Word;Port:SmallInt);
var temp,zuptr :SmallInt;
    temp16,i,j,Nametemp:Integer;
    ff:real;
    dd:SmallInt;
    tempss,bar1,bar2,viewstring: string;
    //f: TextFile;       Data: TStringStream;
    h:HWND;

    save_file_name,SLINE_head,SLINE_head2,SLINE_upper,SLINE_lower,s: string;
  procedure rxdFun01Process();
  var
    i,j:Integer;
  begin
    with Form_line do begin
      if (btn16.tag>9)or( row_num=(VIRLCROWS+2) ) then       //收到状态标志 ；///地址0x00返回：保存参数提示
      begin
        btn16.tag:=0;
        if (rbuf[5] and 1)=1 then btn14.Font.Color:=clRed else btn14.Font.Color:=clWindowText;
        if (rbuf[5] and 8)=8 then btn16.Font.Color:=clRed else btn16.Font.Color:=clWindowText;
        if (rbuf[6] and 8)=8 then begin
          N47.Checked:=True;
          lbl150.Caption:='KB校准中...';
          lbl150.Font.Color:=clred;
        end else begin
          N47.Checked:=False;
          lbl150.Caption:='-';
          lbl150.Font.Color:=clWindow;
        end;
      end;
      if row_num=(VIRLCROWS+3) then        //地址0x20返回：读系统参数，是否使能。
      begin
        if (rbuf[4] and 1)=1 then chk1.Checked:=True else chk1.Checked:=False;
        if (rbuf[4] and 2)=2 then chk2.Checked:=True else chk2.Checked:=False;
        if (rbuf[4] and 4)=4 then chk3.Checked:=True else chk3.Checked:=False;
        if (rbuf[4] and 8)=8 then chk4.Checked:=True else chk4.Checked:=False;
        if (rbuf[4] and 16)=16 then chk5.Checked:=True else chk5.Checked:=False;
        if (rbuf[4] and 32)=32 then chk7.Checked:=True else chk7.Checked:=False;
        if (rbuf[4] and 64)=64 then chk8.Checked:=True else chk8.Checked:=False;
        if (rbuf[4] and 128)=128 then chk9.Checked:=True else chk9.Checked:=False;

        if ( (working and $01)=$00 )and( (rbuf[5] and $01)=$01 )and(btn15.Tag = 0) then begin   //working从0变为1，且非单步；
           FormLogHead.lbl1.Caption:='testing...... 测试中.......'
                        +char($0A)+char($0A)+
                        'Tips提示：离线中... （因在线Debug会会造成测量有波动，请等待测试结束...）'
                                      ;
           FormLogHead.Show;
        end;
        if(btn15.Tag>0) then
          btn15.Tag:=btn15.Tag-1;
        working:=rbuf[5];

        if not btn23.Enabled then
          btn23.Enabled:=true;
        if not btn24.Enabled then
          btn24.Enabled:=true;
      end;
    end;
  end;
  procedure rxdDebugStepPara();
  var
    i,j:Integer;
    procedure rxdDebugExportFile();
    var
      i,j:Integer;
    begin
      with Form_line do begin
          try
            //save_file_name :=Lbl29.caption;
            //assignfile(f, save_file_name);      //ExtractFilePath(application.ExeName)
            //Append(exportF);//f); //rewrite(f);

            SLINE_head:='';
            s := Zu1edit[row_num].Text;
            //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
            s := s + #9;  SLINE_head:=SLINE_head+s;
            s := Zu2edit[row_num].Text;
            //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
            s := s + #9;  SLINE_head:=SLINE_head+s;
            s := Zu3edit[row_num].Text;
            //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
            s := s + #9;  SLINE_head:=SLINE_head+s;
            s := Zu4edit[row_num].Text;
            //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
            s := s + #9;   SLINE_head:=SLINE_head+s;
            s := Zu5edit[row_num].Text;
            //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
            s := s + #9;   SLINE_head:=SLINE_head+s;
            s := Zu6edit[row_num].Text;
            //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
            s := s + #9;  SLINE_head:=SLINE_head+s;
            s := Zu7edit[row_num].Text;
            ////if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
            s := s + #9;   SLINE_head:=SLINE_head+s;
            s := Zu8edit[row_num].Text;
            //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
            s := s + #9;    SLINE_head:=SLINE_head+s;
            s := Zu9edit[row_num].Text;
            //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
            s := s + #9;   SLINE_head:=SLINE_head+s;
            s := Zu10edit[row_num].Text;
            //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
            s := s + #9;   SLINE_head:=SLINE_head+s;

            if rbuf[3]>=36 then
            begin
                s := Zu11edit[row_num].Text;
                //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
                s := s + #9;  SLINE_head:=SLINE_head+s;
                s := Zu12edit[row_num].Text;
                //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
                s := s + #9;  SLINE_head:=SLINE_head+s;
                s := Zu13edit[row_num].Text;
                //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
                s := s + #9;  SLINE_head:=SLINE_head+s;
                s := Zu14edit[row_num].Text;
                //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
                s := s + #9;   SLINE_head:=SLINE_head+s;
                s := Zu15edit[row_num].Text;
                //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
                s := s + #9;   SLINE_head:=SLINE_head+s;
                s := Zu16edit[row_num].Text;
                //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
                s := s + #9;  SLINE_head:=SLINE_head+s;

            end;
            if rbuf[3]>=52 then
            begin
                s := Zu17edit[row_num].Text;                              //备注
                //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
                s := s + #9;   SLINE_head:=SLINE_head+s;

            end;
            writeln(exportF, SLINE_head);
          finally
            //Closefile(f);
          end;
          if (StrToInt('0'+sysedit[5].Text)>=630) then
          begin
            if((rbuf[4]*256+rbuf[5])>=VER630_MAXSTEP) and ((rbuf[4]*256+rbuf[5])<10000)
            then	row_num:=19+VIRLCOFFSET  //V6.30版本以上
          end
          else if (StrToInt('0'+sysedit[5].Text)>=600)  then
          begin
            if((rbuf[4]*256+rbuf[5])>(990+5) ) and ((rbuf[4]*256+rbuf[5])<1000)
            then	row_num:=19+VIRLCOFFSET  //V6版本以上
          end
          else if (StrToInt('0'+sysedit[5].Text)<600) then
          begin
            if((rbuf[4]*256+rbuf[5])>(90+5) ) and ((rbuf[4]*256+rbuf[5])<100)
            then	row_num:=19+VIRLCOFFSET;
          end;
          if  (working >0) then      //MCU在忙，不执行
          begin
             lbl28.Caption:='MCU is busying, Pls press RESET!请按复位';
             modbusfuntion:=$43;
             row_num:=0;
            // Break;
          end;
      end;
    end;
  begin
    with Form_line do begin
      dd:=(rbuf[4]*256+rbuf[5]);        //步骤序号
      if dd=0 then  begin
        Zu1edit[row_num].Text:='0';
        Zu1edit[row_num].Color:=clGray   //删略
      end  else begin
        if(StrToInt('0'+sysedit[5].Text)<600) then        //FCT5
        begin
          if(dd>99)   //6.00版本以下，步骤最大99步
          then Zu1edit[row_num].Color:=clred else Zu1edit[row_num].Color:=clWindow;
          Zu1edit[row_num].Text:=IntToStr((rbuf[4]*256+rbuf[5])mod 100);
        end else   if(StrToInt('0'+sysedit[5].Text)<630) then
        begin
          if(dd>999)   //6.30版本以下，步骤最大999步
          then Zu1edit[row_num].Color:=clred else Zu1edit[row_num].Color:=clWindow;
          Zu1edit[row_num].Text:=IntToStr((rbuf[4]*256+rbuf[5])mod 1000);
        end else                                          //FCT6
        begin
          if(dd>9999)   //6.30版本以上，步骤最大9999步
          then Zu1edit[row_num].Color:=clred else Zu1edit[row_num].Color:=clWindow;
          Zu1edit[row_num].Text:=IntToStr((rbuf[4]*256+rbuf[5])mod 10000);
        end;
      end;
      Zu2edit[row_num].Text:=char(rbuf[7])+char(rbuf[6])+char(rbuf[9])+char(rbuf[8]);
      dd:=(rbuf[10]*256+rbuf[11]);
      Zu3edit[row_num].Text:=IntToStr(dd);
      //if char(rbuf[24])='B'        //上传结果，标准值灰显
      //then zu3edit[row_num].Color:=clGray else  zu3edit[row_num].Color:=clWindow;

      dd:=(rbuf[12]*256+rbuf[13]);
      if Zu3edit[row_num].Text='30005' then begin
        Zu4edit[row_num].Text:=IntToStr(Word(dd));      //测量值
      end else
        Zu4edit[row_num].Text:=IntToStr(dd);      //测量值

      dd:= (rbuf[14]*256+rbuf[15]);
      if Zu3edit[row_num].Text='30005' then begin
        Zu5edit[row_num].Text:=IntToStr(Word(dd));
      end else
        Zu5edit[row_num].Text:=IntToStr(dd);
      if(char(rbuf[24])='K') or (char(rbuf[24])='O') or (char(rbuf[24])='S') then zu5edit[row_num].Color:=clGreen   //可选 or (char(rbuf[24])='Q')
      //else if (char(rbuf[24])='B') or (char(rbuf[24])='W')   then zu5edit[row_num].Color:=clGray
      else  zu5edit[row_num].Color:=clWindow;
      dd:= (rbuf[16]*256+rbuf[17]);
      if Zu3edit[row_num].Text='30005' then begin
        Zu6edit[row_num].Text:=IntToStr(Word(dd));
      end else
        Zu6edit[row_num].Text:=IntToStr(dd);
      if(char(rbuf[24])='K') or (char(rbuf[24])='O') or (char(rbuf[24])='S') then begin
        zu6edit[row_num].Color:=clGreen;   // or  (char(rbuf[24])='Q')
      //else if (char(rbuf[24])='B')or (char(rbuf[24])='W')   then zu6edit[row_num].Color:=clGray
      end else begin
        zu6edit[row_num].Color:=clWindow;                                 
      end;
      Zu7edit[row_num].Text:=IntToStr((rbuf[18]*256+rbuf[19]));
    //  if (char(rbuf[24])='B'){or (char(rbuf[24])='O') or (char(rbuf[24])='S')}then zu7edit[row_num].Color:=clGray else
    //  zu7edit[row_num].Color:=clWindow;
      Zu8edit[row_num].Text:=IntToStr((rbuf[20]*256+rbuf[21]));
    //  if (char(rbuf[24])='B'){or (char(rbuf[24])='O') or (char(rbuf[24])='S')}then zu8edit[row_num].Color:=clGray else
    //  zu8edit[row_num].Color:=clWindow;
      if(char(rbuf[24])='Z')  then zu9edit[row_num].Color:=clGray ;    //系统模式的步骤，不延时；
      Zu9edit[row_num].Text:=IntToStr((rbuf[22]*256+rbuf[23]));
      Zu10edit[row_num].Text:=char(rbuf[25])+char(rbuf[24])+char(rbuf[27])+char(rbuf[26]);

      if ((rbuf[26]-$30)and $01)=$01 then begin          //是测量步骤(需要显示)
         Zu18edit[row_num].Text:='';

         dd:=(rbuf[12]*256+rbuf[13]);                     //提取测量值
         if Zu3edit[row_num].Text='30005' then begin
           ff:=Word(dd);
           if Word(dd)>65000 then                        //读传感器寄存器值（用【高点】或者【比例K%】分频）不应超过65000；否则要用HEX表示
              Zu18edit[row_num].Text:='N/A '
           else if Word(dd)=65000 then
              Zu18edit[row_num].Text:='65000';
         end else begin
           ff:=dd;
           if (dd>30000)or(dd<-30000) then
              Zu18edit[row_num].Text:='N/A '
           else if dd=-30000 then
              Zu18edit[row_num].Text:='-30000'
           else if Word(dd)=30000 then
              Zu18edit[row_num].Text:='30000';
         end;

         if Zu18edit[row_num].Text='' then begin        //先换算出小数点
             case rbuf[25] of                           //单位字符
                $31,
                $34,
                $37:
                    Zu18edit[row_num].Text:=copy(FormatFloat('0.00',ff/100),0,7);
                $32,
                $35,
                $38:
                    Zu18edit[row_num].Text:=copy(FormatFloat('0.0',ff/10),0,7);
                $33,
                $36,
                $39:
                    Zu18edit[row_num].Text:=copy(FormatFloat('0.000',ff/1000),0,7);
             else
                Zu18edit[row_num].Text:=copy(FloatToStr(ff/1),0,7);
             end;
         end else begin

             case rbuf[25] of                           //单位字符
                $31,
                $34,
                $37:
                    Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'.00';      //显示小数点个数；
                $32,
                $35,
                $38:
                    Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'.0';
                $33,
                $36,
                $39:
                    Zu18edit[row_num].Text:=Zu18edit[row_num].Text+'.000';
             end;
         end;

         //放在后面一起处理 DispUnitErrHint(dd,0);

         if(rbuf[10]*256+rbuf[11])>30000 then begin    //绝对上下限
            dd:= (rbuf[14]*256+rbuf[15]);        //上限
            if Zu3edit[row_num].Text='30005' then begin
              ff:=Word(dd);
            end else
              ff:=dd;
            dd:= (rbuf[16]*256+rbuf[17]);         //下限
            if Zu3edit[row_num].Text='30005' then begin
              ff:=ff+Word(dd);
            end else
              ff:=ff+dd;
            ff:= ff / 2;
         end else begin
            ff:= rbuf[10]*256+rbuf[11];        //标准值
         end;

         dd:=rbuf[12]*256+rbuf[13] ;          //dd重新提取测量值
         if (char(rbuf[24])='K') or (char(rbuf[24])='O') or (char(rbuf[24])='S') then     //开短路没有误差
            Zu19edit[row_num].Text:='N/A '     //不适用
         else if (ff>-0.1)and(ff<0.1) then      //上下限，不适用
            Zu19edit[row_num].Text:='N/A '     
         else if Zu3edit[row_num].Text='30005' then begin
           if Word(dd)>65000 then begin
              Zu19edit[row_num].Text:='N/A '     //不适用
           end else
             Zu19edit[row_num].Text:=copy(FloatToStr(-100*(ff-Word(dd))/ff),0,6)+'%';
         end else begin
           if dd>30000 then begin
              Zu19edit[row_num].Text:='N/A '     //不适用
           end else
              Zu19edit[row_num].Text:=copy(FloatToStr(-100*(ff-dd)/ff),0,6)+'%';
         end;

         Zu18edit[row_num].Color:=clYellow; Zu18edit[row_num].Enabled:=true;
         Zu19edit[row_num].Color:=clYellow; Zu19edit[row_num].Enabled:=true;
      end else begin
         Zu18edit[row_num].Text:='-';
         Zu19edit[row_num].Text:='-';
         Zu18edit[row_num].Color:=clSilver; Zu18edit[row_num].Enabled:=false;
         Zu19edit[row_num].Color:=clSilver; Zu19edit[row_num].Enabled:=false;
      end;

      if rbuf[3]>=36 then
      begin
        Zu11edit[row_num].Text:=IntToStr((rbuf[28]*256+rbuf[29]));
        dd:=(rbuf[30]*256+rbuf[31]);
        Zu12edit[row_num].Text:=IntToStr(dd);    //有符号数
        //Zu12edit[row_num].Text:=IntToStr((rbuf[30]*256+rbuf[31]));
        if (StrToInt('0'+sysedit[5].Text)<600)//or (char(rbuf[24])='B')or (char(rbuf[24])='O') or (char(rbuf[24])='S')or (char(rbuf[24])='Q') or (char(rbuf[24])='W')
        then begin
          zu11edit[row_num].Color:=clGray;
          zu12edit[row_num].Color:=clGray;
        end else  begin
          zu11edit[row_num].Color:=clWindow;
          zu12edit[row_num].Color:=clWindow;
        end;
       {
        if (StrToInt('0'+sysedit[5].Text)<600)or(char(rbuf[24])='B')or (char(rbuf[24])='O') or (char(rbuf[24])='S')or (char(rbuf[24])='Q') or (char(rbuf[24])='W')
        then zu12edit[row_num].Color:=clGray else  zu12edit[row_num].Color:=clWindow; }

        Zu13edit[row_num].Text:=IntToStr((rbuf[32]*256+rbuf[33]));
        //if (char(rbuf[25])='F')or(char(rbuf[25])='X') or (char(rbuf[25])='Y')
        //    or (char(rbuf[24])='T')  or (char(rbuf[24])='I')        //切换测试。模式第一个字母决定特殊用法
        //then zu13edit[row_num].Color:=clWindow else  zu13edit[row_num].Color:=clGray;
        Zu14edit[row_num].Text:=IntToStr((rbuf[34]*256+rbuf[35]));     //序号
        //if(char(rbuf[24])='O') or (char(rbuf[24])='S') then zu14edit[row_num].Color:=clGreen
        //else if (char(rbuf[25])='X') or (char(rbuf[25])='Y') or (char(rbuf[24])='T')
        //then zu14edit[row_num].Color:=clWindow else  zu14edit[row_num].Color:=clGray;

        Zu15edit[row_num].Text:=IntToStr((rbuf[36]*256+rbuf[37]));
        Zu16edit[row_num].Text:=IntToStr((rbuf[38]*256+rbuf[39]));
        if(char(rbuf[24])='K') or (char(rbuf[24])='O') or (char(rbuf[24])='S') then
        begin
          zu15edit[row_num].Color:=clGreen ;
          zu16edit[row_num].Color:=clGreen;
        end else if True //(char(rbuf[25])='F')or (char(rbuf[25])='X') or (char(rbuf[25])='Y') or (char(rbuf[25])='S')
               // or (char(rbuf[24])='T') or (char(rbuf[24])='I')  or (char(rbuf[24])='L')  or (char(rbuf[24])='Z')
        then begin
          zu15edit[row_num].Color:=clWindow;
          zu16edit[row_num].Color:=clWindow;
        end  else begin
          zu15edit[row_num].Color:=clGray;
          zu16edit[row_num].Color:=clGray;
        end;

      //---- //显示位16进制！---------------------------------------------------
        if ( (Copy(zu10edit[row_num].Text,2,2)='NL') )  then begin      
           if strtoint(zu15edit[row_num].Text)>255 then zu15edit[row_num].Text:='x'+inttohex(strtoint(zu15edit[row_num].Text),4)else zu15edit[row_num].Text:='x'+inttohex(strtoint(zu15edit[row_num].Text),2);
        end else if ( (Copy(zu10edit[row_num].Text,2,2)='AC') or  (Copy(zu10edit[row_num].Text,2,2)='NC') )  then begin      //CAN通信，显示位16进制！
           if strtoint(zu7edit[row_num].Text)>255 then zu7edit[row_num].Text:='x'+inttohex(strtoint(zu7edit[row_num].Text),4) else zu7edit[row_num].Text:='x'+inttohex(strtoint(zu7edit[row_num].Text),2);
           zu15edit[row_num].Text:='x'+inttohex(strtoint(zu15edit[row_num].Text),4);//ID统一为双字节  if strtoint(zu15edit[row_num].Text)>255 then else zu15edit[row_num].Text:='x'+inttohex(strtoint(zu15edit[row_num].Text),2);
        end else if ( (Copy(zu10edit[row_num].Text,2,1)='A') or  (Copy(zu10edit[row_num].Text,2,1)='N') )
          and( (Copy(zu10edit[row_num].Text,3,1)='T') or  (Copy(zu10edit[row_num].Text,3,1)='I') or  (Copy(zu10edit[row_num].Text,3,1)='J')
           or  (Copy(zu10edit[row_num].Text,3,1)='V') or  (Copy(zu10edit[row_num].Text,3,1)='P') or  (Copy(zu10edit[row_num].Text,3,1)='Q')
           or  (Copy(zu10edit[row_num].Text,3,1)='W') or  (Copy(zu10edit[row_num].Text,3,1)='N') or  (Copy(zu10edit[row_num].Text,3,1)='A')
          )  then begin      //显示位16进制！
           zu7edit[row_num].Text:='x'+inttohex(strtoint(zu7edit[row_num].Text),2);
           zu8edit[row_num].Text:='x'+inttohex(strtoint(zu8edit[row_num].Text),2);
        end;
        if ( (Copy(zu10edit[row_num].Text,2,2)='XH') or (Copy(zu10edit[row_num].Text,2,2)='YH') )  then begin      //显示位16进制！
           if strtoint(zu3edit[row_num].Text)<=30000 then begin
              if (strtoint(zu3edit[row_num].Text)>255)or(strtoint(zu3edit[row_num].Text)<0) then zu3edit[row_num].Text:='x'+copy(inttohex(strtoint(zu3edit[row_num].Text),4),0,4)else zu3edit[row_num].Text:='x'+inttohex(strtoint(zu3edit[row_num].Text),2);
           end else begin
              if strtoint(zu5edit[row_num].Text)>255 then zu5edit[row_num].Text:='x'+inttohex(strtoint(zu5edit[row_num].Text),4)else zu5edit[row_num].Text:='x'+inttohex(strtoint(zu5edit[row_num].Text),2);
              if strtoint(zu6edit[row_num].Text)>255 then zu6edit[row_num].Text:='x'+inttohex(strtoint(zu6edit[row_num].Text),4)else zu6edit[row_num].Text:='x'+inttohex(strtoint(zu6edit[row_num].Text),2);
           end;
           if strtoint(zu4edit[row_num].Text)>255 then zu4edit[row_num].Text:='x'+copy(inttohex(strtoint(zu4edit[row_num].Text),4),0,4)else zu4edit[row_num].Text:='x'+inttohex(strtoint(zu4edit[row_num].Text),2);
           if strtoint(zu15edit[row_num].Text)>255 then zu15edit[row_num].Text:='x'+inttohex(strtoint(zu15edit[row_num].Text),4)else zu15edit[row_num].Text:='x'+inttohex(strtoint(zu15edit[row_num].Text),2);
           if strtoint(zu16edit[row_num].Text)>255 then zu16edit[row_num].Text:='x'+inttohex(strtoint(zu16edit[row_num].Text),4)else zu16edit[row_num].Text:='x'+inttohex(strtoint(zu16edit[row_num].Text),2);
        end;
        if ( (Copy(zu10edit[row_num].Text,2,2)='JB')or(Copy(zu10edit[row_num].Text,2,2)='JT')or(Copy(zu10edit[row_num].Text,2,2)='JU')     //单总线芯片和SPI芯片
           or(Copy(zu10edit[row_num].Text,2,2)='JV')or(Copy(zu10edit[row_num].Text,2,2)='JW')or(Copy(zu10edit[row_num].Text,2,2)='JX') )  then begin      //显示位16进制！
           if strtoint(zu3edit[row_num].Text)<=30000 then begin
              if (strtoint(zu3edit[row_num].Text)>255)or(strtoint(zu3edit[row_num].Text)<0) then zu3edit[row_num].Text:='x'+copy(inttohex(strtoint(zu3edit[row_num].Text),4),0,4)else zu3edit[row_num].Text:='x'+inttohex(strtoint(zu3edit[row_num].Text),2);
           end else begin
              if strtoint(zu5edit[row_num].Text)>255 then zu5edit[row_num].Text:='x'+inttohex(strtoint(zu5edit[row_num].Text),4)else zu5edit[row_num].Text:='x'+inttohex(strtoint(zu5edit[row_num].Text),2);
              if strtoint(zu6edit[row_num].Text)>255 then zu6edit[row_num].Text:='x'+inttohex(strtoint(zu6edit[row_num].Text),4)else zu6edit[row_num].Text:='x'+inttohex(strtoint(zu6edit[row_num].Text),2);
           end;
           if strtoint(zu4edit[row_num].Text)>255 then zu4edit[row_num].Text:='x'+copy(inttohex(strtoint(zu4edit[row_num].Text),4),0,4)else zu4edit[row_num].Text:='x'+inttohex(strtoint(zu4edit[row_num].Text),2);
           if strtoint(zu7edit[row_num].Text)>255 then zu7edit[row_num].Text:='x'+inttohex(strtoint(zu7edit[row_num].Text),4)else zu7edit[row_num].Text:='x'+inttohex(strtoint(zu7edit[row_num].Text),2);
           if strtoint(zu8edit[row_num].Text)>255 then zu8edit[row_num].Text:='x'+inttohex(strtoint(zu8edit[row_num].Text),4)else zu8edit[row_num].Text:='x'+inttohex(strtoint(zu8edit[row_num].Text),2);
        end;
        if ( (Copy(zu10edit[row_num].Text,1,3)='JJX') )  then begin      //显示位16进制！
           if strtoint(zu13edit[row_num].Text)>255 then zu13edit[row_num].Text:='x'+inttohex(strtoint(zu13edit[row_num].Text),4) else zu13edit[row_num].Text:='x'+inttohex(strtoint(zu13edit[row_num].Text),2);
        end;
        if ( (Copy(zu10edit[row_num].Text,2,2)='JB') )  then begin      //显示位16进制！
           if strtoint(zu3edit[row_num].Text)<=30000 then begin
              if (strtoint(zu3edit[row_num].Text)>255)or(strtoint(zu3edit[row_num].Text)<0) then zu3edit[row_num].Text:='x'+copy(inttohex(strtoint(zu3edit[row_num].Text),4),0,4)else zu3edit[row_num].Text:='x'+inttohex(strtoint(zu3edit[row_num].Text),2);
           end else begin
              if strtoint(zu5edit[row_num].Text)>255 then zu5edit[row_num].Text:='x'+inttohex(strtoint(zu5edit[row_num].Text),4)else zu5edit[row_num].Text:='x'+inttohex(strtoint(zu5edit[row_num].Text),2);
              if strtoint(zu6edit[row_num].Text)>255 then zu6edit[row_num].Text:='x'+inttohex(strtoint(zu6edit[row_num].Text),4)else zu6edit[row_num].Text:='x'+inttohex(strtoint(zu6edit[row_num].Text),2);
           end;
           if strtoint(zu4edit[row_num].Text)>255 then zu4edit[row_num].Text:='x'+copy(inttohex(strtoint(zu4edit[row_num].Text),4),0,4)else zu4edit[row_num].Text:='x'+inttohex(strtoint(zu4edit[row_num].Text),2);
           if strtoint(zu7edit[row_num].Text)>255 then zu7edit[row_num].Text:='x'+inttohex(strtoint(zu7edit[row_num].Text),4)else zu7edit[row_num].Text:='x'+inttohex(strtoint(zu7edit[row_num].Text),2);
           if strtoint(zu8edit[row_num].Text)>255 then zu8edit[row_num].Text:='x'+inttohex(strtoint(zu8edit[row_num].Text),4)else zu8edit[row_num].Text:='x'+inttohex(strtoint(zu8edit[row_num].Text),2);
        end;
      end;

    //----------参数提示方式：hint 、颜色、...
      DispUnitErrHint(0,1);                                          //鼠标提示！

      if chk1.Checked and (char(rbuf[24])='Q')and(  (char(rbuf[27])='0') or (char(rbuf[27])='Q')) then begin   //Q0和qq模式输出与结果输出冲突则提示；
         if (Zu7edit[row_num].Text='1')or(Zu7edit[row_num].Text='2') then begin         //高点
           Zu7edit[row_num].Color:=clRed;
           Zu10edit[row_num].Color:=clRed;
           chk1.Color:=clRed; chk1.Tag:=10;
         end;
         if (Zu8edit[row_num].Text='1')or(Zu8edit[row_num].Text='2') then begin        //低点
           Zu8edit[row_num].Color:=clRed;
           Zu10edit[row_num].Color:=clRed;
           chk1.Color:=clRed; chk1.Tag:=10;
         end;
         if (char(rbuf[27])='Q') then begin
           if (Zu5edit[row_num].Text='1')or(Zu5edit[row_num].Text='2') then begin      //上限
             Zu5edit[row_num].Color:=clRed;
             Zu10edit[row_num].Color:=clRed;
             chk1.Color:=clRed; chk1.Tag:=10;
           end;
           if (Zu6edit[row_num].Text='1')or(Zu6edit[row_num].Text='2') then begin      //下限
             Zu6edit[row_num].Color:=clRed;
             Zu10edit[row_num].Color:=clRed;
             chk1.Color:=clRed; chk1.Tag:=10;
           end;
           if (Zu15edit[row_num].Text='1')or(Zu15edit[row_num].Text='2') then begin     //高点2
             Zu15edit[row_num].Color:=clRed;
             Zu10edit[row_num].Color:=clRed;
             chk1.Color:=clRed; chk1.Tag:=10;
           end;
           if (Zu16edit[row_num].Text='1')or(Zu16edit[row_num].Text='2') then begin    //低点2
             Zu16edit[row_num].Color:=clRed;
             Zu10edit[row_num].Color:=clRed;
             chk1.Color:=clRed; chk1.Tag:=10;
           end;
         end;
      end;

      if chk1.Tag<9 then begin                 //模式没有报警，
         zu3edit[row_num].Color:=clWindow;
         zu5edit[row_num].Color:=clWindow;
         zu6edit[row_num].Color:=clWindow;
         zu7edit[row_num].Color:=clWindow;
         zu8edit[row_num].Color:=clWindow;

         zu15edit[row_num].Color:=clWindow;
         zu16edit[row_num].Color:=clWindow;
         if chk1.Color=clred then begin
           if chk1.Tag=0 then
              chk1.Color:=clWindow;
         end;

        if((Pos('O',Zu10edit[row_num].Text)>0)or(Pos('I',Zu10edit[row_num].Text)>0))
             and (StrToInt('0'+sysedit[5].Text)>820) then                       //高版本固件前3个字符 含有不易识别字符
          Zu10edit[row_num].Color:=clYellow
        else if(Char(rbuf[26])='O')or(Char(rbuf[26])='I') then                  //第4个字符含有不易识别字符
          Zu10edit[row_num].Color:=clYellow
        else if(Char(rbuf[25])='1')and(Char(rbuf[24])='D')and(Char(rbuf[27])='0') then              //1D01
          Zu10edit[row_num].Color:=clYellow
        else begin
          Zu10edit[row_num].Color:=clWindow;
        end;
        if(copy(Zu17edit[row_num].Text,length(Zu17edit[row_num].Text),1)=' ') then                  //备注最后字符为空格
          Zu17edit[row_num].Color:=clYellow
        else begin
          Zu17edit[row_num].Color:=clWindow;
        end;
        if (  (char(rbuf[24])='W') or (char(rbuf[24])='Z') )and( char(rbuf[27])='J' ) then begin    //WJ ZJ模式调整参数冲突则提示；
           if StrToInt(Zu3edit[row_num].Text)>0 then begin         //标准值
              if StrToInt(Zu5edit[row_num].Text)>0 then begin
                 Zu3edit[row_num].Color:=clYellow;
                 Zu5edit[row_num].Color:=clYellow;
              end;
              if StrToInt(Zu6edit[row_num].Text)>0 then begin
                 Zu3edit[row_num].Color:=clYellow;
                 Zu6edit[row_num].Color:=clYellow;
              end;
           end else  if StrToInt(Zu6edit[row_num].Text)>0 then begin         //标准值
              if StrToInt(Zu5edit[row_num].Text)>0 then begin
                 Zu5edit[row_num].Color:=clYellow;
                 Zu6edit[row_num].Color:=clYellow;
              end;
           end;
        end;
      end;
      if(chk1.tag>0) then
          chk1.Tag:=chk1.Tag-1;

    //----以下为专用------------------------------------------------------------
   
      if rbuf[3]>=52 then            //有备注参数
      begin
        s:='';
        for i:=0 to 15 do s:=s+char(rbuf[40+i]);
        Zu17edit[row_num].Text:=s;     //备注
      end;

      if(StrToInt('0'+sysedit[5].Text)<654) then Zu17edit[row_num].Text:='0';

      if modbusfuntion=$41 then   begin                       //导出步骤
         rxdDebugExportFile();
      end else  begin       //步骤监控 接收，可以更新每页步骤数量
          if ( VIRLCROWS=65)then     //自适应行数
          begin
            if (row_num>0) then dd:=row_num-1;

            if(row_num>=VIRLCROWS) then row_diff:=0 ;//没循环清空一次

            if (dd>=10)and (dd<64)
              and (Zu1edit[row_num].Text=Zu1edit[dd].Text)  and (Zu2edit[row_num].Text=Zu2edit[dd].Text)
              and (Zu3edit[row_num].Text=Zu3edit[dd].Text)  and (Zu4edit[row_num].Text=Zu4edit[dd].Text)
              and (Zu5edit[row_num].Text=Zu5edit[dd].Text)  and (Zu6edit[row_num].Text=Zu6edit[dd].Text)
              and (Zu7edit[row_num].Text=Zu7edit[dd].Text)  and (Zu8edit[row_num].Text=Zu8edit[dd].Text)
              and (Zu9edit[row_num].Text=Zu9edit[dd].Text)  and (Zu10edit[row_num].Text=Zu10edit[dd].Text)
              and (row_diff>5)      //自适应行数目
            then begin
               VIRLCROWS:=dd;
               Zu1edit[row_num].Text:='';
               Zu2edit[row_num].Text:='';
               Zu3edit[row_num].Text:='';
               Zu4edit[row_num].Text:='';
               Zu5edit[row_num].Text:='';
               Zu6edit[row_num].Text:='';
               Zu7edit[row_num].Text:='';
               Zu8edit[row_num].Text:='';
               Zu9edit[row_num].Text:='';
               Zu10edit[row_num].Text:='';
               Zu11edit[row_num].Text:='';
               Zu12edit[row_num].Text:='';
               Zu13edit[row_num].Text:='';
               Zu14edit[row_num].Text:='';
               Zu15edit[row_num].Text:='';
               Zu16edit[row_num].Text:='';
               row_num:=1;
            end   else  if (dd<10)then begin       //行区别次数
               row_diff:=row_diff+1;
            end;
          end;
      end;
    end;
  end;
  procedure rxdDebugSysPara();
  var
    i,j:Integer;

  begin
    with Form_line do begin
      sysedit[0].Text:=IntToStr((rbuf[5]));
      for i:=1 to 13 do begin
          sysedit[i].Text:=IntToStr((rbuf[4+2*i]*256+rbuf[5+2*i]));
      end;
      sysedit[13].Text:=char(rbuf[31])+char(rbuf[30])+char(rbuf[33])+char(rbuf[32])+char(rbuf[35])+char(rbuf[34]);

      if (StrToInt('0'+sysedit[5].Text)<600) then begin         //51八位内核MCU系统
        Form_line.edt130.Text:=sysedit[5].Text;
        C1.Enabled:=False;     //编辑菜单下6个高级功能
        I2.Enabled:=False;
        I3.Enabled:=False;
        D5.Enabled:=False;
        N33.Enabled:=False;
        C4.Enabled:=False;

         M3.Enabled:=False;   //查询菜单下3个高级功能
        N52.Enabled:=False;
        P5.Enabled:=False;
      end else begin                                            //32位内核MCU系统
        if rbuf[41]>=10 then
          Form_line.edt130.Text:=sysedit[5].Text+'.' + IntToStr(rbuf[41])   //固件版本.修订    rbuf[40]*256+
        else
          Form_line.edt130.Text:=sysedit[5].Text+'.0'+ IntToStr(rbuf[41]);

        if( (StrToFloat(Form_line.edt130.Text)>=640.01)and (StrToFloat(Form_line.edt130.Text)<660.00) ) then begin   //ACT4-5定制，2021-11-22改
          C1.Enabled:=False;     //编辑菜单下6个高级功能
          I2.Enabled:=False;
          I3.Enabled:=False;
          D5.Enabled:=False;
          N33.Enabled:=False;
          C4.Enabled:=False;

          M3.Enabled:=False;   //查询菜单下3个高级功能
          N52.Enabled:=False;
          P5.Enabled:=False;

        end else begin
          C1.Enabled:=TRUE;     //编辑菜单下6个高级功能
          I2.Enabled:=TRUE;
          I3.Enabled:=TRUE;
          D5.Enabled:=TRUE;
          N33.Enabled:=TRUE;
          C4.Enabled:=TRUE;
        end;
        if (StrToFloat(Form_line.edt130.Text)>935.08)
          or( (StrToFloat(Form_line.edt130.Text)>=881.33)and (StrToFloat(Form_line.edt130.Text)<900.00) ) then begin
          M3.Enabled:=True;             //查询菜单下3个高级功能
          N52.Enabled:=true;
          p5.Enabled:=true;
        end else begin
          M3.Enabled:=False;            //查询菜单下3个高级功能
          N52.Enabled:=False;
          P5.Enabled:=False;
        end;
      end;

      if edt45.Text<>IntToStr((rbuf[36]*256+rbuf[37])) then
      begin
        edt45.Text:=IntToStr((rbuf[36]*256+rbuf[37]));     //当前步骤
        if  (rbuf[36]*256+rbuf[37])=0 then  Panel1.VertScrollBar.Position   := 0
        else if  ( (Panel1.VertScrollBar.Position+15*strtoint(Form_line.edt121.Text))<( ((rbuf[36]*256+rbuf[37]-1) mod VIRLCROWS )*strtoint(Form_line.edt121.Text) ) ) //大于15行
            or ( Panel1.VertScrollBar.Position>( (rbuf[36]*256+rbuf[37]-1) mod VIRLCROWS )*strtoint(Form_line.edt121.Text) ) then     //起始位置大
        begin
          Panel1.VertScrollBar.Position   := ((rbuf[36]*256+rbuf[37]-1) mod VIRLCROWS )*strtoint(Form_line.edt121.Text);  //20;
        end;
      end;
      for i:=1 to  VIRLCROWS do
        zu2edit[i].color:=clwindow; //颜色归位

      i:=(rbuf[36]*256+rbuf[37]);                                               //当前步骤
      if i>0 then i:=i-1;
      zu2edit[1+ i mod VIRLCROWS ].Color:=clskyblue;      //名称选中

      if(Copy(zu10edit[1+ i mod VIRLCROWS ].Text,2,1)='K')or(Copy(zu10edit[1+ i mod VIRLCROWS ].Text,2,1)='O')or(Copy(zu10edit[1+ i mod VIRLCROWS ].Text,2,1)='S')then
      begin
         Form_line.chk8.Color:=clYellow;
         syslabel[7].Color:=clYellow;          //系统参数值学习参数颜色提醒；
         syslabel[9].Color:=clYellow;
         syslabel[10].Color:=clYellow;
         sysedit[7].Color:=clYellow;
         sysedit[9].Color:=clYellow;
         sysedit[10].Color:=clYellow;
      end else begin
         if Form_line.chk8.Color<>clLime then Form_line.chk8.Color:=clBtnFace;
         if syslabel[7].Color<>clLime then syslabel[7].Color:=clBtnFace;          //系统参数值学习参数颜色回归；
         if syslabel[9].Color<>clLime then syslabel[9].Color:=clBtnFace;
         if syslabel[10].Color<>clLime then syslabel[10].Color:=clBtnFace;
         sysedit[7].Color:=clWindow;
         sysedit[9].Color:=clWindow;
         sysedit[10].Color:=clWindow;
      end;
      if(StepNowFresh>0) then row_num:=StepNowFresh;//否则会继续读状态和勾选参数 else row_num:=0;
      StepNowFresh:=0;
      
      FormStepCopy.edt1.Text:=IntToStr((rbuf[38]*256+rbuf[39]));    //固件源码<-联板拷贝单PCS步骤数
      FormStepCopy.edt2.Text:=IntToStr((rbuf[40]*256+rbuf[41]));    //修订版本<-联板拷贝PIN偏移
      FormStepCopy.edt5.Text:=IntToStr(rbuf[42]);    //nc<-联板拷贝开短路次数
      FormStepCopy.edt3.Text:=IntToStr(rbuf[43]);    //绿灯偏移<-联板拷贝 总电路板数

      FormStepCopy.edt4.Text:=IntToStr((rbuf[44]*256+rbuf[45]));    //联板拷贝 绿灯偏移
      if ( VIRLCROWS=66)then     //固件版本适应行数
      begin
         if ( StrToInt('0'+sysedit[5].Text)>750 )         //大于V7.50
            or( ( StrToInt('0'+sysedit[5].Text)>=670 )and( StrToInt('0'+sysedit[5].Text)<700 ) )then  //兼容V6.70~6.99
         begin
           VIRLCROWS:=50;        //默认值
         end else  begin
           VIRLCROWS:=10;
           chk5.Enabled:=True;
           chk5.Caption:='双启动按键';
         end;
      end;
    end;
  end;
  procedure rxdDebugExportSysPara();
  var
    i,j:Integer;
    sss:string;
  begin
    with Form_line do begin
      sysedit[0].Text:=IntToStr((rbuf[5]));
      for i:=1 to 13 do begin
          sysedit[i].Text:=IntToStr((rbuf[4+2*i]*256+rbuf[5+2*i]));
      end;
      sysedit[13].Text:=char(rbuf[31])+char(rbuf[30])+char(rbuf[33])+char(rbuf[32])+char(rbuf[35])+char(rbuf[34]);
      try
        //save_file_name :=Lbl29.caption;
        //assignfile(f, save_file_name);      //ExtractFilePath(application.ExeName)
        //Append(exportF); //rewrite(f);

        SLINE_head:='';
        s := '$对比度';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '$找点数';  //自检s
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
         s := '$找点阀';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := 'O/S点数';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := 'O/S阀值';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '@V版本';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '485地址';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := 'O/S总时';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := 'F1点';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := 'O/S充时';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := 'F2点O/S';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := 'F2点时';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := 'S机台号';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := 'N机种名';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '$使能位';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;

        s := '@C版本';//-步骤数';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
         s := '@R修订';//-P偏移';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '@OS总板';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '@绿灯偏';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;

        writeln(exportF, SLINE_head);

        SLINE_head:='';
        s := IntToStr((rbuf[5]));     //对比度 rbuf[4]*256+
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;   SLINE_head:=SLINE_head+s;
        s := IntToStr((rbuf[6]*256+rbuf[7]));
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;   SLINE_head:=SLINE_head+s;
        s := IntToStr((rbuf[8]*256+rbuf[9]));
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;    SLINE_head:=SLINE_head+s;
        s := IntToStr((rbuf[10]*256+rbuf[11]));
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;     SLINE_head:=SLINE_head+s;
        s := IntToStr((rbuf[12]*256+rbuf[13]));
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;    SLINE_head:=SLINE_head+s;
        s := IntToStr((rbuf[14]*256+rbuf[15]));
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;     SLINE_head:=SLINE_head+s;
        s := IntToStr((rbuf[16]*256+rbuf[17]));
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;    SLINE_head:=SLINE_head+s;
        s := IntToStr((rbuf[18]*256+rbuf[19]));
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;     SLINE_head:=SLINE_head+s;
        s := IntToStr((rbuf[20]*256+rbuf[21]));
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;     SLINE_head:=SLINE_head+s;
        s := IntToStr((rbuf[22]*256+rbuf[23]));
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;     SLINE_head:=SLINE_head+s;
        s := IntToStr((rbuf[24]*256+rbuf[25]));
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;     SLINE_head:=SLINE_head+s;
        s := IntToStr((rbuf[26]*256+rbuf[27]));
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;     SLINE_head:=SLINE_head+s;

        s := IntToStr((rbuf[28]*256+rbuf[29]));
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;      SLINE_head:=SLINE_head+s;
        s := char(rbuf[31])+char(rbuf[30])+char(rbuf[33])+char(rbuf[32])+char(rbuf[35])+char(rbuf[34]);
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;     SLINE_head:=SLINE_head+s;
        s := IntToStr((rbuf[4]));     //使能位 rbuf[4]*256+
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;   SLINE_head:=SLINE_head+s;

        s := IntToStr((rbuf[38]*256+rbuf[39]));
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;      SLINE_head:=SLINE_head+s;

        if rbuf[41]>=10 then
          s:='v'+IntToStr(rbuf[14]*256+rbuf[15])+'.' + IntToStr(rbuf[41])   //rbuf[40]*256+固件版本.修订    rbuf[40]*256+
        else
          s:='v'+IntToStr(rbuf[14]*256+rbuf[15])+'.0'+ IntToStr(rbuf[41]);

        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;      SLINE_head:=SLINE_head+s;
        s := IntToStr((rbuf[42]*256+rbuf[43]));
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;      SLINE_head:=SLINE_head+s;
        s := IntToStr((rbuf[44]*256+rbuf[45]));
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;      SLINE_head:=SLINE_head+s;

        writeln(exportF, SLINE_head);
        datetimetostring(sss,'yyyy-mm-dd hh:nn:ss',Now);
        write(exportF, 'Open/Short拓扑群，一次短路群用()表示，二次短路群用[]表示。----导出时间---'+sss+'---');
        row_num:=20+VIRLCOFFSET;
      finally
        //Closefile(f);
      end;
    end;
  end;


  procedure rxdMonitorUppLow();
  var
    i,j:Integer;
    SLINE_upper2,SLINE_lower2:string;
    procedure UpperAdd();
    begin
      with Form_line do begin
        if (Copy(ModeZu[i],2,2)='XH')or(Copy(ModeZu[i],2,2)='YH') then begin
          UpperZu[i]:=IntToStr(dd);
          SLINE_upper:= SLINE_upper+'0x'+inttohex(word(dd),2);
          SLINE_upper2:= SLINE_upper2+'0x'+inttohex(word(dd),2);
        end else begin
          UpperZu[i]:=copy(floattostr(ff),0,7);   ////最早从FCT主板取得//if UpperZu[i]='' then 仅一次上限值仅一次不可取，因FCT主板会更换程序
          if Copy(ModeZu[i],2,1)='S' then begin
            SLINE_upper:= SLINE_upper+'>='+copy(floattostr(ff),0,7);
            SLINE_upper2:= SLINE_upper2+'>='+copy(floattostr(ff),0,7);
          end else if (Copy(ModeZu[i],2,1)='K')or(Copy(ModeZu[i],2,1)='O') then begin
            SLINE_upper:= SLINE_upper+'-' ;
            SLINE_upper2:= SLINE_upper2+'-' ;
          end else begin
            SLINE_upper:= SLINE_upper+copy(floattostr(ff),0,7);
            SLINE_upper2:= SLINE_upper2+copy(floattostr(ff),0,7);
          end;
        end;
        SLINE_upper2:= SLINE_upper2+#9;
        SLINE_upper:= SLINE_upper+',';      //+copy(floattostr(ff),0,7)
      end;
    end;
    procedure lowerAdd();
    begin
      with Form_line do begin
        if (Copy(ModeZu[i],2,2)='XH')or(Copy(ModeZu[i],2,2)='YH') then begin
          LowerZu[i]:=IntToStr(dd);
          SLINE_lower:= SLINE_lower+'0x'+inttohex((word(dd)),2);
          SLINE_lower2:= SLINE_lower2+'0x'+inttohex((word(dd)),2);
        end else begin
          if copy(floattostr(ff),0,7)='30009' then         //浮动下限转为0；
            ff:=0;
          LowerZu[i]:=copy(floattostr(ff),0,7);  //最早从FCT主板取得//if LowerZu[i]='' then  仅一次下限值仅一次不可取，因FCT主板会更换程序
          if Copy(ModeZu[i],2,1)='S' then begin
            SLINE_lower:= SLINE_lower+'-' ;
            SLINE_lower2:= SLINE_lower2+'-';
          end else if (Copy(ModeZu[i],2,1)='K')or(Copy(ModeZu[i],2,1)='O') then begin
            SLINE_lower:= SLINE_lower+'<='+copy(floattostr(ff),0,7);
            SLINE_lower2:= SLINE_lower2+'<='+copy(floattostr(ff),0,7);
          end else  begin
            SLINE_lower:= SLINE_lower+copy(floattostr(ff),0,7);
            SLINE_lower2:= SLINE_lower2+copy(floattostr(ff),0,7);
          end;
        end;
        SLINE_lower2:= SLINE_lower2+#9;
        SLINE_lower:= SLINE_lower+',';       //+copy(floattostr(ff),0,7)
      end;
    end;
  begin
    with Form_line do begin
      temp:=((tt-5)div 4) ;

      SLINE_upper2:='-upper-'+#9;
      SLINE_lower2:='-lower-'+#9;

      begin
        if edt62.text='25' then begin
          SLINE_upper := 'Upper limit(上限）------->'+',N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,';
          SLINE_lower := 'Lower limit(下限）------->'+',N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,';
        end else if edt62.text='24' then begin       //tesla车间
            SLINE_upper := ''+','+ 'LSL'+',';        //代表上面一行门限值：tes专用下限值；；
            SLINE_lower := ''+','+ 'USL'+',';
        end;
      end;

      if NameCount<1 then temp:=1 else temp:=NameCount;
      if edt74.text='1' then begin       //UponePCSName=1  [联板]使能单一PCS名称单位、上下限上传
        for i:=1 to temp do
        begin
          dd:=(rbuf[ 4+4*OnePCSNamePos[i] ]*256+rbuf[5+4*OnePCSNamePos[i]]);    //从0开始...
          if StdValZu[OnePCSNamePos[i]+1]='30005' then begin
            if word(dd)<65000 then
              ff:=word(dd) / strtofloat(ScaleZu[i])
            else
              ff:=word(dd);
          end else  begin
            if dd<=-30000 then
              ff:=dd
            else if dd<30000 then
              ff:=dd / strtofloat(ScaleZu[i])
            else
              ff:=dd;
          end;
          UpperAdd;

          dd:=(rbuf[6+4*OnePCSNamePos[i]]*256+rbuf[7+4*OnePCSNamePos[i]]);
          if StdValZu[OnePCSNamePos[i]+1]='30005' then begin
            if word(dd)<65000 then
              ff:=word(dd) / strtofloat(ScaleZu[i])
            else
              ff:=word(dd);
          end else  begin
            if dd<=-30000 then
              ff:=dd
            else if dd<30000 then
              ff:=dd / strtofloat(ScaleZu[i])
            else
              ff:=dd;
          end;
          LowerAdd;

        end;
      end else begin            
        for i:=1 to temp do
        begin
          dd:=(rbuf[4+4*i-4]*256+rbuf[5+4*i-4]);
          if StdValZu[i]='30005' then begin
            if word(dd)<65000 then
              ff:=word(dd) / strtofloat(ScaleZu[i])
            else
              ff:=word(dd);
          end else  begin
            if dd<=-30000 then
              ff:=dd
            else if dd<30000 then
              ff:=dd / strtofloat(ScaleZu[i])
            else
              ff:=dd;
          end;
          UpperAdd;
        
          dd:=(rbuf[6+4*i-4]*256+rbuf[7+4*i-4]);
          if StdValZu[i]='30005' then begin
            if word(dd)<65000 then
              ff:=word(dd) / strtofloat(ScaleZu[i])
            else
              ff:=word(dd);
          end else  begin
            if dd<=-30000 then
              ff:=dd
            else if dd<30000 then
              ff:=dd / strtofloat(ScaleZu[i])
            else
              ff:=dd;
          end;
          lowerAdd;

        end;
      end;
      if edt62.Text='25' then begin
        mmo2String[4]:= SLINE_upper;
        mmo2String[5]:= SLINE_lower;
      end else if(grp1.Tag<>2)or(edt81.Text='0') then begin      //CSVen<>2:tesla低压四线 or 不是config配置（conductorNUm:导线数=0） ，（仅TESLA专用设置不显示上限、下限，只能在debug里看，保存的是由config指定！）
        if StrToInt(edt75.Text)>0 then begin                          //有bug，会被下面覆盖！启用打!，则关闭上下限显示；
          mmo2String[4]:= '';
          mmo2String[5]:= '';
        end else begin
          if Readchis('result','mmo2UpperLine')='11' then begin
            mmo2.Lines[11]:= Copy(SLINE_upper,0,1024);//SLINE_upper;
            mmo2.Lines[12]:= Copy(SLINE_lower,0,1024);//SLINE_lower;
          end else begin
            mmo2.Lines[4]:= Copy(SLINE_upper,0,1024);//SLINE_upper;
            mmo2.Lines[5]:= Copy(SLINE_lower,0,1024);//SLINE_lower;
          end;
        end;
        mmo2String[4]:= SLINE_upper;
        mmo2String[5]:= SLINE_lower;
      end;

      begin        //       for i:=1 to NameCount do      //上传名称个数相同的单位
        mmo2.Lines[4]:= Copy(SLINE_upper2,0,1024);
        mmo2.Lines[5]:= Copy(SLINE_lower2,0,1024);
      end;

      if (inportNewFileflag mod 1000)=0 then begin                  //新收到数据并生成文件后可再更新文件头（否则会造成前一个log文件表头异常！！导入新文件后不可更新文件头、可加载上下限）；
        FreshDatalogFromMemo(5);  
      end;
      inportNewFileflag:=inportNewFileflag mod 1000;        //加载中标志取消；
      
      if Form_line.sbtbtn1.Visible then
        Form_line.sbtbtn1.Enabled:=true;
      if(label11.Caption='Loading...') then begin
        isvnsgmntclck1.Enabled:=False;
        Label11.Font.Color:=clWindowText; Label11.Font.Size:=150;   Lbl169.Caption:=''; lbl169.Font.Size:=10;
        label11.Caption:='Ready';    //参数加载完成！
        if(modbusfun06dat=$00F6) then       //已加载完成，禁止重复加载！
          modbusfun06dat:=$0000;

        if (Readchis('Server','MESprogram')<>'')or(Readchis('Server','MESprogramdefault')<>'') then
          Form_line.btn56.Enabled:=true;
      end;
      lbl178.Caption:='';
      FormLogHead.close;    //实际loghead显示不会执行到此处吗？？？
    end;
  end;
  procedure rxdMonitorMCUstatue();
  var
    i,j,k:Integer;
    stime:string;
  begin
    with Form_line do begin

      begin
        case rbuf[4] of
          $00:                      //复位
          begin
            if (strtoint(edt145.Text)mod 1000)>0 then begin        //group组
              for j:=0 to (strtoint(edt145.Text)mod 1000)*StrToInt(edt59.Text)*strtoint(edt60.Text)-1 do      //搜索有输出结果的连片序号
              begin
                 if Rleds[j].Active= TRUE then     //指示灯需要关闭
                 begin
                   Rleds[j].Active:=false;
                 end;
              end;
            end else begin
              for j:=0 to StrToInt(edt59.Text)*strtoint(edt60.Text)-1 do      //搜索有输出结果的连片序号
              begin
                 if Rleds[j].Active= TRUE then     //指示灯需要关闭
                 begin
                   Rleds[j].Active:=false;
                 end;
              end;
            end;
            if Label7.hint='11' then begin     //双工位同步系统
               iledround1.Active:=false;
               iledround2.Active:=false;
               ilabel1.Font.Color:=clwindowtext;
               ilabel1.Caption:='Reandy';
               ilabel2.Font.Color:=clwindowtext;
               ilabel2.Caption:='Reandy';
            end;
            mmo2String[17]:='';
            isvnsgmntclck1.Enabled:=False;
            ts7.Tag:=0;         //多连片测试复位
            Lbl140.Font.Color:=clDefault;
            btn33.Hint:=''; btn34.Hint:='';

            Label11.Font.Color:=clWindowText; Label11.Font.Size:=150; Lbl169.Caption:='';lbl169.Font.Size:=10;
            Label11.Caption:='Ready';
            if Readchis('Model Para','BarCodeEdit')<>'' then begin
              edit1.Clear;
              if StrToInt(Readchis('Model Para','BarCodeEdit'))<1000 then  begin
                edit1.Enabled:=true;
                if TabSheet2.TabVisible and grp36.Visible  then begin
                  if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then edit1.SetFocus;
                  edit1.SelectAll;
                end;
              end;

            end;
            if Readchis('Model Para','StepLogen')='1' then begin       //使能LOG 指示
                mmo15.Lines.Add('---'+char(9)
                    +datetostr(Date)+' '+TimeToStr(Time) +char(9)
                    +'Ready'+char(9)+char(9)
                    );
              ts12.TabVisible:=True;
            end;


          end;
          $01:         //开始测试，执行很多复位需要的操作！
          begin
            if StrToInt(edt145.Text)<1000 then begin         //group的千位为0则不持续显示；
              if (strtoint(edt145.Text)mod 1000)>0 then begin
                for j:=0 to (strtoint(edt145.Text)mod 1000)*StrToInt(edt59.Text)*strtoint(edt60.Text)-1 do      //搜索有输出结果的连片序号
                begin
                   if Rleds[j].Active= TRUE then     //指示灯需要关闭
                   begin
                     Rleds[j].Active:=false;
                   end;
                end;
              end else begin
                for j:=0 to StrToInt(edt59.Text)*strtoint(edt60.Text)-1 do      //搜索有输出结果的连片序号
                begin
                   if Rleds[j].Active= TRUE then     //指示灯需要关闭
                   begin
                     Rleds[j].Active:=false;
                   end;
                end;
              end;
            end;
            if Label7.hint='11' then begin     //双工位同步系统
               iledround1.Active:=false;
               iledround2.Active:=false;
               ilabel1.Font.Color:=clwindowtext;
               ilabel1.Caption:='Reandy';
               ilabel2.Font.Color:=clwindowtext;
               ilabel2.Caption:='Reandy';
            end;
                        
            if edt77.Text<>'' then begin              //使能PN料号，如下两个字段如果有定义则使用定义的固定字段。
              if Readchis(edt77.Text,'BarCodeLeft')<>'' then  edit1.Text:=Readchis(edt77.Text,'BarCodeLeft');
              if Readchis(edt77.Text,'BarCodeRight')<>'' then  medit1.Text:=Readchis(edt77.Text,'BarCodeRight');
            end;
            if Readchis('Result','CloseErrlist')='' then begin           //启动之后清空数据；没有禁止ERRlist显示；
              lv1.Clear;
              lv1.Items.Clear;
            //  mmo4.Clear;
            end;
            if edt62.Text='25' then begin             //MES系统使能，所有标志初始化！
              edt168.Text:='0';           //重压次数置零；
              chk25.Color:=clWindow;
              chk26.Color:=clWindow;
              chk27.Color:=clWindow;
              chk28.Color:=clWindow;
              chk25.Checked:=False;                  //没勾选代表没有执行过；
              chk26.Checked:=False;
              chk27.Checked:=False;
              chk28.Checked:=False;
              chk25.Caption:='ETP1条码校验';
              chk26.Caption:='ETP2条码校验';
              chk27.Caption:='ETP3条码校验';
              chk28.Caption:='ETP4条码校验';
              lbl73.Caption:='#1';
              lbl185.Caption:='#2';
              lbl186.Caption:='#3';
              lbl187.Caption:='#4';
            end;
            isvnsgmntclck1.Enabled:=True;
            isvnsgmntclck1.ResetToZero;
            datetimetostring(stime,'yyyy-mm-dd hh:nn:ss',Now);
            isvnsgmntclck1.Hint:=stime;
            edt77.tag:=0;
            Label11.Tag:=0;                   //以5mS为间隔记录测试时长
            Lbl177.Tag:=999999;
            ts7.Tag:=0;                       //多连片测试复位
            lbl193.Color:=clBtnFace;          //标准板错误提示
            Lbl140.Font.Color:=clDefault;
            btn33.Hint:=''; btn34.Hint:='';
            
            mmo2String[10]:='';                   //开始时清空，如果收不到开始命令，会异常吗？ //if edt47.Text<>'2'then  //=2:定制只收一次数据；
            mmo2String[17]:=''; 
            if Label11.Caption='Loading...' then begin
               begin
                  lbl178.Caption:='warning：请按<复位>后等待,或者重启软件............';
               end;
            end else begin

              Label11.Font.Color:=clGreen;
              Label11.Caption:='Testing...';Label11.Font.Size:=150;   Lbl169.Caption:=''; lbl169.Font.Size:=10;
              if((edt62.text='25'))
                and(StrToInt(edt59.Text)*strtoint(edt60.Text) >1) then begin
                for j:=1 to StrToInt(edt59.Text)*strtoint(edt60.Text) do
                  Redits[j-1].Value:=IntToStr(j);
              end;
            end;
            GroupNowNum:=0;
            stat1.Panels[4].Text := '--';
            if Form_line.edt82.Text='99' then begin                         //enPN
              if (Length(Form_line.edt77.Text)<2)or(FormPN.cbb1.Tag=0) then begin
                 btn26Click(Form_line);
              end;
            end;



            if Readchis('result','TipCodeEnd')<>'' then begin
             // s:= Readchis('result','TipCodeEnd');
                h := FindWindow(nil,PAnsiChar(AnsiString ('TipCodeEnd') ) );
               if h>0 then
               begin
                  PostMessage(h,WM_SYSCOMMAND, SC_CLOSE,0);
               end;
            end;
            if Readchis('Model Para','StepLogen')='1' then begin //使能LOG
                mmo15.Lines.Add('---'+char(9)
                    +datetostr(Date)+' '+TimeToStr(Time)+char(9)
                    + 'Start test...'+char(9)+char(9)
                    );
                ts12.TabVisible:=True;
            end;



            if Form_line.edt82.Text='99' then begin                      //enPN
              if (Length(Form_line.edt77.Text)<2)or(FormPN.cbb1.Tag=0) then begin   //只有一个字母则报错！
                 Label11.Caption:='PN错误'; Label11.Font.Color:=clRed;
                   Form_line.edt77.Color:=clRed;
              end;
            end;
            
          end;
          $02:         //--短路测试

          begin
          Label11.Font.Color:=clWindowText;
          Label11.Caption:='Short Test';

          end;
          $03:         //--开路测试
          begin

          Label11.Font.Color:=clWindowText;
          Label11.Caption:='Open Test';
          end;
          $04:         //--步骤测试
          begin
          Label11.Font.Color:=clWindowText;
          Label11.Caption:='Step Test';

          end;
          $05:         //步骤测试 log显示
          begin
            temp:=0;
            if(rbuf[16]>=$30)and(rbuf[16]<=$39) then begin
               temp:=rbuf[16]-$30;
            end;
            if(rbuf[17]>=$30)and(rbuf[17]<=$39) then begin
               temp:=10*temp;
               temp:=temp+(rbuf[17]-$30);
            end;
            if(rbuf[18]>=$30)and(rbuf[18]<=$39) then begin
               temp:=10*temp;
               temp:=temp+(rbuf[18]-$30);
            end;
            //禁止清空 if temp=0 then mmo15.Clear;
            dd:=rbuf[19]*256+rbuf[20];     //测量值

            s:=Readchis('Model Para','StepLog'+inttostr(temp));
            if s='' then     //没有在config里配置log名称
              for i:=21 to 36 do s:=s+char(rbuf[i]);  //显示字符串
            mmo15.Lines.Add( char(rbuf[15])+char(rbuf[16])+char(rbuf[17])+char(rbuf[18])
                    +char(9)+datetostr(Date)+' '+TimeToStr(Time)
                    +char(9)+ s +char(9)+char(9)
                    );
            ts12.TabVisible:=True;
            isvnsgmntclck1.Enabled:=True;
            
            Label11.Font.Color:=clGreen;

            Label11.Font.Size:=80; lbl169.Caption:=s+'  ';lbl169.Font.Size:=60;                     //补加两个空格防止显示不完整 //Label11
          end;

          108,
          $08:         //----步骤测试 PASS
          begin
            isvnsgmntclck1.Enabled:=False;
            Label11.Font.Color:=clLime;
            if rbuf[4]=108 then begin
              Label11.Font.Size:=80;
              Lbl169.Caption:='pls add 2BU0 on last step'; lbl169.Font.Size:=60;
            end;
            Label11.Caption:='PASS';
            if (Readchis('Result','CountFromMCU')='1')or(Readchis('Result','CountFromMCU')='on')or(Readchis('Result','CountFromMCU')='ON') then begin
              PassCount:=rbuf[5]*$1000000+rbuf[6]*$10000+rbuf[7]*$100+rbuf[8];
              NGCount:=rbuf[9]*$1000000+rbuf[10]*$10000+rbuf[11]*$100+rbuf[12];

              edt4.Text:=inttostr(PassCount+NGcount);
              edt63.Text:= inttostr(PassCount);
              edt64.Text:= inttostr(NGCount);
              if (PassCount+NGcount)>0 then  begin
                edt65.Text:= floattostrF((PassCount*100)/(PassCount+NGcount),ffFixed,5,2);//inttostr((PassCount*100) div (PassCount+NGcount));
                edt66.Text:= floattostrF((NGCount*100)/(PassCount+NGcount),ffFixed,5,2);//inttostr((NGCount*100) div (PassCount+NGcount) );
              end else begin
                edt65.Text:='0';        //统计清零
                edt66.Text:='0';
              end;

            end;



          end;
          109,
          $09:         //----步骤测试 NG
          begin
            isvnsgmntclck1.Enabled:=False;
            Label11.Font.Color:=clRed;
            if rbuf[4]=109 then begin
              Label11.Font.Size:=80;
              Lbl169.Caption:='pls add 2BU0 on last step';lbl169.Font.Size:=60;
            end;
            Label11.Caption:='FAIL';
            if (Readchis('Result','CountFromMCU')='1')or(Readchis('Result','CountFromMCU')='on')or(Readchis('Result','CountFromMCU')='ON') then begin
              PassCount:=rbuf[5]*$1000000+rbuf[6]*$10000+rbuf[7]*$100+rbuf[8];
              NGCount:=rbuf[9]*$1000000+rbuf[10]*$10000+rbuf[11]*$100+rbuf[12];
              
              edt4.Text:=inttostr(PassCount+NGcount);
              edt63.Text:= inttostr(PassCount);
              edt64.Text:= inttostr(NGCount);
              if (PassCount+NGcount)>0 then  begin
                edt65.Text:= floattostrF((PassCount*100)/(PassCount+NGcount),ffFixed,5,2);//inttostr((PassCount*100) div (PassCount+NGcount));
                edt66.Text:= floattostrF((NGCount*100)/(PassCount+NGcount),ffFixed,5,2);//inttostr((NGCount*100) div (PassCount+NGcount) );
              end else begin
                edt65.Text:='0';        //统计清零
                edt66.Text:='0';
              end;

            end;


          end;
        end;
        if(rbuf[4]<>$99) then       //开机初始化，不返回
        begin
          //sbuf[1]:=1;
          sbuf[2]:=5;
          sbuf[3]:=$50;
          sbuf[4]:=$02;
          sbuf[5]:=$FF;
          sbuf[6]:=$00;
          send_len:=6;
          try
              send_crcdata(send_len,2);
      //          nsend:=nsend+1;
      //          fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
          except
              messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
          end;
        end;
      end;
    end;
  end;
  procedure rxdMonitor1BT0();
  var
    i,j:Integer;
  begin
    with Form_line do begin
      begin
        s:='';    Lbl177.Tag:=Label11.Tag;
        if (Label7.hint='11')and(rbuf[tt]=2) then begin   //双条码 ，最后一个字节标志上传条码应填入位置。
          if(tt>=6) then
            begin
              mEdit1.Color:=clWindow;
              for i:=4 to (tt-2) do
              begin
                if (rbuf[i]=$0D)and(rbuf[i+1]=$0A) then
                  break;
                if (rbuf[i]<>$0D)and(rbuf[i]<>$0A) then
                  s:=s+char(rbuf[i]);
              end;
              medit1.Text:=Copy(s+'                                                  ',0,Length(medit1.Text));    //+50个空格，从而左对齐

            end
          else  medit1.Text:='';
        end else begin
          if(tt>=6) then  begin
            Edit1.Color:=clWindow;
            for i:=4 to (tt-2) do begin
              if (rbuf[i]=$0D)and(rbuf[i+1]=$0A) then
                break;
              if (rbuf[i]<>$0D)and(rbuf[i]<>$0A) then
                s:=s+char(rbuf[i]);
            end;
            
            edit1.Text:=Copy(s+'                                                  ',0,Length(edit1.Text));    //+50个空格，从而左对齐
            if(Form_line.edt62.text='24')or(Form_line.edt62.text='25')then begin
              if(StrToInt(edt59.Text)*strtoint(edt60.Text) >1) then begin     //jaguar多连片
               if ( rbuf[tt]=99 ) then begin     //载板条码
                  edt163.Visible:=True;
                  lbl133.Font.Size:=25;
                  lbl133.Caption:='治具载板条形码ID';
                  lbl133.Left:=edt163.Left;
                  lbl133.Top:=edt163.Top+30;

                  edt163.Text:=Trim(edit1.Text);
                  if Length(edt163.Text)<5 then
                    edt163.Color:=clRed
                  else
                    edt163.Color:=clWindow;
               end else if ( rbuf[tt]>0 )and( rbuf[tt]<=StrToInt(edt59.Text)*strtoint(edt60.Text) ) then
                  Redits[rbuf[tt]-1].Value:=Trim(edit1.Text);                       //Redits[]从0开始
              end else begin
                Redits[0].Value:=Trim(edit1.Text);     //单PCS条码
              end;
            end;
          end else
            edit1.Text:='';
        end;
        if ( strtoint(edt111.Text)>0 ) and ( TripCount>=strtoint(edt111.Text) ) then begin     //达到最大测试次数
           if(Label11.Caption<>'STOP!') then begin     //没有弹出对话框
               Label11.Caption:='STOP!';
               Label11.Font.Color:=clRed;
               edit1.Clear;

              if MessageDlg('测试次数达到最大值！是否重新计数？',  mtWarning, mbOKCancel, 0) = mrOk then
              begin
                  edt111.Color:=clWindow;
                  TripCount:=0;
                  lbl134.Caption:=inttostr(tripcount);
                  Label11.Caption:='Ready'; Label11.Font.Size:=150;    Lbl169.Caption:=''; lbl169.Font.Size:=10;
                  Label11.Font.Color:=clWindowText;

              end else begin

                 Label11.Caption:='-STOP-';
                 edt111.Color:=clred;
                 Form_line.grp31.Visible:=True;
              end;
           end;
        end
         ///------判断条码是否正确
        else if (Label7.hint<>'11') and      //非双条码系统
              ( ( Trim(edit1.Text)='' )     //条形码为空，或者长度不够
               or( (lbl140.Color=clgreen)and(Trim(edit1.Text)=lbl140.Caption) )     //连续条码重复，报错！
               or( (edt7.Text<>'0')and ( Length(Trim(edit1.Text))<>strtoint(edt7.Text) ) )     //指定长度，且不相等
               or( (edt8.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt8.Text),strtoint(edt9.Text)))=0 ) )         //指定子串，且不存在
               or( (edt182.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt182.Text),strtoint(edt9.Text)))=0 ) )     //指定子串2，且不存在
               or( (edt183.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt183.Text),strtoint(edt9.Text)))=0 ) )     //指定子串3，且不存在
               or( (edt184.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt184.Text),strtoint(edt9.Text)))=0 ) )     //指定子串4，且不存在
               or( (edt181.Text<>'0')and (not matchesmask(copy(Trim(edit1.Text),strtoint(edt181.Text),strtoint(edt9.Text)),edt10.Text) ) )     //指定通配符子串，且不存在
              )  then    
        begin
            isvnsgmntclck1.Enabled:=False;      //条码错误，默认测试停止！
            if( (lbl140.Color=clgreen)and(Trim(edit1.Text)=lbl140.Caption) ) then    //连续条码重复，报错！
              lbl140.Font.Color:=clRed;

            if ( (edt7.Text<>'0')and ( Length(Trim(edit1.Text))<>strtoint(edt7.Text) ) ) then
            begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt7.Color:=clred;
            end;
            if ( (edt8.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt8.Text),strtoint(edt9.Text)))=0 ) )
            then begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt8.Color:=clred;edt10.Color:=clred;
            end;
            if ( (edt182.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt182.Text),strtoint(edt9.Text)))=0 ) )
            then begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt182.Color:=clred;edt10.Color:=clred;
            end;
            if ( (edt183.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt183.Text),strtoint(edt9.Text)))=0 ) )
            then begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt183.Color:=clred;edt10.Color:=clred;
            end;
            if ( (edt184.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt184.Text),strtoint(edt9.Text)))=0 ) )
            then begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt184.Color:=clred;edt10.Color:=clred;
            end;
            if (edt181.Text<>'0') then begin
              mmo5.Lines.Add('当前条码【'+Trim(edit1.Text)+'】设定规则：长度='+edt7.Text+'，从第'+edt181.Text+'位置规则字符='+edt10.Text +char(9)
                  +datetostr(Date)+' '+TimeToStr(Time)+char(9)
                  + 'Start test...'+char(9)+char(9));
              if ( (not matchesmask(copy(Trim(edit1.Text),strtoint(edt181.Text),strtoint(edt9.Text)),edt10.Text) ) )
              then begin
                if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                  ts8.Show;
                edt181.Color:=clred;edt10.Color:=clred;
              end;
            end;

            Edit1.Color:=clRed;
            Label7.Font.Color:=clRed;
            Label11.Caption:='FAIL';

            Label11.Font.Color:=clRed;
            NGOverT:=  StrToInt('0'+edt42.Text);
            PassOverT:=StrToInt('0'+edt40.Text);    //超时显示待机

        end  else begin
            ///////////////////确认条码OK//////////////////////////////////
            //sbuf[1]:=1;
            sbuf[2]:=5;
            sbuf[3]:=$50;
            sbuf[4]:=$00;
            sbuf[5]:=$FF;
            sbuf[6]:=$00;
            send_len:=6;
            try
                send_crcdata(send_len,2);
        //          nsend:=nsend+1;
        //          fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
            except
                messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
            end;
        end;
        if Lbl140.Color=clGreen then begin
           lbl140.Caption:=Trim(edit1.Text);
        end;
      end;
    end;
  end;
  procedure rxdMonitor1BP2();
  var
    i,j:Integer;
    year,month,day:Word;
  begin
    with Form_line do begin
      begin
        s:='';
        if rbuf[4]=1 then begin               //SIM卡条码 上传到PC
          ts13.TabVisible:=True;
          for i:=6 to (tt-2) do
          begin
              s:=s+HexChar[ rbuf[i] div 16 ];
              s:=s+HexChar[ rbuf[i] mod 16 ];
          end;
          mmo16.Lines[rbuf[5]]:=s;
        end;

        if ( lbl82.tag>0 ) and( (PassCount+NGcount)>=lbl80.tag ) and ( lbl80.tag>0 ) and ( (PassCount*100) div (PassCount+NGcount)<lbl82.tag ) then begin       //良率低于标准,强制ET报错
          if(Label11.Caption<>'STOP!') then begin     //没有弹出对话框
              Label11.Caption:='STOP!';
              Label11.Font.Color:=clRed;
              edit1.Clear;                           //强制上传报错！

              if MessageDlg('良率低于设置值！请停机维修！',  mtWarning, mbOKCancel, 0) = mrOk then
              begin
                 edt65.Color:=clWindow;
                 Label11.Caption:='Ready';  Label11.Font.Size:=150;Lbl169.Caption:='';lbl169.Font.Size:=10;
                 Label11.Font.Color:=clWindowText;
              end else begin
                 edt65.Color:=clred;
                 Label11.Caption:='-STOP-';
                 Form_line.grp17.Visible:=True;
              end;
          end;
          exit;
        end;
        if ( strtoint(edt111.Text)>0 ) and ( TripCount>=strtoint(edt111.Text) ) then begin       //TESLA最大计数功能,强制ET报错
          if(Label11.Caption<>'STOP!') then begin     //没有弹出对话框
               Label11.Caption:='STOP!';
               Label11.Font.Color:=clRed;
               edit1.Clear;                       //强制上传报错！

              if MessageDlg('测试次数达到最大值！是否重新计数？',  mtWarning, mbOKCancel, 0) = mrOk then
              begin
                  edt111.Color:=clWindow;
                  TripCount:=0;
                  lbl134.Caption:=inttostr(tripcount);
                  Label11.Caption:='Ready';  Label11.Font.Size:=150;Lbl169.Caption:='';lbl169.Font.Size:=10;
                  Label11.Font.Color:=clWindowText;

              end else begin

                 Label11.Caption:='-STOP-';
                 edt111.Color:=clred;
                 Form_line.grp31.Visible:=True;
              end;
          end;
        end else  if (char(rbuf[4])='E')and(char(rbuf[5])='T')and(char(rbuf[6])='P') then     //JAGUAR ET设备 [ABP2]
        begin
          if char(rbuf[7])='1' then
            edt136.Text:=Redits[0].Value;
          if char(rbuf[7])='2' then
            edt136.Text:=Redits[1].Value;
          if char(rbuf[7])='3' then
            edt136.Text:=Redits[2].Value;
          if char(rbuf[7])='4' then
            edt136.Text:=Redits[3].Value;
          if (char(rbuf[8])<>'t')and(Readchis('Server','MESfun')<>'') then begin      //非自测试，非MES功能
            lbl164.Caption:='';          //MES返回结果
            if TabSheet2.Showing then begin   //校验显示界面；
              ts7.Show;
              if (Readchis('Server','MESfun')<>'') then begin
                chk25.Visible:=true;
                chk26.Visible:=true;
                chk27.Visible:=true;
                chk28.Visible:=true;
              end else begin
                chk25.Visible:=false;
                chk26.Visible:=false;
                chk27.Visible:=false;
                chk28.Visible:=false;
              end;
            end;
            btn118Click(Form_line);        //函数不区别是第几PCS校验；
            if (char(rbuf[7])='4')or(StrToInt(edt59.Text)*strtoint(edt60.Text)=1)  then begin     //把测试按钮关闭；
              chk30.Checked:=false;
              chk31.Checked:=false;
            end;

            if (lbl164.Caption='true')
              or(Readchis('Server','MESfun')='31')
            //  or( (Readchis('Server','MESfun')='0')and(lbl164.Caption='false') )
            then begin
              //if(Readchis('Server','MESfun')<>'10') then begin           //仅校验，为“true" 不启动上传
                if char(rbuf[7])='1' then   begin
                  chk25.Checked:=true; chk25.Color:=clGreen;
                  chk25.Caption:='ETP1条码校验'+':true';
                end;
                if char(rbuf[7])='2' then   begin
                  chk26.Checked:=true; chk26.Color:=clGreen;
                  chk26.Caption:='ETP2条码校验'+':true';
                end;
                if char(rbuf[7])='3' then   begin
                  chk27.Checked:=true; chk27.Color:=clGreen;
                  chk27.Caption:='ETP3条码校验'+':true';
                end;
                if char(rbuf[7])='4' then   begin
                  chk28.Checked:=true; chk28.Color:=clGreen;
                  chk28.Caption:='ETP4条码校验'+':true';
                end;
              //end;
              sbuf[2]:=5;  sbuf[3]:=$50;  sbuf[4]:=$00; sbuf[5]:=$FF; sbuf[6]:=$00;
              send_len:=6;
              try
                  send_crcdata(send_len,2);
              except
                  messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
              end;
            end else if lbl164.Caption='false' then begin                 //=1,11,4,14
              if char(rbuf[7])='1' then begin
                chk25.Color:=clRed;
                chk25.Caption:='ETP1条码校验'+':false/'+Copy(stat1.Panels[5].Text,Pos('":"',stat1.Panels[5].Text)+3,50);
              end;
              if char(rbuf[7])='2' then  begin
                chk26.Color:=clRed;
                chk26.Caption:='ETP2条码校验'+':false/'+Copy(stat1.Panels[5].Text,Pos('":"',stat1.Panels[5].Text)+3,50);
              end;
              if char(rbuf[7])='3' then begin
                chk27.Color:=clRed;
                chk27.Caption:='ETP3条码校验'+':false/'+Copy(stat1.Panels[5].Text,Pos('":"',stat1.Panels[5].Text)+3,50);
              end;
              if char(rbuf[7])='4' then begin
                chk28.Color:=clRed;
                chk28.Caption:='ETP4条码校验'+':false/'+Copy(stat1.Panels[5].Text,Pos('":"',stat1.Panels[5].Text)+3,50);
              end;

              if(Readchis('Server','MESfun')='11')or(Readchis('Server','MESfun')='13')                //校验错提示，并勾选
                or(Readchis('Server','MESfun')='21')or(Readchis('Server','MESfun')='23')then begin    //异常提示，并勾选
                  if char(rbuf[7])='1' then begin
                    chk25.Checked:=true;
                  end;
                  if char(rbuf[7])='2' then begin
                    chk26.Checked:=true;
                  end;
                  if char(rbuf[7])='3' then begin
                    chk27.Checked:=true;
                  end;
                  if char(rbuf[7])='4' then begin
                    chk28.Checked:=true;
                  end;

              end;
              //Label11.Caption:='MES校验错'; //isvnsgmntclck1.Enabled:=False;
              //Label11.Font.Color:=clRed;
              sbuf[2]:=5;  sbuf[3]:=$50;  sbuf[4]:=$00; sbuf[5]:=$FF; sbuf[6]:=$00;
              send_len:=6;
              try
                  send_crcdata(send_len,2);
              except
                  messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
              end;

            end else begin
              if(Readchis('Server','MESfun')='21')or(Readchis('Server','MESfun')='23')then begin          //异常提示，并勾选
                  if char(rbuf[7])='1' then begin
                    chk25.Checked:=true;
                    chk25.Color:=clRed;
                    chk25.Caption:='ETP1条码校验'+':异常';
                  end;
                  if char(rbuf[7])='2' then begin
                    chk26.Checked:=true;
                    chk26.Color:=clRed;
                    chk26.Caption:='ETP2条码校验'+':异常';
                  end;
                  if char(rbuf[7])='3' then begin
                    chk27.Checked:=true;
                    chk27.Color:=clRed;
                    chk27.Caption:='ETP3条码校验'+':异常';
                  end;
                  if char(rbuf[7])='4' then begin
                    chk28.Checked:=true;
                    chk28.Color:=clRed;
                    chk28.Caption:='ETP4条码校验'+':异常';
                  end;
              end;
            //  Label11.Caption:='MES异常'; //isvnsgmntclck1.Enabled:=False;
            //  Label11.Font.Color:=clRed;
              sbuf[2]:=5;  sbuf[3]:=$50;  sbuf[4]:=$00; sbuf[5]:=$FF; sbuf[6]:=$00;
              send_len:=6;
              try
                  send_crcdata(send_len,2);
              except
                  messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
              end;
            end;
          end else begin          ////关闭MES功能，自测试///////////////确认条码OK//////////////////////////////////
            //sbuf[1]:=1;
            sbuf[2]:=5;  sbuf[3]:=$50;  sbuf[4]:=$00; sbuf[5]:=$FF; sbuf[6]:=$00;
            send_len:=6;
            try
                send_crcdata(send_len,2);
            except
                messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
            end;
          end;

        end else  if (char(rbuf[4])='R')and(char(rbuf[5])='E')and(char(rbuf[6])='T')and(char(rbuf[7])='E') then     //获取PC机RETEST次数
        begin
          modbusfun16int:=$0022;
          modbusfun16len:=$02;
          //sbuf[1]:=1;
          sbuf[2]:=$10;
          sbuf[3]:=$00;
          sbuf[4]:=$22;
          sbuf[5]:=$00;
          sbuf[6]:=$02;
          sbuf[7]:=2*sbuf[6];

          sbuf[8]:=0; sbuf[9]:=0;       //默认不重压
          sbuf[10]:=StrToInt(edt168.Text);sbuf[11]:=StrToInt(edt169.Text);
          if(sbuf[10]<sbuf[11]) then begin       //允许重压，显示计数加一；
            sbuf[9]:=1;
            sbuf[10]:=sbuf[10]+1;
          end;
          edt168.Text:=IntToStr(sbuf[10]);

        end else  if (Label7.hint='11') and (char(rbuf[4])='C')      //双条码核对
            and  (edt48.Text='1') then  //barcode check=1: 核对使能
        begin
          bar1:=copy( Trim(edit1.Text),strtoint(edt123.Text),strtoint(edt124.Text));
          bar2:=copy(Trim(medit1.Text),strtoint(edt125.Text),strtoint(edt126.Text));
          if Readchis('Barcode Check',bar1)<> bar2 then  begin   //规则错误
            if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
              ts8.Show;
            edt48.Color:=clred;
            Label11.Caption:='FAIL';
            Label11.Font.Color:=clRed;
            NGOverT:=  StrToInt('0'+edt42.Text);
            PassOverT:=StrToInt('0'+edt40.Text);    //超时显示待机

          end else begin
            edt48.Color:=clwindow;
            MEdit1.Color:=clWindow;
            Edit1.Color:=clWindow;
            ///////////////////确认条码OK//////////////////////////////////
            //sbuf[1]:=1;
            sbuf[2]:=5;
            sbuf[3]:=$50;
            sbuf[4]:=$00;
            sbuf[5]:=$FF;
            sbuf[6]:=$00;
            send_len:=6;
            try
                send_crcdata(send_len,2);
        //          nsend:=nsend+1;
        //          fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
            except
                messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
            end;
          end;
        end  else if ( (rbuf[4]>=$20)and(rbuf[4]<>$32)and(rbuf[4]<>$33)and(rbuf[4]<>$34)     ///------判断第一个条码是否正确 (命令的第一个字节为字符 且非#2，#3，#4板条码 ，则默认为#1板条码 )
               and( ( Trim(edit1.Text)='' )    //条形码为空，或者长度不够
                  or(Length(Trim(edit1.Text))<StrToInt(edt55.Text))
                  or( (lbl140.Color=clgreen)and(Trim(edit1.Text)=lbl140.Caption) )     //连续条码重复，报错！
                  or( (edt7.Text<>'0')and ( Length(Trim(edit1.Text))<>strtoint(edt7.Text) ) )     //指定长度，且不相等
                  or( (edt8.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt8.Text),strtoint(edt9.Text)))=0 ) )         //指定子串，且不存在
                 or( (edt182.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt182.Text),strtoint(edt9.Text)))=0 ) )     //指定子串2，且不存在
                 or( (edt183.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt183.Text),strtoint(edt9.Text)))=0 ) )     //指定子串3，且不存在
                 or( (edt184.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt184.Text),strtoint(edt9.Text)))=0 ) )     //指定子串4，且不存在
                 or( (edt181.Text<>'0')and (not matchesmask(copy(Trim(edit1.Text),strtoint(edt181.Text),strtoint(edt9.Text)),edt10.Text) ) )     //指定通配符子串，且不存在
              //    or( (edt106.Text<>'')and (edt77.text='') )     //使能’process', 即jiashida的PN为空
              )
            ) then    //判断条码规则，有问题报警        
        begin
            isvnsgmntclck1.Enabled:=False;      //条码错误，默认测试停止！
            if( (lbl140.Color=clgreen)and(Trim(edit1.Text)=lbl140.Caption) ) then    //连续条码重复，报错！
              lbl140.Font.Color:=clRed;

            if (Length(Trim(edit1.Text))<StrToInt(edt55.Text)) then begin
              //ts12.TabVisible:=true;
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;        //ts12.
              edt55.Color:=clred;
              edt55.Visible:=True;
              lbl137.Visible:=True;
            end;
            if ( (edt7.Text<>'0')and ( Length(Trim(edit1.Text))<>strtoint(edt7.Text) ) ) then
            begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt7.Color:=clred;
            end;
            if ( (edt8.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt8.Text),strtoint(edt9.Text)))=0 ) )
            then begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt8.Color:=clred;edt10.Color:=clred;
            end;
            if ( (edt182.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt182.Text),strtoint(edt9.Text)))=0 ) )
            then begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt182.Color:=clred;edt10.Color:=clred;
            end;
            if ( (edt183.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt183.Text),strtoint(edt9.Text)))=0 ) )
            then begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt183.Color:=clred;edt10.Color:=clred;
            end;
            if ( (edt184.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt184.Text),strtoint(edt9.Text)))=0 ) )
            then begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt184.Color:=clred;edt10.Color:=clred;
            end;
            if (edt181.Text<>'0') then begin
              mmo5.Lines.Add('当前条码【'+Trim(edit1.Text)+'】设定规则：长度='+edt7.Text+'，从第'+edt181.Text+'位置规则字符='+edt10.Text +char(9)
                  +datetostr(Date)+' '+TimeToStr(Time)+char(9)
                  + 'Start test...'+char(9)+char(9));
              if ( (not matchesmask(copy(Trim(edit1.Text),strtoint(edt181.Text),strtoint(edt9.Text)),edt10.Text) ) )
              then begin
                if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                  ts8.Show;
                edt181.Color:=clred;edt10.Color:=clred;
              end;
            end;

            Edit1.Color:=clRed;
            Label7.Font.Color:=clRed;
            Label11.Caption:='FAIL';

            Label11.Font.Color:=clRed;
            if grp45.Visible and ( (edt171.Color<>clLime)or(edt173.Color<>clLime)or(edt175.Color<>clLime) ) then  begin
               Label11.Font.Color:=clWindowText;
               Label11.Caption:='标准板验证...';
               lbl193.Caption:='条码规则错误！'; lbl193.Color:=clred;
            end;
            NGOverT:=  StrToInt('0'+edt42.Text);
            PassOverT:=StrToInt('0'+edt40.Text);    //超时显示待机

        end  else begin
          if (Label7.hint='11') and (char(rbuf[4])='2')      //第二条码
            and ( ( Trim(edit1.Text)=trim(medit1.Text )  )  //条码重复
                  or( (edt7.Text<>'0')and ( Length(Trim(medit1.Text))<>strtoint(edt7.Text) ) )     //指定长度，且不相等
                  or( (edt8.Text<>'0')and (pos(edt10.Text,copy(Trim(medit1.Text),strtoint(edt8.Text),strtoint(edt9.Text)))=0 ) )         //指定子串，且不存在
                 or( (edt182.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt182.Text),strtoint(edt9.Text)))=0 ) )     //指定子串2，且不存在
                 or( (edt183.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt183.Text),strtoint(edt9.Text)))=0 ) )     //指定子串3，且不存在
                 or( (edt184.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt184.Text),strtoint(edt9.Text)))=0 ) )     //指定子串4，且不存在
                 or( (edt181.Text<>'0')and (not matchesmask(copy(Trim(edit1.Text),strtoint(edt181.Text),strtoint(edt9.Text)),edt10.Text) ) )     //指定通配符子串，且不存在
              //    or( (edt106.Text<>'')and (edt77.text='') )     //报警：使能’process', 即jiashida的PN为空
            ) then begin   //判断第二条码规则
            isvnsgmntclck1.Enabled:=False;      //条码错误，默认测试停止！

            mEdit1.Color:=clRed;
            if( Trim(edit1.Text)=trim(medit1.Text )  ) then  //条码重复
              edit1.Color:=clred;

            if(edt7.Text<>'0') and ( Length(Trim(medit1.Text))<>strtoint(edt7.Text) )      //指定长度，且不相等
            then begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt7.Color:=clred;
            end;
            if ( (edt8.Text<>'0')and (pos(edt10.Text,copy(Trim(medit1.Text),strtoint(edt8.Text),strtoint(edt9.Text)))=0 ) )
            then begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt8.Color:=clred;edt10.Color:=clred;
            end;
            if ( (edt182.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt182.Text),strtoint(edt9.Text)))=0 ) )
            then begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt182.Color:=clred;edt10.Color:=clred;
            end;
            if ( (edt183.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt183.Text),strtoint(edt9.Text)))=0 ) )
            then begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt183.Color:=clred;edt10.Color:=clred;
            end;
            if ( (edt184.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt184.Text),strtoint(edt9.Text)))=0 ) )
            then begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt184.Color:=clred;edt10.Color:=clred;
            end;
            if (edt181.Text<>'0') then begin
              mmo5.Lines.Add('当前条码【'+Trim(edit1.Text)+'】设定规则：长度='+edt7.Text+'，从第'+edt181.Text+'位置规则字符='+edt10.Text +char(9)
                  +datetostr(Date)+' '+TimeToStr(Time)+char(9)
                  + 'Start test...'+char(9)+char(9));
              if ( (not matchesmask(copy(Trim(edit1.Text),strtoint(edt181.Text),strtoint(edt9.Text)),edt10.Text) ) )
              then begin
                if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                  ts8.Show;
                edt181.Color:=clred;edt10.Color:=clred;
              end;
            end;

            Label11.Caption:='FAIL';
            Label11.Font.Color:=clRed;
            NGOverT:=  StrToInt('0'+edt42.Text);
            PassOverT:=StrToInt('0'+edt40.Text);    //超时显示待机
          end else begin
            if (Label7.hint='11') and (char(rbuf[4])='2') then begin     //有选择清条码颜色，因后面用到此颜色
              MEdit1.Color:=clWindow;
            end else begin
              Edit1.Color:=clWindow;
            end;

            edt7.Color:=clwindow;     //条码规则清除
            edt8.Color:=clWindow;
            edt10.Color:=clWindow;
            edt181.Color:=clWindow;
            edt182.Color:=clWindow;
            edt183.Color:=clWindow;
            edt184.Color:=clWindow;
            edt77.Color:=clwindow;
            edt55.Color:=clWindow;

            ///////////////////确认条码OK//////////////////////////////////
            //sbuf[1]:=1;
            sbuf[2]:=5;
            sbuf[3]:=$50;
            sbuf[4]:=$00;
            sbuf[5]:=$FF;
            sbuf[6]:=$00;
            send_len:=6;
            try
                send_crcdata(send_len,2);
        //          nsend:=nsend+1;
        //          fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
            except
                messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
            end;
          end;
        end;
        if Lbl140.Color=clGreen then begin
           lbl140.Caption:=Trim(edit1.Text);
        end;
      end;
    end;
  end;
  procedure rxdMonitorName();
  var
    i,j:Integer;

    procedure rxdNameZuProcess();
    var
      i,j:Integer;
    begin
        ///////////////接收MCU上传步骤名称，PC机没有配置的步骤名配置为默认
      with Form_line do begin

         tempss:=''; zuptr:=1;
         Nametemp:=0; dd:=0; temp:=0;     //上传步骤名称 未设置 统计
         for i:=4 to (tt-2) do
         begin
            if (char(rbuf[i])='#') and (edt74.Text='1') then dd:=1;    //含有'#'的名称，不上传到PC
            if rbuf[i]=$9 then    //一个步骤名结束
            begin
              if dd<>1 then begin
                OnePCSNamePos[zuptr]:=temp;    //名称字符 所在位置指针；
                NameStrZu[zuptr]:=tempss;     //最先从FCT主板赋值 ，后面再依config.ini文件(高优先级且靠近文件存储)替换    //if NameStrZu[zuptr]='' then仅一次不可取，因FCT会更换程序
                if (zuptr<UpStepMax) then zuptr:=zuptr+1;
                Nametemp:=Nametemp+1;  //名称数目 加一
              end;
              tempss:='';
              dd:=0;
              temp:=temp+1;
            end else
              tempss:=tempss+char(rbuf[i]);      //名称字符串增加
         end;
         NameCount:=Nametemp;   //上传名称总数
      end;
    end;
  begin
    with Form_line do begin
      begin        //csv文件，公用信息一行
        SLINE_head := '';               //SLINE_head2:='';
        s := 'SerialNumber';           //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then    s := AnsiQuotedStr(s, '"');
        s := s + ',';
        SLINE_head := SLINE_head + s;  //SLINE_head2:=SLINE_head2+',';

        s := 'TestTime';        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then    s := AnsiQuotedStr(s, '"');
        s := s + ',';
        SLINE_head := SLINE_head + s;        //SLINE_head2:=SLINE_head2+',';
        s := 'TestResult';       //3         //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then    s := AnsiQuotedStr(s, '"');
        s := s + ',';
        SLINE_head := SLINE_head + s;        //SLINE_head2:=SLINE_head2+',';

        s := 'Machine';//8  Form_line.edt35.Text;    //EMP        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then  s := AnsiQuotedStr(s, '"');
        s := s + ',';
        SLINE_head := SLINE_head + s;

        begin
          s := 'Operator';//9 Form_line.edt36.Text;    //LOT        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then  s := AnsiQuotedStr(s, '"');
          s := s + ',';
          SLINE_head := SLINE_head + s;
        end;
      //------------------可选参数----------------------

        if (grp1.Tag=2)and(StrToInt(edt81.Text)=0)then begin       //jaguar测试板定义名称和单位
          s := 'online';                                              //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then  s := AnsiQuotedStr(s, '"');
          s := s + ',';
          SLINE_head := SLINE_head + s;
        end;

        mmo2StringCSV[0]:= 'SerialNumber,'+'TestTime,' +'TestResult,'
                            +','+','+','+','+','+',';

        if edt62.text='25' then
          SLINE_head  := 'Product'+',Lot No,SerialNumber,Test Pass/Fail Status,StartTime,EndTime,Fixture ID,Operator,List of Failing Tests,';

        if(edt62.Text='24') then 
          SLINE_head:='Time'+','+'Serial Barcode'+','+'Result'+',';    //tesla专用 ，时间在前

      ///////////////接收MCU上传步骤名称，项目名称单独一行-------------PC机没有配置的步骤名配置为默认
        SLINE_head2:='';//SLINE_head2+'Name:'+#9;

        rxdNameZuProcess();

        GetNameUnitFromConfig($02);    //congfig定义的名称替换上传的名称
        for i:=1 to NameCount do      //上传名称个数
        begin
          SLINE_head:=SLINE_head+ NameStrZu[i]+ ',';    //更新csv文件时用到
          mmo2StringCSV[0] :=mmo2StringCSV[0] + NameStrZu[i]+ ',';
          if Length(NameStrZu[i])>6 then
            SLINE_head2 := SLINE_head2 +copy(NameStrZu[i],0,7)+#9
          else
            SLINE_head2 := SLINE_head2 +NameStrZu[i]+#9 ;
        end;
        mmo2String[0]:=SLINE_head;  //更新cvs文件时，需要  ,但tesla格式不更新
        mmo2.Lines[2]:=Copy('Name:'+#9+SLINE_head2,0,1024);  //只有名称，更新txt文件时需要 .Add(SLINE_head) ;
      end;
    end;
  end;
  procedure rxdMonitorUnit();
  var
    i,j:Integer;
    procedure rxdUnitStr();
    var i:Integer;
    begin
      with Form_line do begin
         tempss:=''; zuptr:=1;     dd:=0; temp:=0;                 //标志是否保存单位
         if readchis('Model Para', 'Scale')<>'Man' then begin      //非手动设置单位小数点；
           for i:=4 to (tt-2) do
           begin
              if (OnePCSNamePos[zuptr]<>temp) and (edt74.Text='1') then dd:=1;    //含有'#'的名称，且联板测试，不上传到PC
              if rbuf[i]=$9 then    //一个单位名结束
              begin
                if dd<>1 then
                begin
                  ScaleZu[zuptr]:='1';    //默认值；
                  if pos('1f',tempss)>0 then begin tempss:=StringReplace (tempss, '1f', 'p', []) ;ScaleZu[zuptr]:='1000'   end
                  else if pos('10f',tempss)>0 then begin tempss:=StringReplace (tempss, '10f', 'p', []) ;ScaleZu[zuptr]:='100'   end
                  else if pos('100f',tempss)>0 then begin tempss:=StringReplace (tempss, '100f', 'p', []) ;ScaleZu[zuptr]:='10'   end
                  else if pos('1p',tempss)>0 then begin tempss:=StringReplace (tempss, '1p', 'n', []) ;ScaleZu[zuptr]:='1000'   end
                  else if pos('10p',tempss)>0 then begin tempss:=StringReplace (tempss, '10p', 'n', []) ;ScaleZu[zuptr]:='100'   end
                  else if pos('100p',tempss)>0 then begin tempss:=StringReplace (tempss, '100p', 'n', []) ;ScaleZu[zuptr]:='10'   end
                  else if pos('1n',tempss)>0 then begin tempss:=StringReplace (tempss, '1n', 'u', []) ;ScaleZu[zuptr]:='1000'   end
                  else if pos('10n',tempss)>0 then begin tempss:=StringReplace (tempss, '10n', 'u', []) ;ScaleZu[zuptr]:='100'   end
                  else if pos('100n',tempss)>0 then begin tempss:=StringReplace (tempss, '100n', 'u', []) ;ScaleZu[zuptr]:='10'   end
                  else if pos('1u',tempss)>0 then begin tempss:=StringReplace (tempss, '1u', 'm', []) ;ScaleZu[zuptr]:='1000'   end
                  else if pos('10u',tempss)>0 then begin tempss:=StringReplace (tempss, '10u', 'm', []) ;ScaleZu[zuptr]:='100'   end
                  else if pos('100u',tempss)>0 then begin tempss:=StringReplace (tempss, '100u', 'm', []) ;ScaleZu[zuptr]:='10'   end
                  else if pos('1m',tempss)>0 then begin tempss:=StringReplace (tempss, '1m', '', []) ;ScaleZu[zuptr]:='1000'   end
                  else if pos('10m',tempss)>0 then begin tempss:=StringReplace (tempss, '10m', '', []) ;ScaleZu[zuptr]:='100'   end
                  else if pos('100m',tempss)>0 then begin tempss:=StringReplace (tempss, '100m', '', []) ;ScaleZu[zuptr]:='10'   end
                  else if pos('1K',tempss)>0 then begin tempss:=StringReplace (tempss, '1K', 'M', []) ;ScaleZu[zuptr]:='1000'   end
                  else if pos('10K',tempss)>0 then begin tempss:=StringReplace (tempss, '10K', 'M', []) ;ScaleZu[zuptr]:='100'   end
                  else if pos('100K',tempss)>0 then begin tempss:=StringReplace (tempss, '100K', 'M', []) ;ScaleZu[zuptr]:='10'   end
                //  else if pos('1M',tempss)>0 then begin tempss:=StringReplace (tempss, '1M', 'G', []) ;ScaleZu[zuptr]:='1000'   end
                //  else if pos('10M',tempss)>0 then begin tempss:=StringReplace (tempss, '10M', 'G', []) ;ScaleZu[zuptr]:='100'   end
                //  else if pos('100M',tempss)>0 then begin tempss:=StringReplace (tempss, '100M', 'G', []) ;ScaleZu[zuptr]:='10'   end
                  else if pos('100',tempss)>0 then begin tempss:=StringReplace (tempss, '100', 'K', []) ;ScaleZu[zuptr]:='10'   end
                  else if pos('10',tempss)>0 then begin tempss:=StringReplace (tempss, '10', 'K', []) ;ScaleZu[zuptr]:='100'   end
                  else if pos('1',tempss)>0 then begin tempss:=StringReplace (tempss, '1', '', []) ;ScaleZu[zuptr]:='1'   end
                  else tempss:='-';      //2021-12-24增加:单位不允许为空

                  UnitStrZu[zuptr]:=tempss;     //直接赋值 ，后面再依config.ini文件(高优先级且靠近文件存储)替换    //if UnitStrZu[zuptr]='' then仅一次不可取，应FCT主板会更新程序
                  if (zuptr<UpStepMax) then zuptr:=zuptr+1;
                end;
                tempss:=''; dd:=0;temp:=temp+1;
              end else
                tempss:=tempss+char(rbuf[i]);      //名称字符串增加
           end;
         end else begin
           for i:=4 to (tt-2) do
           begin
              if (OnePCSNamePos[zuptr]<>temp) and (edt74.Text='1') then dd:=1;    //含有'#'的名称，且联板测试，不上传到PC
              if rbuf[i]=$9 then    //一个步骤名结束
              begin
                if dd<>1 then
                begin
                  UnitStrZu[zuptr]:=tempss;     //直接赋值 ，后面再依config.ini文件(高优先级且靠近文件存储)替换    //if UnitStrZu[zuptr]='' then仅一次不可取，应FCT主板会更新程序
                  if (zuptr<UpStepMax) then zuptr:=zuptr+1;
                end;
                tempss:=''; dd:=0;temp:=temp+1;
              end else
                tempss:=tempss+char(rbuf[i]);      //名称字符串增加
           end;
         end;
         GetNameUnitFromConfig($20);        //遍历最大数组，与USP频繁接收数据！congfig定义的名称替换上传的单位   NameCount:=
      end;
    end;
  begin
    with Form_line do begin
      begin          //CSV文件
        SLINE_head2:='';   SLINE_head:='';       //SLINE_head2:=SLINE_head2+'Unit:'+#9;
        
        if edt62.text='25' then
          SLINE_head := 'Measurement unit(单位）-->'+',N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,';
        if edt62.text='24' then
          SLINE_head:=''+','+ 'UNIT'+',';    //tesla车间

        ///////////////接收MCU上传步骤名称单位，PC机没有配置的步骤单位配置为默认
         rxdUnitStr();
         for i:=1 to NameCount do      //上传名称个数 相同的单位
         begin
            SLINE_head:=SLINE_head+ UnitStrZu[i]+ ',';    //更新csv文件时用到
            SLINE_head2 := SLINE_head2 +UnitStrZu[i]+#9 ;
         end;
         mmo2.Lines[10]:=Copy('Unit:'+#9+SLINE_head2,0,1024);//SLINE_head2;//单位 .Add(SLINE_head2) ;
         if (edt62.text='25')or(edt62.text='24') then begin
           mmo2String[2]:=SLINE_head ;                                     //单位
         end else
            mmo2String[2]:=','+','+','+','+','+','+','+','+','+SLINE_head ;
      end;
    end;
  end;
  procedure rxdMonitor3BData();
  var
    i,j:Integer;
  begin
    with Form_line do begin
      if true then        //非多主板数据接收
      begin
        if (strtoint(edt59.Text)>0) and (strtoint(edt60.Text)>0) then
        begin
          if tabsheet2.TabVisible then begin ts7.Show; ts7.Tag:=1;  end;      //强制显示多连片指示界面

          if (Label7.hint='11')and(edt48.Text<>'1') then begin         //非条码核对，双PCS双条码系统，不显示FAIL或者PASS
              Label11.Caption:='FINISH';
              Label11.Font.Color:=clWindowText;
          end else begin
            Label11.Font.Size:=50;
            if (rbuf[4]=0)              //结果OK
            then begin
                Label11.Font.Color:=clLime;
                Label11.Caption:='PASS';
            end else begin
                Label11.Font.Color:=clRed;
                Label11.Caption:='FAIL';
            end;
          end;

          PassOverT:=StrToInt('0'+edt40.Text);    //超时显示待机 双工位同步测试系统不适用！
          NGOverT:=StrToInt('0'+edt42.Text);
                                        //绿灯偏移高8位 <--不必要 for循环变量
          temp:=rbuf[12]*256+rbuf[13];   //绿灯偏移位置    (tt-15) ;     //3个modbus开始字节+10个固定字节+...+ 2个crc，共计减去15字节
          if (strtoint(edt145.Text)mod 1000)>0 then begin
            for I := 0 to ((strtoint(edt145.Text)mod 1000)*StrToInt(edt59.Text)*strtoint(edt60.Text)-1) do //遍历//相等数量的LED    //temp-1 do     //测量结果指示总数
            begin
              if ( rbuf[14+(i div 8)]and (1 shl( i mod 8 ) )  )<>0 then    //NG置位   temp16
              begin
                  with Rleds[I] do
                  begin
                    //NGcount:=NGcount+1;
                    ActiveColor:=clred;
                    Active:=TRUE;
                  end;
                  if(Label7.hint='11')and (I<=1) then begin
                    Rleds[i].Visible:=false;
                    if I=0 then begin
                      iledround1.Active:=true;
                      iledround1.ActiveColor:=clred;
                      ilabel1.Caption:='FAIL';
                      ilabel1.Font.Color:=clred;
                    end;
                    if I=1 then begin
                      iledround2.Active:=true;
                      iledround2.ActiveColor:=clred;
                      ilabel2.Caption:='FAIL';
                      ilabel2.Font.Color:=clred;
                    end;
                  end;
              //AutoInactiveColor:=False;
              end  else if (rbuf[14+( (i+temp) div 8 )]and (1 shl( (i+temp) mod 8 ) ) )<>0 then
              begin
                  with Rleds[I] do
                  begin
                    //PassCount:=PassCount+1;
                    ActiveColor:=clgreen;
                    Active:=TRUE;
                  end;
                  if(Label7.hint='11') and (I<=1) then begin
                    Rleds[i].Visible:=false;
                    if I=0 then begin
                      iledround1.Active:=true;
                      iledround1.ActiveColor:=clgreen;
                      ilabel1.Caption:='PASS';
                      ilabel1.Font.Color:=clgreen;
                    end;
                    if I=1 then begin
                      iledround2.Active:=true;
                      iledround2.ActiveColor:=clgreen;
                      ilabel2.Caption:='PASS';
                      ilabel2.Font.Color:=clgreen;
                    end;
                  end;
              end;
            end;
          end else begin
            for I := 0 to (StrToInt(edt59.Text)*strtoint(edt60.Text)-1) do //遍历//相等数量的LED    //temp-1 do     //测量结果指示总数
            begin
              //if (i mod 8)=0 then   temp16:=$01;

              if ( rbuf[14+(i div 8)]and (1 shl( i mod 8 ) )  )<>0 then    //NG置位   temp16
              begin
                  with Rleds[ I] do
                  begin
                    if(edt74.Text<>'10') then  NGcount:=NGcount+1;         //在连片界面计数，没有强制为1个条码！
                    ActiveColor:=clred;
                    Active:=TRUE;
                  end;
                  if(Label7.hint='11')and (I<=1) then begin
                    Rleds[i].Visible:=false;
                    if I=0 then begin
                      iledround1.Active:=true;
                      iledround1.ActiveColor:=clred;
                      ilabel1.Caption:='FAIL';
                      ilabel1.Font.Color:=clred;
                    end;
                    if I=1 then begin
                      iledround2.Active:=true;
                      iledround2.ActiveColor:=clred;
                      ilabel2.Caption:='FAIL';
                      ilabel2.Font.Color:=clred;
                    end;
                  end;
              //AutoInactiveColor:=False;
              end  else if (rbuf[14+( (i+temp) div 8 )]and (1 shl( (i+temp) mod 8 ) ) )<>0 then
              begin
                  with Rleds[ I] do
                  begin
                    if(edt74.Text<>'10') then PassCount:=PassCount+1;      //，没有强制为1个条码！
                    ActiveColor:=clgreen;
                    Active:=TRUE;
                  end;
                  if(Label7.hint='11') and (I<=1) then begin
                    Rleds[i].Visible:=false;
                    if I=0 then begin
                      iledround1.Active:=true;
                      iledround1.ActiveColor:=clgreen;
                      ilabel1.Caption:='PASS';
                      ilabel1.Font.Color:=clgreen;
                    end;
                    if I=1 then begin
                      iledround2.Active:=true;
                      iledround2.ActiveColor:=clgreen;
                      ilabel2.Caption:='PASS';
                      ilabel2.Font.Color:=clgreen;
                    end;
                  end;
              end;
            end;
              //temp16:=temp16 shl 1;
          end;
          //---------良品统计 --------------------------------------------------
          edt4.Text:=inttostr(PassCount+NGcount);
          edt63.Text:= inttostr(PassCount);
          edt64.Text:= inttostr(NGCount);
          if (PassCount+NGcount)>0 then  begin
            edt65.Text:= floattostrF((PassCount*100)/(PassCount+NGcount),ffFixed,5,2);//inttostr((PassCount*100) div (PassCount+NGcount));
            edt66.Text:= floattostrF((NGCount*100)/(PassCount+NGcount),ffFixed,5,2);//inttostr((NGCount*100) div (PassCount+NGcount) );
          end else begin
            edt65.Text:='0';        //统计清零
            edt66.Text:='0';
          end;
          //sbuf[1]:=1;    //回传MCU,确认
          sbuf[2]:=5;
          sbuf[3]:=$50;
          sbuf[4]:=$00;
          sbuf[5]:=$FF;
          sbuf[6]:=$00;
          send_len:=6;
          try
              send_crcdata(send_len,2);
      //          nsend:=nsend+1;
      //          fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
          except
              messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
          end;
        end;
      end;
    end;
  end;
  procedure rxdMonitor0BData();           //0b00或2bu0 模式上传
  var
    i,j:Integer;
  begin
    with Form_line do begin
      if Readchis('Model Para','StepLogen')='1' then begin  //使能LOG
          mmo15.Lines.Add( '---'+char(9)
                    +datetostr(Date)+' '+TimeToStr(Time)  +char(9)
                    +'Upload Result!'+char(9) +char(9)
                    );
          ts12.TabVisible:=True;
      end;
      temp:=((tt-15)div 2) ;     //3个modbus开始字节+10个固定字节+...+ 2个crc，共计减去15字节
      /////---------------通信，回应确认通信OK，固件等待delay1时间/////)////////////////////////////
      if (rbuf[3]=$F0) or (rbuf[3]=$EF) or (rbuf[3]=$EE) then
      begin
        //sbuf[1]:=1;
        sbuf[2]:=5;
        sbuf[3]:=$50;
        sbuf[4]:=$01;
        sbuf[5]:=$FF;
        sbuf[6]:=$00;
        send_len:=6;
        try
            send_crcdata(send_len,2);
    //          nsend:=nsend+1;
    //          fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
        except
            messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
        end;
      end;

      isvnsgmntclck1.Enabled:=False;
      Label11.Font.Size:=150;
      lbl169.caption:='';  lbl169.Font.Size:=10;
      ///------判断条码是否正确，错误则不保存数据（因无法追溯，故无意义）
      if ( (Length(Trim(edit1.Text))<StrToInt(edt55.Text)) or
           (Trim(edit1.Text)='')     //条形码为空，或者长度不够
           or( (edt7.Text<>'0')and ( Length(Trim(edit1.Text))<>strtoint(edt7.Text) ) )     //指定长度，且不相等
           or( (edt8.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt8.Text),strtoint(edt9.Text)))=0 ) )         //指定子串，且不存在
           or( (edt182.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt182.Text),strtoint(edt9.Text)))=0 ) )     //指定子串2，且不存在
           or( (edt183.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt183.Text),strtoint(edt9.Text)))=0 ) )     //指定子串3，且不存在
           or( (edt184.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt184.Text),strtoint(edt9.Text)))=0 ) )     //指定子串4，且不存在
           or( (edt181.Text<>'0')and (not matchesmask(copy(Trim(edit1.Text),strtoint(edt181.Text),strtoint(edt9.Text)),edt10.Text) ) )     //指定通配符子串，且不存在
          ) and ( (StrToInt(Label7.hint)<=1) or (StrToInt(Label7.hint)>11) ) then    //clearbarcode=0和1使能条形码判断, =2不判断，=11则两个条码框分别判断        // 没有扫描二维码edit1有多个空格 //not edit1.IsMasked then 和.Text='' then 都不行
      begin

        if (Length(Trim(edit1.Text))<StrToInt(edt55.Text)) then begin
          //ts12.TabVisible:=true;
          if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
            ts8.Show;     //ts12.
          edt55.Color:=clred;
          edt55.Visible:=True;
          lbl137.Visible:=True;
        end;
         if tabsheet2.TabVisible then begin
            if ( (edt7.Text<>'0')and ( Length(Trim(edit1.Text))<>strtoint(edt7.Text) ) ) then
            begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt7.Color:=clred;
            end;
            if ( (edt8.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt8.Text),strtoint(edt9.Text)))=0 ) )
            then begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt8.Color:=clred;edt10.Color:=clred;
            end;
            if ( (edt182.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt182.Text),strtoint(edt9.Text)))=0 ) )
            then begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt182.Color:=clred;edt10.Color:=clred;
            end;
            if ( (edt183.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt183.Text),strtoint(edt9.Text)))=0 ) )
            then begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt183.Color:=clred;edt10.Color:=clred;
            end;
            if ( (edt184.Text<>'0')and (pos(edt10.Text,copy(Trim(edit1.Text),strtoint(edt184.Text),strtoint(edt9.Text)))=0 ) )
            then begin
              if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                ts8.Show;
              edt184.Color:=clred;edt10.Color:=clred;
            end;
            if (edt181.Text<>'0') then begin
              mmo5.Lines.Add('当前条码【'+Trim(edit1.Text)+'】设定规则：长度='+edt7.Text+'，从第'+edt181.Text+'位置规则字符='+edt10.Text +char(9)
                  +datetostr(Date)+' '+TimeToStr(Time)+char(9)
                  + 'Start test...'+char(9)+char(9));
              if ( (not matchesmask(copy(Trim(edit1.Text),strtoint(edt181.Text),strtoint(edt9.Text)),edt10.Text) ) )
              then begin
                if(Readchis('Comm Para','FormLeft')='')then                       //非双软件系统
                  ts8.Show;
                edt181.Color:=clred;edt10.Color:=clred;
              end;
            end;
        end;
        Edit1.Color:=clRed;          //条码框变红色
        Label7.Font.Color:=clRed;
        edit1.Text:='';

        //2022-1-8条码错误不记录数据不保存 if not ts7.Showing then  NGcount:=NGcount+1;
        if (Label7.hint='11')and(edt48.Text<>'1') then begin      //非条码核对，双PCS双条码系统，不显示FAIL或者PASS
          Label11.Caption:='FINISH';
          Label11.Font.Color:=clWindowText;
        end else begin
          Label11.Caption:='FAIL';
          Label11.Font.Color:=clRed;
        end;
        NGOverT:=  StrToInt('0'+edt42.Text);
        PassOverT:=StrToInt('0'+edt40.Text);    //超时显示待机
        if grp45.Visible and ( (edt171.Color<>clLime)or(edt173.Color<>clLime)or(edt175.Color<>clLime) ) then  begin
           Label11.Font.Color:=clWindowText;
           Label11.Caption:='标准板验证...';
           lbl193.Caption:='条码规则错误！';  lbl193.Color:=clred;
        end;

        //messagedlg('No Barcode!',mterror,[mbok],0);
      end else  begin  //成功收到记录，保存        或者clearbarcode=2 可以不输入条形码
        Sleep(10);         //改成下一条确认命令之前，防止数据异常<-- 150 延时，等待通信链接确认完成！
        if (Label7.hint<>'11') then Edit1.Color:=clWindow;        //双条码系统只能有1BP0或者1B00来清除颜色

        edt7.Color:=clwindow;                   //条码规则清除
        edt8.Color:=clWindow;
        edt10.Color:=clWindow;
        edt181.Color:=clWindow;
        edt182.Color:=clWindow;
        edt183.Color:=clWindow;
        edt184.Color:=clWindow;
        edt55.Color:=clWindow;

        if(StrToInt(Label7.hint)=2)and (Trim(edit1.Text)='') then edit1.Text:='nc             ';
        Label7.Font.Color:=clDefault;

          if(temp>0) and ( (StrToInt(Label7.hint)=1) or (StrToInt(Label7.hint)>10 ) )  //clearbarcode=1 or 11
          then begin         //满足在TIM2中开启条码框的条件
            Edit1.Enabled:=false;           //禁止条码框输入新的条码
            medit1.Enabled:=false;
          end;

          if FormPN.Showing then begin
          //  Saveflag:=0;
          end else begin
          //  Saveflag:=temp;      //传递数据个数，转到定时器执行保存函数
            SaveFlagProcess(0,temp);
          end;

      end;
      if strtoint(edt111.Text)>0 then begin
        tripcount:=tripcount+1; //单次计数加一
        lbl134.Caption:=inttostr(tripcount);
      end;
    end;
  end;

begin

  with Form_line do begin


    viewstring:='';
  //-------------------接收数据监控解析；tt带有2个字节CRC不能随意处理；-------------------------------------
    if form_line.rb5.Checked
      and( not Form_line.chk24.checked
           or (Form_line.chk24.Tag=1)                           //主动发指令
           or( ((rbuf[2] mod $20)<>$03)or(rbuf[3]<>$34) ) )              //非刷新数据，必须显示
    then begin
        Form_line.chk24.Tag:=0;                                                 //清楚刷新标志;

        dd:=tt;
        if Port=1 then begin          //USB数据，则减去2个虚增的字节；可能是乱码；
          if tt>2 then dd:=tt-2;
        end;
        for i:=1 to dd do  begin
            viewstring:=viewstring+inttohex(rbuf[i],2)+' ';
        end;
        viewstring:='Recv:'+viewstring;
      //----解析说明显示-----------------------------
        case (rbuf[2] mod $20) of
            $01:begin
                  viewstring:=viewstring+'[返回~测试板读写信息]';
                end;
            $02:begin
                  viewstring:=viewstring+'[返回~测试板只读状态信息]';
                end;

            $03: begin
                case rbuf[3] of
                  $10: begin
                    viewstring:=viewstring+'[返回~系统参数]';
                  end;
                  $18..$28: begin
                    viewstring:=viewstring+'[返回~短路群]';
                  end;
                  $30..$4F: begin
                    viewstring:=viewstring+'[返回~测试步骤]';
                  end;
                  $C0..$CF: begin
                    viewstring:=viewstring+'[仪表数据]';
                  end;
                  $D2: begin
                    viewstring:=viewstring+'[波形数据]';
                  end;
                  $EE: begin
                    viewstring:=viewstring+'[扩展测量结果]';
                  end;
                  $F1: begin
                    viewstring:=viewstring+'[找点数据，第4字节为扩展功能码]';
                  end;
                  $F2: begin
                    viewstring:=viewstring+'[NG项目]';
                  end;
                  $F3: begin
                    viewstring:=viewstring+'[开路NG数据]';
                  end;
                  $F4: begin
                    viewstring:=viewstring+'[短路NG数据]';
                  end;
                  $F5: begin
                    viewstring:=viewstring+'[短路群数据]';
                  end;
                  $F6: begin
                    viewstring:=viewstring+'[上下限数据]';
                  end;
                  $F7: begin
                    viewstring:=viewstring+'[一次自检数据]';
                  end;
                  $F8: begin
                    viewstring:=viewstring+'[二次自检数据]';
                  end;
                  $F9: begin
                    viewstring:=viewstring+'[返回测试状态，第4字节为状态标志]';
                  end;
                  $FA..$FB: begin
                    viewstring:=viewstring+'[条码数据]';
                  end;
                  $FC: begin
                    viewstring:=viewstring+'[步骤名称]';
                  end;
                  $FD: begin
                    viewstring:=viewstring+'[步骤单位]';
                  end;
                  $FE: begin
                    viewstring:=viewstring+'[连片测试结果]';
                  end;
                  $F0: begin
                    viewstring:=viewstring+'[早期结果数据]';
                  end;
                else
                  viewstring:=viewstring+'[无说明]';
                end;
            end;
            $05: begin
                case rbuf[3] of
                  $00: begin
                       case rbuf[4] of
                          $00:begin
                            if rbuf[5]<$FF then begin         //上传最大步骤数
                               btn24.Tag:=rbuf[5]*256+rbuf[6];
                            end;
                            viewstring:=viewstring+'[返回~复位命令]';
                          end;
                          $01: begin
                              viewstring:=viewstring+'[返回~找点命令]';
                          end;
                          $05: begin
                              viewstring:=viewstring+'[返回~一次短路群学习命令]';
                          end;
                          $06: begin
                              viewstring:=viewstring+'[返回~启二次短路群学习命令]';
                          end;
                          $08: begin
                              viewstring:=viewstring+'[返回~保存系统参数命令]';
                          end;
                          $09: begin
                              viewstring:=viewstring+'[返回~保存短路群命令]';
                          end;
                          $0B: begin
                              viewstring:=viewstring+'[返回~保存步骤命令]';
                          end;
                          $0C: begin
                              viewstring:=viewstring+'[返回~单步命令]';
                          end;
                          $0D: begin
                              viewstring:=viewstring+'[返回~上翻页命令]';
                          end;
                          $0E: begin
                              viewstring:=viewstring+'[返回~下翻页命令]';
                          end;
                          $10: begin
                              viewstring:=viewstring+'[返回~启动命令]';
                          end;
                          $11: begin
                              viewstring:=viewstring+'[返回~复位命令]';
                          end;
                       else
                          viewstring:=viewstring+'[无说明]';
                       end;
                  end;
                  $50: begin
                       case rbuf[4] of
                          $00: begin
                              viewstring:=viewstring+'[返回~上传PC数据已保存]';
                          end;
                           $01: begin
                              viewstring:=viewstring+'[返回~USB通讯正常]';
                          end;
                           $02: begin
                              viewstring:=viewstring+'[返回~已收到上传状态]';
                          end;
                       else
                          viewstring:=viewstring+'[无说明]';
                       end;
                  end;
                else
                  viewstring:=viewstring+'[无说明]';
                end;
            end;
            $06: begin
                case rbuf[3] of
                  $00: begin
                      case rbuf[4] of
                        $41: begin
                             viewstring:=viewstring+'[返回~测试程序选择]';
                        end;
                      else
                        viewstring:=viewstring+'[无说明]';
                      end;
                  end;
                else
                    viewstring:=viewstring+'[无说明]';
                end;
            end;
        else
          viewstring:=viewstring+'[无说明]';
        end;
      //----ASCII码显示-----------------------------
        for i:=1 to dd do  begin              
          if Form_line.chk24.checked and(i<4)  then Continue;     //头3个字节不显示为字符，否则乱码
          viewstring:=viewstring+char(rbuf[i]);
        end;
        if form_line.chk38.Checked then
          viewstring:=GetTimemS+viewstring;
        form_line.Memo1.Lines.add(viewstring);
        Viewstring:='';
    end;
  //-------------------双向加载，如果复位则报异常！-------------------------------------
    if (rbuf[2] div $20)>0 then begin
      if (rbuf[2] and $40)=$40 then begin         //复位异常标志！
        rbuf[2]:=rbuf[2] mod $20;
        if(rbuf[2]<>$05)and(rbuf[3]<>$00)and(rbuf[4]<>$11)then begin      //非复位返回
        {数据异常(自定义交互)会导致复位!  if (lbl28.Tag>0) then begin         //加载程序时：
              Application.MessageBox('加载程序时设备异常复位，请重启PC软件！', '警告',
                MB_OK + MB_ICONSTOP);
              Application.Terminate;
          end else if ((label11.Caption='Loading...')) then begin
              Application.MessageBox('加载程序时设备异常复位，请重启PC软件！', '警告',
                MB_OK + MB_ICONSTOP);
              Application.Terminate;
          end;  }
        end;
      end;
      rbuf[2]:=rbuf[2] mod $20;
    end;

  //---------0----优先------接收数据处理-------------------------------------
    case rbuf[2] of
       $06: begin
          case rbuf[3] of
            $00: begin
                case rbuf[4] of
                  $41: begin
                         if A1.Checked then begin
                            Sleep(1000);
                            form_line.btn23Click(Form_line);   //  A1.Checked:=False;
                            Exit;
                         end else if N50.Checked then begin    //CloseFile(inportF);
                            Sleep(1000);            //兼容早期固件版本显示0.5秒+
                            if(strtoint('0'+sysedit[5].text)<930) then  // v929.09以前版本存在的开机步骤
                              Sleep(500+spthdlg1.tag*1000);
                            AutoImportNextPN();
                            Exit;
                         end else if N51.Checked then begin
                            Sleep(1000);            //兼容早期固件版本显示0.5秒+
                            if(strtoint('0'+sysedit[5].text)<930) then  // v929.09以前版本存在的开机步骤
                              Sleep(500+spthdlg1.tag*1000);
                            AutoExportNextPN(0);
                            Exit;
                         end;
                      end;
                end;
            end;
          end;
       end;
    end;

    
  //--------1------之后-----接收数据处理-------------------------------------
    if tmr2.Tag>0 then begin      //接收的数据，步骤指针+1；
      row_num:=row_num+1;
      tmr2.Tag:=0;
    end;
    inportflag:=$00;
    if rbuf[2]=$01 then            //返回01功能吗,位状态控制 ,只在调试界面用到！
    begin
       rxdFun01Process();
    end;

    if rbuf[2]=$03 then    //导出和监控共用
    begin
        if modbusfuntion=$68 then begin     // formPN.showing,专用指令返回！，存放测试程序第1行
          FormLogHead.Close;             //关闭加载长文件名提示窗口...
          if rbuf[3]>=52 then            //有备注参数
          begin
            s:='';
            for i:=0 to 15 do begin
              if (rbuf[40+i]>=$20) and (rbuf[40+i]<$7F) then
                s:=s+char(rbuf[40+i]);
            end;
          end;
          if(  ( char(rbuf[25])+char(rbuf[24])+char(rbuf[27])+char(rbuf[26])='AZV0' )     //通用
             or(                char(rbuf[24])+char(rbuf[27])+char(rbuf[26])= 'Z10' )     //专业:触摸屏用
             or((char(rbuf[25])+char(rbuf[24])+char(rbuf[27])+char(rbuf[26])='VZV0' ) and( ((rbuf[32]*256+rbuf[33])=10)or((rbuf[32]*256+rbuf[33])=0) ) )   //平均P填写10或者0
            )and (s<>'') and (Length(Trim(s))>StrToInt(edt158.Text)) then begin       //主板第一步骤存放 PN号，必须大于4个字符，否则使用短PN号
            //机种名变化会导致config参数出错  formPN.cbb1.Text:=s;
            edt77.Text:=s; edt77.Color:=clWindow;                                               //长机种名
            FormPN.cbb1.Tag:=FormPN.cbb1.Tag+1;      //从MCU获取文件名一次

            edt77.Hint:=char(rbuf[24])+char(rbuf[27])+char(rbuf[26]);            //对应MCU【模式】，Z10生成文件带机种名称；

          end;
          modbusfuntion:=0;

          if (lbl28.tag=1001)and(edt82.tag>=100)then begin   //启用双向加载检查：确认导入之前的’？‘已保存！
            if(edt77.Text='?') then begin
              sleep(20);
              modbusfuntion:=$53;
            end else begin
              Application.MessageBox('加载程序时设备异常复位，请重启PC软件！', '警告',
                MB_OK + MB_ICONSTOP);
              Application.Terminate;
            end;
          end else if (Readchis('Model Para','BarCodeLen')='999')or(Form_line.edt82.Text='99') then begin   //读取下位机条码规则
            //if StrToInt(Readchis('Model Para','BarCodeLen'))>=500 then begin
              modbusfuntion:=$69;
              modbusfun03:=$3020;              //读取机种PN
              modbusfun03dat:=$1A;               // sleep(10);    //留给单片机复位的时间，最小100
            //end;
          end else if Readchis('Model Para','CheckList')='1' then begin   //不实用：预留：读取下位机出厂校验

              modbusfuntion:=$6A;
              modbusfun03:=$3040;              //读取机种PN
              modbusfun03dat:=$1A;               // sleep(10);    //留给单片机复位的时间，最小100
          end;
        end else if modbusfuntion=$69 then begin     // 读取条码规则，存放测试程序第2行
          if rbuf[3]>=52 then                         //有备注参数
          begin
            s:='';
            for i:=0 to 15 do begin
              if (rbuf[40+i]>=$20) and (rbuf[40+i]<$7F) then
                s:=s+char(rbuf[40+i]);
            end;
          end;
          if ( char(rbuf[25])+char(rbuf[24])+char(rbuf[27])+char(rbuf[26])='BZV0' )
             and (s<>'') then begin                          //主板第二步骤存放 条码规则
              edt7.Text:=IntToStr(rbuf[34]*256+rbuf[35]);
              edt8.Text:=IntToStr(rbuf[36]*256+rbuf[37]);
              edt9.Text:=IntToStr(rbuf[38]*256+rbuf[39]);
              edt10.Text:=s;                                       //长机种名
              if edt9.Text='0' then  begin                  //用通配的高级用法
                edt9.Text:=IntToStr(Length(edt10.Text));
                edt181.Text:=edt8.Text;
                edt8.Text:='0';
              end else if( StrToInt(edt9.Text)>16 )then begin

                for i:=23 downto 14 do begin
                  if (rbuf[i]>=$20) and (rbuf[i]<$7F) then  begin
                    s:=char(rbuf[i])+s;
                  end;
                end;

                if (rbuf[11]>=$20) and (rbuf[11]<$7F) then  begin
                  s:=char(rbuf[11])+s;
                end;
                if (rbuf[10]>=$20) and (rbuf[10]<$7F) then  begin
                  s:=char(rbuf[10])+s;
                end;
                
                if (rbuf[8]>=$20) and (rbuf[8]<$7F) then  begin
                  s:=char(rbuf[8])+s;
                end;
                if (rbuf[9]>=$20) and (rbuf[9]<$7F) then  begin
                  s:=char(rbuf[9])+s;
                end;
                if (rbuf[6]>=$20) and (rbuf[6]<$7F) then  begin
                  s:=char(rbuf[6])+s;
                end;
                if (rbuf[7]>=$20) and (rbuf[7]<$7F) then  begin
                  s:=char(rbuf[7])+s;
                end;

                edt10.Text:=s;                                       //条码规则！
              end;
          end;
          modbusfuntion:=0;
          if (Readchis('Model Para','BarCodeLen')='999')or(Form_line.edt82.Text='99')  or (Readchis('Model Para','CheckList')='1') then begin   //不实用：预留：读取下位机出厂校验
              modbusfuntion:=$6A;
              modbusfun03:=$3040;              //读取机种PN
              modbusfun03dat:=$1A;               // sleep(10);    //留给单片机复位的时间，最小100
          end;
        end else if modbusfuntion=$6A then begin     // 读取第3行步骤信息:checklist或者标准板信息
          if rbuf[3]>=52 then                         //有备注参数
          begin
            s:='';
            for i:=0 to 15 do begin
              if (rbuf[40+i]>=$20) and (rbuf[40+i]<$7F) then
                s:=s+char(rbuf[40+i]);
            end;
          end;
          if ( char(rbuf[25])+char(rbuf[24])+char(rbuf[27])+char(rbuf[26])='BZV0' )
             and (s<>'') then begin
              {if (Form_line.edt82.Text='99') then begin
                  edt10.Text:=edt10.Text+s;
                  edt9.Text:=IntToStr(Length(edt10.Text));
              end else }begin
                grp45.Visible:=true; btn138.Visible:=True;
                edt170.Text:=char(rbuf[7])+char(rbuf[6])+char(rbuf[9])+char( rbuf[8]);
                edt171.Text:=s;
              end;
              modbusfuntion:=$6B;
              modbusfun03:=$3060;
              modbusfun03dat:=$1A;               // sleep(10);    //留给单片机复位的时间，最小100
          end else begin
            if ( char(rbuf[25])+char(rbuf[24])+char(rbuf[27])+char(rbuf[26])='CZV0' )    //checklist出厂信息
               and (s<>'') then begin
               // edt7.Text:=IntToStr(rbuf[34]*256+rbuf[35]);
               //rbuf[38]*256+rbuf[39]
            end;
            modbusfuntion:=0;
          end;
        end else if modbusfuntion=$6B then begin     // 读取第3行步骤信息
          if rbuf[3]>=52 then  begin                       //有备注参数
            s:='';
            for i:=0 to 15 do begin
              if (rbuf[40+i]>=$20) and (rbuf[40+i]<$7F) then
                s:=s+char(rbuf[40+i]);
            end;
          end;

          if ( char(rbuf[25])+char(rbuf[24])+char(rbuf[27])+char(rbuf[26])='BZV0' )
             and (s<>'') then begin
              {if (Form_line.edt82.Text='99') then begin
                  edt10.Text:=edt10.Text+s;
                  edt9.Text:=IntToStr(Length(edt10.Text));
              end else }begin
              //edt170.Text:=char(rbuf[7])+char(rbuf[6])+char(rbuf[9])+char( rbuf[8]);
                edt171.Text:=edt171.Text+s;
              end;
              modbusfuntion:=$6C;
              modbusfun03:=$3080;
              modbusfun03dat:=$1A;               // sleep(10);    //留给单片机复位的时间，最小100
          end else begin
            modbusfuntion:=0;
          end;
        end else if modbusfuntion=$6C then begin     // 读取第3行步骤信息
          if rbuf[3]>=52 then  begin                       //有备注参数
            s:='';
            for i:=0 to 15 do begin
              if (rbuf[40+i]>=$20) and (rbuf[40+i]<$7F) then
                s:=s+char(rbuf[40+i]);
            end;
          end;

          if ( char(rbuf[25])+char(rbuf[24])+char(rbuf[27])+char(rbuf[26])='BZV0' )
             and (s<>'') then begin
              {if (Form_line.edt82.Text='99') then begin
                  edt10.Text:=edt10.Text+s;
                  edt9.Text:=IntToStr(Length(edt10.Text));
              end else }begin
                edt172.Text:=char(rbuf[7])+char(rbuf[6])+char(rbuf[9])+char( rbuf[8]);
                edt173.Text:=s;
              end;
              modbusfuntion:=$6D;
              modbusfun03:=$30A0;
              modbusfun03dat:=$1A;               // sleep(10);    //留给单片机复位的时间，最小100
          end else begin
            modbusfuntion:=0;
          end;
        end else if modbusfuntion=$6D then begin     // 读取第3行步骤信息
          if rbuf[3]>=52 then  begin                       //有备注参数
            s:='';
            for i:=0 to 15 do begin
              if (rbuf[40+i]>=$20) and (rbuf[40+i]<$7F) then
                s:=s+char(rbuf[40+i]);
            end;
          end;

          if ( char(rbuf[25])+char(rbuf[24])+char(rbuf[27])+char(rbuf[26])='BZV0' )
             and (s<>'') then begin
              {if (Form_line.edt82.Text='99') then begin
                  edt10.Text:=edt10.Text+s;
                  edt9.Text:=IntToStr(Length(edt10.Text));
              end else }begin
              //edt172.Text:=char(rbuf[7])+char(rbuf[6])+char(rbuf[9])+char( rbuf[8]);
                edt173.Text:=edt173.Text+s;
              end;
              modbusfuntion:=$6E;
              modbusfun03:=$30C0;
              modbusfun03dat:=$1A;               // sleep(10);    //留给单片机复位的时间，最小100
          end else begin
            modbusfuntion:=0;
          end;
        end else if modbusfuntion=$6E then begin     // 读取第7行步骤信息
          if rbuf[3]>=52 then  begin                       //有备注参数
            s:='';
            for i:=0 to 15 do begin
              if (rbuf[40+i]>=$20) and (rbuf[40+i]<$7F) then
                s:=s+char(rbuf[40+i]);
            end;
          end;

          if ( char(rbuf[25])+char(rbuf[24])+char(rbuf[27])+char(rbuf[26])='BZV0' )
             and (s<>'') then begin
              {if (Form_line.edt82.Text='99') then begin
                  edt10.Text:=edt10.Text+s;
                  edt9.Text:=IntToStr(Length(edt10.Text));
              end else }begin
                edt174.Text:=char(rbuf[7])+char(rbuf[6])+char(rbuf[9])+char( rbuf[8]);
                edt175.Text:=s;
              end;
              modbusfuntion:=$6F;
              modbusfun03:=$30E0;
              modbusfun03dat:=$1A;               // sleep(10);    //留给单片机复位的时间，最小100
          end else begin
            modbusfuntion:=0;
          end;
        end else if modbusfuntion=$6F then begin     // 读取第8行步骤信息
          if rbuf[3]>=52 then  begin                       //有备注参数
            s:='';
            for i:=0 to 15 do begin
              if (rbuf[40+i]>=$20) and (rbuf[40+i]<$7F) then
                s:=s+char(rbuf[40+i]);
            end;
          end;

          if ( char(rbuf[25])+char(rbuf[24])+char(rbuf[27])+char(rbuf[26])='BZV0' )
             and (s<>'') then begin
              {if (Form_line.edt82.Text='99') then begin
                  edt10.Text:=edt10.Text+s;
                  edt9.Text:=IntToStr(Length(edt10.Text));
              end else }begin
                //edt174.Text:=char(rbuf[7])+char(rbuf[6])+char(rbuf[9])+char( rbuf[8]);
                edt175.Text:=edt175.Text+s;
              end;
              modbusfuntion:=$00;                //读标准板信息结束！
          end else begin
            modbusfuntion:=0;
          end;
    //会关闭提示忙界面    end else if  rbuf[3]=$FB then begin                       //PC机交互：例如 判断条码规则   [1BP2] [ABP2]
    //      rxdMonitor1BP2();

        end else if (TabSheet2.Showing and not FormLoad.Showing)    //监控界面（数据上传记录） 或者组监控界面 或者pwr界面 (需要利用监控界面查看基本信息）
            or FormFind.Showing  or FormOS.Showing  then        //找点
        begin
            case rbuf[3] of        //不同指令对应不同界面


               //...  对应FCT5早期的一些上传数据指令

              $C0..$C1:
              begin                            //4线设备读取
                 s:='';    temp:=1;
                 ts9.TabVisible:=True;
                 for dd:=1 to StrToInt(edt81.Text) do  Condutor[dd]:='NC';
                 dd:=1;
                 if (Char(rbuf[4])='M')and(Char(rbuf[5])='A')and(Char(rbuf[6])='C')and(Char(rbuf[7])='H')and(Char(rbuf[8])='I') then begin     //8761仪器
                   for i:=4 to (tt-2) do
                   begin
                      if rbuf[i]=$0D then
                        s:=s+'\'
                      else if rbuf[i]=$0A then
                        s:=s+'/'
                      else
                        s:=s+char(rbuf[i]);
                   end;
                 end else begin
                   for i:=4 to (tt-2) do
                   begin
                     if (rbuf[i]=$EA)                                          //早期8700
                       or( (char(rbuf[i])='R')and(char(rbuf[i-1])>='0')and(char(rbuf[i-1])<='9') )  //新款8700
                     then  begin        //欧；标志
                       if (dd<=StrToInt(edt81.Text) ) and (Char(rbuf[i-1])<>'M')and (Char(rbuf[i-1])<>'G') //and (Char(rbuf[i-4])<>'>') and (Char(rbuf[i-5])<>' ')
                       then begin
                          if(Char(rbuf[i-4])='>')  then begin
                            Condutor[dd]:=Copy(s,Length(s)-3,4);
                          end else if(char(rbuf[i-6])>='0')and(char(rbuf[i-6])<='9')then begin
                            Condutor[dd]:=Copy(s,Length(s)-5,6);
                          end else
                            Condutor[dd]:=Copy(s,Length(s)-4,5);
                          dd:=dd+1;                                //错列测试，移动了dd:=dd+1;
                       end;    
                       rbuf[i]:=$6F;     //OHM改为'o'：适用8700仪器
                     end;
                     //用转义符号，退出数据不完整<--  if rbuf[i]=10 then  Break //第一LF（0x0A）退出
                     if rbuf[i]=$0D then
                        s:=s+'\'
                     else if rbuf[i]=$0A then
                        s:=s+'/'
                     else begin
                       if rbuf[i]=0 then
                       begin
                          if  temp>0  then
                          begin
                            s:=s+',';     //不是连续的0,  用，替代                  // dd:=dd+1;
                          end;
                          temp:=0;
                       end else begin
                          s:=s+char(rbuf[i]);
                          temp:=temp+1;
                       end;
                     end;
                   end;
                 end;
                 mmo6.Clear;
                 mmo2String[10]:=mmo2String[10]+s;   //接收数据累计
                 mmo6.Lines.Add('');
                 mmo6.Lines.Add('');
                 mmo6.Lines[0]:='Four Wire Data!';
                 mmo6.Lines[1]:=s;                                //mmo2.Lines[3]:= ' ';       //光标控制
              end;
               $C7:
              begin                             //读取st eeprom芯片测试数据，并保存本地
                 s:='';    // temp:=1;     ts9.TabVisible:=True;        dd:=1;
                 if(tt>6)then begin
                   for i:=2 to ( (tt-2)div 2 ) do
                   begin
                      s:=s+Inttohex(rbuf[2*i]*256+rbuf[2*i+1],4)+'/';
                   end;
                   mmo2String[17]:=mmo2String[17]+s;
                 end;
              end;


              $E0:                         //学习短路群指令
              begin                                      //短路群
                  if(tt>7) then
                      s:='ShortGroups:'
                  else
                      s:='Empty()';
                 for i:=4 to (tt-2) do s:=s+char(rbuf[i]);
                 if(tt>7) then mmo2.Lines[9]:=Copy(s,0,1024) else mmo2.Lines[9]:='';
                 mmo2String[9]:=s;
                 FormOS.mmo1.Clear;
                 FormOS.mmo1.Lines[0]:=s;
              end;
              $F1:             //找点 ，调试类子命令集合
              begin
                s:='';
                for i:=8 to 60 do s:=s+char(rbuf[i]);
                //mmo8.Lines[0]:= s;
                FormFind.edt1.text:= IntToStr(rbuf[4]);
                FormFind.edt2.text:= IntToStr(rbuf[5]);
                FormFind.edt3.text:= IntToStr(rbuf[6]*256+rbuf[7]);
                FormFind.mmo1.Lines[0]:= s;
              end;
              $F2:            //实际用$EE集中上传<--不通用！错误项目上传，error类子命令集合
              begin
                if true then         //多主板数据接收
                begin
                   ts4.Show;
                   s:='err list:';
                   for i:=4 to (tt-2) do begin
                      s:=s+char(rbuf[i]);
                   end;
                   mmo2.Lines[6]:=Copy(s,0,1024);
                   mmo2String[6]:=s;

                   if Label11.Caption='Testing...' then begin     //显示结果
                      if(tt>6) then begin
                         Label11.Caption:='FAIL';
                         Label11.Font.Color:=clRed;
                      end else begin
                         Label11.Caption:='END';
                         Label11.Font.Color:=clLime;
                      end;
                   end;
                end;
              end;
              $F3:              //open类子命令集合
              begin
                if true then         //多主板数据接收
                begin
                  s:=''; btn33.Hint:='';
                  for i:=4 to (tt-2) do s:=s+char(rbuf[i]);
                  //if IsNumberic(sysedit[1].Text) and (StrToInt(sysedit[1].Text)>=768+1) then
                  btn33.Hint:=StringReplace(s, ',', '-', [rfReplaceAll]);
                  s:='open:'+s;
                  mmo2.Lines[7]:=Copy(s,0,1024);        //s;//mmo4.Lines[1]:= s;
                  mmo2String[7]:=s;                     //  mmo13.Lines[0]:=s;
                end;
              end;
              $F4:                  //short类子命令集合
              begin
                if true then         //多主板数据接收
                begin
                  s:=''; btn34.Hint:='';
                  for i:=4 to (tt-2) do s:=s+char(rbuf[i]);
                  //if IsNumberic(sysedit[1].Text) and (StrToInt(sysedit[1].Text)>=768+1) then
                  btn34.Hint:=StringReplace(s, ',', '-', [rfReplaceAll]);;
                  s:='short:'+s;
                  mmo2.Lines[8]:=Copy(s,0,1024);        //s;//mmo4.Lines[2]:= s;
                  mmo2String[8]:=s;                       //  mmo14.Lines[0]:=s;
                end;
              end;

              $F5:                            //基本配置参数类指令集合；多连片则保存所有参数；
              begin
                if tmr4.Enabled then
                  tmr4.Interval:=2000-1;          //收到则刷新，避免重复加载！
                case rbuf[4] of
                  $F2:                        //标准值  ，值保存不显示；YIJIA专用！
                  begin
                    temp:=((tt-7)div 4) ;     //减去 地址+功能码+长度+两字节分支 +2字节CRC=-7

                    if temp<0 then temp:=0;
                    if temp>UpStepMax then temp:=UpStepMax; //if NameCount<1 then temp:=1 else temp:=NameCount;
                    for i:=1 to temp do
                    begin
                      dd:=(rbuf[2+4*(i)]*256+rbuf[3+4*(i)]);
                      if (dd>30000)and(dd<>30005) then dd:=0;
                      if dd<30000  then
                        ff:=dd / strtofloat(ScaleZu[i])              //小数点个数
                      else
                        ff:=dd;                                        //只存在30000(无穷大)和30005(无符号标志)两种情况，其他特殊值都用0表示
                        
                      StdValZu[i]:=copy(floattostr(ff),0,7);        //标准值

                      if Readchis('Name->Redef',NameStrZu[i]+'->Spec')<>'' then //(nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Spec', '')<>'') then
                         SpecValZu[i]:=Readchis('Name->Redef',NameStrZu[i]+'->Spec')
                      else
                         SpecValZu[i]:=StdValZu[i];

                    end;

                  end;
                  $F6:        //高低点  ，值保存不显示；YIJIA专用！
                  begin
                    temp:=((tt-7)div 4) ;

                    if temp<0 then temp:=0;
                    if temp>UpStepMax then temp:=UpStepMax; //if NameCount<1 then temp:=1 else temp:=NameCount;
                    for i:=1 to temp do
                    begin
                      dd:=(rbuf[2+4*(i)]*256+rbuf[3+4*(i)]);
                      HpointZu[i]:=copy(inttostr(dd),0,7);        //高点
                      dd:=(rbuf[4+4*(i)]*256+rbuf[5+4*(i)]);
                      LpointZu[i]:=copy(inttostr(dd),0,7);        //低点
                    end;

                  end;
                  $F9:        //模式
                  begin
                     tempss:=''; zuptr:=1;
                     dd:=0; temp:=0;              //标志是否保存单位
                     for i:=6 to (tt-2) do        //第4,5字节为分支
                     begin
                        //xx全部模式都要记录，否则数据不完整  if (OnePCSNamePos[zuptr]<>temp) and (edt74.Text='1') then dd:=1;    //含有'#'的名称，且联板测试，不上传到PC
                        if rbuf[i]=$9 then    //一个步骤名结束
                        begin
                          if dd<>1 then
                          begin
                            ModeZu[zuptr]:=tempss;     //直接赋值 ，后面再依config.ini文件 (高优先级且靠近文件存储)替换    //if UnitStrZu[zuptr]='' then仅一次不可取，应FCT主板会更新程序
                            if (zuptr<UpStepMax) then zuptr:=zuptr+1;
                          end;
                          tempss:=''; dd:=0; temp:=temp+1;
                        end   else
                          tempss:=tempss+char(rbuf[i]);      //名称字符串增加
                     end;
                  end
                else begin             //读取短路群
                     if(tt>7) then
                      s:='ShortGroups:'
                     else
                      s:='Empty()';

                     s:=s+',';
                     for i:=4 to (tt-2) do s:=s+char(rbuf[i]);
                     if(tt>7) then mmo2.Lines[9]:=Copy(s,0,1024) else mmo2.Lines[9]:='';

                     mmo2String[9]:=s;
                     FormOS.mmo1.Clear;
                     FormOS.mmo1.Lines[0]:=s;
                     //先于上下限上传，故在收到上下限保存
                  end;
                end;
              end;
              $F6:                       //上限、下限
              begin
                rxdMonitorUppLow();
              end;
             $F7:                     //自检类命令集合
              begin
              end;
              $F8:                            
              begin                                      //自检结果2
                                 
                if grp18.Visible and(groupbox1.hint<>'5') then begin //读取长机种名  ;FCT5定义主板不可// Readchis('Model Para','Process')<>'' then begin    //jiashida读取机种
                  FormLogHead.lbl1.Caption:='LongP/N Loading.... 正在加载长机种名。。。';
                  FormLogHead.Show;
                  modbusfuntion:=$68;
                  modbusfun03:=$3000;              //读取机种PN
                  modbusfun03dat:=$1A;
                  sleep(150);    //留给单片机复位的时间，最小100
                end;
              end;
              $F9:                        //测试状态上传类命令集合
              begin                                      //测试状态
                rxdMonitorMCUstatue();
              end;
              $FA:                        //条码类命令集合，上传备注信息作为条码测试用；
              begin                                      //条形码输入，包含1B00;
                rxdMonitor1BT0();
              end;
              $FB:                        //PC机仅 判断条码规则   [1BP2] [ABP2]
              begin                                      //适用于 手动条形码输入
                rxdMonitor1BP2();
              end;
             $FC:                         //接收MCU上传步骤名称
              begin
                if tmr4.Enabled then
                  tmr4.Interval:=2000-3;          //收到则刷新，避免重复加载！
                rxdMonitorName();
              end;
             $FD:                         //接收MCU上传单位
              begin
                rxdMonitorUnit();
              end;
              $FE:                        //3B00,UB30联板结果LED组上传 ，只显示，不保存
              begin
                rxdMonitor3BData();
              end else  if true and (modbusfuntion<>$68)and     //非专用指令；非读取长机种名(前面已执行，此处<>$68无意义！)    //组监控，用独立界面接收数据
                  ( ((rbuf[3]<=100)and (rbuf[3]>10)and (Comm1.BaudRate=9600)and not tmr3.Enabled) //SpeedButton12.Enabled )  //兼容 FCT5 功能码20 串口开启且波特率为9600;FCT7使用38400bps以上的波特率
                    or (rbuf[3]>$E0) ) then      //FCT6功能码**0xF0 oxEF**（0B10判断名称一致性） 0xEE(0B20联板分组上传) 测量数据自动上传
              begin
                //if edt2.Enabled then begin
                //  edt2.Enabled:=False;
                //-可移动到SaveFlag函数内---强制处理一些rbuf数据-----------
                  if (grp1.Tag=2)and(rbuf[4]=0) then begin          
                    if(edt62.text='24')                                         //tesla用8700仪器可防止bug；
                      or((edt62.text='25')and(StrToInt(edt59.Text)*strtoint(edt60.Text)=1))   //jaguar单PCS用8700仪器可防止bug;
                    then begin                        
                      if Pos('OPEN',mmo2String[10])>0 then   rbuf[4]:=11;
                      if Pos('SHORT',mmo2String[10])>0 then   rbuf[4]:=11;
                      if Pos('X COND',mmo2String[10])>0 then   rbuf[4]:=11;
                      if Pos('XCOND',mmo2String[10])>0 then   rbuf[4]:=11;      //8761仪器
                      if Pos('>6.0',mmo2String[10])>0 then     rbuf[4]:=11;
                      if Pos('X INSU',mmo2String[10])>0  then   rbuf[4]:=11;
                    end;
                  end;
                  rxdMonitor0BData();
              end;
            end;     //监控界面专用次命令码处理结束
        end else if  Formloghead.Showing then               //提示界面显示中......
        begin
          case rbuf[3] of
            $FB:                        //PC机仅 判断条码规则   [1BP2] [ABP2]
              begin                                      //适用于 手动条形码输入
                rxdMonitor1BP2();
              end;
            $F9:
              begin
                if true then         //多主板数据接收
                begin
                  case rbuf[4] of
                    $00,$08,$09,108,109:                //复位 或者测试结束
                      begin
                        FormLogHead.Close;
                      end;
                  end;
                end;
              end;
            $EE,$F0,$FE:                                  //
              begin
                if rbuf[4]=0 then FormLogHead.lbl1.Caption:='pass!' else FormLogHead.lbl1.Caption:='fail!';
                 //modbusfun16int:=$5000;    //确认收到数据
                // modbusfun16len:=$FF00;
                //sbuf[1]:=1;
                sbuf[2]:=5; sbuf[3]:=$50;sbuf[4]:=$00;
                sbuf[5]:=$FF; sbuf[6]:=$00; send_len:=6;
                try
                    send_crcdata(send_len,2);    //          nsend:=nsend+1;  //          fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
                except
                    messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
                end;

                //FormLogHead.lbl1.Caption:='Tips提示：收到FCT测试主板上传的测量值，切换到<Monitor测试>界面（才可正常接收）'
                //                      +char($0A)+'testing...... 测试中.......';
              end;
          end;
        end else if (TabSheet4.Showing or ts1.Showing or FormLoad.Showing) then   //避免debug数据传给测试界面  and Form_line.Active then      //debug界面 或者PC SET界面，读取步骤参数 and (Panel1.Color=clbtnface)
        begin
          case rbuf[3] of


            $F9:
            begin
                if true then         //多主板数据接收
                begin
                  case rbuf[4] of
                    $01:                      //非单步测试开始...
                      begin
                        FormLogHead.lbl1.Caption:='testing...... 测试中.......'
                                +char($0A)+char($0A)+'Tips提示：离线中... （因在线Debug会造成测量有波动，请等待测试结束...）';
                        FormLogHead.Show;
                      end;
                  end;
                end;
            end;
            $F3:
            begin
                if true then         //多主板数据接收
                begin
                  s:='open:';
                  for i:=4 to (tt-2) do s:=s+char(rbuf[i]);
                //  mmo2.Lines[7]:=Copy(s,0,1024);//s;//mmo4.Lines[1]:= s;
                //   mmo2String[7]:=s;
                  mmo13.Lines[0]:=s;   //debug显示
                end;
            end;
            $F4:
            begin
                if true then         //多主板数据接收
                begin
                 s:='short:';
                 for i:=4 to (tt-2) do s:=s+char(rbuf[i]);
                //  mmo2.Lines[8]:=Copy(s,0,1024);//s;//mmo4.Lines[2]:= s;
                //  mmo2String[8]:=s;
                  mmo14.Lines[0]:=s;    //debug显示
                end;
            end  else begin
              if(row_num<1) then exit; //begin end else         //空，==0跳过 ,下传时的一个0状态,真正监控会变为1
              if (row_num>0) and (row_num<(VIRLCROWS+1)) then begin  //有效步骤内
                rxdDebugStepPara();
              end else if row_num=(VIRLCROWS+1) then begin   //调试系统参数 。而导出时为翻页命令05,所以不会进来！
                  rxdDebugSysPara();
              end  else if(row_num<=(19+VIRLCOFFSET)) then begin      //空闲

              end else if(row_num<=(20+VIRLCOFFSET)) then  begin     //19,20导出系统参数【rxdDebugStepPara设置row为 19+VIRLCOFFSET，而后进入这里】
                  rxdDebugExportSysPara();
              end else if(row_num<=(SHORTPINS+20+VIRLCOFFSET)) then begin         //短路群导出，以大于保存的OS点数 结束
                  try
                    for i:=0 to 15 do begin

											if (rbuf[4+2*i]*256+rbuf[5+2*i])>=$8000 then begin			//群开始
                          if ((row_num-21-VIRLCOFFSET)*16+i)<StrToInt(sysedit[3].Text)  then begin //一次群个数小于设定OS点数。
                            writeln(exportF, ')');write(exportF,'(');
                          end else begin
                            writeln(exportF, ']');write(exportF,'[');
                          end;
                      end else begin
                          write(exportF,',');
                      end;
                      
                      if(StrToInt('0'+sysedit[5].Text)>=624) then     //6.24版本后支持大于255点开短路
                      begin
                        if IsNumberic(sysedit[1].Text) and (StrToInt(sysedit[1].Text)>=768+1+1280) then begin      //大于2048，屏蔽标志位前移动
                          if ((rbuf[4+2*i] mod 32)*256+rbuf[5+2*i])>=StrToInt(sysedit[3].Text) then begin //群结束  //用设定OS点判定群结束
                              write(exportF,inttostr(4095*2));                  //8190：结束点
                              modbusfuntion:=$43;
                              row_num:=0;
                              Break;
                          end else begin
                            if ((rbuf[4+2*i]*256+rbuf[5+2*i])and $2000)=$2000 then           //屏蔽点，写成负数
                              write( exportF,inttostr(-(1+rbuf[5+2*i]+(rbuf[4+2*i] mod 32)*256)) )
                            else
                              write(exportF,inttostr(1+rbuf[5+2*i]+(rbuf[4+2*i] mod 32)*256));
                          end;
                        end else begin
                          if ((rbuf[4+2*i] mod 16)*256+rbuf[5+2*i])>=StrToInt(sysedit[3].Text) then begin //群结束  //用设定OS点判定群结束
                              write(exportF,inttostr(4095));
                              modbusfuntion:=$43;
                              row_num:=0;
                              Break;
                          end else begin
                            if ((rbuf[4+2*i]*256+rbuf[5+2*i])and $1000)=$1000 then           //屏蔽点，写成负数
                              write( exportF,inttostr(-(1+rbuf[5+2*i]+(rbuf[4+2*i] mod 16)*256)) )
                            else
                              write(exportF,inttostr(1+rbuf[5+2*i]+(rbuf[4+2*i] mod 16)*256));
                          end;
                        end;
                      end else begin
                        write(exportF,inttostr(1+rbuf[5+2*i]));
                        if rbuf[5+2*i]>=StrToInt(sysedit[3].Text) then          //群结束
                        begin
                            modbusfuntion:=$43;
                            row_num:=0;
                            Break;
                        end;
                      end;
                    end;
                  finally
                   
                  end;
              end else if(row_num<=(SHORTPINS+20+VIRLCOFFSET)) then          //短路群导出，以大于保存的OS点数 结束
              begin

              end;
            end;
          end;
        end;    //界面分支结束
    end;     //03命令码处理结束
    if sbtbtn1.Visible then begin                           //测试界面加载程序
      if(modbusfuntion=$41)and(rbuf[2]=$03) then begin      //导出过程，读取到数据
         if(tmr1.Interval>20) then begin
            tmr1.Enabled:=false;
            tmr1.Interval:=5;
            tmr1.Enabled:=True;
         end;
      end;
      if(modbusfuntion=$51)and(rbuf[2]=$10) then begin      //导入过程，写入到数据

         if(tmr1.Interval>20) then begin
            tmr1.Enabled:=false;
            tmr1.Interval:=5;
            tmr1.Enabled:=True;
         end;

      end;

    end   else if (edt15.Text='0') then begin           //默认：加速交互通信，各个代板的加速有区别（受主板固件接收超时限制，不能在没超时的情况下发二次指令）

      if StrToInt(ComboBox2.Text)<=9600 then begin      //FCT5等低波特率需要更长的读取超时，否则读取的系统参数机种名不完整
        if StrToInt(edt14.Text)<60 then
          edt14.Text:='60';
      end else begin
        if StrToInt(edt14.Text)<40 then                   //会有随机性的丢数据  //Comm1.ReadIntervalTimeout:=30;    //串口调试加速
          edt14.Text:='40';
      end;
      if (StrToInt(edt14.Text)<100)and(StrToInt(edt14.Text)>60)and(Comm1.ReadIntervalTimeout<>StrToInt(edt14.Text)) then begin  //

        Comm1.ReadIntervalTimeout:=StrToInt(edt14.Text);    //无条码时软件会死机？因没有响应命令串口控件不能退出超时吗？ 默认：100

      end;  //else 否则：默认100mS
      
      if(StrToInt('0'+sysedit[5].Text)>800) then begin //版本大于8代则加速；
        if (rbuf[2]=$01)or(rbuf[2]=$02)or(rbuf[2]=$03) then begin      //读取到数据
           if(tmr1.Interval>20) then begin
              tmr1.Enabled:=false;
              tmr1.Interval:=10;
              tmr1.Enabled:=True;
           end;
        end;
        if (rbuf[2]=$10) then begin                   //写入到数据
           if(tmr1.Interval>20) then begin
              tmr1.Enabled:=false;
              tmr1.Interval:=10;         //主板串口接收超时的间隔；
              tmr1.Enabled:=True;
           end;
        end;

      end else  if(StrToInt('0'+sysedit[5].Text)>600) then begin //版本大于6代则加速；
        if (rbuf[2]=$01)or(rbuf[2]=$02)or(rbuf[2]=$03) then begin      //读取到数据
           if(tmr1.Interval>20) then begin
              tmr1.Enabled:=false;
              tmr1.Interval:=20;
              tmr1.Enabled:=True;
           end;
        end;
        if (rbuf[2]=$10) then begin                   //写入到数据
           if(tmr1.Interval>20) then begin
              tmr1.Enabled:=false;
              tmr1.Interval:=20;         //主板串口接收超时的间隔；
              tmr1.Enabled:=True;
           end;
        end;

      end else  begin                      //FCT5代
        if (rbuf[2]=$01)or(rbuf[2]=$02)or(rbuf[2]=$03) then begin      //读取到数据
           if(tmr1.Interval>20) then begin
              tmr1.Enabled:=false;
              tmr1.Interval:=10;
              tmr1.Enabled:=True;
           end;
        end;
        if (rbuf[2]=$10) then begin                   //写入到数据
           if(tmr1.Interval>20) then begin
              tmr1.Enabled:=false;
              tmr1.Interval:=10;         //主板串口接收超时的间隔；
              tmr1.Enabled:=True;
           end;
        end;

      end;
      //=$05 命令处理时间不确定，不能加速；

      //rbuf[2]:=$00;   //防止重复执行接收数据的处理！
    end;
  end;       //with主界面
end;

end.
