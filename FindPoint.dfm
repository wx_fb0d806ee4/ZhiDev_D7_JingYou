object FormFind: TFormFind
  Left = 235
  Top = 241
  Width = 859
  Height = 378
  Caption = 'Search Pin --'#27979#35797#38024#26816#27979'-'#25214#28857
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 8
    Top = 8
    Width = 47
    Height = 13
    Caption = 'Command'
  end
  object lbl2: TLabel
    Left = 128
    Top = 8
    Width = 25
    Height = 13
    Caption = 'PIN#'
  end
  object lbl3: TLabel
    Left = 288
    Top = 8
    Width = 26
    Height = 13
    Caption = 'IniAD'
  end
  object lbl4: TLabel
    Left = 128
    Top = 59
    Width = 408
    Height = 16
    Caption = 'Probe '#21457#29616#28857#21495#65288'FCT5'#31995#21015#35831#30475'LCD'#26174#31034#23631#65292#27492#22788#19981#26174#31034#65289
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #23435#20307
    Font.Style = []
    ParentFont = False
  end
  object btn1: TButton
    Left = 744
    Top = 8
    Width = 91
    Height = 41
    Caption = 'Quit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = btn1Click
  end
  object edt1: TEdit
    Left = 8
    Top = 24
    Width = 105
    Height = 21
    TabOrder = 1
    Text = 'edt1'
    OnClick = edt1Click
  end
  object edt2: TEdit
    Left = 128
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'edt2'
  end
  object edt3: TEdit
    Left = 288
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 3
    Text = 'edt3'
  end
  object mmo1: TMemo
    Left = 128
    Top = 80
    Width = 601
    Height = 249
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Lines.Strings = (
      '')
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 4
  end
  object btn2: TButton
    Left = 552
    Top = 16
    Width = 177
    Height = 33
    Caption = '114'#26597#30475#33258#26816#27874#24418
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnClick = btn2Click
  end
  object btn3: TButton
    Left = 416
    Top = 16
    Width = 121
    Height = 33
    Caption = '101'#25214#28857#27874#24418
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    OnClick = btn3Click
  end
  object btn4: TButton
    Left = 744
    Top = 128
    Width = 81
    Height = 41
    Caption = #20572#27490#25214#28857
    TabOrder = 7
    OnClick = btn4Click
  end
  object mmo2: TMemo
    Left = 8
    Top = 48
    Width = 105
    Height = 273
    Enabled = False
    Lines.Strings = (
      #21629#20196#21015#34920#65306
      '101'#65306#36890#29992#25214#28857
      '105'#65306#26597#30475'HD-LS'#38459#20540
      '106'#65306#26597#30475'HS-LD'#38459#20540
      '111'#65306#20986#21378#27979#35797#36755#20837#36755#20986
      '112'#65306#32487#30005#22120#20999#25442#26495#26816#27979
      '113'#65306#25351#31034#28783#26495#26816#27979
      '114'#65306#26597#30475#33258#26816#27874#24418
      ''
      '133'#65306'SIM'#21345'#01'#25214#28857
      '134'#65306'SIM'#21345'#02'#25214#28857
      '...'
      '148'#65306'SIM'#21345'#16'#25214#28857)
    ScrollBars = ssHorizontal
    TabOrder = 8
  end
  object tmr1: TTimer
    Enabled = False
    Interval = 500
    OnTimer = tmr1Timer
    Left = 744
    Top = 56
  end
end
