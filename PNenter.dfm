object FormPN: TFormPN
  Left = 395
  Top = 315
  Width = 466
  Height = 345
  Hint = '0'
  Caption = 'FormPN '#26009#21495#36873#25321#12289#30331#24405
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDblClick = FormDblClick
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 62
    Top = 24
    Width = 121
    Height = 21
    Caption = 'PartNumber '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = #26032#23435#20307
    Font.Style = []
    ParentFont = False
  end
  object lbl2: TLabel
    Left = 62
    Top = 68
    Width = 66
    Height = 21
    Caption = 'Order '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = #26032#23435#20307
    Font.Style = []
    ParentFont = False
  end
  object lbl3: TLabel
    Left = 62
    Top = 112
    Width = 99
    Height = 21
    Caption = 'Operator '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = #26032#23435#20307
    Font.Style = []
    ParentFont = False
  end
  object lbl4: TLabel
    Left = 62
    Top = 156
    Width = 132
    Height = 21
    Caption = 'Fix/OrderPos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = #26032#23435#20307
    Font.Style = []
    ParentFont = False
  end
  object lbl11: TLabel
    Left = 62
    Top = 200
    Width = 88
    Height = 21
    Caption = 'Resource'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = #26032#23435#20307
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object btn1: TButton
    Left = 184
    Top = 240
    Width = 233
    Height = 57
    Caption = 'OK'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnClick = btn1Click
  end
  object edt2: TEdit
    Left = 184
    Top = 66
    Width = 232
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnEnter = edt2Enter
  end
  object edt3: TEdit
    Left = 184
    Top = 108
    Width = 232
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnEnter = edt3Enter
    OnKeyUp = edt3KeyUp
  end
  object cbb1: TComboBox
    Left = 184
    Top = 24
    Width = 233
    Height = 28
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 20
    ParentFont = False
    TabOrder = 0
    OnChange = cbb1Change
    Items.Strings = (
      ''
      '1'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '99'#35831#20174#12304#27979#35797#30028#38754#12305#21152#36733#31243#24207)
  end
  object cbb2: TComboBox
    Left = 184
    Top = 150
    Width = 233
    Height = 28
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 20
    ItemIndex = 0
    ParentFont = False
    TabOrder = 3
    Text = '0'
    Items.Strings = (
      '0'
      '1'
      '2'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
  end
  object edt13: TEdit
    Left = 184
    Top = 152
    Width = 193
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
  end
  object edt15: TEdit
    Left = 184
    Top = 192
    Width = 193
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    Text = '-'
    Visible = False
  end
  object cbb3: TComboBox
    Left = 184
    Top = 190
    Width = 233
    Height = 28
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 20
    ItemIndex = 0
    ParentFont = False
    TabOrder = 7
    Text = '0'
    Visible = False
    Items.Strings = (
      '0'
      '1'
      '2'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
  end
  object Panel1: TPanel
    Left = 480
    Top = 24
    Width = 281
    Height = 273
    TabOrder = 8
    object lbl5: TLabel
      Left = 56
      Top = 8
      Width = 38
      Height = 13
      Caption = 'MaxLen'
    end
    object lbl6: TLabel
      Left = 17
      Top = 8
      Width = 29
      Height = 13
      Caption = 'bit Dis'
    end
    object lbl7: TLabel
      Left = 100
      Top = 8
      Width = 35
      Height = 13
      Caption = 'MinLen'
    end
    object lbl8: TLabel
      Left = 62
      Top = 208
      Width = 20
      Height = 13
      Caption = 'Size'
    end
    object lbl9: TLabel
      Left = 144
      Top = 32
      Width = 4
      Height = 13
      Caption = '*'
    end
    object lbl10: TLabel
      Left = 192
      Top = 28
      Width = 36
      Height = 20
      Caption = '=480'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edt1: TEdit
      Left = 57
      Top = 227
      Width = 33
      Height = 21
      Enabled = False
      TabOrder = 0
      Text = '10'
    end
    object edt4: TEdit
      Left = 24
      Top = 24
      Width = 25
      Height = 21
      Enabled = False
      TabOrder = 1
      Text = '0'
    end
    object edt5: TEdit
      Left = 96
      Top = 27
      Width = 33
      Height = 21
      Enabled = False
      TabOrder = 2
      Text = '0'
    end
    object edt6: TEdit
      Left = 57
      Top = 69
      Width = 33
      Height = 21
      Enabled = False
      TabOrder = 3
      Text = '0'
    end
    object edt7: TEdit
      Left = 96
      Top = 69
      Width = 33
      Height = 21
      Enabled = False
      TabOrder = 4
      Text = '0'
    end
    object edt8: TEdit
      Left = 57
      Top = 112
      Width = 33
      Height = 21
      Enabled = False
      TabOrder = 5
      Text = '0'
    end
    object edt9: TEdit
      Left = 96
      Top = 112
      Width = 33
      Height = 21
      Enabled = False
      TabOrder = 6
      Text = '0'
    end
    object edt10: TEdit
      Left = 57
      Top = 155
      Width = 33
      Height = 21
      Enabled = False
      TabOrder = 7
      Text = '0'
    end
    object edt11: TEdit
      Left = 96
      Top = 155
      Width = 33
      Height = 21
      Enabled = False
      TabOrder = 8
      Text = '0'
    end
    object edt12: TEdit
      Left = 58
      Top = 27
      Width = 31
      Height = 21
      Enabled = False
      TabOrder = 9
      Text = '0'
    end
    object edt14: TEdit
      Left = 156
      Top = 26
      Width = 37
      Height = 21
      TabOrder = 10
      Text = '0'
    end
  end
  object tmr1: TTimer
    Enabled = False
    OnTimer = tmr1Timer
    Left = 48
    Top = 248
  end
end
