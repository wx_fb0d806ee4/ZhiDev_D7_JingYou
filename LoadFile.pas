//给客户用；

unit LoadFile;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, sListView, acShellCtrls, acProgressBar, StdCtrls, FileCtrl ,
  sEdit, ExtCtrls, sPanel, sLabel, Mask, sMaskEdit, sCustomComboEdit,Unit0_globalVariant,
  Unit2_Communication,sToolEdit, sDialogs, Buttons;

type
  TFormLoad = class(TForm)
    spnl1: TsPanel;
    sprgrsbr1: TsProgressBar;
    edt2: TsEdit;
    lbl1: TsLabel;
    lbl3: TsLabel;
    lbl4: TsLabel;
    edt3: TsEdit;
    edt1: TsDirectoryEdit;
    tmr1: TTimer;
    lv1: TsListView;
    dlg1: TsOpenDialog;
    btn1: TButton;
    btn2: TSpeedButton;
    dlgOpen2: TOpenDialog;
    btn4: TButton;
    btn3: TButton;
    edt4: TEdit;
    grp1: TGroupBox;
    cbb1: TComboBox;
    edt5: TEdit;
    edt6: TEdit;
    cbb2: TComboBox;
    lbl8: TLabel;
    lbl7: TLabel;
    lbl6: TLabel;
    lbl5: TLabel;
    btn5: TButton;
    procedure edt2Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edt1Change(Sender: TObject);
    procedure lv1DblClick(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure edt4KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btn5Click(Sender: TObject);
    procedure edt6KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure grp1DblClick(Sender: TObject);
    procedure lbl5DblClick(Sender: TObject);
    procedure cbb1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormLoad: TFormLoad;

  procedure Pnlistshow(ii:SmallInt);

implementation

uses main, PNenter;

{$R *.dfm}

procedure TFormLoad.edt2Change(Sender: TObject);
begin
  //lv1.Mask:=edt2.Text;
end;

procedure Pnlistshow(ii:SmallInt);
var
   SearchRec:TSearchRec;
   found:integer;
begin
  with FormLoad do begin
    if FormLoad.grp1.Visible then begin
       FormLoad.cbb1.Items.Clear;
       found:=FindFirst(edt1.Text+'*.txt',faAnyFile,SearchRec);  // +'\'+cbb1.text
       while    found=0    do
       begin
          cbb1.Items.Add(SearchRec.Name);             // DateTimeToString(sst,'yyyy-mm-dd HH:nn:ss',SearchRec.FindData.ftLastWriteTime);
          found:=FindNext(SearchRec);
       end;
       FindClose(SearchRec);
       grp1.Width:=FormLoad.Width-35;      
       FormLoad.Height:=215;
       FormLoad.lv1.Enabled:=false;
       FormLoad.lv1.Visible:=False;
       FormLoad.cbb1.SetFocus;
    end;
  end;
end;
procedure TFormLoad.FormShow(Sender: TObject);
var
   sst:string;

begin
  if grp1.Visible then begin
  //  if (StrToInt(FormPN.edt4.Text)and $01)=$01 then begin
  //必须启用！    cbb1.Enabled:=False;
  //  end;
    if (StrToInt(FormPN.edt4.Text)and $02)=$02 then begin        //输入订单被禁止
      edt5.Text:='0';
      edt5.Enabled:=False;
      if (StrToInt(FormPN.edt4.Text)and $04)<>$04 then begin     //员工号输入开启
        edt6.SetFocus;
      end else
        btn5.setfocus;
    end else
      edt5.SetFocus;
      
    if (StrToInt(FormPN.edt4.Text)and $04)=$04 then begin
      edt6.Text:='0';
      edt6.Enabled:=False;
    end;
    if (StrToInt(FormPN.edt4.Text)and $08)=$08 then begin
      cbb2.Text:='0';
      cbb2.Enabled:=False;
    end;
  end;

  lv1.Enabled:=true;
  edt3.Text:=form_line.edt77.text;
  FormLoad.sprgrsbr1.Position:=0;
  Form_line.sprgrsbr1.Position:=0;
  if Formload.btn3.Caption<>'从MES加载' then begin          //从本地选择文件
    if ReadChis('Comm Para', 'PathLoadfile', '')<>'' then begin
       Formload.edt1.Text:=ReadChis('Comm Para', 'PathLoadfile', '');
       if not DirectoryExists(edt1.Text) then                            //FileExists
          Formload.edt1.Text:=ExtractFilePath(application.ExeName) ;
    end else
       Formload.edt1.Text:=ExtractFilePath(application.ExeName) ;
  end;

  if (Readchis('Server','MESprogram')='auto')or(Readchis('Server','MESprogram')='autoload') then begin
     if FormLoad.btn3.Visible and (Formload.btn3.Caption='从MES加载') then
         btn3Click(Self);
     if FormLoad.btn4.Visible then
         btn4click(Self);
  end;
  if edt4.Visible then begin
    edt4.SetFocus;
    edt4.SelectAll;
    edt2.Visible:=false;
  end;
  Pnlistshow(0);
  if (Readchis('Model Para', 'LoginItem1')<>'')  then begin
    Formload.lbl5.Caption:=Readchis('Model Para', 'LoginItem1');
    Form_line.lbl90.Caption:=Readchis('Model Para', 'LoginItem1');
  end;
  if (Readchis('Model Para', 'LoginItem2')<>'')  then begin
    Formload.lbl6.Caption:=Readchis('Model Para', 'LoginItem2');
    Form_line.lbl91.Caption:=Readchis('Model Para', 'LoginItem2');
  end;
  if (Readchis('Model Para', 'LoginItem3')<>'')  then begin
    Formload.lbl7.Caption:=Readchis('Model Para', 'LoginItem3');
    Form_line.lbl92.Caption:=Readchis('Model Para', 'LoginItem3');
  end;
  if (Readchis('Model Para', 'LoginItem4')<>'')  then begin
    Formload.lbl8.Caption:=Readchis('Model Para', 'LoginItem4');
    Form_line.lbl93.Caption:=Readchis('Model Para', 'LoginItem4');
  end;

end;

procedure TFormLoad.edt1Change(Sender: TObject);
var
   SearchRec:TSearchRec;
   found:integer;

   sst:string;
   
  function   CovFileDate(Fd:_FileTime):TDateTime;
    {   转换文件的时间格式   }
  var
    Tct:_SystemTime;   
    Temp:_FileTime;   
  begin
    FileTimeToLocalFileTime(Fd,Temp);
    FileTimeToSystemTime(Temp,Tct);
    CovFileDate:=SystemTimeToDateTime(Tct);   
  end;
begin
  if (edt1.Text<>'')and(edt1.text<>'debug') then begin

    if (Readchis('Server','MESprogram')='')  then              //MES非联机
      WriteChis('Comm Para', 'PathLoadfile', edt1.Text);

    lv1.Clear; //清空
    lv1.Columns.Clear;      //lv1.ViewStyle := lv1

    lv1.Columns.Add;       //增加3项列表头
    lv1.Columns.Add;
    lv1.Columns.Add;
    lv1.Columns.Items[0].Caption:='电路板测试文件名';
    lv1.Columns.Items[1].Caption:='修改日期';
    lv1.Columns.Items[2].Caption:='文件大小bytes';
    lv1.Columns.Items[0].Width:=400;
    lv1.Columns.Items[1].Width:=200;
    lv1.Columns.Items[2].Width:=200;

     found:=FindFirst(edt1.Text+'\'+edt2.text+'*',faAnyFile,SearchRec);    // '*.txt'
     while    found=0    do
     begin
         with lv1.items.add do
         begin
            caption:=SearchRec.Name;             // DateTimeToString(sst,'yyyy-mm-dd HH:nn:ss',SearchRec.FindData.ftLastWriteTime);
            subitems.add(FormatDateTime('yyyy-mm-dd HH:nn:ss',  CovFileDate(SearchRec.FindData.ftLastWriteTime)));
            subitems.add(IntToStr(SearchRec.Size));
         end;

         found:=FindNext(SearchRec);
     end;
     FindClose(SearchRec);
  end;
end;

procedure TFormLoad.lv1DblClick(Sender: TObject);
var
  f: TextFile;
  save_file_name: string;
  TxtFile:TStringList;
begin
  with Form_line do begin
    if ( (label11.Caption<>'Ready')and(label11.Caption<>'PN错误') ) or (working >0) then      //没有通讯读到版本号或者工作忙，不执行
      begin
        if(working >0) then
          ShowMessage('MCU is busying, Pls press RESET!请回测试界面重新导入')
        else
          ShowMessage('Pls retry to click!请重新上电，或重启软件试一次！');
      end
    else begin
      if (FormLoad.lv1.SelCount<1) then begin //or ( pos(FormLoad.lv1.Selected.Caption,'.txt')<1 ) then  begin     
          ShowMessage('没有选中文件，请重试！');
      end else begin
        lbl28.Caption:='Start Inport Data to MCU';
        lbl28.Font.Color:=clRed;
        
        save_file_name :=FormLoad.edt1.text+'\'+FormLoad.lv1.Selected.Caption;//FormLoad.dlg1.FileName;

        TxtFile:=TStringList.Create;
        TxtFile.LoadFromFile(save_file_name);
        sprgrsbr1.Max:=1+TxtFile.Count;        //不能为0
        sprgrsbr1.Position:=0;
        FormLoad.sprgrsbr1.Max:=1+TxtFile.Count;
        FormLoad.sprgrsbr1.Position:=0;
        TxtFile.Destroy;

        if ( VIRLCROWS=66)then                //固件版本适应行数
        begin
             sysedit[5].Text:='806';          //通讯会使用版本号，此处必须强行设置固件版本号；
             VIRLCROWS:=50;
        end;
        lbl29.Caption:=save_file_name;
        try
          assignfile(f, save_file_name);
          //rewrite(f);
          Reset(f);
          //application.ProcessMessages;
          modbusfuntion:=$4E;
          FormLoad.lv1.Enabled:=False;
          FormLoad.edt3.Text:='';
        finally
          Closefile(f);
        end;
        edt77.Color:=clRed; edt77.Text:='正在加载新程序...';
        FormPN.cbb1.Tag:=0;
        Label11.Font.Color:=clred;
        Addtolog('导入文件');
      end;
    end;
  end; 
end;

procedure TFormLoad.tmr1Timer(Sender: TObject);
begin
  sprgrsbr1.max:=Form_line.sprgrsbr1.max;
  sprgrsbr1.Position:=Form_line.sprgrsbr1.Position;
  if FormLoad.Visible and (sprgrsbr1.Position=sprgrsbr1.max) and  not btn1.Enabled then begin   //加载结束
    btn1.Enabled:=True;
  end else if (sprgrsbr1.Position>0)and (sprgrsbr1.Position<sprgrsbr1.max) then begin          //加载中...
    btn1.Enabled:=False;
  end else  begin                                       //
    btn1.Enabled:=True;
  end;

  if not grp1.Visible and(edt3.Text='')and(sprgrsbr1.Position=sprgrsbr1.max)then
     edt3.Text:=FormLoad.lv1.Selected.Caption;    //文件名更新

end;

procedure TFormLoad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if grp1.Visible and(btn5.Tag=0) then  Application.Terminate;
  if (not lv1.Enabled)and (sprgrsbr1.position=sprgrsbr1.max) then begin
    Form_line.tmr3.Enabled:=true;       //定时读取主板信息  //通过定时器执行不同的开机读取指令      ，可启动//读上限下限 及短路群
    if Form_line.groupbox1.hint<>'5' then begin         //FCT5不支持加载和读取长文件名
      //Form_line.label11.Caption:='Loading...';  Form_line.Label11.Font.Size:=150;
      Form_line.Lbl169.Caption:='';Form_line.lbl169.Font.Size:=10;
    end;
    if Form_line.sbtbtn1.Visible then
       Form_line.sbtbtn1.Enabled:=False;
    
    Form_line.tmr4.Enabled:=true;       //光标定位在条码框
  end;

end;

procedure TFormLoad.btn1Click(Sender: TObject);
begin
  FormLoad.Close;
end;

const  SELDIRHELP   =   1000;
procedure TFormLoad.btn2Click(Sender: TObject);
var 
    Dir:   string;
begin
    Dir   := edt1.Text;// '';
    if   SelectDirectory(Dir,   [sdAllowCreate,   sdPerformCreate,   sdPrompt],SELDIRHELP)   then
    begin
        edt1.Text   :=   Dir+'\';
        Pnlistshow(0);
    end;
end;

procedure TFormLoad.btn4Click(Sender: TObject);
begin
    if ( VIRLCROWS=66)then                //固件版本适应行数
    begin
      sysedit[5].Text:='806';          //通讯会使用版本号，此处必须强行设置固件版本号；
      VIRLCROWS:=50;
    end;
    AutoExportNextPN(1);
end;

procedure TFormLoad.btn3Click(Sender: TObject);
begin
  with Form_line do begin
    if FormLoad.btn3.Visible and (Formload.btn3.Caption='从MES加载') then begin
       btn118Click(Form_line);
       FormLoad.edt1Change(Self);
       if lbl164.Caption='OK' then begin
          if FormLoad.lv1.Items.Count>0 then
            FormLoad.lv1.ItemIndex:=0;
          if(FormLoad.edt1.Text<>'')and(FormLoad.edt1.Text<>'debug') then
            FormLoad.lv1DblClick(Self)
          else
            FormLoad.lv1.Clear;
       end else begin
          case
            Application.MessageBox('从MES系统加载测试程序失败！请重新加载',
            '提示', MB_OK + MB_ICONQUESTION) of
            IDOK:
              begin
                  FormLoad.lv1.Selected.Caption:='';
              end;
          end;
       end;
    end;
  end;
end;

procedure TFormLoad.edt4KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if Key=$0D then begin
    if FormLoad.lv1.Items.Count>0 then
      FormLoad.lv1.ItemIndex:=0;
    if(FormLoad.edt1.Text<>'')and(FormLoad.edt1.Text<>'debug') then
      FormLoad.lv1DblClick(Self)
    else
      FormLoad.lv1.Clear;
  end else begin
     edt2.Text:= edt4.Text+'.txt';
     edt1Change(Self);
  end;
end;

procedure TFormLoad.btn5Click(Sender: TObject);
var
  f: TextFile;
  save_file_name: string;
  TxtFile:TStringList;

begin
    if( Trim(cbb1.Text)='')then cbb1.Color:=clRed else cbb1.Color:=clWindow;   //or(FormPN.cbb1.ItemIndex<1)

    if Trim(edt5.Text)='' then begin edt5.Color:=clRed ;exit; end;
    edt5.Color:=clWindow;

    if Trim(edt6.Text)='' then begin edt6.Color:=clRed ;exit; end;
    edt6.Color:=clWindow;

    save_file_name :=FormLoad.edt1.text+formLoad.cbb1.Text;
    if Pos('.txt',formLoad.cbb1.Text)<=0 then
      save_file_name:=save_file_name+'.txt';
    if not FileExists(save_file_name) then begin cbb1.Color:=clRed ;exit; end;
    form_line.edt77.text:='正在加载机种名';//cbb1.Text;               //PC软件定义的机种名，->主板含有VZV0则有更高优先级
    form_line.edt78.text:=edt5.Text;
    form_line.edt79.text:=edt6.Text;
    form_line.edt80.text:=cbb2.Text;               //治具编号
    cbb1.Enabled:=False;cbb2.Enabled:=False;
    edt5.Enabled:=False;edt6.Enabled:=False;
    with Form_line do begin
      lbl28.Caption:='Start Inport Data to MCU';
      lbl28.Font.Color:=clRed;

      TxtFile:=TStringList.Create;
      TxtFile.LoadFromFile(save_file_name);
      sprgrsbr1.Max:=1+TxtFile.Count;        //不能为0
      sprgrsbr1.Position:=0;
      FormLoad.sprgrsbr1.Max:=1+TxtFile.Count;
      FormLoad.sprgrsbr1.Position:=0;
      TxtFile.Destroy;

      if ( VIRLCROWS=66)then                //固件版本适应行数
      begin
        sysedit[5].Text:='806';          //通讯会使用版本号，此处必须强行设置固件版本号；
        VIRLCROWS:=50;
      end;
      lbl29.Caption:=save_file_name;
      try
        assignfile(f, save_file_name);       
        Reset(f);            //application.ProcessMessages;
        modbusfuntion:=$4E;
        FormLoad.lv1.Enabled:=False;
        FormLoad.edt3.Text:='';
      finally
        Closefile(f);
      end;
      edt77.Color:=clRed; edt77.Text:='正在加载新程序...';
      FormPN.cbb1.Tag:=0;
      Label11.Font.Color:=clred;
      Addtolog('导入文件');
    end;
end;

procedure TFormLoad.edt6KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=$0D then begin
    edt3.SelectAll;
    btn5Click(self);
  end
end;

procedure TFormLoad.grp1DblClick(Sender: TObject);
var str,password:string;

begin
   if readchis('Comm Para', 'PassWord')<>'' then  begin    //不为空
      password:=readchis('Comm Para', 'PassWord');
   end else
      password:='999';
   if  InputQuery('直接进入', '请输入密码', str) then
   begin
     if (Str=password) or (Str='137760') then  begin
       btn5.tag:=10;
        form_line.edt77.text:='正在加载机种名';
        form_line.edt78.text:=edt5.Text;
        form_line.edt79.text:=edt6.Text;
        form_line.edt80.text:=cbb2.Text;               //治具编号
       Close;
     end;
   end;
end;

procedure TFormLoad.lbl5DblClick(Sender: TObject);
var str,password:string;

begin
   if readchis('Comm Para', 'PassWord')<>'' then  begin    //不为空
      password:=readchis('Comm Para', 'PassWord');
   end else
      password:='999';
   if  InputQuery('选择程序路径', '请输入密码', str) then
   begin
     if (Str=password) or (Str='137760') then  begin
       btn2Click(self);
     end;
   end;
end;

procedure TFormLoad.cbb1DblClick(Sender: TObject);
begin
  lbl5DblClick(Self);
end;

end.
