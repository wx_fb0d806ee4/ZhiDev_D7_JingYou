unit StepCopy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,Unit0_globalVariant;

type
  TFormStepCopy = class(TForm)
    lbl1: TLabel;
    edt1: TEdit;
    lbl2: TLabel;
    edt2: TEdit;
    lbl3: TLabel;
    edt3: TEdit;
    btn1: TButton;
    lbl4: TLabel;
    edt4: TEdit;
    edt5: TEdit;
    lbl5: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    btn2: TButton;
    edt6: TEdit;
    lbl6: TLabel;
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormStepCopy: TFormStepCopy;

implementation

{$R *.dfm}

procedure TFormStepCopy.btn1Click(Sender: TObject);
begin
  if MessageDlg('从步骤1到步骤'+edt1.text+'之测试步骤将拷贝至其余'+edt3.Text+'个测试板'
          +',且从针点1到针点'+edt2.text+'之短路群将拷贝至其余'+edt3.Text+'个测试板'
          +',LCD显示屏显示拷贝过程',  mtWarning, mbOKCancel, 0) = mrOk then
  begin
    modbusfun16int:=$10B1;     //拷贝指令
    modbusfun16len:=$0004;
    //sbuf[1]:=1;
    sbuf[2]:=$10;

    sbuf[8+2*0]:=StrToInt('0'+edt1.Text) div 256;      //用于代码版本
    sbuf[9+2*0]:=StrToInt('0'+edt1.Text) mod 256;
    sbuf[8+2*1]:=StrToInt('0'+edt2.Text) div 256;      //用于修订版本
    sbuf[9+2*1]:=StrToInt('0'+edt2.Text) mod 256;

    sbuf[8+2*2]:=StrToInt('0'+edt5.Text) mod 256;       //开短路次数
    if(edt3.Text='') or (edt3.Text='0') then edt3.Text:='1';
    sbuf[9+2*2]:=StrToInt('0'+edt3.Text) mod 256;       //total联板总数
    //px_internal:=StrToInt('0'+edt3.Text);
    sbuf[8+2*3]:=StrToInt('0'+edt4.Text)  div 256;      //绿色指示灯偏移
    sbuf[9+2*3]:=StrToInt('0'+edt4.Text)  mod 256;

    close;
  end;

end;

procedure TFormStepCopy.btn2Click(Sender: TObject);
begin
  if MessageDlg('从针点1到针点'+edt2.text+'之短路群将拷贝至其余'+edt3.Text+'个测试板'
          +',LCD显示屏显示拷贝过程',  mtWarning, mbOKCancel, 0) = mrOk then
  begin
    modbusfun16int:=$10B2;     //拷贝指令
    modbusfun16len:=$0004;
    //sbuf[1]:=1;
    sbuf[2]:=$10;

    sbuf[8+2*0]:=StrToInt('0') div 256;                     //默认：0，不拷贝步骤
    sbuf[9+2*0]:=StrToInt('0') mod 256;

    if(edt6.Text='')or(edt6.Text='0') then edt6.Text:='32';
    sbuf[8+2*1]:=StrToInt('0'+edt6.Text) div 256;           //复制点偏移 ，默认：32
    sbuf[9+2*1]:=StrToInt('0'+edt6.Text) mod 256;

    sbuf[8+2*2]:=StrToInt('0'+edt5.Text) mod 256;           //开短路次数，默认：0或者1 仅拷贝一次短路群；

    if(edt3.Text='') or (edt3.Text='0')or (edt3.Text='1') then edt3.Text:='2';
    sbuf[9+2*2]:=StrToInt('0'+edt3.Text) mod 256;           //total总板数量，默认：2复制一次

  //不执行  sbuf[8+2*3]:=StrToInt('96')  div 256;                   //默认：96  ，不改变绿灯偏移
  //  sbuf[9+2*3]:=StrToInt('96')  mod 256;
    addToLog('按第一单元的短路群组，拷贝出其它单元短路群组' );

    close;
  end;

end;

procedure TFormStepCopy.FormShow(Sender: TObject);
begin
    if btn2.Visible then begin
      //lbl1.Visible:=False;
      //edt1.Visible:=False;
      //lbl4.Visible:=False;
      //edt4.Visible:=False;
    end else begin

      lbl1.Visible:=True;
      edt1.Visible:=True;
      lbl4.Visible:=True;
      edt4.Visible:=True;
    end;
end;

end.
