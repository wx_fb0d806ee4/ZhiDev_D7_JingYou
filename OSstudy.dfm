object FormOS: TFormOS
  Left = 299
  Top = 190
  Width = 722
  Height = 392
  Caption = 'OSwindow '#24320'/'#30701#36335#23398#20064
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = mm1
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 20
  object lbl1: TLabel
    Left = 16
    Top = 40
    Width = 403
    Height = 20
    Caption = #30701#36335#32676#31383#21475#65288'FCT5'#31995#21015#35831#30475'LCD'#26174#31034#23631#65292#27492#22788#26080#26174#31034#65289'   '
  end
  object mmo1: TMemo
    Left = 8
    Top = 64
    Width = 673
    Height = 217
    Lines.Strings = (
      'mmo1')
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object btn1: TButton
    Left = 592
    Top = 288
    Width = 99
    Height = 41
    Caption = 'Quit'
    TabOrder = 1
    OnClick = btn1Click
  end
  object btn2: TButton
    Left = 16
    Top = 288
    Width = 233
    Height = 41
    Caption = '1'#39's  '#36890#29992#65306#23398#20064#19968#27425#30701#36335#32676'           '
    TabOrder = 2
    OnClick = btn2Click
  end
  object btn3: TButton
    Left = 304
    Top = 288
    Width = 233
    Height = 41
    Caption = '2'#39's  '#19987#29992#65306#23398#20064#20108#27425#30701#36335#32676'         '
    TabOrder = 3
    OnClick = btn3Click
  end
  object btn4: TButton
    Left = 8
    Top = 0
    Width = 105
    Height = 33
    Caption = 'Read '#35835#30701#36335#32676
    TabOrder = 4
    OnClick = btn4Click
  end
  object btn6: TButton
    Left = 352
    Top = 0
    Width = 145
    Height = 33
    Caption = #32763#39029' PageDown'
    TabOrder = 5
    OnClick = btn6Click
  end
  object btn5: TBitBtn
    Left = 184
    Top = 0
    Width = 129
    Height = 33
    Caption = 'Save '#20445#23384#30701#36335#32676
    TabOrder = 6
    OnClick = btn5Click
  end
  object mm1: TMainMenu
    Left = 568
    Top = 8
    object N11: TMenuItem
      Caption = 'ExStudy '#25193#23637#23398#20064
      object N12: TMenuItem
        Caption = '1'#39'sA'
        OnClick = N12Click
      end
      object N21: TMenuItem
        Caption = '2'#39'sA'
        OnClick = N21Click
      end
      object N13: TMenuItem
        Caption = '1'#39'sB '#19968#27425#23398#20064#65288'5'#20493#39537#21160#30005#27969#65289
        OnClick = N13Click
      end
      object N22: TMenuItem
        Caption = '2'#39'sB '#20108#27425#23398#20064#65288'5'#20493#39537#21160#30005#27969#65289
        OnClick = N22Click
      end
    end
  end
end
