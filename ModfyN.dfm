object FormModfyNstep: TFormModfyNstep
  Left = 332
  Top = 339
  Width = 1042
  Height = 435
  Caption = 'Nstep '#22810#27493#39588#22810#21442#25968#22359#20462#25913
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object grp1: TGroupBox
    Left = 16
    Top = 8
    Width = 505
    Height = 57
    Caption = 'Page N '#39029#20869#33539#22260#35774#23450
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object lbl1: TsWebLabel
      Left = 8
      Top = 24
      Width = 57
      Height = 20
      Caption = 'From '#20174
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HoverFont.Charset = DEFAULT_CHARSET
      HoverFont.Color = clWindowText
      HoverFont.Height = -16
      HoverFont.Name = 'MS Sans Serif'
      HoverFont.Style = []
    end
    object lbl2: TsWebLabel
      Left = 285
      Top = 24
      Width = 38
      Height = 20
      Caption = 'To '#21040
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HoverFont.Charset = DEFAULT_CHARSET
      HoverFont.Color = clWindowText
      HoverFont.Height = -16
      HoverFont.Name = 'MS Sans Serif'
      HoverFont.Style = []
    end
    object edt1: TsDecimalSpinEdit
      Left = 98
      Top = 24
      Width = 121
      Height = 28
      MaxLength = 4
      TabOrder = 0
      Text = '1'
      OnChange = edt1Change
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 4999.000000000000000000
      MinValue = 1.000000000000000000
      Value = 1.000000000000000000
      DecimalPlaces = 0
    end
    object edt2: TsDecimalSpinEdit
      Left = 352
      Top = 24
      Width = 121
      Height = 28
      MaxLength = 4
      TabOrder = 1
      Text = '10'
      OnChange = edt1Change
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 4999.000000000000000000
      MinValue = 1.000000000000000000
      Value = 10.000000000000000000
      DecimalPlaces = 0
    end
  end
  object grp2: TsGroupBox
    Left = 16
    Top = 72
    Width = 993
    Height = 305
    Caption = 'Modfy '#20462#25913#35774#23450
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    SkinData.SkinSection = 'GROUPBOX'
    object chk1: TsCheckBox
      Left = 24
      Top = 24
      Width = 89
      Height = 24
      Caption = 'Num '#24207#21495
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = edt1Change
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk2: TsCheckBox
      Left = 24
      Top = 56
      Width = 154
      Height = 24
      Caption = 'Name '#27493#39588#21517#31216'      '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = edt1Change
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk3: TsCheckBox
      Left = 24
      Top = 88
      Width = 105
      Height = 24
      Caption = 'Std '#26631#20934#20540'  '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = edt1Change
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk4: TsCheckBox
      Left = 24
      Top = 120
      Width = 104
      Height = 24
      Caption = 'Upp'#19978#38480'%  '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = edt1Change
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk5: TsCheckBox
      Left = 301
      Top = 26
      Width = 103
      Height = 24
      Caption = 'Low'#19979#38480'%  '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = edt1Change
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk6: TsCheckBox
      Left = 300
      Top = 56
      Width = 111
      Height = 24
      Caption = 'Hpoint '#39640#28857'  '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = edt1Change
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk7: TsCheckBox
      Left = 300
      Top = 88
      Width = 108
      Height = 24
      Caption = 'Lpoint'#20302#28857'   '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      OnClick = edt1Change
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk8: TsCheckBox
      Left = 300
      Top = 120
      Width = 104
      Height = 24
      Caption = 'Delay'#24310#26102'   '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      OnClick = edt1Change
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk10: TsCheckBox
      Left = 536
      Top = 24
      Width = 78
      Height = 24
      Caption = 'K'#27604#20363'    '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnClick = edt1Change
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk11: TsCheckBox
      Left = 536
      Top = 56
      Width = 75
      Height = 24
      Caption = 'B'#20559#31227'   '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      OnClick = edt1Change
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk12: TsCheckBox
      Left = 535
      Top = 88
      Width = 78
      Height = 24
      Caption = 'P '#24179#22343'   '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 10
      OnClick = edt1Change
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk13: TsCheckBox
      Left = 535
      Top = 120
      Width = 100
      Height = 24
      Caption = #32852#26495#24207#21495'    '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 11
      OnClick = edt1Change
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk14: TsCheckBox
      Left = 760
      Top = 24
      Width = 94
      Height = 24
      Caption = 'H2'#39640#28857'2   '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 12
      OnClick = edt1Change
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk15: TsCheckBox
      Left = 760
      Top = 56
      Width = 91
      Height = 24
      Caption = 'L2'#20302#28857'2   '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 13
      OnClick = edt1Change
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object edt3: TsDecimalSpinEdit
      Left = 144
      Top = 80
      Width = 97
      Height = 28
      TabOrder = 14
      Text = '1000'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      Value = 1000.000000000000000000
      DecimalPlaces = 0
    end
    object edt4: TsDecimalSpinEdit
      Left = 144
      Top = 112
      Width = 97
      Height = 28
      TabOrder = 15
      Text = '10'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      Value = 10.000000000000000000
      DecimalPlaces = 0
    end
    object edt5: TsDecimalSpinEdit
      Left = 416
      Top = 24
      Width = 89
      Height = 28
      TabOrder = 16
      Text = '10'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      Value = 10.000000000000000000
      DecimalPlaces = 0
    end
    object edt6: TsDecimalSpinEdit
      Left = 416
      Top = 56
      Width = 89
      Height = 28
      TabOrder = 17
      Text = '1'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      Value = 1.000000000000000000
      DecimalPlaces = 0
    end
    object edt7: TsDecimalSpinEdit
      Left = 416
      Top = 88
      Width = 89
      Height = 28
      TabOrder = 18
      Text = '2'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      Value = 2.000000000000000000
      DecimalPlaces = 0
    end
    object edt8: TsDecimalSpinEdit
      Left = 416
      Top = 120
      Width = 89
      Height = 28
      TabOrder = 19
      Text = '5'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 65535.000000000000000000
      Value = 5.000000000000000000
      DecimalPlaces = 0
    end
    object edt9: TsDecimalSpinEdit
      Left = 640
      Top = 24
      Width = 73
      Height = 28
      TabOrder = 20
      Text = '100'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 32222.000000000000000000
      Value = 100.000000000000000000
      DecimalPlaces = 0
    end
    object edt10: TsDecimalSpinEdit
      Left = 640
      Top = 56
      Width = 73
      Height = 28
      TabOrder = 21
      Text = '0'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      DecimalPlaces = 0
    end
    object edt11: TsDecimalSpinEdit
      Left = 640
      Top = 88
      Width = 73
      Height = 28
      TabOrder = 22
      Text = '0'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 32222.000000000000000000
      DecimalPlaces = 0
    end
    object edt12: TsDecimalSpinEdit
      Left = 640
      Top = 120
      Width = 73
      Height = 28
      TabOrder = 23
      Text = '0'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 32222.000000000000000000
      DecimalPlaces = 0
    end
    object edt13: TsDecimalSpinEdit
      Left = 856
      Top = 24
      Width = 81
      Height = 28
      TabOrder = 24
      Text = '3'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 32222.000000000000000000
      Value = 3.000000000000000000
      DecimalPlaces = 0
    end
    object edt14: TsDecimalSpinEdit
      Left = 856
      Top = 56
      Width = 81
      Height = 28
      TabOrder = 25
      Text = '4'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 32222.000000000000000000
      Value = 4.000000000000000000
      DecimalPlaces = 0
    end
    object edt15: TsEdit
      Left = 176
      Top = 48
      Width = 97
      Height = 28
      TabOrder = 26
      Text = 'R???'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object grp3: TsGroupBox
      Left = 16
      Top = 144
      Width = 969
      Height = 153
      Caption = 'Mode'
      TabOrder = 27
      SkinData.SkinSection = 'GROUPBOX'
      object edt16: TsEdit
        Left = 216
        Top = 13
        Width = 89
        Height = 28
        TabOrder = 0
        Text = '0R01'
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object chk9: TsCheckBox
        Left = 68
        Top = 17
        Width = 108
        Height = 24
        Caption = 'Mode'#27169#24335'    '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = edt1Change
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object cbb1: TsComboBox
        Left = 24
        Top = 73
        Width = 201
        Height = 28
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        ItemHeight = 22
        ItemIndex = -1
        TabOrder = 2
        Text = '1---'#37327#31243#36873#25321#25110#27425#27169#24335
        OnChange = cbb1Change
        Items.Strings = (
          '0'#65306#21333#20301#20026'10'#30340'0'#27425#26041
          '1'#65306#21333#20301#20026'10'#30340'1'#27425#26041
          '2'#65306#21333#20301#20026'10'#30340'2'#27425#26041
          '3'#65306#21333#20301#20026'10'#30340'3'#27425#26041
          '4'#65306#21333#20301#20026'10'#30340'4'#27425#26041
          '5'#65306#21333#20301#20026'10'#30340'5'#27425#26041
          '6'#65306#21333#20301#20026'10'#30340'6'#27425#26041
          'F'#65306#22235#32447#30005#38459#21333#20301#20026'mohm'
          'L'#65306' LED'#27979#37327'0'
          'M'#65306'LED'#27979#37327'1'
          'X'#65306#32487#30005#22120#20999#25442'+'
          'Y'#65306#32487#30005#22120#20999#25442
          'Z'#65306#32487#30005#22120#20851#38381)
      end
      object chk16: TsCheckBox
        Left = 808
        Top = 24
        Width = 37
        Height = 24
        Caption = 'nc'
        Enabled = False
        TabOrder = 3
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object chk17: TsCheckBox
        Left = 808
        Top = 53
        Width = 144
        Height = 24
        Caption = #32467#26524#26174#31034#22312'LCD   '
        TabOrder = 4
        OnClick = chk17Click
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object chk18: TsCheckBox
        Left = 808
        Top = 82
        Width = 100
        Height = 24
        Caption = #27493#39588#21069#26174#31034
        TabOrder = 5
        OnClick = chk18Click
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object chk19: TsCheckBox
        Left = 808
        Top = 112
        Width = 116
        Height = 24
        Caption = #26597#30475#27979#37327#32467#26524
        TabOrder = 6
        OnClick = chk19Click
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object cbb2: TsComboBox
        Left = 240
        Top = 73
        Width = 233
        Height = 28
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        ItemHeight = 22
        ItemIndex = -1
        TabOrder = 7
        Text = '2---'#20027#27979#37327#27169#24335
        OnChange = cbb2Change
        Items.Strings = (
          'S'#65306#30701#36335#27979#35797
          'O'#65306#24320#36335#27979#35797
          'R'#65306#30005#38459#27979#35797
          'C'#65306#30005#23481#27979#35797
          'D'#65306#20108#26497#31649#27979#35797
          'Q'#65306#36755#20986#25511#21046
          'G'#65306'Agilent'#19975#29992#34920
          'H'#65306'minghe'
          'A'#65306'485'#36890#20449
          'B'#65306#19978#20256'PC'
          'I'#65306'I2C'#36890#20449
          'P'#65306#36895#24230#27979#37327
          'F'#65306#39057#29575#27979#37327
          'T'#65306#20132#27969#20449#21495
          'Z'#65306#31995#32479#21442#25968#35774#23450
          '')
      end
      object cbb3: TsComboBox
        Left = 496
        Top = 73
        Width = 273
        Height = 28
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        ItemHeight = 22
        ItemIndex = -1
        TabOrder = 8
        Text = '3----'#27425#27979#37327#27169#24335
        OnChange = cbb3Change
        Items.Strings = (
          '0'#65306#38454#27573'1'
          '1'#65306#38454#27573'2'
          '2'#65306#38454#27573'3'
          '3'#65306#38454#27573'4'
          '4'#65306#38454#27573'5'
          'R:'#20132#27969#27979#30005#38459
          'C:'#20132#27969#27979#30005#23481
          'L:'#20132#27969#27979#30005#24863)
      end
    end
    object chk20: TCheckBox
      Left = 128
      Top = 24
      Width = 145
      Height = 17
      Caption = 'SKIP'#21024#30053'/'#21453#21024#30053#27493#39588
      TabOrder = 28
    end
  end
  object spnl1: TsPanel
    Left = 712
    Top = 8
    Width = 297
    Height = 57
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object btn1: TsButton
      Left = 16
      Top = 8
      Width = 113
      Height = 33
      Caption = 'OK '#30830#23450
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = btn1Click
      SkinData.SkinSection = 'BUTTON'
    end
    object btn2: TsButton
      Left = 152
      Top = 8
      Width = 129
      Height = 33
      Caption = 'Cancle '#21462#28040
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btn2Click
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object chk21: TsCheckBox
    Left = 536
    Top = 12
    Width = 60
    Height = 20
    Caption = #38388#38548' =  '
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = edt1Change
    SkinData.SkinSection = 'CHECKBOX'
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object edt17: TsDecimalSpinEdit
    Left = 600
    Top = 8
    Width = 97
    Height = 28
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    Text = '0'
    OnChange = edt1Change
    SkinData.SkinSection = 'EDIT'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    Increment = 1.000000000000000000
    DecimalPlaces = 0
  end
  object edt18: TsDecimalSpinEdit
    Left = 600
    Top = 40
    Width = 97
    Height = 28
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Text = '0'
    OnChange = edt1Change
    SkinData.SkinSection = 'EDIT'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    Increment = 1.000000000000000000
    DecimalPlaces = 0
  end
  object chk22: TsCheckBox
    Left = 536
    Top = 48
    Width = 60
    Height = 20
    Caption = #20313#25968'=   '
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    OnClick = edt1Change
    SkinData.SkinSection = 'CHECKBOX'
    ImgChecked = 0
    ImgUnchecked = 0
  end
end
