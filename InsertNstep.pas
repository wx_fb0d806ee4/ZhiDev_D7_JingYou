unit InsertNstep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,Unit0_globalVariant, StdCtrls, sButton, ExtCtrls, sPanel,
  sComboBox, sCheckBox, sGroupBox, sEdit, sSpinEdit, sLabel;

type
  TFormInsertN = class(TForm)
    grp1: TGroupBox;
    lbl1: TsWebLabel;
    lbl2: TsWebLabel;
    edt1: TsDecimalSpinEdit;
    edt2: TsDecimalSpinEdit;
    grp2: TsGroupBox;
    chk1: TsCheckBox;
    chk2: TsCheckBox;
    chk3: TsCheckBox;
    chk4: TsCheckBox;
    chk5: TsCheckBox;
    chk6: TsCheckBox;
    chk7: TsCheckBox;
    chk8: TsCheckBox;
    chk10: TsCheckBox;
    chk11: TsCheckBox;
    chk12: TsCheckBox;
    chk13: TsCheckBox;
    chk14: TsCheckBox;
    chk15: TsCheckBox;
    edt3: TsDecimalSpinEdit;
    edt4: TsDecimalSpinEdit;
    edt5: TsDecimalSpinEdit;
    edt6: TsDecimalSpinEdit;
    edt7: TsDecimalSpinEdit;
    edt8: TsDecimalSpinEdit;
    edt9: TsDecimalSpinEdit;
    edt10: TsDecimalSpinEdit;
    edt11: TsDecimalSpinEdit;
    edt12: TsDecimalSpinEdit;
    edt13: TsDecimalSpinEdit;
    edt14: TsDecimalSpinEdit;
    edt15: TsEdit;
    grp3: TsGroupBox;
    edt16: TsEdit;
    chk9: TsCheckBox;
    cbb1: TsComboBox;
    chk16: TsCheckBox;
    chk17: TsCheckBox;
    chk18: TsCheckBox;
    chk19: TsCheckBox;
    cbb2: TsComboBox;
    cbb3: TsComboBox;
    spnl1: TsPanel;
    btn1: TsButton;
    btn2: TsButton;
    procedure btn2Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure chk19Click(Sender: TObject);
    procedure chk18Click(Sender: TObject);
    procedure chk17Click(Sender: TObject);
    procedure cbb3Change(Sender: TObject);
    procedure cbb2Change(Sender: TObject);
    procedure cbb1Change(Sender: TObject);
    procedure edt1Change(Sender: TObject);
    procedure edt2Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edt1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btn1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btn2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormInsertN: TFormInsertN;

implementation

uses main;

{$R *.dfm}

procedure TFormInsertN.btn2Click(Sender: TObject);
begin
  close;
end;

procedure TFormInsertN.btn1Click(Sender: TObject);
var tt,i:SmallInt;
  temp8 :byte;
  str:string;
begin
    modbusfun16int:=$2F22;
    modbusfun16len:=$12;
    //sbuf[1]:=1;
    sbuf[2]:=$10;
    //3~7为地址，长度共5个字节；
    begin
      sbuf[8]:=StrToInt('0'+edt1.Text)div 256;      //序号 开始步骤
      sbuf[9]:=StrToInt('0'+edt1.Text)mod 256;
    end;
    //end;
    sbuf[10]:=byte('?');sbuf[11]:=byte(' ');sbuf[12]:=byte(' ');sbuf[13]:=byte(' ');
    str:= AnsiUpperCase(edt15.Text);
    Move(str[1],sbuf[10],Length(str));
    temp8:=sbuf[10]; sbuf[10]:=sbuf[11];sbuf[11]:=temp8;
    temp8:=sbuf[12]; sbuf[12]:=sbuf[13];sbuf[13]:=temp8;

    if Trim(edt3.Text)<>'' then
    begin
      sbuf[14]:=StrToInt(edt3.Text)shr 8;//div 256; //标准值
      sbuf[15]:=StrToInt(edt3.Text)mod 256;
    end;
    sbuf[16]:=StrToInt('0'+edt2.Text)div 256; //运算值（测量值）--- 转为步骤总数
    sbuf[17]:=StrToInt('0'+edt2.Text)mod 256;
    
    if Trim(edt4.Text)<>'' then
    begin
      sbuf[18]:=StrToInt(edt4.Text)shr 8;//div 256; //上限
      sbuf[19]:=StrToInt(edt4.Text)mod 256;
    end;
    if Trim(edt5.Text)<>'' then
    begin
      sbuf[20]:=StrToInt(edt5.Text)shr 8;//div 256; //下限
      sbuf[21]:=StrToInt(edt5.Text)mod 256;
    end;
    sbuf[22]:=StrToInt('0'+edt6.Text) div 256;  //高点
    sbuf[23]:=StrToInt('0'+edt6.Text);
    sbuf[24]:=StrToInt('0'+edt7.Text) div 256;  //低点
    sbuf[25]:=StrToInt('0'+edt7.Text);
    sbuf[26]:=StrToInt('0'+edt8.Text) div 256;  //延时
    sbuf[27]:=StrToInt('0'+edt8.Text);

    sbuf[28]:=byte('R');sbuf[29]:=byte('0');sbuf[30]:=byte('1');sbuf[31]:=byte('0');
    str:= AnsiUpperCase(edt16.Text);
    Move(str[1],sbuf[28],Length(str));
    temp8:=sbuf[28]; sbuf[28]:=sbuf[29];sbuf[29]:=temp8;
    temp8:=sbuf[30]; sbuf[30]:=sbuf[31];sbuf[31]:=temp8;

    sbuf[32]:=StrToInt('0'+edt9.Text)div 256; //k值
    sbuf[33]:=StrToInt('0'+edt9.Text)mod 256;
    sbuf[34]:=StrToInt(edt10.Text)shr 8; //b值
    sbuf[35]:=StrToInt(edt10.Text)mod 256;
    sbuf[36]:=StrToInt('0'+edt11.Text)div 256; //上限 2
    sbuf[37]:=StrToInt('0'+edt11.Text)mod 256;
    sbuf[38]:=StrToInt('0'+edt12.Text)div 256; //下限2
    sbuf[39]:=StrToInt('0'+edt12.Text)mod 256;
    sbuf[40]:=StrToInt('0'+edt13.Text) div 256;  //高点2
    sbuf[41]:=StrToInt('0'+edt13.Text);
    sbuf[42]:=StrToInt('0'+edt14.Text) div 256;  //低点2
    sbuf[43]:=StrToInt('0'+edt14.Text);
 {    tt:=0;
   if chk1.Checked then tt:=tt+$0001;
    if chk2.Checked then tt:=tt+$0002;
    if chk3.Checked then tt:=tt+$0004;
    if chk4.Checked then tt:=tt+$0010;
    if chk5.Checked then tt:=tt+$0020;
    if chk6.Checked then tt:=tt+$0040;
    if chk7.Checked then tt:=tt+$0080;
    if chk8.Checked then tt:=tt+$0100;
    if chk9.Checked then tt:=tt+$0200;
    if chk10.Checked then tt:=tt+$0400;
    if chk11.Checked then tt:=tt+$0800;
    if chk12.Checked then tt:=tt+$1000;
    if chk13.Checked then tt:=tt+$2000;
    if chk14.Checked then tt:=tt+$4000;
    if chk15.Checked then tt:=tt+$8000;
    sbuf[44]:=tt shr 8;  //修改标
    sbuf[45]:=tt;   }
    sbuf[44]:=StrToInt('0')div 256;      //指定特殊序号
    sbuf[45]:=StrToInt('0')mod 256;
    case cbb2.Text[1] of
       '0':       //结束步骤
       begin
          if (edt15.Text='END')and(edt16.Text='0000') then begin
            sbuf[44]:=StrToInt('9999')div 256;
            sbuf[45]:=StrToInt('9999')mod 256;
            modbusfun16len:=modbusfun16len+1;
          end;
       end;
    end;
    addToLog('插入第'+edt1.Text+'~'+edt2.Text+'步骤' );
    close;
end;

procedure TFormInsertN.chk19Click(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);


end;

procedure TFormInsertN.chk18Click(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);


end;

procedure TFormInsertN.chk17Click(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);


end;

procedure TFormInsertN.cbb3Change(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);


end;

procedure TFormInsertN.cbb2Change(Sender: TObject);
var tt:SmallInt;
begin
  cbb1.ItemIndex:=0;
  cbb3.ItemIndex:=0;

  chk18.Checked:=False;
  chk19.Checked:=False;
  case cbb2.Text[1] of
       '0':       //结束步骤
       begin
          edt15.Text:='END';
          edt3.Text:='0';
          edt4.Text:='0';
          edt5.Text:='0';
          edt6.Text:='0';
          edt7.Text:='0';
          edt8.Text:='0';
          edt11.Text:='0';
          edt13.Text:='0';
          edt14.Text:='0';
          cbb3.ItemIndex:=0;
       end;
       'O','K':        //开路测试
       begin
          edt15.Text:='OPEN';
          edt3.Text:='50';
          edt4.Text:='0';
          edt5.Text:='0';
          edt6.Text:='0';
          edt7.Text:='0';
          edt8.Text:='5';
          edt11.Text:='0';
          edt13.Text:='0';
          edt14.Text:='0';
          cbb3.ItemIndex:=1;       //0O11
          chk19.Checked:=True;
       end;
       'S':
       begin
          edt15.Text:='SHOR';
          edt3.Text:='100';
          edt4.Text:='0';
          edt5.Text:='0';
          edt6.Text:='0';
          edt7.Text:='0';
          edt8.Text:='5';
          edt11.Text:='0';
          edt13.Text:='0';
          edt14.Text:='0';
          cbb3.ItemIndex:=1;       //0S11
          chk19.Checked:=True;
       end;
       'R':
       begin
          edt15.Text:='R1';
          edt3.Text:='0';
          edt4.Text:='10';
          edt5.Text:='10';
          edt6.Text:='1';
          edt7.Text:='2';
          edt8.Text:='5';
          edt11.Text:='0';
          edt13.Text:='0';
          edt14.Text:='0';
          cbb3.ItemIndex:=0;      //0R01
          chk19.Checked:=True;
       end;
       'C':
       begin
          edt15.Text:='C1';
          edt3.Text:='100';
          edt4.Text:='30';
          edt5.Text:='30';
          edt6.Text:='1';
          edt7.Text:='2';
          edt8.Text:='5';
          edt11.Text:='0';
          edt13.Text:='0';
          edt14.Text:='0';
          cbb1.ItemIndex:=3;
          cbb3.ItemIndex:=1;      //3C11
          chk19.Checked:=True;
       end;
       'D':
       begin
          edt15.Text:='D1';
          edt3.Text:='700';
          edt4.Text:='30';
          edt5.Text:='30';
          edt6.Text:='1';
          edt7.Text:='2';
          edt8.Text:='5';
          edt11.Text:='0';
          edt13.Text:='0';
          edt14.Text:='0';
          cbb3.ItemIndex:=0;     //0D01
          chk19.Checked:=True;
       end;
       'A':
       begin
          edt15.Text:='A';
          edt3.Text:='700';
          edt4.Text:='30';
          edt5.Text:='30';
          edt6.Text:='0';
          edt7.Text:='0';
          edt8.Text:='50';
          edt11.Text:='0';
          edt13.Text:='0';
          edt14.Text:='10';
          cbb3.Text:='V';     //0AV1
          chk19.Checked:=True;
       end;
       'P':
       begin
          edt15.Text:='SPD';
          edt3.Text:='1000';
          edt4.Text:='30';
          edt5.Text:='30';
          edt6.Text:='8998';
          edt7.Text:='1000';
          edt8.Text:='5';
          edt11.Text:='10';
          edt13.Text:='1000';
          edt14.Text:='1906';
          cbb3.Text:='N';      //0PN1
          chk19.Checked:=True;
       end;
       'F':
       begin
          edt15.Text:='WP';
          edt3.Text:='1000';
          edt4.Text:='0';
          edt5.Text:='0';
          edt6.Text:='22';
          edt7.Text:='0';
          edt8.Text:='5';
          edt11.Text:='0';
          edt13.Text:='0';
          edt14.Text:='0';
          cbb3.Text:='F';
          chk19.Checked:=True;
       end;
       'B':
       begin
          edt15.Text:='UPLD';
          edt3.Text:='0';
          edt4.Text:='0';
          edt5.Text:='0';
          edt6.Text:='0';
          edt7.Text:='0';
          edt8.Text:='100';
          edt11.Text:='0';
          edt13.Text:='0';
          edt14.Text:='0';
          cbb1.Text:='2';
          cbb3.Text:='U';
       end;
       'Q':
       begin
          edt15.Text:='Q';
          edt3.Text:='0';
          edt4.Text:='0';
          edt5.Text:='0';
          edt6.Text:='1';
          edt7.Text:='1';
          edt8.Text:='5';
          edt11.Text:='0';
          edt13.Text:='0';
          edt14.Text:='0';
          cbb1.ItemIndex:=1;
          cbb3.ItemIndex:=0;
          chk19.Checked:=False;
       end;
       'U'..'V':
       begin
          edt15.Text:='pF';
          edt3.Text:='1000';
          edt4.Text:='20';
          edt5.Text:='20';
          edt6.Text:='1';
          edt7.Text:='2';
          edt8.Text:='5';
          edt11.Text:='10';   //平均次数
          edt13.Text:='120';   //120*100频率，20延时
          edt14.Text:='1';     //测量幅值<-- 10K校准
          cbb3.ItemIndex:=6;
          chk19.Checked:=true;
       end;
       'W':
       begin
          edt15.Text:='WAIT';
          edt3.Text:='600';
          edt4.Text:='0';
          edt5.Text:='0';
          edt6.Text:='1';
          edt7.Text:='2';
          edt8.Text:='0';
          edt11.Text:='0';
          edt13.Text:='0';
          edt14.Text:='0';
          cbb1.ItemIndex:=1;
          cbb3.ItemIndex:=0;
          chk18.Checked:=True;
          chk19.Checked:=False;
       end;
       'Z':     //基本系统参数
       begin
         if cbb3.Text[1]='S' then begin
            edt15.Text:='SYS0';
            edt3.Text:='1';
            edt4.Text:='32';
            edt5.Text:='100';
            edt6.Text:='32';
            edt7.Text:='200';
            edt8.Text:='0';
            edt9.Text:='10';
            edt10.Text:='0';
            edt11.Text:='10';
            edt12.Text:='0';
            edt13.Text:='0';
            edt14.Text:='1';
            cbb1.ItemIndex:=0;              //cbb3.Text:='S';
            chk18.Checked:=False;
            chk19.Checked:=False;
         end else begin
            edt15.Text:='S0';
            edt3.Text:='0';
            edt4.Text:='0';
            edt5.Text:='0';
            edt6.Text:='0';
            edt7.Text:='0';
            edt8.Text:='0';
            edt9.Text:='100';
            edt10.Text:='0';
            edt11.Text:='0';
            edt12.Text:='0';
            edt13.Text:='0';
            edt14.Text:='0';
            cbb1.ItemIndex:=0;
            cbb3.Text:='B';
            chk18.Checked:=False;
            chk19.Checked:=False;
         end;
       end;
  else begin
          edt15.Text:='0';
          edt3.Text:='0';
          edt4.Text:='0';
          edt5.Text:='0';
          edt6.Text:='0';
          edt7.Text:='0';
          edt8.Text:='5';
          edt11.Text:='0';
          edt13.Text:='0';
          edt14.Text:='0';
          cbb3.ItemIndex:=0;
          chk19.Checked:=False;
      end;
  end;
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);
  btn1.SetFocus;
end;

procedure TFormInsertN.cbb1Change(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);


end;

procedure TFormInsertN.edt1Change(Sender: TObject);
var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持
    for ii:=1 to 50 do begin        //颜色先还原
      zu1edit[ii].Color:=clWhite;
      zu2edit[ii].Color:=clWhite;
      zu3edit[ii].Color:=clWhite;
      zu5edit[ii].Color:=clWhite;
      zu6edit[ii].Color:=clWhite;

      zu7edit[ii].Color:=clWhite;
      zu8edit[ii].Color:=clWhite;
      zu9edit[ii].Color:=clWhite;
      zu10edit[ii].Color:=clWhite;
      zu11edit[ii].Color:=clWhite;
      zu12edit[ii].Color:=clWhite;
      zu13edit[ii].Color:=clWhite;
      zu14edit[ii].Color:=clWhite;
      zu15edit[ii].Color:=clWhite;
      zu16edit[ii].Color:=clWhite;
    end;
    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于1，
          and (StrToInt(edt1.Text)>=((StrToInt(form_line.edt45.text)div 50)*50) ) //大于当前页起始步骤
          and (StrToInt(edt2.Text)>0) )                         //且步骤数
      //  and ((StrToInt(edt2.Text)div 50)=(StrToInt(edt1.Text)div 50))
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      for ii:=1 to StrToInt(edt2.Text) do begin
        if(lineInsert+ii)>50 then Break;
        zu1edit [lineInsert+ii].Color:=clMoneyGreen;
        zu9edit [lineInsert+ii].Color:=clMoneyGreen;
        zu11edit[lineInsert+ii].Color:=clMoneyGreen;
        zu12edit[lineInsert+ii].Color:=clMoneyGreen;
        zu13edit[lineInsert+ii].Color:=clMoneyGreen;

        zu10edit[lineInsert+ii].Color:=clSkyBlue;
        zu7edit [lineInsert+ii].Color:=clMoneyGreen;
        zu8edit [lineInsert+ii].Color:=clMoneyGreen;
        zu14edit[lineInsert+ii].Color:=clMoneyGreen;
        zu15edit[lineInsert+ii].Color:=clMoneyGreen;
        zu16edit[lineInsert+ii].Color:=clMoneyGreen;
      end;
    end;
  end;
end;

procedure TFormInsertN.edt2Change(Sender: TObject);
var ii,lineInsert:SmallInt;
begin
 { if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持
    for ii:=1 to 50 do begin        //颜色先还原
      zu1edit[ii].Color:=clWhite;
      zu9edit[ii].Color:=clWhite;
      zu11edit[ii].Color:=clWhite;
      zu12edit[ii].Color:=clWhite;
      zu13edit[ii].Color:=clWhite;

      zu10edit[ii].Color:=clWhite;
      zu7edit[ii].Color:=clWhite;
      zu8edit[ii].Color:=clWhite;
      zu14edit[ii].Color:=clWhite;
      zu15edit[ii].Color:=clWhite;
      zu16edit[ii].Color:=clWhite;
    end;
    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于1，
          and (StrToInt(edt2.Text)>0) )                         //且步骤数
      //  and ((StrToInt(edt2.Text)div 50)=(StrToInt(edt1.Text)div 50))
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      for ii:=1 to StrToInt(edt2.Text) do begin
        if(lineInsert+ii)>50 then Break;
        zu1edit [lineInsert+ii].Color:=clMoneyGreen;
        zu9edit [lineInsert+ii].Color:=clMoneyGreen;
        zu11edit[lineInsert+ii].Color:=clMoneyGreen;
        zu12edit[lineInsert+ii].Color:=clMoneyGreen;
        zu13edit[lineInsert+ii].Color:=clMoneyGreen;

        zu10edit[lineInsert+ii].Color:=clSkyBlue;
        zu7edit [lineInsert+ii].Color:=clMoneyGreen;
        zu8edit [lineInsert+ii].Color:=clMoneyGreen;
        zu14edit[lineInsert+ii].Color:=clMoneyGreen;
        zu15edit[lineInsert+ii].Color:=clMoneyGreen;
        zu16edit[lineInsert+ii].Color:=clMoneyGreen;
      end;
    end;
  end;   }
end;

procedure TFormInsertN.FormShow(Sender: TObject);
begin
//enablewindow(form_line.handle,true);
end;

procedure TFormInsertN.edt1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_RETURN then begin
    btn1click(self);
  end;
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

procedure TFormInsertN.btn1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

procedure TFormInsertN.btn2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

end.
