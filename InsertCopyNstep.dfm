object FormInsertCopyN: TFormInsertCopyN
  Left = 258
  Top = 296
  Width = 1059
  Height = 521
  Caption = 'InsertCopyNstep '#25554#20837#22797#21046#22810#20010#27493#39588
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grp2: TsGroupBox
    Left = 20
    Top = 168
    Width = 993
    Height = 305
    Caption = 'Insert '#35774#23450
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    SkinData.SkinSection = 'GROUPBOX'
    object chk1: TsCheckBox
      Left = 24
      Top = 24
      Width = 246
      Height = 24
      Caption = 'Num '#24207#21495#29983#25104'('#36873#20013#19981#21024#30053#65289'      '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 0
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk2: TsCheckBox
      Left = 24
      Top = 56
      Width = 154
      Height = 24
      Caption = 'Name '#27493#39588#21517#31216'      '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk6: TsCheckBox
      Left = 300
      Top = 56
      Width = 111
      Height = 24
      Caption = 'Hpoint '#39640#28857'  '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 2
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk7: TsCheckBox
      Left = 300
      Top = 88
      Width = 108
      Height = 24
      Caption = 'Lpoint'#20302#28857'   '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 3
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk8: TsCheckBox
      Left = 300
      Top = 120
      Width = 104
      Height = 24
      Caption = 'Delay'#24310#26102'   '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 4
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk10: TsCheckBox
      Left = 536
      Top = 24
      Width = 78
      Height = 24
      Caption = 'K'#27604#20363'    '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 5
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk13: TsCheckBox
      Left = 535
      Top = 120
      Width = 100
      Height = 24
      Caption = #32852#26495#24207#21495'    '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 6
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk14: TsCheckBox
      Left = 760
      Top = 24
      Width = 94
      Height = 24
      Caption = 'H2'#39640#28857'2   '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 7
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object chk15: TsCheckBox
      Left = 760
      Top = 56
      Width = 91
      Height = 24
      Caption = 'L2'#20302#28857'2   '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 8
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object edt6: TsDecimalSpinEdit
      Left = 416
      Top = 56
      Width = 89
      Height = 28
      TabOrder = 9
      Text = '1'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      Value = 1.000000000000000000
      DecimalPlaces = 0
    end
    object edt7: TsDecimalSpinEdit
      Left = 416
      Top = 88
      Width = 89
      Height = 28
      TabOrder = 10
      Text = '2'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      Value = 2.000000000000000000
      DecimalPlaces = 0
    end
    object edt8: TsDecimalSpinEdit
      Left = 416
      Top = 120
      Width = 89
      Height = 28
      TabOrder = 11
      Text = '5'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 65535.000000000000000000
      Value = 5.000000000000000000
      DecimalPlaces = 0
    end
    object edt9: TsDecimalSpinEdit
      Left = 640
      Top = 24
      Width = 73
      Height = 28
      TabOrder = 12
      Text = '100'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      Value = 100.000000000000000000
      DecimalPlaces = 0
    end
    object edt12: TsDecimalSpinEdit
      Left = 640
      Top = 120
      Width = 73
      Height = 28
      TabOrder = 13
      Text = '0'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      DecimalPlaces = 0
    end
    object edt13: TsDecimalSpinEdit
      Left = 856
      Top = 24
      Width = 81
      Height = 28
      TabOrder = 14
      Text = '3'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      Value = 3.000000000000000000
      DecimalPlaces = 0
    end
    object edt14: TsDecimalSpinEdit
      Left = 856
      Top = 56
      Width = 81
      Height = 28
      TabOrder = 15
      Text = '4'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 9999.000000000000000000
      Value = 4.000000000000000000
      DecimalPlaces = 0
    end
    object edt15: TsEdit
      Left = 176
      Top = 48
      Width = 97
      Height = 28
      TabOrder = 16
      Text = 'R???'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object grp3: TsGroupBox
      Left = 8
      Top = 144
      Width = 969
      Height = 153
      Caption = 'Mode'
      TabOrder = 17
      SkinData.SkinSection = 'GROUPBOX'
      object edt16: TsEdit
        Left = 216
        Top = 13
        Width = 89
        Height = 28
        TabOrder = 0
        Text = '0R01'
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object chk9: TsCheckBox
        Left = 68
        Top = 17
        Width = 108
        Height = 24
        Caption = 'Mode'#27169#24335'    '
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        State = cbChecked
        TabOrder = 1
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object cbb1: TsComboBox
        Left = 24
        Top = 73
        Width = 201
        Height = 28
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Enabled = False
        ItemHeight = 22
        ItemIndex = -1
        TabOrder = 2
        Text = '1---'#37327#31243#36873#25321#25110#27425#27169#24335
        OnChange = cbb1Change
        Items.Strings = (
          '0'#65306#21333#20301#20026'10'#30340'0'#27425#26041
          '1'#65306#21333#20301#20026'10'#30340'1'#27425#26041
          '2'#65306#21333#20301#20026'10'#30340'2'#27425#26041
          '3'#65306#21333#20301#20026'10'#30340'3'#27425#26041
          '4'#65306#21333#20301#20026'10'#30340'4'#27425#26041
          '5'#65306#21333#20301#20026'10'#30340'5'#27425#26041
          '6'#65306#21333#20301#20026'10'#30340'6'#27425#26041
          'F'#65306#22235#32447#30005#38459#21333#20301#20026'mohm'
          'L'#65306' LED'#27979#37327'0'
          'M'#65306'LED'#27979#37327'1'
          'X'#65306#32487#30005#22120#20999#25442'+'
          'Y'#65306#32487#30005#22120#20999#25442
          'Z'#65306#32487#30005#22120#20851#38381)
      end
      object chk16: TsCheckBox
        Left = 808
        Top = 24
        Width = 37
        Height = 24
        Caption = 'nc'
        Enabled = False
        TabOrder = 3
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object chk17: TsCheckBox
        Left = 808
        Top = 53
        Width = 144
        Height = 24
        Caption = #32467#26524#26174#31034#22312'LCD   '
        TabOrder = 4
        OnClick = chk17Click
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object chk18: TsCheckBox
        Left = 808
        Top = 82
        Width = 100
        Height = 24
        Caption = #27493#39588#21069#26174#31034
        TabOrder = 5
        OnClick = chk18Click
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object chk19: TsCheckBox
        Left = 808
        Top = 112
        Width = 116
        Height = 24
        Caption = #26597#30475#27979#37327#32467#26524
        TabOrder = 6
        OnClick = chk19Click
        SkinData.SkinSection = 'CHECKBOX'
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object cbb2: TsComboBox
        Left = 240
        Top = 73
        Width = 233
        Height = 28
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        ItemHeight = 22
        ItemIndex = -1
        TabOrder = 7
        Text = '2---'#20027#27979#37327#27169#24335
        OnChange = cbb2Change
        Items.Strings = (
          'S'#65306#30701#36335#27979#35797
          'O'#65306#24320#36335#27979#35797
          'R'#65306#30005#38459#27979#35797
          'C'#65306#30005#23481#27979#35797
          'D'#65306#20108#26497#31649#27979#35797
          'Q'#65306#36755#20986#25511#21046
          'G'#65306'Agilent'#19975#29992#34920
          'H'#65306'minghe'
          'A'#65306'485'#36890#20449
          'B'#65306#19978#20256'PC'
          'I'#65306'I2C'#36890#20449
          'P'#65306#36895#24230#27979#37327
          'F'#65306#39057#29575#27979#37327
          'T'#65306#20132#27969#20449#21495
          'Z'#65306#31995#32479#21442#25968#35774#23450
          '')
      end
      object cbb3: TsComboBox
        Left = 496
        Top = 73
        Width = 273
        Height = 28
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Enabled = False
        ItemHeight = 22
        ItemIndex = -1
        TabOrder = 8
        Text = '3----'#27425#27979#37327#27169#24335
        OnChange = cbb3Change
        Items.Strings = (
          '0'#65306#38454#27573'1'
          '1'#65306#38454#27573'2'
          '2'#65306#38454#27573'3'
          '3'#65306#38454#27573'4'
          '4'#65306#38454#27573'5'
          'R:'#20132#27969#27979#30005#38459
          'C:'#20132#27969#27979#30005#23481
          'L:'#20132#27969#27979#30005#24863)
      end
    end
    object edt10: TsDecimalSpinEdit
      Left = 640
      Top = 56
      Width = 73
      Height = 28
      TabOrder = 18
      Text = '0'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      DecimalPlaces = 0
    end
    object chk11: TsCheckBox
      Left = 537
      Top = 56
      Width = 67
      Height = 24
      Caption = 'B '#20559#31227
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 19
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
  end
  object spnl1: TsPanel
    Left = 668
    Top = 24
    Width = 345
    Height = 49
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object btn1: TsButton
      Left = 16
      Top = 8
      Width = 113
      Height = 33
      Caption = 'OK '#30830#23450
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = btn1Click
      SkinData.SkinSection = 'BUTTON'
    end
    object btn2: TsButton
      Left = 176
      Top = 8
      Width = 129
      Height = 33
      Caption = 'Cancle '#21462#28040
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btn2Click
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object grp1: TGroupBox
    Left = 24
    Top = 16
    Width = 609
    Height = 209
    Caption = 'Insert '#25554#20837'('#27169#26495#27493#39588#22312#25335#36125#27493#39588#20301#32622#21069')'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object lbl1: TsWebLabel
      Left = 8
      Top = 24
      Width = 157
      Height = 20
      Caption = 'Pos '#25335#36125#27493#39588#20301#32622'------'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HoverFont.Charset = DEFAULT_CHARSET
      HoverFont.Color = clWindowText
      HoverFont.Height = -16
      HoverFont.Name = 'MS Sans Serif'
      HoverFont.Style = []
    end
    object lbl2: TsWebLabel
      Left = 13
      Top = 64
      Width = 116
      Height = 20
      Caption = 'Sum '#25335#36125#27425#25968'---'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HoverFont.Charset = DEFAULT_CHARSET
      HoverFont.Color = clWindowText
      HoverFont.Height = -16
      HoverFont.Name = 'MS Sans Serif'
      HoverFont.Style = []
    end
    object edt1: TsDecimalSpinEdit
      Left = 202
      Top = 21
      Width = 121
      Height = 28
      Color = clSkyBlue
      MaxLength = 4
      TabOrder = 0
      Text = '2'
      OnChange = edt1Change
      OnKeyUp = edt1KeyUp
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 4999.000000000000000000
      MinValue = 1.000000000000000000
      Value = 2.000000000000000000
      DecimalPlaces = 0
    end
    object edt2: TsDecimalSpinEdit
      Left = 200
      Top = 59
      Width = 121
      Height = 28
      MaxLength = 4
      TabOrder = 1
      Text = '1'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 4999.000000000000000000
      MinValue = 1.000000000000000000
      Value = 1.000000000000000000
      DecimalPlaces = 0
    end
    object chk3: TsCheckBox
      Left = 336
      Top = 25
      Width = 152
      Height = 24
      Caption = 'Num '#27169#26495#27493#39588#25968'---'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 2
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object edt3: TsDecimalSpinEdit
      Left = 496
      Top = 21
      Width = 97
      Height = 28
      Color = clSkyBlue
      TabOrder = 3
      Text = '1'
      OnChange = edt1Change
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 65535.000000000000000000
      MinValue = 1.000000000000000000
      Value = 1.000000000000000000
      DecimalPlaces = 0
    end
    object chk4: TsCheckBox
      Left = 8
      Top = 105
      Width = 194
      Height = 24
      Caption = 'PIN '#39640#28857'/'#20302#28857' '#20559#31227'--------'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 4
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object edt4: TsDecimalSpinEdit
      Left = 200
      Top = 101
      Width = 121
      Height = 28
      Color = clMoneyGreen
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      Text = '0'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 65535.000000000000000000
      MinValue = -9999.000000000000000000
      DecimalPlaces = 0
    end
    object chk5: TsCheckBox
      Left = 341
      Top = 98
      Width = 136
      Height = 24
      Caption = 'PCS'#32852#26495#20559#31227'----'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 6
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object edt5: TsDecimalSpinEdit
      Left = 498
      Top = 96
      Width = 89
      Height = 28
      Color = clMoneyGreen
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      Text = '0'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 65535.000000000000000000
      MinValue = -9999.000000000000000000
      DecimalPlaces = 0
    end
    object chk12: TsCheckBox
      Left = 11
      Top = 160
      Width = 169
      Height = 24
      Caption = #39640#28857'2/'#20302#28857'2 '#20559#31227'   ---'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 8
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object edt11: TsDecimalSpinEdit
      Left = 200
      Top = 160
      Width = 121
      Height = 28
      Color = clMoneyGreen
      TabOrder = 9
      Text = '0'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      Increment = 1.000000000000000000
      MaxValue = 65535.000000000000000000
      MinValue = -9999.000000000000000000
      DecimalPlaces = 0
    end
  end
end
