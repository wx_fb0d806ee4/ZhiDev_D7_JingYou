unit OSstudy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,Unit0_globalVariant, Menus, Buttons;

type
  TFormOS = class(TForm)
    mmo1: TMemo;
    btn1: TButton;
    btn2: TButton;
    btn3: TButton;
    btn4: TButton;
    btn6: TButton;
    lbl1: TLabel;
    mm1: TMainMenu;
    N11: TMenuItem;
    N12: TMenuItem;
    N21: TMenuItem;
    N13: TMenuItem;
    N22: TMenuItem;
    btn5: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btn5Click(Sender: TObject);
    procedure btn6Click(Sender: TObject);
    procedure FormShow(Sender: TObject);

//    procedure btn7Click(Sender: TObject);
//    procedure btn8Click(Sender: TObject);
//    procedure btn9Click(Sender: TObject);
//    procedure btn10Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure N22Click(Sender: TObject);
//    procedure btn7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormOS: TFormOS;

implementation

{$R *.dfm}

procedure TFormOS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 modbusfun05:=3; modbusfun05dat:=$FF;
 syslabel[3].Color:=clBtnFace;          //系统参数值学习参数颜色回归；
 syslabel[4].Color:=clBtnFace;
 sysedit[3].Color:=clWindow;
 sysedit[4].Color:=clWindow;

end;

procedure TFormOS.btn1Click(Sender: TObject);
begin
close;
end;

procedure TFormOS.btn2Click(Sender: TObject);
begin
      modbusfun06dat:=$00E0;
      modbusfun06:=$0001;
      btn5.Font.Color:=clRed;
      mmo1.Lines.Clear;
      mmo1.Lines.Add('First O/S learning......');
      mmo1.Lines.Add('一次短路群学习中......');
//  modbusfun05:=5; modbusfun05dat:=$FF;
  addToLog('学习一次短路群组' );

end;

procedure TFormOS.btn3Click(Sender: TObject);
begin
  if StrToInt(sysedit[3].Text)>=4095 then begin
      ShowMessage('点数(>=4095)太多!，不支持二次学习！');
  end else  begin
      modbusfun06dat:=$00E1;
      modbusfun06:=$0001;
      btn5.Font.Color:=clRed;      //  modbusfun05:=6; modbusfun05dat:=$FF;
      mmo1.Lines.Clear;
      mmo1.Lines.Add('Next O/S learning......');
      mmo1.Lines.Add('二次短路群学习中......');
      addToLog('学习二次短路群组' );
  end;
end;

procedure TFormOS.btn4Click(Sender: TObject);
begin
      modbusfun06dat:=$00F5;
      modbusfun06:=$0001;
end;

procedure TFormOS.btn5Click(Sender: TObject);
begin
 modbusfun05:=9; modbusfun05dat:=$FF;
 btn5.Font.Color:=clDefault;
 addToLog('SaveOS,保存短路群' );

end;

procedure TFormOS.btn6Click(Sender: TObject);
begin
   modbusfun05:=14; modbusfun05dat:=$FF;

end;

procedure TFormOS.FormShow(Sender: TObject);
begin
  mmo1.Clear;
 // modbusfun06dat:=$00F5;
 // modbusfun06:=$0001;
   syslabel[3].Color:=clYellow;          //系统参数值学习参数颜色提醒；
   syslabel[4].Color:=clYellow;
   sysedit[3].Color:=clYellow;
   sysedit[4].Color:=clYellow;
end;
 {
procedure TFormOS.btn7Click(Sender: TObject);
begin
      modbusfun06dat:=$00E2;
      modbusfun06:=$0001;

end;

procedure TFormOS.btn8Click(Sender: TObject);
begin
      modbusfun06dat:=$00E3;
      modbusfun06:=$0001;

end;

procedure TFormOS.btn9Click(Sender: TObject);
begin
      modbusfun06dat:=$00E4;
      modbusfun06:=$0001;

end;

procedure TFormOS.btn10Click(Sender: TObject);
begin
      modbusfun06dat:=$00E5;
      modbusfun06:=$0001;

end;     }

procedure TFormOS.N12Click(Sender: TObject);
begin
      modbusfun06dat:=$00E2;
      modbusfun06:=$0001;

end;

procedure TFormOS.N21Click(Sender: TObject);
begin
      modbusfun06dat:=$00E3;
      modbusfun06:=$0001;

end;

procedure TFormOS.N13Click(Sender: TObject);
begin
      modbusfun06dat:=$00E4;
      modbusfun06:=$0001;

end;

procedure TFormOS.N22Click(Sender: TObject);
begin
      modbusfun06dat:=$00E5;
      modbusfun06:=$0001;

end;


end.
