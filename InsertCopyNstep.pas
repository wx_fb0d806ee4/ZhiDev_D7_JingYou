unit InsertCopyNstep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,Unit0_globalVariant, StdCtrls, sButton, ExtCtrls, sPanel,
  sComboBox, sCheckBox, sGroupBox, sEdit, sSpinEdit, sLabel;

type
  TFormInsertCopyN = class(TForm)
    grp1: TGroupBox;
    lbl1: TsWebLabel;
    lbl2: TsWebLabel;
    edt1: TsDecimalSpinEdit;
    edt2: TsDecimalSpinEdit;
    grp2: TsGroupBox;
    chk1: TsCheckBox;
    chk2: TsCheckBox;
    chk3: TsCheckBox;
    chk4: TsCheckBox;
    chk5: TsCheckBox;
    chk6: TsCheckBox;
    chk7: TsCheckBox;
    chk8: TsCheckBox;
    chk10: TsCheckBox;
    chk11: TsCheckBox;
    chk12: TsCheckBox;
    chk13: TsCheckBox;
    chk14: TsCheckBox;
    chk15: TsCheckBox;
    edt3: TsDecimalSpinEdit;
    edt4: TsDecimalSpinEdit;
    edt5: TsDecimalSpinEdit;
    edt6: TsDecimalSpinEdit;
    edt7: TsDecimalSpinEdit;
    edt8: TsDecimalSpinEdit;
    edt9: TsDecimalSpinEdit;
    edt10: TsDecimalSpinEdit;
    edt11: TsDecimalSpinEdit;
    edt12: TsDecimalSpinEdit;
    edt13: TsDecimalSpinEdit;
    edt14: TsDecimalSpinEdit;
    edt15: TsEdit;
    grp3: TsGroupBox;
    edt16: TsEdit;
    chk9: TsCheckBox;
    cbb1: TsComboBox;
    chk16: TsCheckBox;
    chk17: TsCheckBox;
    chk18: TsCheckBox;
    chk19: TsCheckBox;
    cbb2: TsComboBox;
    cbb3: TsComboBox;
    spnl1: TsPanel;
    btn1: TsButton;
    btn2: TsButton;
    procedure btn2Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure chk19Click(Sender: TObject);
    procedure chk18Click(Sender: TObject);
    procedure chk17Click(Sender: TObject);
    procedure cbb3Change(Sender: TObject);
    procedure cbb2Change(Sender: TObject);
    procedure cbb1Change(Sender: TObject);
    procedure edt1Change(Sender: TObject);
    procedure edt1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormInsertCopyN: TFormInsertCopyN;

implementation

uses main;

{$R *.dfm}

procedure TFormInsertCopyN.btn2Click(Sender: TObject);
var ii:SmallInt;
begin
  if(VIRLCROWS>=50) then begin
    for ii:=1 to 50 do begin        //颜色还原
      zu10edit[ii].Color:=clWhite;
      zu7edit [ii].Color:=clWhite;
      zu8edit [ii].Color:=clWhite;
      zu14edit[ii].Color:=clWhite;
    end;
  end;
  close;
end;

procedure TFormInsertCopyN.btn1Click(Sender: TObject);
var tt,i:SmallInt;
  temp8 :byte;
  str:string;
begin
    modbusfun16int:=$2F28;
    modbusfun16len:=$12;
    //sbuf[1]:=1;
    sbuf[2]:=$10;      //16功能码！

    sbuf[8]:=StrToInt('0'+edt1.Text)div 256;      //序号 开始步骤
    sbuf[9]:=StrToInt('0'+edt1.Text)mod 256;
    sbuf[10]:=byte('?');sbuf[11]:=byte(' ');sbuf[12]:=byte(' ');sbuf[13]:=byte(' ');
    str:= AnsiUpperCase(edt15.Text);
    Move(str[1],sbuf[10],Length(str));
    temp8:=sbuf[10]; sbuf[10]:=sbuf[11];sbuf[11]:=temp8;
    temp8:=sbuf[12]; sbuf[12]:=sbuf[13];sbuf[13]:=temp8;

    sbuf[14]:=StrToInt('0'+edt3.Text)div 256;   //标准值--步骤数
    sbuf[15]:=StrToInt('0'+edt3.Text)mod 256;
    sbuf[16]:=StrToInt('0'+edt2.Text)div 256;   //运算值（测量值）--- >转为 复制次数
    sbuf[17]:=StrToInt('0'+edt2.Text)mod 256;
    
    sbuf[18]:=StrToInt(edt4.Text)shr 8;     //有符号上限--- >转为 高点、低点针点偏移
    sbuf[19]:=StrToInt(edt4.Text)mod 256;
    sbuf[20]:=StrToInt(edt5.Text)shr 8;     //有符号下限--- >转为 联板偏移
    sbuf[21]:=StrToInt(edt5.Text)mod 256;
    sbuf[22]:=StrToInt('0'+edt6.Text) div 256;  //高点
    sbuf[23]:=StrToInt('0'+edt6.Text);
    sbuf[24]:=StrToInt('0'+edt7.Text) div 256;  //低点
    sbuf[25]:=StrToInt('0'+edt7.Text);
    sbuf[26]:=StrToInt('0'+edt8.Text) div 256;  //延时
    sbuf[27]:=StrToInt('0'+edt8.Text);

    sbuf[28]:=byte('R');sbuf[29]:=byte('0');sbuf[30]:=byte('1');sbuf[31]:=byte('0');
    str:= AnsiUpperCase(edt16.Text);
    Move(str[1],sbuf[28],Length(str));
    temp8:=sbuf[28]; sbuf[28]:=sbuf[29];sbuf[29]:=temp8;
    temp8:=sbuf[30]; sbuf[30]:=sbuf[31];sbuf[31]:=temp8;

    sbuf[32]:=StrToInt('0'+edt9.Text)div 256;     //k值
    sbuf[33]:=StrToInt('0'+edt9.Text)mod 256;
    sbuf[34]:=StrToInt(edt10.Text)shr 8;          //有符号 b值
    sbuf[35]:=StrToInt(edt10.Text)mod 256;
    sbuf[36]:=StrToInt('0'+edt11.Text)div 256;    //上限2--- >转为 高点2、低点2针点偏移
    sbuf[37]:=StrToInt('0'+edt11.Text)mod 256;
    sbuf[38]:=StrToInt('0'+edt12.Text)div 256;    //下限2
    sbuf[39]:=StrToInt('0'+edt12.Text)mod 256;
    sbuf[40]:=StrToInt('0'+edt13.Text) div 256;  //高点2
    sbuf[41]:=StrToInt('0'+edt13.Text);
    sbuf[42]:=StrToInt('0'+edt14.Text) div 256;  //低点2
    sbuf[43]:=StrToInt('0'+edt14.Text);
    addToLog('拷贝第'+edt1.Text+'步骤前'+edt3.Text+'个步骤，拷贝'+edt2.Text+'次' );

    close;
end;

procedure TFormInsertCopyN.chk19Click(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);


end;

procedure TFormInsertCopyN.chk18Click(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);


end;

procedure TFormInsertCopyN.chk17Click(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);


end;

procedure TFormInsertCopyN.cbb3Change(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);


end;

procedure TFormInsertCopyN.cbb2Change(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);


end;

procedure TFormInsertCopyN.cbb1Change(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);


end;

procedure TFormInsertCopyN.edt1Change(Sender: TObject);
var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持
    for ii:=1 to 50 do begin        //颜色先还原
      zu10edit[ii].Color:=clWhite;
      zu7edit[ii].Color:=clWhite;
      zu8edit[ii].Color:=clWhite;
      zu14edit[ii].Color:=clWhite;
      zu15edit[ii].Color:=clWhite;
      zu16edit[ii].Color:=clWhite;
    end;
    if ( (Trim(edt1.Text)<>'') and (Trim(edt3.Text)<>'')
          and (StrToInt(edt1.Text)>1)                                       //位置大于1，
          and (StrToInt(edt1.Text)>=((StrToInt(form_line.edt45.text)div 50)*50) ) //大于当前页起始步骤
          and (StrToInt(edt3.Text)>0) and (StrToInt(edt3.Text)<50) )   //且步骤数范围1~49
      //  and ((StrToInt(edt2.Text)div 50)=(StrToInt(edt1.Text)div 50)) 
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;            //从0开始计算的当前行编号
      if(  lineInsert >= StrToInt(edt3.Text) )   then begin     //插入行编号比模板数多1
        for ii:=1 to StrToInt(edt3.Text) do begin
          zu10edit[lineInsert-StrToInt(edt3.Text)+ii].Color:=clSkyBlue;
          zu7edit [lineInsert-StrToInt(edt3.Text)+ii].Color:=clMoneyGreen;
          zu8edit [lineInsert-StrToInt(edt3.Text)+ii].Color:=clMoneyGreen;
          zu14edit[lineInsert-StrToInt(edt3.Text)+ii].Color:=clMoneyGreen;
          zu15edit[lineInsert-StrToInt(edt3.Text)+ii].Color:=clMoneyGreen;
          zu16edit[lineInsert-StrToInt(edt3.Text)+ii].Color:=clMoneyGreen;
        end;
      end else begin                                 //模板数太多，不在同一页
        for ii:=1 to  lineInsert  do begin          //最多49行模板被标志
          zu10edit[ii].Color:=clSkyBlue;
          zu7edit [ii].Color:=clMoneyGreen;
          zu8edit [ii].Color:=clMoneyGreen;
          zu14edit[ii].Color:=clMoneyGreen;
          zu15edit[ii].Color:=clMoneyGreen;
          zu16edit[ii].Color:=clMoneyGreen;
        end;
      end;
    end;
  end;
end;

procedure TFormInsertCopyN.edt1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_RETURN then begin
    btn1click(self);
  end;
  if Key=VK_ESCAPE then begin
    Close;
  end;

end;

end.
