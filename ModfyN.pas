unit ModfyN;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sCheckBox, sButton, ExtCtrls, sPanel, sGroupBox,
  sLabel, sEdit, sSpinEdit,Unit0_globalVariant, sComboBox;

type
  TFormModfyNstep = class(TForm)
    grp1: TGroupBox;
    edt1: TsDecimalSpinEdit;
    edt2: TsDecimalSpinEdit;
    lbl1: TsWebLabel;
    lbl2: TsWebLabel;
    grp2: TsGroupBox;
    spnl1: TsPanel;
    btn1: TsButton;
    btn2: TsButton;
    chk1: TsCheckBox;
    chk2: TsCheckBox;
    chk3: TsCheckBox;
    chk4: TsCheckBox;
    chk5: TsCheckBox;
    chk6: TsCheckBox;
    chk7: TsCheckBox;
    chk8: TsCheckBox;
    chk9: TsCheckBox;
    chk10: TsCheckBox;
    chk11: TsCheckBox;
    chk12: TsCheckBox;
    chk13: TsCheckBox;
    chk14: TsCheckBox;
    chk15: TsCheckBox;
    edt3: TsDecimalSpinEdit;
    edt4: TsDecimalSpinEdit;
    edt5: TsDecimalSpinEdit;
    edt6: TsDecimalSpinEdit;
    edt7: TsDecimalSpinEdit;
    edt8: TsDecimalSpinEdit;
    edt9: TsDecimalSpinEdit;
    edt10: TsDecimalSpinEdit;
    edt11: TsDecimalSpinEdit;
    edt12: TsDecimalSpinEdit;
    edt13: TsDecimalSpinEdit;
    edt14: TsDecimalSpinEdit;
    edt15: TsEdit;
    edt16: TsEdit;
    cbb1: TsComboBox;
    grp3: TsGroupBox;
    chk16: TsCheckBox;
    chk17: TsCheckBox;
    chk18: TsCheckBox;
    chk19: TsCheckBox;
    cbb2: TsComboBox;
    cbb3: TsComboBox;
    chk20: TCheckBox;
    edt17: TsDecimalSpinEdit;
    edt18: TsDecimalSpinEdit;
    chk21: TsCheckBox;
    chk22: TsCheckBox;
    procedure btn2Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chk19Click(Sender: TObject);
    procedure chk18Click(Sender: TObject);
    procedure chk17Click(Sender: TObject);
    procedure cbb3Change(Sender: TObject);
    procedure cbb2Change(Sender: TObject);
    procedure cbb1Change(Sender: TObject);
    procedure edt1Change(Sender: TObject);
{    procedure edt2Change(Sender: TObject);
    procedure chk1Click(Sender: TObject);
    procedure chk2Click(Sender: TObject);
    procedure chk3Click(Sender: TObject);
    procedure chk4Click(Sender: TObject);
    procedure chk5Click(Sender: TObject);
    procedure chk6Click(Sender: TObject);
    procedure chk7Click(Sender: TObject);
    procedure chk8Click(Sender: TObject);
    procedure chk9Click(Sender: TObject);
    procedure chk10Click(Sender: TObject);
    procedure chk11Click(Sender: TObject);
    procedure chk12Click(Sender: TObject);
    procedure chk13Click(Sender: TObject);
    procedure chk14Click(Sender: TObject);
    procedure chk15Click(Sender: TObject);  }
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormModfyNstep: TFormModfyNstep;

implementation

uses main;

{$R *.dfm}

procedure TFormModfyNstep.btn2Click(Sender: TObject);
begin
  Close;
end;

procedure TFormModfyNstep.btn1Click(Sender: TObject);
var tt,i:SmallInt;
  temp8 :byte;
  str:string;
begin
    modbusfun16int:=$2F81;        
    modbusfun16len:=$12+1;        //增加2个字节  多 一个 修改标志WORD
    //sbuf[1]:=1;
    sbuf[2]:=$10;

    sbuf[8]:=StrToInt('0'+edt1.Text)div 256;//序号
    sbuf[9]:=StrToInt('0'+edt1.Text)mod 256;
    sbuf[10]:=byte('?');sbuf[11]:=byte(' ');sbuf[12]:=byte(' ');sbuf[13]:=byte(' ');
    str:= edt15.Text;//AnsiUpperCase(edt15.Text);
    Move(str[1],sbuf[10],Length(str));
    temp8:=sbuf[10]; sbuf[10]:=sbuf[11];sbuf[11]:=temp8;
    temp8:=sbuf[12]; sbuf[12]:=sbuf[13];sbuf[13]:=temp8;
  {  for i:=2 to 8 do
      begin
          sbuf[10+2*i]:=0;sbuf[11+2*i]:=0;
      end;    }
    if Trim(edt3.Text)<>'' then
    begin
      sbuf[14]:=StrToInt(edt3.Text)shr 8;//div 256; //标准值
      sbuf[15]:=StrToInt(edt3.Text)mod 256;
    end;
    if Trim(edt2.Text)<>'' then
    begin
      sbuf[16]:=StrToInt(edt2.Text)div 256; //（测控值）（测量值） 转为结束步骤
      sbuf[17]:=StrToInt(edt2.Text)mod 256;
    end;
    if Trim(edt4.Text)<>'' then
    begin
      sbuf[18]:=StrToInt(edt4.Text)shr 8;//div 256; //上限
      sbuf[19]:=StrToInt(edt4.Text)mod 256;
    end;
    if Trim(edt5.Text)<>'' then
    begin
      sbuf[20]:=StrToInt(edt5.Text)shr 8;//div 256; //下限
      sbuf[21]:=StrToInt(edt5.Text)mod 256;
    end;
    sbuf[22]:=StrToInt('0'+edt6.Text) div 256;  //高点
    sbuf[23]:=StrToInt('0'+edt6.Text);
    sbuf[24]:=StrToInt('0'+edt7.Text) div 256;  //低点
    sbuf[25]:=StrToInt('0'+edt7.Text);
    sbuf[26]:=StrToInt('0'+edt8.Text) div 256;  //延时
    sbuf[27]:=StrToInt('0'+edt8.Text);

    sbuf[28]:=byte('R');sbuf[29]:=byte('0');sbuf[30]:=byte('1');sbuf[31]:=byte('0');
    str:= AnsiUpperCase(edt16.Text);
    Move(str[1],sbuf[28],Length(str));
    temp8:=sbuf[28]; sbuf[28]:=sbuf[29];sbuf[29]:=temp8;
    temp8:=sbuf[30]; sbuf[30]:=sbuf[31];sbuf[31]:=temp8;

    sbuf[32]:=StrToInt('0'+edt9.Text)div 256; //k值
    sbuf[33]:=StrToInt('0'+edt9.Text)mod 256;
    if Trim(edt10.Text)<>'' then
    begin
      sbuf[34]:=StrToInt(edt10.Text)shr 8; //b值
      sbuf[35]:=StrToInt(edt10.Text)mod 256;
    end;
    sbuf[36]:=StrToInt('0'+edt11.Text)div 256; //上限 2
    sbuf[37]:=StrToInt('0'+edt11.Text)mod 256;
    sbuf[38]:=StrToInt('0'+edt12.Text)div 256; //下限2
    sbuf[39]:=StrToInt('0'+edt12.Text)mod 256;
    sbuf[40]:=StrToInt('0'+edt13.Text) div 256;  //高点2
    sbuf[41]:=StrToInt('0'+edt13.Text);
    sbuf[42]:=StrToInt('0'+edt14.Text) div 256;  //低点2
    sbuf[43]:=StrToInt('0'+edt14.Text);
    tt:=0;
    if chk1.Checked then tt:=tt+$0001;
    if chk20.Checked then tt:=tt+$0008;     //删略功能受序号使能控制

    if chk2.Checked then tt:=tt+$0002;
    if chk3.Checked then tt:=tt+$0004;
    if chk4.Checked then tt:=tt+$0010;
    if chk5.Checked then tt:=tt+$0020;
    if chk6.Checked then tt:=tt+$0040;
    if chk7.Checked then tt:=tt+$0080;
    if chk8.Checked then tt:=tt+$0100;
    if chk9.Checked then tt:=tt+$0200;
    if chk10.Checked then tt:=tt+$0400;
    if chk11.Checked then tt:=tt+$0800;
    if chk12.Checked then tt:=tt+$1000;
    if chk13.Checked then tt:=tt+$2000;
    if chk14.Checked then tt:=tt+$4000;
    if chk15.Checked then tt:=tt+$8000;
    sbuf[44]:=tt shr 8;  //修改标志
    sbuf[45]:=tt;


    if chk21.Checked then begin
      sbuf[46]:=StrToInt(edt17.Text) ;  //修改标志
      sbuf[47]:=StrToInt(edt18.Text);
      modbusfun16len:=modbusfun16len+1;
    end;
    addToLog('整块修改第'+edt1.Text+'~'+edt2.Text+'步骤' );

    close;

end;

procedure TFormModfyNstep.FormShow(Sender: TObject);
begin
//   edt1.Text:=Form_line.edt45.Text;
//   edt2.Text:=Form_line.edt45.Text;
end;

procedure TFormModfyNstep.chk19Click(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);
end;

procedure TFormModfyNstep.chk18Click(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);

end;

procedure TFormModfyNstep.chk17Click(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);

end;

procedure TFormModfyNstep.cbb3Change(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);

end;

procedure TFormModfyNstep.cbb2Change(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);

end;

procedure TFormModfyNstep.cbb1Change(Sender: TObject);
var tt:SmallInt;
begin
  tt:=0;
  if chk16.Checked   then tt:=tt+8;
  if chk17.Checked   then tt:=tt+4;
  if chk18.Checked   then tt:=tt+2;
  if chk19.Checked   then tt:=tt+1;
  edt16.Text:=cbb1.Text[1]+cbb2.Text[1]+cbb3.Text[1]+inttostr(tt);

end;

procedure TFormModfyNstep.edt1Change(Sender: TObject);
var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持
    for ii:=1 to 50 do begin        //颜色先还原
      zu1edit[ii].Color:=clWhite;
      zu2edit[ii].Color:=clWhite;
      zu3edit[ii].Color:=clWhite;

      zu5edit[ii].Color:=clWhite;
      zu6edit[ii].Color:=clWhite;
      zu7edit[ii].Color:=clWhite;
      zu8edit[ii].Color:=clWhite;
      zu9edit[ii].Color:=clWhite;
      zu10edit[ii].Color:=clWhite;
      zu11edit[ii].Color:=clWhite;
      zu12edit[ii].Color:=clWhite;
      zu13edit[ii].Color:=clWhite;
      zu14edit[ii].Color:=clWhite;
      zu15edit[ii].Color:=clWhite;
      zu16edit[ii].Color:=clWhite;
    end;
    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'') and (Trim(edt17.Text)<>'')and (Trim(edt18.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于0，     //  and (StrToInt(edt1.Text)>=( ( (StrToInt(form_line.edt45.text)-1)div 50 )*50 ) ) //大于当前页起始步骤
          and (StrToInt(edt2.Text)>0) )                         //且步骤数        //  and ((StrToInt(edt2.Text)div 50)=(StrToInt(edt1.Text)div 50))
    then begin
      if edt17.Value<1 then begin          //关闭间隔功能！
         chk21.Checked:=false;
         chk21.Checked:=false;
         edt17.Color:=clGray;  edt18.Color:=clGray;
         exit;
      end else begin
        edt17.Color:=clMoneyGreen;
        edt18.Color:=clMoneyGreen;
      end;
                                                                                    //if edt17.Value<1 then edt17.Value:=1; //if StrToInt(edt17.Text)<1   then  edt17.Text:='1';
      if edt18.Value<0 then edt18.Value:=0;                                         //if StrToInt(edt18.Text)<0   then   edt18.Text:='0';
      if edt18.Value>edt17.Value then edt18.Value:=0;                                //if StrToInt(edt18.Text) >StrToInt(edt17.Text)   then  edt18.Text:='0';   //lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;                        //从0开始计算的当前行编号
      for ii:=StrToInt(edt1.Text) to StrToInt(edt2.Text) do begin     //遍历所有数   //差值+1； 1 to (1+StrToInt(edt2.Text)-StrToInt(edt1.Text))

        if ii-1 <( (StrToInt(form_line.edt45.text)-1)div 50 )*50 then Continue; //于当前页！  //(StrToInt(edt1.Text)+ii-2)
        if ii-1 >=50+( (StrToInt(form_line.edt45.text)-1)div 50 )*50 then break;//大于        //(StrToInt(edt1.Text)+ii-2)     //if(lineInsert+ii)>50 then Break;

        if (Trim(edt17.Text)<>'') and (Trim(edt18.Text)<>'') then begin
          chk21.Checked:=True;
          chk22.Checked:=true;
          if ( ( (ii-1)mod(1+StrToInt(edt17.Text)) )<> StrToInt(edt18.Text) )   //不修改的步骤跳过；起始步骤绝对余数！//  (StrToInt(edt1.Text)+ii-2
          then begin
            Continue;
          end;
        end;
        lineInsert:= ( (ii-1) mod 50 )+1;                   //当前页位置
        if chk1.Checked then zu1edit [lineInsert].Color:=clMoneyGreen;
        if chk2.Checked then zu2edit [lineInsert].Color:=clMoneyGreen else zu2edit [lineInsert].Color:=clLtGray;
        if chk3.Checked then zu3edit [lineInsert].Color:=clMoneyGreen else zu3edit [lineInsert].Color:=clLtGray;

        if chk4.Checked then zu5edit [lineInsert].Color:=clMoneyGreen else zu5edit [lineInsert].Color:=clLtGray;
        if chk5.Checked then zu6edit [lineInsert].Color:=clMoneyGreen else zu6edit [lineInsert].Color:=clLtGray;
        if chk6.Checked then zu7edit [lineInsert].Color:=clMoneyGreen else zu7edit [lineInsert].Color:=clLtGray;
        if chk7.Checked then zu8edit [lineInsert].Color:=clMoneyGreen else zu8edit [lineInsert].Color:=clLtGray;
        if chk8.Checked then zu9edit [lineInsert].Color:=clMoneyGreen else zu9edit [lineInsert].Color:=clLtGray;
        if chk9.Checked then zu10edit[lineInsert].Color:=clSkyBlue    else zu10edit [lineInsert].Color:=clLtGray;
        if chk10.Checked then zu11edit[lineInsert].Color:=clMoneyGreen else zu11edit [lineInsert].Color:=clLtGray;
        if chk11.Checked then zu12edit[lineInsert].Color:=clMoneyGreen else zu12edit [lineInsert].Color:=clLtGray;
        if chk12.Checked then zu13edit[lineInsert].Color:=clMoneyGreen else zu13edit [lineInsert].Color:=clLtGray;
        if chk13.Checked then zu14edit[lineInsert].Color:=clMoneyGreen else zu14edit [lineInsert].Color:=clLtGray;
        if chk14.Checked then zu15edit[lineInsert].Color:=clMoneyGreen else zu15edit [lineInsert].Color:=clLtGray;
        if chk15.Checked then zu16edit[lineInsert].Color:=clMoneyGreen else zu16edit [lineInsert].Color:=clLtGray;
      end;
    end;
  end;
end;
{
procedure TFormModfyNstep.edt2Change(Sender: TObject);
var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持
    for ii:=1 to 50 do begin        //颜色先还原
      zu1edit[ii].Color:=clWhite;
      zu2edit[ii].Color:=clWhite;
      zu3edit[ii].Color:=clWhite;

      zu5edit[ii].Color:=clWhite;
      zu6edit[ii].Color:=clWhite;
      zu7edit[ii].Color:=clWhite;
      zu8edit[ii].Color:=clWhite;
      zu9edit[ii].Color:=clWhite;
      zu10edit[ii].Color:=clWhite;
      zu11edit[ii].Color:=clWhite;
      zu12edit[ii].Color:=clWhite;
      zu13edit[ii].Color:=clWhite;
      zu14edit[ii].Color:=clWhite;
      zu15edit[ii].Color:=clWhite;
      zu16edit[ii].Color:=clWhite;
    end;
    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于1，
          and (StrToInt(edt2.Text)>0) )                         //且步骤数
      //  and ((StrToInt(edt2.Text)div 50)=(StrToInt(edt1.Text)div 50))
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      for ii:=1 to StrToInt(edt2.Text) do begin
        if(lineInsert+ii)>50 then Break;
        if chk21.Checked and chk22.Checked
          and ( ( lineInsert+ii)mod(1+strtoint(edt17.Text))=StrToInt(edt18.Text))
        then begin
          Continue;
        end;

        if chk1.Checked then zu1edit [lineInsert+ii].Color:=clMoneyGreen;
        if chk2.Checked then zu2edit [lineInsert+ii].Color:=clMoneyGreen;
        if chk3.Checked then zu3edit [lineInsert+ii].Color:=clMoneyGreen;

        if chk4.Checked then zu5edit [lineInsert+ii].Color:=clMoneyGreen;
        if chk5.Checked then zu6edit [lineInsert+ii].Color:=clMoneyGreen;
        if chk6.Checked then zu7edit [lineInsert+ii].Color:=clMoneyGreen;
        if chk7.Checked then zu8edit [lineInsert+ii].Color:=clMoneyGreen;
        if chk8.Checked then zu9edit [lineInsert+ii].Color:=clMoneyGreen;
        if chk9.Checked then zu10edit[lineInsert+ii].Color:=clSkyBlue;
        if chk10.Checked then zu11edit[lineInsert+ii].Color:=clMoneyGreen;
        if chk11.Checked then zu12edit[lineInsert+ii].Color:=clMoneyGreen;
        if chk12.Checked then zu13edit[lineInsert+ii].Color:=clMoneyGreen;
        if chk13.Checked then zu14edit[lineInsert+ii].Color:=clMoneyGreen;
        if chk14.Checked then zu15edit[lineInsert+ii].Color:=clMoneyGreen;
        if chk15.Checked then zu16edit[lineInsert+ii].Color:=clMoneyGreen;
      end;
    end;
  end;
end;

procedure TFormModfyNstep.chk1Click(Sender: TObject);

var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持

    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于1，
          and (StrToInt(edt2.Text)>0) )                         //且步骤数
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      for ii:=1 to StrToInt(edt2.Text) do begin
        if(lineInsert+ii)>50 then Break;
        if chk1.Checked  then begin
          zu1edit [lineInsert+ii].Color:=clMoneyGreen;
        end else begin
          zu1edit [lineInsert+ii].Color:=clWindow;
        end;
      end;
    end;
  end;
end;

procedure TFormModfyNstep.chk2Click(Sender: TObject);
var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持

    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于1，
          and (StrToInt(edt2.Text)>0) )                         //且步骤数
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      for ii:=1 to StrToInt(edt2.Text) do begin
        if(lineInsert+ii)>50 then Break;
        if chk2.Checked  then begin
          zu2edit [lineInsert+ii].Color:=clMoneyGreen;
        end else begin
          zu2edit [lineInsert+ii].Color:=clWindow;
        end;
      end;
    end;
  end;
end;

procedure TFormModfyNstep.chk3Click(Sender: TObject);

var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持

    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于1，
          and (StrToInt(edt2.Text)>0) )                         //且步骤数
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      for ii:=1 to StrToInt(edt2.Text) do begin
        if(lineInsert+ii)>50 then Break;
        if chk3.Checked  then begin
          zu3edit [lineInsert+ii].Color:=clMoneyGreen;
        end else begin
          zu3edit [lineInsert+ii].Color:=clWindow;
        end;
      end;
    end;
  end;
end;

procedure TFormModfyNstep.chk4Click(Sender: TObject);
var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持

    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于1，
          and (StrToInt(edt2.Text)>0) )                         //且步骤数
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      for ii:=1 to StrToInt(edt2.Text) do begin
        if(lineInsert+ii)>50 then Break;
        if chk4.Checked  then begin
          zu5edit [lineInsert+ii].Color:=clMoneyGreen;
        end else begin
          zu5edit [lineInsert+ii].Color:=clWindow;
        end;
      end;
    end;
  end;
end;

procedure TFormModfyNstep.chk5Click(Sender: TObject);
var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持

    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于1，
          and (StrToInt(edt2.Text)>0) )                         //且步骤数
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      for ii:=1 to StrToInt(edt2.Text) do begin
        if(lineInsert+ii)>50 then Break;
        if chk5.Checked  then begin
          zu6edit [lineInsert+ii].Color:=clMoneyGreen;
        end else begin
          zu6edit [lineInsert+ii].Color:=clWindow;
        end;
      end;
    end;
  end;
end;

procedure TFormModfyNstep.chk6Click(Sender: TObject);
var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持

    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于1，
          and (StrToInt(edt2.Text)>0) )                         //且步骤数
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      for ii:=1 to StrToInt(edt2.Text) do begin
        if(lineInsert+ii)>50 then Break;
        if chk6.Checked  then begin
          zu7edit [lineInsert+ii].Color:=clMoneyGreen;
        end else begin
          zu7edit [lineInsert+ii].Color:=clWindow;
        end;
      end;
    end;
  end;
end;

procedure TFormModfyNstep.chk7Click(Sender: TObject);
var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持

    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于1，
          and (StrToInt(edt2.Text)>0) )                         //且步骤数
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      for ii:=1 to StrToInt(edt2.Text) do begin
        if(lineInsert+ii)>50 then Break;
        if chk7.Checked  then begin
          zu8edit [lineInsert+ii].Color:=clMoneyGreen;
        end else begin
          zu8edit [lineInsert+ii].Color:=clWindow;
        end;
      end;
    end;
  end;
end;

procedure TFormModfyNstep.chk8Click(Sender: TObject);
var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持

    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于1，
          and (StrToInt(edt2.Text)>0) )                         //且步骤数
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      for ii:=1 to StrToInt(edt2.Text) do begin
        if(lineInsert+ii)>50 then Break;
        if chk8.Checked  then begin
          zu9edit [lineInsert+ii].Color:=clMoneyGreen;
        end else begin
          zu9edit [lineInsert+ii].Color:=clWindow;
        end;
      end;
    end;
  end;
end;

procedure TFormModfyNstep.chk9Click(Sender: TObject);
var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持

    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于1，
          and (StrToInt(edt2.Text)>0) )                         //且步骤数
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      for ii:=1 to StrToInt(edt2.Text) do begin
        if(lineInsert+ii)>50 then Break;
        if chk9.Checked  then begin
          zu10edit [lineInsert+ii].Color:=clMoneyGreen;
        end else begin
          zu10edit [lineInsert+ii].Color:=clWindow;
        end;
      end;
    end;
  end;
end;

procedure TFormModfyNstep.chk10Click(Sender: TObject);
var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持

    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于1，
          and (StrToInt(edt2.Text)>0) )                         //且步骤数
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      for ii:=1 to StrToInt(edt2.Text) do begin
        if(lineInsert+ii)>50 then Break;
        if chk10.Checked  then begin
          zu11edit [lineInsert+ii].Color:=clMoneyGreen;
        end else begin
          zu11edit [lineInsert+ii].Color:=clWindow;
        end;
      end;
    end;
  end;
end;

procedure TFormModfyNstep.chk11Click(Sender: TObject);

var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持

    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于1，
          and (StrToInt(edt2.Text)>0) )                         //且步骤数
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      for ii:=1 to StrToInt(edt2.Text) do begin
        if(lineInsert+ii)>50 then Break;
        if chk11.Checked  then begin
          zu12edit [lineInsert+ii].Color:=clMoneyGreen;
        end else begin
          zu12edit [lineInsert+ii].Color:=clWindow;
        end;
      end;
    end;
  end;
end;

procedure TFormModfyNstep.chk12Click(Sender: TObject);
var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持

    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于1，
          and (StrToInt(edt2.Text)>0) )                         //且步骤数
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      for ii:=1 to StrToInt(edt2.Text) do begin
        if(lineInsert+ii)>50 then Break;
        if chk12.Checked  then begin
          zu13edit [lineInsert+ii].Color:=clMoneyGreen;
        end else begin
          zu13edit [lineInsert+ii].Color:=clWindow;
        end;
      end;
    end;
  end;
end;

procedure TFormModfyNstep.chk13Click(Sender: TObject);
var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持

    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于1，
          and (StrToInt(edt2.Text)>0) )                         //且步骤数
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      for ii:=1 to StrToInt(edt2.Text) do begin
        if(lineInsert+ii)>50 then Break;
        if chk13.Checked  then begin
          zu14edit [lineInsert+ii].Color:=clMoneyGreen;
        end else begin
          zu14edit [lineInsert+ii].Color:=clWindow;
        end;
      end;
    end;
  end;
end;

procedure TFormModfyNstep.chk14Click(Sender: TObject);
var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持

    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于1，
          and (StrToInt(edt2.Text)>0) )                         //且步骤数
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      for ii:=1 to StrToInt(edt2.Text) do begin
        if(lineInsert+ii)>50 then Break;
        if chk14.Checked  then begin
          zu15edit [lineInsert+ii].Color:=clMoneyGreen;
        end else begin
          zu15edit [lineInsert+ii].Color:=clWindow;
        end;
      end;
    end;
  end;

end;

procedure TFormModfyNstep.chk15Click(Sender: TObject);
var ii,lineInsert:SmallInt;
begin
  if(VIRLCROWS>=50) then begin      //50行步骤的FCT7才支持

    if ( (Trim(edt1.Text)<>'') and (Trim(edt2.Text)<>'')
          and (StrToInt(edt1.Text)>0)                           //位置大于1，
          and (StrToInt(edt2.Text)>0) )                         //且步骤数
    then begin
      lineInsert:= (StrToInt(edt1.Text)-1 ) mod 50 ;              //从0开始计算的当前行编号
      for ii:=1 to StrToInt(edt2.Text) do begin
        if(lineInsert+ii)>50 then Break;
        if chk15.Checked  then begin
          zu16edit [lineInsert+ii].Color:=clMoneyGreen;
        end else begin
          zu16edit [lineInsert+ii].Color:=clWindow;
        end;
      end;
    end;
  end;
end;    }

end.
