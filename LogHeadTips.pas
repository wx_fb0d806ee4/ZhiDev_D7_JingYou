unit LogHeadTips;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFormLogHead = class(TForm)
    lbl1: TLabel;
    btn1: TButton;
    procedure btn1Click(Sender: TObject);
    procedure btn1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormLogHead: TFormLogHead;

implementation

{$R *.dfm}

procedure TFormLogHead.btn1Click(Sender: TObject);
begin
  FormLogHead.Close;
end;

procedure TFormLogHead.btn1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_ESCAPE) then begin
    close;
  end;

end;

end.
