program calline;

uses
  Forms,
  windows,
  main in 'main.pas' {Form_line},
  Unit0_globalVariant in 'Unit0_globalVariant.pas',
  OSstudy in 'OSstudy.pas' {FormOS},
  FindPoint in 'FindPoint.pas' {FormFind},
  StepCopy in 'StepCopy.pas' {FormStepCopy},
  ModfyN in 'ModfyN.pas' {FormModfyNstep},
  InsertNstep in 'InsertNstep.pas' {FormInsertN},
  DeleteNstep in 'DeleteNstep.pas' {FormDeleteN},
  MoveNstep in 'MoveNstep.pas' {FormMoveN},
  InsertCopyNstep in 'InsertCopyNstep.pas' {FormInsertCopyN},
  PNenter in 'PNenter.pas' {FormPN},
  Unit1_MainCreateShowlCode in 'Unit1_MainCreateShowlCode.pas',
  Unit2_Communication in 'Unit2_Communication.pas',
  LoadFile in 'LoadFile.pas' {FormLoad},
  LogHeadTips in 'LogHeadTips.pas' {FormLogHead},
  Unit4_logFileCode in 'Unit4_logFileCode.pas',
  UnitDM in 'UnitDM.pas' {DMLJK: TDataModule};

{$R *.RES}
var Mutex:THandle;
begin
 Mutex := CreateMutex(nil,true,'one');  {第3个参数任意设置}
 if (GetLastError <> ERROR_ALREADY_EXISTS)
    or ( (Readchis('Comm Para','LANen')<>'0') and (Readchis('Comm Para','LANen')<>'') )     //有网口通信，可多个启动
    or (Readchis('Comm Para','FormLeft')<>'')                                  //双软件系统启动
 then begin
  Application.Initialize;
  Application.Title := '';
  Application.CreateForm(TDMLJK, DMLJK);
  Application.CreateForm(TForm_line, Form_line);
  Application.CreateForm(TFormOS, FormOS);
  Application.CreateForm(TFormFind, FormFind);
  Application.CreateForm(TFormStepCopy, FormStepCopy);
  Application.CreateForm(TFormModfyNstep, FormModfyNstep);
  Application.CreateForm(TFormInsertN, FormInsertN);
  Application.CreateForm(TFormDeleteN, FormDeleteN);
  Application.CreateForm(TFormMoveN, FormMoveN);
  Application.CreateForm(TFormInsertCopyN, FormInsertCopyN);
  Application.CreateForm(TFormPN, FormPN);
  Application.CreateForm(TFormLoad, FormLoad);
  Application.CreateForm(TFormLogHead, FormLogHead);
  Application.Run;
 end else
    Application.MessageBox('The Program is Runing...该程序正在运行！','Tips提示',MB_OK);

  ReleaseMutex(Mutex);   {释放资源}
end.
