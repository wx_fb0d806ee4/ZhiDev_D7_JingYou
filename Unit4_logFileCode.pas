{
//=======数据处理 重点===================================================================
V时间点 \ 要素   文件夹  文件名  表头项目名  测量数据  显示在界面  MES网盘及备份
V开启软件show     √      √      √                    √
V加载完成                         √                    √
V第一次上传数据   √      √

1.数据预处理                                  √        √
2.数据显示                                              √
3.数据终处理并保存        √       √         √                    √

V关闭软件                                                           √

  依child搜edt62，所有类型数据的全过程；
//1.SaveFlagProcess数据保存入口函数
    1.1依据配置和收到的机种名生成文件夹，及设置新文件标志；
    1.2调用保存数据 函数

//2.CreatFIle生成文件（有3个时间点：开启软件时、第一次测试完成、每一次测试完成），依客户不同
    2.1主路径文件为必须的，也是多样的，都是先生成文件后保存数据
        2.1.1 处理文件名的定义；
        2.1.2 处理文件表头数据的定义；
    2.2 分只生产一个文件和一个条码一个文件；
        2.2.1txt格式基本只有一个文件
        2.2.2 专用格式一个条码一个文件；
    2.3 第二备份路径文件名生成，针对CHILDFOLDER为两位数，如=12或13
    2.4.文件表头几行 获取方法：
     2.4.0 A,早期8700数据有PC解析，直接从config指定表头，禁止加载；
     2.4.1 B,txt格式和通用CSV格式在切换机种时要刷新（从主板获取信息，先生成文件，等加载成功后再刷新）
     2.4.2 C,一个条码一个文件的专用文件禁止刷新；

//3.SaveFile保存文件 ，依客户不同
    3.1主路径文件为必须的，也是多样的 ，都是先生成文件后保存数据
        3.1.2 每天一个文件或者 MFLEX条码文件则处理是否生成文件；
        3.1.3  先保存log文件表头部分
        3.1.4   要保存的数据先在mmo2显示；
        3.1.5  区分单PCS和多PCS数据文件的保存；
     3.2 生成judge文件并保存数据
     3.3 第二备份路径文件打开或生成并保存数据 针对CHILDFOLDER=12或13，
     3.4 第三备份路径文件生成并保存数据
//4.备份数据到服务器，函数存放在main文件中；
}
unit Unit4_logFileCode;

interface
uses
  Windows, Messages, SysUtils,Variants, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ActnList, NB30, registry, shellapi, dbclient, inifiles, db, adodb,
  FileCtrl, StdCtrls,StrUtils;

  procedure   saveCsvReport(len_ss:integer);
  procedure   SaveFlagProcess(devnum:integer;SaveLen:SmallInt);
  procedure   CreatAnewCsv(tt:integer;barcode:SmallInt);
  procedure   saveCsvdata(len_ss:integer;NewLogFileFlag:byte);
  procedure   FreshDatalogFromMemo(len_ss:integer);

implementation

uses StepCopy,main,OSstudy,FindPoint,PNenter,Unit0_globalVariant,
      LogHeadTips,Unit2_Communication,LoadFile, UnitDM;

//二次刷新文件表头参数，
//标准文件格式，在加载完MCU测试程序参数后刷新，由rxdmonitorName-unit-upp函数决定文件表头
//非标准文件格式（yijia或者机箱专用、加贺格式、早期jaguar）由creatFile函数决定文件表头
procedure FreshDatalogFromMemo(len_ss:integer);       //Log文件更新前面几行
var
//    f: TextFile;
    save_file_name: string;
    ResultList : TStringList;
begin
  with Form_line do begin      //不同文件格式，处理方式不同
    if (grp1.Tag=2) then  begin         //tesla格式，不更新
      if(StrToInt(edt81.Text)=0)and (Label10.caption<>'Label10') then begin           //仅从主板获取名称和单位；否则config指定；
        save_file_name :=Label10.caption;
        ResultList := TStringList.Create;
        try
          ResultList.LoadFromFile(edt2.Text + save_file_name);
          if edt62.text='25' then begin        //新ET设备
            ResultList[1]:=mmo2String[0];      //名称在文件第2行
            ResultList[2]:=mmo2String[4];      //上限
            ResultList[3]:=mmo2String[5];      //下限
            ResultList[4]:=mmo2String[2];      //单位
          end else begin
            if(edt62.Text='24') then
              ResultList[8]:=mmo2String[0]      //改造后tesla设备
            else
              ResultList[7]:=mmo2String[0];      //早期tesla设备吗？
            ResultList[4]:=','+mmo2String[4];
            ResultList[5]:=','+mmo2String[5];
            ResultList[6]:=','+mmo2String[2];  //ResultList[4]:=mmo2String[9];
          end;
          ResultList.SaveToFile(edt2.Text + save_file_name); //保存到原txt文件中
          ResultList.Free;
        except
          ShowMessage('Save Err! Pls Check the File is Close');
        end;
      end;
    end;
  end;
end;

procedure saveCsvReport(len_ss:integer);       //CSV文件保存统计报告
var
    f: TextFile;
    save_file_name: string;
    s,  SLINE_head,SLINE_head1: string;
begin
  with Form_line do begin
     if (Label10.caption<>'Label10')and(Label10.caption<>'OneBarcode-OneFile') then
     begin
        save_file_name :=Label10.caption;
        if  not isfileinuse(edt2.Text + save_file_name) then  begin   //没有占用
          try
            assignfile(f, edt2.Text + save_file_name);      //ExtractFilePath(application.ExeName)
            Append(f); //rewrite(f);
            //application.ProcessMessages;
            SLINE_head:=lbl79.caption+edt4.Text+','+lbl80.caption+edt63.Text+'('+edt65.text +'%)'
                  +','+edt64.Text+'('+edt66.text +'%)';
            writeln(f, SLINE_head);
          finally
            Closefile(f);
           // Application.Terminate;
          end;
        end else begin
          Application.MessageBox('LogFile in use！Log文件被占用，不能添加统计信息',
                '警告', MB_OK + MB_ICONWARNING);
        end;
     end;
  end;
end;

procedure SaveFlagProcess(devnum:integer;SaveLen:SmallInt);         //数据保存函数，唯一入口
var
//  f: TextFile;
  NewLogFileFlag:Byte;        //应该生成新文件标志
  devi,i,j,k,crc_temp,tt:SmallInt;
  save_file_name,str,ss,mss,sst: string;  //  ff:Real;
  UsbBuf: array [0..64] of Byte;
  Written: Cardinal;
  ToWrite: Cardinal;
  Err: DWORD;


  procedure UnlockClearBarcode();
  begin
    with Form_line do begin
      if (StrToInt(Label7.hint)=1) or (StrToInt(Label7.hint)=2) or (StrToInt(Label7.hint)>10 ) then       //自动清除条码
      begin
          if not  Edit1.Enabled {and not FormLoad.Showing} then begin
            if Readchis('Model Para','BarCodeEdit')<>'' then begin
              if StrToInt(Readchis('Model Para','BarCodeEdit'))<1000 then  begin
                 edit1.Enabled:=true;
                 if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then   Edit1.SetFocus;
              end;
            end else begin
              Edit1.Enabled:=true;
              if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then   Edit1.SetFocus;
            end;
          end;       //开启条码框输入功能
          if not mEdit1.Enabled then medit1.Enabled:=true;
          
          Edit1.Text:='';
          medit1.Text:='';
      end;
    end;
  end;
begin
  with Form_line do begin


    //-------------------------------------单机数据更新 ------------------------------
    if (SaveLen>0)  then
    begin
      if grp45.Visible and (Readchis('Result','StandardSave')='' )    //通用：只有配置了保存标准板才执行
        and ( (edt171.Color<>clLime)or( edt173.Color<>clLime )or(edt175.Color<>clLime) ) then begin    //有一个标准板没有验证
         Label11.Font.Color:=clWindowText;
         Label11.Caption:='标准板验证...';
         if (rbuf[4]=0) then lbl191.Caption:='当前结果：PASS' else lbl191.Caption:='当前结果：FAIL';
         lbl193.Caption:='当前条码：';
         edt176.Text:=Trim(edit1.Text);
         if Trim(edt171.Text)=Trim(edit1.Text) then begin
           if (rbuf[4]=0)and((edt170.Text='PASS')or(edt170.Text='pass')or(edt170.Text='OK')or(edt170.Text='ok')) then begin
             edt171.Color:=clLime;  edt170.Color:=clLime;
           end else if (rbuf[4]>0)and((edt170.Text='FAIL')or(edt170.Text='fail')or(edt170.Text='NG')or(edt170.Text='ng')) then begin
             edt171.Color:=clLime;  edt170.Color:=clLime;
           end else
             edt171.Color:=clred;
         end;
         if Trim(edt173.Text)=Trim(edit1.Text) then begin
           if (rbuf[4]=0)and((edt172.Text='PASS')or(edt172.Text='pass')or(edt172.Text='OK')or(edt172.Text='ok')) then begin
             edt173.Color:=clLime;  edt172.Color:=clLime;
           end else if (rbuf[4]>0)and((edt172.Text='FAIL')or(edt172.Text='fail')or(edt172.Text='NG')or(edt172.Text='ng')) then begin
             edt173.Color:=clLime;  edt172.Color:=clLime;
           end else
             edt173.Color:=clred;

         end;
         if Trim(edt175.Text)=Trim(edit1.Text) then begin
           if (rbuf[4]=0)and((edt174.Text='PASS')or(edt174.Text='pass')or(edt174.Text='OK')or(edt174.Text='ok')) then begin
             edt175.Color:=clLime;  edt174.Color:=clLime;
           end else if (rbuf[4]>0)and((edt174.Text='FAIL')or(edt174.Text='fail')or(edt174.Text='NG')or(edt174.Text='ng')) then begin
             edt175.Color:=clLime;  edt174.Color:=clLime;
           end else
             edt175.Color:=clred;

         end;
         if ( Trim(edt171.Text)<>Trim(edit1.Text) )and( Trim(edt173.Text)<>Trim(edit1.Text) )and( Trim(edt175.Text)<>Trim(edit1.Text) ) then begin
           lbl193.Caption:='非标准板条码：'; lbl193.Color:=clred;
           edt176.Text:=Trim(edit1.Text);
         end;
        //------------数据上传成功，给MCU确认信息//////////--------------------------------------------------------------

          Sleep(40);             //延时，等待通信链接命令确认完成！
          if (Comm1.BaudRate=9600)and (not SpeedButton12.Enabled ) then        //FCT5增加100mS延时；
            Sleep(100);
          if (grp1.Tag=2)and(edt62.text='24')and(rbuf[4]=11) then begin       //强制FAIL<--- tesla用8700仪器有bug；
            ;
          end else begin
            //sbuf[1]:=1;
            sbuf[2]:=5; sbuf[3]:=$50;sbuf[4]:=$00;
            sbuf[5]:=$FF; sbuf[6]:=$00; send_len:=6;
            try
                send_crcdata(send_len,2);    //          nsend:=nsend+1;  //          fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
            except
                messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
            end;
          end;
        SaveLen:=0;    //保存数据个数清零
        UnlockClearBarcode();       //解锁条码并清空
        mmo19.Lines.Add('8.标准板测试') ;
        LV1MemoDisp(SaveLen);
      //  Saveflag:=0;
        exit;
      end;
                            //有收到数据，新文件导入标志清除
      NewLogFileFlag:=0;       //测量结果预处理，是否用下位机机种生产数据文件前先清除标志；


        /////////////-----------文件夹名字来源,用save_file_name表示----------------------------------------
        if (edt77.Text<>'') and (edt77.Text<>'-')          //不能 空或者-文件夹。
        then begin
          if Length(edt77.text)<2 then begin
          //   SaveLen:=Saveflag; Saveflag:=0;              //数据长度替换，避免多线程造成重复进入！
             case
               Application.MessageBox('请确认测试程序第一步骤是否使用AZV0正确设置P/N，继续保存log数据吗？',
               'P/N warning! 料号长度异常！', MB_YESNO + MB_ICONWARNING + 
               MB_DEFBUTTON2) of
               IDYES:
                 begin
                   save_file_name:=edt77.Text;
                 end;
               IDNO:
                 begin
                   edt77.Color:=clRed;
                   edit1.Enabled:=True;
                   Exit;
                 end;
             end;
          end else
            save_file_name:=edt77.Text;
        end else begin            //非tesla格式，文件夹名取自主板名称,MFLEX不生产文件夹，见下面代码！
          save_file_name:='';
          if (Char(rbuf[6])>='0') and (Char(rbuf[6])<='z') then save_file_name:=save_file_name+Char(rbuf[6]);
          if (Char(rbuf[5])>='0') and (Char(rbuf[5])<='z') then save_file_name:=save_file_name+Char(rbuf[5]);
          if (Char(rbuf[8])>='0') and (Char(rbuf[8])<='z') then save_file_name:=save_file_name+Char(rbuf[8]);
          if (Char(rbuf[7])>='0') and (Char(rbuf[7])<='z') then save_file_name:=save_file_name+Char(rbuf[7]);
          if (Char(rbuf[10])>='0') and (Char(rbuf[10])<='z') then save_file_name:=save_file_name+Char(rbuf[10]);
          if (Char(rbuf[9])>='0') and (Char(rbuf[9])<='z') then save_file_name:=save_file_name+Char(rbuf[9]);

          if (edt77.Text='') or (edt77.Text='-')  then        //不能为空或者-文件夹。
             edt77.Text:= save_file_name;
        end;

        //--不知何故会在此处循环警告，用edt77.visible内容控制不循环---------------------
        if edt77.Visible and ( (edt77.Text='') or (edt77.Text='') ) then begin   //PN料号不能为空或者-，否提示
          edt77.Visible:=False;
          case
            Application.MessageBox('Invalid P/N,no logfile,continue?;料号无效，不能生成测试Log文件，继续吗？',
            '警告', MB_OKCANCEL + MB_ICONINFORMATION) of
            IDOK:
              begin
                edt77.Visible:=true;
              end;
            IDCANCEL:
              begin
                Form_line.Close;
              end;
          end;
        end;
        /////////////-----------文件夹生成，比文件生成慢？----------------------------------------

        if(save_file_name<>'')and ( lbl90.hint<>(lbl114.Caption+save_file_name) ) then    //机种名不同，产生新文件
        begin
          save_file_name:=lbl114.Caption+save_file_name;    //文件名命名（加上前缀）
          lbl90.hint:=save_file_name;     //机种名称，不同于label10(文件名)同时作为子文件夹名 ，可限制重复生成文件。

          if grp1.Tag=2 then begin    //tesla数据格式，一次扩展，保存本地,生成子文件夹
              if not DirectoryExists(edt2.Text+save_file_name+'\') then      //通用文件
              try
                begin
                  CreateDir(edt2.Text+save_file_name+'\');//   创建目录
                end;
              except  //finally
                raise Exception.Create('Cannot Create '+edt2.Text+save_file_name+'\');
              end;
              if not DirectoryExists(edt1.Text+save_file_name+'\') then      //订单+作业员 命名文件
              try
                begin
                CreateDir(edt1.Text+save_file_name+'\');
                end;
              except  //finally
                raise Exception.Create('Cannot Create '+edt1.Text+save_file_name+'\');
              end;

              if(edt62.Text='24')or(Form_line.edt62.text='25') then begin
                if Readchis('Model Para', 'LoginItem4')=''  then lbl93.Caption:='Fixture';
                if not DirectoryExists(edt157.Text+save_file_name+'\') then      //订单+作业员 命名文件
                try
                  begin
                    CreateDir(edt157.Text+save_file_name+'\');
                  end;
                except  //finally
                  raise Exception.Create('Cannot Create '+edt157.Text+save_file_name+'\');
                end;
              end;
          end else if (StrToInt(edt62.Text) >=10) then begin   //childfloder>=10: mflex数据格式，保存本地，由服务器来读取,不生成子文件夹

          end else if (StrToInt(edt62.Text) >1) then  begin  //childfloder>0: 生成子文件夹
          //tesla基本文件格式，其他 等.....
              if not DirectoryExists(edt2.Text+save_file_name+'\') then                            //FileExists
              try
                begin
                CreateDir(edt2.Text+save_file_name+'\');        //   创建目录
                end;
              except  //finally
                raise Exception.Create('Cannot Create '+edt2.Text+save_file_name+'\');
              end;

          end;
          Sleep(10);      //2018-12-01由100改成20
        /////////////-----以上处理文件夹生成------以下处理 文件生成与写数据时的文件生成冲突？？----------------------------------------
          NewLogFileFlag:=1;TestNum:=0;     //标志应该新生成文件，计数清空
        end;
    //  end;
      //---------以上处理完文【件夹命】名问题^^^^--------------------VVVV文件命名在数据保存函数中--------------------------------------------------------

    //  SaveLen:=Saveflag; Saveflag:=0;   //数据长度替换
      if SaveLen<2 then begin              //去掉数据包头后：
          case
            Application.MessageBox('接收到的数据异常（少于2个字）！',
            '警告', MB_OKCANCEL + MB_ICONINFORMATION) of
            IDOK:
              begin
              //   Form_line.Close;
              end;
            IDCANCEL:
              begin
              //   Form_line.edt127.Visible:=true;
              end;
          end;
      end;
      Label7.Caption:='BarCode';    //PC收到上传数据则更新标志，与UDP通信对应！
      /////////结果显示，记录更新，良品统计----------------------------------------------------------------------------
      TestNum:=TestNum+1;      //序号
      if TestNum >999999 then TestNum:=1;

      lbl191.Caption:='-';lbl193.Caption:='-'; edt176.Text:='-';
      if grp45.Visible and ( (edt171.Color<>clLime)or(edt173.Color<>clLime)or(edt175.Color<>clLime) ) then begin    //3PCS标准版：有一个标准板没有验证
         Label11.Font.Color:=clWindowText;
         Label11.Caption:='标准板验证...';
         if (rbuf[4]=0) then lbl191.Caption:='当前结果：PASS' else lbl191.Caption:='当前结果：FAIL';
         lbl193.Caption:='当前条码：';
         edt176.Text:=Trim(edit1.Text);
         if Trim(edt171.Text)=Trim(edit1.Text) then begin
           if (rbuf[4]=0)and((edt170.Text='PASS')or(edt170.Text='pass')or(edt170.Text='OK')or(edt170.Text='ok')) then begin
             edt171.Color:=clLime;
           end else if (rbuf[4]>0)and((edt170.Text='FAIL')or(edt170.Text='fail')or(edt170.Text='NG')or(edt170.Text='ng')) then begin
             edt171.Color:=clLime;
           end else
             edt171.Color:=clred;
         end else if Trim(edt173.Text)=Trim(edit1.Text) then begin
           if (rbuf[4]=0)and((edt172.Text='PASS')or(edt172.Text='pass')or(edt172.Text='OK')or(edt172.Text='ok')) then begin
             edt173.Color:=clLime;
           end else if (rbuf[4]>0)and((edt172.Text='FAIL')or(edt172.Text='fail')or(edt172.Text='NG')or(edt172.Text='ng')) then begin
             edt173.Color:=clLime;
           end else
             edt173.Color:=clred;

         end else if Trim(edt175.Text)=Trim(edit1.Text) then begin
           if (rbuf[4]=0)and((edt174.Text='PASS')or(edt174.Text='pass')or(edt174.Text='OK')or(edt174.Text='ok')) then begin
             edt175.Color:=clLime;
           end else if (rbuf[4]>0)and((edt174.Text='FAIL')or(edt174.Text='fail')or(edt174.Text='NG')or(edt174.Text='ng')) then begin
             edt175.Color:=clLime;
           end else
             edt175.Color:=clred;

         end else begin
           lbl193.Caption:='非标准板条码：'; lbl193.Color:=clred;
           edt176.Text:=Trim(edit1.Text);
         end;

      end else begin

        begin
          if (rbuf[4]=0)              //结果OK
          then begin
              Label11.Font.Color:=clLime;
              if (ts7.Tag=0){not ts7.Showing} or(StrToInt(edt59.Text)*strtoint(edt60.Text)=1)or (edt74.Text='10')  then  PassCount:=PassCount+1;
              Label11.Caption:='PASS';
          end else begin
              Label11.Font.Color:=clRed;
              if (ts7.Tag=0){not ts7.Showing} or(StrToInt(edt59.Text)*strtoint(edt60.Text)=1)or (edt74.Text='10') then  NGcount:=NGcount+1;
              Label11.Caption:='FAIL'
          end;
        end;

        PassOverT:=StrToInt('0'+edt40.Text);    //超时显示待机
        NGOverT:=  StrToInt('0'+edt42.Text);

          if (ts7.Tag=0){not ts7.Showing}  or(StrToInt(edt59.Text)*strtoint(edt60.Text)=1)or(edt74.Text='10') then     //////只有单PCS测试，才此处良品与不良品统计 。否则在联板测试良品与不良品统计
          begin
            edt4.Text:=inttostr(PassCount+NGcount);
            edt63.Text:= inttostr(PassCount);
            edt64.Text:= inttostr(NGCount);
            if (PassCount+NGcount)>0 then  begin
              edt65.Text:= floattostrF((PassCount*100)/(PassCount+NGcount),ffFixed,5,2);//inttostr((PassCount*100) div (PassCount+NGcount));
              edt66.Text:= floattostrF((NGCount*100)/(PassCount+NGcount),ffFixed,5,2);//inttostr((NGCount*100) div (PassCount+NGcount) );
            end else begin
              edt65.Text:='0';
              edt66.Text:='0';
            end;
          end;

      end;

      //------------------正常 数据上传及保存VVVVVVVVVVVVVVV-----------------------------------------------------------------------
      if StrToInt(edt38.text)mod 10 >0 then begin                               //uplocal 且
        edt77.Color:=clWindow;
        if Pos('?',edt77.Text)>0 then edt77.Color:=clred;
        if Pos('*',edt77.Text)>0 then edt77.Color:=clred;
        if Pos(':',edt77.Text)>0 then edt77.Color:=clred;
        if Pos('"',edt77.Text)>0 then edt77.Color:=clred;
        if Pos('<',edt77.Text)>0 then edt77.Color:=clred;
        if Pos('>',edt77.Text)>0 then edt77.Color:=clred;
        if Pos('\',edt77.Text)>0 then edt77.Color:=clred;
        if Pos('/',edt77.Text)>0 then edt77.Color:=clred;
        if Pos('|',edt77.Text)>0 then edt77.Color:=clred;

        if  ( (StrToInt(edt38.text)div 10 =1)and(rbuf[4]>0) )                   //仅pass保存文件
          or( (StrToInt(edt38.text)div 10 =2)and(rbuf[4]=0) ) then begin        //仅FAIL保存文件
           form_line.label10.caption:='test fail';    ;                                                                //不保存
        end else begin
           SaveCsvData(SaveLen,NewLogFileFlag)       //保存为CSV文件
        end;
      end else begin
        Label11.Font.Size:=80;
        Lbl169.Caption:='pls set Uplocal=1'; lbl169.Font.Size:=60;
        LV1MemoDisp(SaveLen);                    //没有启用保存log文件时的显示！
      end;

      if( Readchis('Comm Para','DoubleBarcodeStart')='1' )
        and tabsheet2.TabVisible and (Label7.hint='11') then     //双条码，显示结果指示界面
        ts7.Show;

      mmo19.Lines.Add('6.local finish') ;
      if StrToInt(edt75.Text)>0 then          //打印 //结果打印
      begin
        printdata(SaveLen);
        if (edt62.Text<>'25') then          //非 jaguar项目
          ts8.Show;
        tt:=StrToInt(edt75.Text) div 1000;  //千位位选择打印
        if   (tt=2)
          or (( rbuf[4]=0)and (tt=0))       //ok打印
          or (( rbuf[4]=1)and (tt=1))       //ng打印
        then
          PrintClickProcess(0);          
      end;

      datetimetostring(sst,'yyyymmddhhnnss',Now);    //生成数据的时间，上传服务器用。

      /////////数据上传成功，给MCU确认信息//////////--------------------------------------------------------------
      begin
        Sleep(40);             //延时，等待通信链接命令确认完成！
        if (Comm1.BaudRate=9600)and (not SpeedButton12.Enabled ) then        //FCT5增加100mS延时；
          Sleep(100);
        if (grp1.Tag=2)and(edt62.text='24')and(rbuf[4]=11) then begin       //强制FAIL<--- tesla用8700仪器有bug；
          ;
        end else begin
          //sbuf[1]:=1;
          sbuf[2]:=5; sbuf[3]:=$50;sbuf[4]:=$00;
          sbuf[5]:=$FF; sbuf[6]:=$00; send_len:=6;
          try
              send_crcdata(send_len,2);    //          nsend:=nsend+1;  //          fswitch.StatusBar1.Panels[1].Text:='发送记数：'+inttostr(nsend);
          except
              messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
          end;
        end;
      end;

      SaveLen:=0;    //保存数据个数清零

      UnlockClearBarcode();       //解锁条码并清空
      mmo19.Lines.Add('8.barcode enable') ;

      if Form_line.groupbox1.hint<>'5' then begin         //FCT5不支持加载和读取长文件名

        if (inportNewFileflag mod 1000)>0 then begin     //收到一条数据后，提示重新导入文件表头!（提取更新表头会造成前一个数据文件头异常！！）；安捷利防止刷新原来的文件头格式
          if Form_line.edt82.Text='99' then begin        //enPN =99通用！ //jaguar等启用PC存放程序  enPN   demo
            FreshDatalogFromMemo(5);                     //立即更新文件头，减少重复loading..时间！(前提是前面loading正常)；
            
          end else begin                                 //此处再加载会导致第1次测试出现ready!
            modbusfun06dat:=$00F6;        //读上限下限 及短路群
            modbusfun06:=$0001;
            sbtbtn1.Enabled:=False;       //loading...之后才使能
            tmr4.Enabled:=true;tmr4.Interval:=2000+3;
          end;

            inportNewFileflag:=1000;    //加载中标志... ：允许重新更新文件头
        end;
      end else begin
        inportNewFileflag:=0;    //其它：允许重新更新文件头
      end;
    end;

  end;
end;

//////////////////////////////启动，生成一个新的带日期.CSV文件
//输入 tt=0：开机生成文件，tt=1:非开机生成文件  tt=3:  备份文件生成 ,没有启用
//输入 barcode:取第几个条码框的条码 ,=9999则不生成条码文件
procedure CreatAnewCSV(tt:integer;barcode:SmallInt);

var
    nrf_dat_config: Tinifile;
    ini_path: string;

    s, SLINE_head,SLINE_head2: string;
    i,temp: integer;
    f,file2: TextFile;
    save_file_name,sst,ssdatetime: string;
  procedure createLedBoxFile();        //车灯机箱专用
  var
    i,j:Integer;
  begin
    with Form_line do begin
    //  if not fileexists(edt2.Text + save_file_name) and (barcode<9999) then    //barcode允许新生成文件
    //  begin
        try
            if(tt=9000)  then
              assignfile(f, edt135.Text + save_file_name)
            else
              assignfile(f, edt2.Text + save_file_name);
            rewrite(f);Sleep(20);
          {  if (readchis('result','PrintRow')<>'')or(readchis('result','PrintMaxRows')<>'') then begin     //安捷利定义
                if rbuf[4]=0 then
                  SLINE_head := 'PASS'
                else
                  SLINE_head := 'FAIL';
                if(readchis('Model Para','MachineNum')<>'')  then
                  SLINE_head :=SLINE_head+','+readchis('Model Para','MachineNum')
                else
                  SLINE_head :=SLINE_head+',ICT0' +inttostr(rbuf[11]);   //机台
                SLINE_head :=SLINE_head+',' +inttostr(TestNum);         //序号
                SLINE_head :=SLINE_head+',' +Lbl114.Caption+ edt77.Text;  //机种名->程序名称
                SLINE_head :=SLINE_head+',' +trim(edit1.Text);      //条码
                SLINE_head :=SLINE_head+',' +Copy(ssdatetime,0,8) ; //日期
                SLINE_head :=SLINE_head+',' +Copy(ssdatetime,9,6) ; //时间
                if rbuf[4]=0 then
                  SLINE_head :=SLINE_head+',' +'P'
                else
                  SLINE_head :=SLINE_head+',' +'F';       //结果
                SLINE_head :=SLINE_head+',' +'0';         //是否重测
                SLINE_head :=SLINE_head+',' +'1';         //工作表格
                SLINE_head :=SLINE_head+',' +edt79.Text;  //<11>人员编号

                SLINE_head :=SLINE_head+',' +'PPPPPNPNNNNN';    //详细结果
                SLINE_head :=SLINE_head+',' +'0';         //多联板

                if rbuf[4]=0 then
                  SLINE_head :=SLINE_head+',' +'P'        //多联板结果
                else
                  SLINE_head :=SLINE_head+',' +'F';       //结果
                SLINE_head :=SLINE_head+',' +'0';         //多板重测

                writeln(f, SLINE_head);             //原始csv文件只写一行头数据
                writeln(f, '');
            end else begin   }
                if rbuf[4]=0 then
                  SLINE_head := trim(edit1.Text)+',PASS,FCT'
                else
                  SLINE_head := trim(edit1.Text)+',FAIL,FCT';

                if(readchis('Model Para','MachineNum')<>'')  then
                  SLINE_head :=SLINE_head+','+readchis('Model Para','MachineNum')
                else
                  SLINE_head :=SLINE_head+',CT-'+inttostr(rbuf[11]);   //机台
                SLINE_head :=SLINE_head+','+ edt77.Text;
                SLINE_head :=SLINE_head+','+ edt78.Text;   //线别          //SLINE_head :=SLINE_head+',NO.' +inttostr(rbuf[11]);    //机台
                SLINE_head :=SLINE_head+','+ edt79.Text;
                SLINE_head :=SLINE_head+','+ edt80.Text;   //线别
                SLINE_head :=SLINE_head+','+ ssdatetime;
                writeln(f, SLINE_head);             //原始csv文件只写一行头数据
          //  end;
        finally
          Closefile(f);
        end;
    //  end;
    end;
  end;

  procedure createTeslaFile();
  var
    i,j:Integer;
  begin
    with Form_line do begin
      if not fileexists(edt2.Text + save_file_name) then
      begin
        try
          assignfile(f, {ExtractFilePath(application.ExeName)}edt2.Text + save_file_name);
          rewrite(f);Sleep(50);

          if(Form_line.edt62.text='25') then begin
            if Readchis('Model Para', 'LoginItem1')=''  then lbl90.Caption:='Product';
            if Readchis('Model Para', 'LoginItem2')=''  then lbl91.Caption:='Lot No';
            if Readchis('Model Para', 'LoginItem4')=''  then lbl93.Caption:='Station:';
            writeln(f, 'TestSystem:hFCT9'+',Version:'+label9.caption+',Station:'+edt80.Text);        //暂借用为机台编号

            if(StrToInt(edt81.Text)>0) then
              NameCount:=GetNameUnitFromConfig($3333);                   //先清空名称和单位，再从config赋值
            SLINE_head  := 'Product'+',Lot No,SerialNumber,Test Pass/Fail Status,StartTime,EndTime,Fixture ID,Operator,List of Failing Tests,';
            for i:=1 to UpStepMax do
            begin
              if NameStrZu[i]<>'' then
                SLINE_head := SLINE_head +NameStrZu[i]+',' ;     //名称
            end;
            if tt=0 then                  //（开启软件）第一次显示或者一个条码一个文件 才会更新 ,如果数据上传产生文件则不更新名称等...
            begin
                mmo2String[0]:=SLINE_head;        // 名称
            end;
            writeln(f, SLINE_head);
             //  writeln(f, 'Lot No'+','+ edt78.Text);
             //  writeln(f, lbl92.Caption+','+ edt79.Text);
            SLINE_head  := 'Upper limit(上限）------->'+',N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,';
            SLINE_head2 := 'Lower limit(下限）------->'+',N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,';
            for i:=1 to UpStepMax do
            begin
              if UpperZu[i]<>'' then SLINE_head  := SLINE_head  +UpperZu[i]+',' ;     //上限
              if LowerZu[i]<>'' then SLINE_head2 := SLINE_head2 +LowerZu[i]+',' ;    // 下限
            end;
            writeln(f, SLINE_head );
            writeln(f, SLINE_head2);

            SLINE_head2 := 'Measurement unit(单位）-->'+',N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,';
            for i:=1 to UpStepMax do
            begin
              if UnitStrZu[i]<>'' then
                SLINE_head2 := SLINE_head2 +UnitStrZu[i]+',' ;    //   单位
            end;
            writeln(f, SLINE_head2);      //单位
            if tt=0 then                  //（开启软件）第一次显示或者一个条码一个文件 才会更新 ,如果数据上传产生文件则不更新名称等...
            begin           //mmo2String[0]:=SLINE_head;        // 名称
              mmo2String[2]:=SLINE_head2;       //单位
            end;
          end else begin                       //早期的一些格式！ 只取config配置
             begin
               writeln(f, 'Customer PN'+','+ edt77.Text);                       //tesla总文件！
               writeln(f, 'Supplier Lot Code'+','+ edt78.Text);
               writeln(f, lbl92.Caption+','+ edt79.Text);
               writeln(f, lbl93.Caption+','+ edt80.Text);
             end;
            if (StrToInt(Form_line.edt81.Text)>0) then
              NameCount:=GetNameUnitFromConfig($3333);              //兼容早期:只取config配置   //先清空名称和单位，再从config赋值
            SLINE_head := ''+','+ 'LSL'+','+',';
            SLINE_head2 := ''+','+ 'USL'+','+',';
            for i:=1 to UpStepMax do
            begin
              if LowerZu[i]<>'' then  SLINE_head := SLINE_head +LowerZu[i]+',' ;    // 下限
              if UpperZu[i]<>'' then SLINE_head2 := SLINE_head2 +UpperZu[i]+',' ;     //上限
            end;
            writeln(f, SLINE_head );     //下限
            writeln(f, SLINE_head2);

            SLINE_head := ''+','+ ''+','+''+',';
            SLINE_head2:=''+','+ 'UNIT'+','+''+',';
            for i:=1 to UpStepMax do
            begin
                if NameStrZu[i]<>'' then SLINE_head := SLINE_head +NameStrZu[i]+',' ;     //名称
                if UnitStrZu[i]<>'' then  SLINE_head2 := SLINE_head2 +UnitStrZu[i]+',' ;    //   单位
            end;
            writeln(f, SLINE_head2);      //单位

            if tt=0 then         //tesla专用（防止mmo2为空）： （开启软件）第一次显示或者一个条码一个文件 才会更新 ,如果数据上传产生文件则不更新名称等...
            begin
              mmo2.Lines[0]:=lbl90.Caption+','+ edt77.Text;      //前缀+机种名
              if (edt62.Text<>'24')and(Readchis('Comm Para','BarcodeCSVposCol')<>'2') then begin           //非指定第2列，则默认第1列
                mmo2.Lines[2]:=Copy('Serial Barcode'+','+'Time'+','+'Result'+','+'Trace',0,1024);
              end else
                mmo2.Lines[2]:=Copy('Time'+','+'Serial Barcode'+','+'Result'+','+'Trace',0,1024);      //早期：条码在第二列
              mmo2String[0]:=SLINE_head;        // 名称
              mmo2String[2]:=SLINE_head2;       //单位
            end;

            if(edt62.Text='24') then begin                                        //tesla改造后格式，项目名合成在一行；
              writeln(f, '');           //空一行
              SLINE_head:='Time'+','+'Serial Barcode'+','+'Result'+',';    //tesla专用 ，时间在前
              for i:=1 to UpStepMax do
              begin
                if NameStrZu[i]<>'' then SLINE_head := SLINE_head +NameStrZu[i]+',' ;     //名称
              end;
              writeln(f, SLINE_head);           //网络名称
            end else begin                                                      //更早期的文件！
              if Readchis('Comm Para','BarcodeCSVposCol')<>'2' then begin           //非指定第2列，则默认第1列
                writeln(f, 'Serial Barcode'+','+'Time'+','+'Result'+','+'Trace');    
              end else
                writeln(f, 'Time'+','+'Serial Barcode'+','+'Result'+','+'Trace');    //早期：tesla专用 ，时间在前
              writeln(f, SLINE_head);           //网络名称
            end;
          end;
        finally
          Closefile(f);
        end;
      end;

    end;
  end;


begin             //-----------------生成CSV总的入口----------------------------

  datetimetostring(ssdatetime,'yyyymmddhhnnss',Now);
  with Form_line do begin
    if( label10.tag<24 ) then
      sst:=Copy(ssdatetime,0,8)    //一天一个文件
    else
      sst:=ssdatetime;
  //----------------集总主文件名 生成--------(只有MFLEX条码作为主文件)-----------------------------
    if (StrToInt(edt62.Text)>=1) then begin    //通用：文件夹使能 ,文件名 不能含有以下9种字符：?*:"<>\/|

        if  (tt=9000) then begin        //udp //xx车灯机箱专用 文件  (edt47.text='5')or
            if edt77.Hint='Z10' then begin     //淘汰！第一个步骤为Z10
              edt77.tag:=edt77.tag+1;
              if edt77.tag<=1 then
                save_file_name :=edt77.Text+ '-' + DelSpecialChar(trim(edit1.Text))+'-'
              else
                Exit;
            end else  begin
              if edt62.Text='2' then  begin
                 save_file_name :=edt77.Text+'\' + DelSpecialChar(trim(edit1.Text))+'-';
              end else
                save_file_name := DelSpecialChar(trim(edit1.Text))+'-';
            end;
            if(rbuf[4]=0) then
              save_file_name:=save_file_name+'PASS'+'-'+sst   //+'.txt'
            else
              save_file_name:=save_file_name+'FAIL'+'-'+sst;  //+'.txt';
            if (lbl177.Caption='工位1 ：') then begin
               save_file_name:=save_file_name+'-1';
            end;
            if (lbl177.Caption='工位2 ：') then begin
               save_file_name:=save_file_name+'-2';
            end;
            if (lbl177.Caption='工位3 ：') then begin
               save_file_name:=save_file_name+'-3';
            end;
            if (lbl177.Caption='工位4 ：') then begin
               save_file_name:=save_file_name+'-4';
            end;
            if readchis('Comm Para','FileExtName')<>'' then
              save_file_name:=save_file_name+'.'+readchis('Comm Para','FileExtName')
            else
              save_file_name:=save_file_name+'.txt';
        end else if (grp1.Tag=2) then  begin       //tesla或者jaguar文件
          if(edt62.Text='25')then
            save_file_name := edt77.Text+'\'+edt77.Text+'-'+sst +'-' + edt78.Text +'_' +IdIPWatch1.LocalIP+ '.csv'       //料号来自输入 ，+时间，+订单号 +IP
          else if(edt62.Text='24')then begin
            datetimetostring(sst,'yyyy-mm-dd-HHnnss',Now);
            if StrToInt(edt81.Text)>0 then
              save_file_name := edt77.Text+'\Tesla '+edt77.Text +' ('+ edt78.Text+') resistance data '+copy(sst,1,10)+'('+copy(sst,12,6) + ').csv'       //料号来自输入 ，+时间，+订单号
            else
              save_file_name := edt77.Text+'\'+Readchis('Comm Para','ProjectName')+edt77.Text +' ('+ edt78.Text+') resistance data '+copy(sst,1,10)+'('+copy(sst,12,6) + ').csv'       //料号来自输入 ，+时间，+订单号
          end else
            save_file_name := edt77.Text+'\'+edt77.Text+'-'+sst +'-' + edt78.Text + '.csv'       //料号来自输入 ，+时间，+订单号
        end else begin                 //childfolder<10
            save_file_name := lbl90.hint+'\'+lbl90.hint+'-'+sst + '.csv';  //料号来自主板，并用于文件夹命名
        end;
    end else                                     //仅生产单一文件
      save_file_name := lbl90.hint+'-'+sst + '.csv';

    if(tt=9000) then
      Lbl161.Caption:=save_file_name            //获取文件名，每次生成都会更新
    else
      Label10.Caption:=save_file_name;           //获取文件名，每次生成都会更新
  //-----------------------创建文件  处理-------------------------------
    if (tt=9000) then begin           //    udp//xx或车灯机箱专用(edt47.text='5')or
        createLedBoxFile();
    end else if grp1.Tag=2 then  begin     //tesla或者jagura文件头有4行 ,专用格式
      createTeslaFile();
    end;
  end;
end;



{   函数：saveCsvdata(len_ss:integer)
说明:接收到的数据保存为.txt文件，可支持
1.连片输出数据的保存，仅保存到同一个文件里
2.根据条码规则自动生成所有同一规律的条码
3.区分指示灯显示
}
procedure saveCsvdata(len_ss:integer;NewLogFileFlag:byte);

var
    f: TextFile;
    save_file_name: string;
    strtemp,stime,SLINE_head,SLINE_head1,SLINE_head2,sline_sn,sntime: string;
    i,j,bb: integer;
    zuptr,dd,cboflag,zzoflag:SmallInt;
    ff:Real;
    ResultList:TStringList;
    procedure  saveCarLedlog(tt:SmallInt);
    var
      i,j:Integer;
    begin
      with Form_line do begin
        try
          if(tt=9000)  then
            assignfile(f, edt135.Text + lbl161.Caption)
          else
            assignfile(f, edt2.Text + save_file_name);
          Append(f); //rewrite(f);                      //application.ProcessMessages;

          if(btn30.hint<>'')and(readchis('Comm Para','OSpointPos')<>'OFF') then
            writeln(f,btn30.Hint);
        //  if (StrToInt(edt59.Text)*strtoint(edt60.Text)=1)or(edt74.Text<>'1') then begin
            for i:=1 to (len_ss div 2) do      //遍历所有数据， 放在多行存放
            begin
              dd:=(rbuf[10+4*i]*256+rbuf[11+4*i]);   //rbuf[14]为第一个
              ff:=dd / strtofloat(ScaleZu[zuptr]);
              if (Copy(ModeZu[i],2,2)='XH')or(Copy(ModeZu[i],2,2)='YH')
               or(Copy(ModeZu[i],2,2)='IZ')or(Copy(ModeZu[i],2,2)='JZ') then begin                    //专用文件格式
                strtemp:=inttostr(zuptr)+','+NameStrZu[zuptr]
                          +',-'//+inttohex(StrToInt(StdValZu[i]),2)+ UnitStrZu[zuptr]     //序号+名称+规格值带单位
                          +',0x'+inttohex(StrToInt(UpperZu[zuptr]),2)+ UnitStrZu[zuptr]
                          +',0x'+inttohex(StrToInt(LowerZu[zuptr]),2)+ UnitStrZu[zuptr]   //上限带单位+下限带单位
                          +',0x'+inttohex(dd,2)+ UnitStrZu[zuptr] ;
              end else begin
                strtemp:=inttostr(zuptr)+','+NameStrZu[zuptr]+',-'//+StdValZu[i]+ UnitStrZu[zuptr]  //序号+名称+规格值带单位
                          +','+UpperZu[zuptr]+ UnitStrZu[zuptr]+','+LowerZu[zuptr]+ UnitStrZu[zuptr]  //上限带单位+下限带单位
                          ;
                if    ( (Copy(ModeZu[i],2,1)='S')or(Copy(ModeZu[i],2,1)='O')or(Copy(ModeZu[i],2,1)='K') )     then begin
                  if ( (copy(NameStrZu[i],0,4)='OPEN')or(copy(NameStrZu[i],0,4)='SHOR')or(copy(NameStrZu[i],0,5)='SHORT') )  then begin
                    if dd<1 then begin
                      strtemp:=strtemp+',right' ;
                    end else begin
                      if (j=0)and(readchis('Comm Para','OSpointPos')<>'OFF') then begin
                        if (Copy(ModeZu[i],2,1)='S')and(btn34.Hint<>'') then
                          strtemp:=strtemp+',error'+btn34.Hint
                        else if (Copy(ModeZu[i],2,1)='O')and(btn33.Hint<>'') then
                          strtemp:=strtemp+',error'+btn33.Hint
                        else if (Copy(ModeZu[i],2,1)='K')and(btn33.Hint<>'') then
                          strtemp:=strtemp+',error'+btn33.Hint
                        else
                          SLINE_head:=SLINE_head+',error' ;
                      end else
                        strtemp:=strtemp+',error' ;
                    end;
                  end else begin
                    strtemp:=strtemp+',' +copy(floattostr(ff),0,7) ;                //不带单位
                  end;
                end else begin
                  strtemp:=strtemp+',' +copy(floattostr(ff),0,7)+ UnitStrZu[zuptr] ;                //测量值带单位
                end;
              end;

              if(Stat1.tag=9000)and(readchis('Comm Para','CSV5raw')='')then begin        //使用新的数据格式！
                if (rbuf[13+4*i] and $01)=$01 then begin         //NG
                  if ((copy(NameStrZu[zuptr],0,4))='OPEN') then begin           //UpperCase
                    strtemp:=strtemp  +'↑,ng,--'   ;
                  end else if((copy(NameStrZu[zuptr],0,4))='SHOR')or((copy(NameStrZu[zuptr],0,5))='SHORT') then begin    //UpperCase
                    strtemp:=strtemp  +'↓,ng,--'   ;
                  end else if (UpperZu[zuptr]<>'')and( LowerZu[zuptr]<>'')
                           and(UpperZu[zuptr]<>'30009')and( LowerZu[zuptr]<>'30009') then begin
                    if ff>=StrToFloat(UpperZu[zuptr]) then
                      strtemp:=strtemp  +'↑,ng,--'
                    else if ff<=StrToFloat(LowerZu[zuptr]) then
                      strtemp:=strtemp  +'↓,ng,--'
                    else
                      strtemp:=strtemp  +',ng,--'   ;
                  end else
                    strtemp:=strtemp  +',ng,--'   ;
                end else
                  strtemp:=strtemp  +',ok,--' ;
              end else begin
                if (rbuf[13+4*i] and $01)=$01 then begin         //NG
                  if (copy(NameStrZu[zuptr],0,4)='OPEN')or(copy(NameStrZu[zuptr],0,4)='SHOR')or(copy(NameStrZu[zuptr],0,5)='SHORT') then begin
                    strtemp:=strtemp  +',NG,--'   ;
                  end else if (UpperZu[zuptr]<>'')and( LowerZu[zuptr]<>'')
                           and(UpperZu[zuptr]<>'30009')and( LowerZu[zuptr]<>'30009') then begin
                    if ff>=StrToFloat(UpperZu[zuptr]) then
                      strtemp:=strtemp  +'↑,NG,--'
                    else if ff<=StrToFloat(LowerZu[zuptr]) then
                      strtemp:=strtemp  +'↓,NG,--'
                    else
                      strtemp:=strtemp  +',NG,--'   ;
                  end else
                    strtemp:=strtemp  +',NG,--'   ;
                end else
                  strtemp:=strtemp  +',PASS,--' ;
              end;
              if (zuptr<UpStepMax) then zuptr:=zuptr+1;
              writeln(f,  strtemp);
            end;
        {有bug  end else begin
            for i:=1 to (len_ss div 2) do      //遍历所有数据， 放在多行存放
            begin
            //  zuptr:=i;
              if rbuf[12+4*i]=j+1 then              //当前连片数据 指示
              begin
                dd:=(rbuf[10+4*i]*256+rbuf[11+4*i]);
                ff:=dd / strtofloat(ScaleZu[OnePCSNamePos[zuptr]]);
              end else
                Continue;

              if (Copy(ModeZu[i],2,2)='XH')or(Copy(ModeZu[i],2,2)='YH')
               or(Copy(ModeZu[i],2,2)='IZ')or(Copy(ModeZu[i],2,2)='JZ') then begin                    //专用文件格式
                strtemp:=inttostr(zuptr)+','+NameStrZu[OnePCSNamePos[zuptr]]
                          +',-'//+inttohex(StrToInt(StdValZu[i]),2)+ UnitStrZu[OnePCSNamePos[zuptr]]     //序号+名称+规格值带单位
                          +',0x'+inttohex(StrToInt(UpperZu[OnePCSNamePos[zuptr]]),2)+ UnitStrZu[OnePCSNamePos[zuptr]]
                          +',0x'+inttohex(StrToInt(LowerZu[OnePCSNamePos[zuptr]]),2)+ UnitStrZu[OnePCSNamePos[zuptr]]   //上限带单位+下限带单位
                          +',0x'+inttohex(dd,2)+ UnitStrZu[OnePCSNamePos[zuptr]] ;
              end else begin
                strtemp:=inttostr(zuptr)+','+NameStrZu[OnePCSNamePos[zuptr]]+',-'//+StdValZu[i]+ UnitStrZu[OnePCSNamePos[zuptr]]  //序号+名称+规格值带单位
                          +','+UpperZu[OnePCSNamePos[zuptr]]+ UnitStrZu[OnePCSNamePos[zuptr]]+','+LowerZu[OnePCSNamePos[zuptr]]+ UnitStrZu[OnePCSNamePos[zuptr]]  //上限带单位+下限带单位
                          ;
                if (Copy(ModeZu[i],2,1)='S')or(Copy(ModeZu[i],2,1)='O')or(Copy(ModeZu[i],2,1)='K')  then begin
                  if dd<1 then begin
                    strtemp:=strtemp+',right' ;
                  end else begin
                    if (j=0)and(readchis('Comm Para','OSpointPos')<>'OFF') then begin
                      if (Copy(ModeZu[i],2,1)='S')and(btn34.Hint<>'') then
                        strtemp:=strtemp+',error'+btn34.Hint
                      else if (Copy(ModeZu[i],2,1)='O')and(btn33.Hint<>'') then
                        strtemp:=strtemp+',error'+btn33.Hint
                      else if (Copy(ModeZu[i],2,1)='K')and(btn33.Hint<>'') then
                        strtemp:=strtemp+',error'+btn33.Hint
                      else
                        SLINE_head:=SLINE_head+',error' ;
                    end else
                      strtemp:=strtemp+',error' ;
                  end;
              
                end else begin
                  strtemp:=strtemp+',' +copy(floattostr(ff),0,7)+ UnitStrZu[OnePCSNamePos[zuptr]] ;                //测量值带单位
                end;
              end;

              if(Stat1.tag=9000)and(readchis('Comm Para','CSV5raw')='')then begin        //使用新的数据格式！
                if (rbuf[13+4*i] and $01)=$01 then begin         //NG
                  if ((copy(NameStrZu[OnePCSNamePos[zuptr]],0,4))='OPEN') then begin     //UpperCase
                    strtemp:=strtemp  +'↑,ng,--'   ;
                  end else if((copy(NameStrZu[OnePCSNamePos[zuptr]],0,4))='SHOR')or((copy(NameStrZu[OnePCSNamePos[zuptr]],0,5))='SHORT') then begin   //UpperCase
                    strtemp:=strtemp  +'↓,ng,--'   ;
                  end else if (UpperZu[OnePCSNamePos[zuptr]]<>'')and( LowerZu[OnePCSNamePos[zuptr]]<>'')
                           and(UpperZu[OnePCSNamePos[zuptr]]<>'30009')and( LowerZu[OnePCSNamePos[zuptr]]<>'30009') then begin
                    if ff>=StrToFloat(UpperZu[OnePCSNamePos[zuptr]]) then
                      strtemp:=strtemp  +'↑,ng,--'
                    else if ff<=StrToFloat(LowerZu[OnePCSNamePos[zuptr]]) then
                      strtemp:=strtemp  +'↓,ng,--'
                    else
                      strtemp:=strtemp  +',ng,--'   ;
                  end else
                    strtemp:=strtemp  +',ng,--'   ;
                end else
                  strtemp:=strtemp  +',ok,--' ;
              end else begin
                if (rbuf[13+4*i] and $01)=$01 then begin         //NG
                  if (copy(NameStrZu[OnePCSNamePos[zuptr]],0,4)='OPEN')or(copy(NameStrZu[OnePCSNamePos[zuptr]],0,4)='SHOR')or(copy(NameStrZu[OnePCSNamePos[zuptr]],0,5)='SHORT') then begin
                    strtemp:=strtemp  +',NG,--'   ;
                  end else if (UpperZu[OnePCSNamePos[zuptr]]<>'')and( LowerZu[OnePCSNamePos[zuptr]]<>'')
                           and(UpperZu[OnePCSNamePos[zuptr]]<>'30009')and( LowerZu[OnePCSNamePos[zuptr]]<>'30009') then begin
                    if ff>=StrToFloat(UpperZu[OnePCSNamePos[zuptr]]) then
                      strtemp:=strtemp  +'↑,NG,--'
                    else if ff<=StrToFloat(LowerZu[OnePCSNamePos[zuptr]]) then
                      strtemp:=strtemp  +'↓,NG,--'
                    else
                      strtemp:=strtemp  +',NG,--'   ;
                  end else
                    strtemp:=strtemp  +',NG,--'   ;
                end else
                  strtemp:=strtemp  +',PASS,--' ;
              end;

              if (zuptr<UpStepMax) then zuptr:=zuptr+1;
              writeln(f,  strtemp);
            end; 
          end;  }
        //  if(Stat1.tag=9000)and(readchis('Comm Para','CSV5raw')='')then begin        //使用新的数据格式！

        //  end else
          writeln(f, '--');              //标志下面数据是扩展原始数据
                  
          if (mmo2String[10]<>'')and(Readchis('Result','saveRawData')='1') then begin
            writeln(f, mmo2String[10]);
            mmo2String[10]:='';
          end;
        finally
            Closefile(f);
        end;


        if(edt53.hint='9000')and(Stat1.tag=9000)then begin    // 2BU0上传数据,用UDP接口转发
        
          idpclnt1.Active:=true; idpclnt1.tag:=0;             //  idpsrvr1.Active:=True;
          ResultList := TStringList.Create;
          try
            if(tt=9000)  then
              ResultList.LoadFromFile(edt135.Text + lbl161.Caption)
            else
              ResultList.LoadFromFile(edt2.Text + save_file_name);
          finally
          end;
          if edt53.tag=9002 then begin             //可靠发送：等待返回信号
            idpclnt1.Send(Edt100.Text, 9002, ResultList.Text);
            for i:=0 to idpsrvr1.Tag do begin
              Sleep(100);  application.ProcessMessages;
              if  idpclnt1.tag>0 then
                Break;
            end;
            ShowMessage('secondary development error!二次开发软件没有响应！');
          end else
            idpclnt1.Send(Edt100.Text, 9001, ResultList.Text);    //默认：不可靠发送：udp发送
        end;

      //重复了  if( StrToInt(edt6.Text)>0 )then begin         //启用映射盘，必须放在  UDP之后
      //    lbl181.Caption:='5';   btn2Click(form_line);
      //  end;
      end;
    end;
  procedure TeslaJiaheDataPre();                 //数据的前面几个项目
  var
    i,j,PP:Integer;
    stempljk:string;
    kljk:Integer;
  begin
    with Form_line do begin
      datetimetostring(stime,'yyyy-mm-dd HH:nn:ss',Now);
       //-------------------先处理前几个项目----------------------------------------------
      SLINE_head :='';
      if (edt48.Text='1') then begin                      //专用：barcode check=1:  条码核对字段排列
        SLINE_head := SLINE_head + edt77.Text+',' ;
        SLINE_head := SLINE_head + edt78.Text+',' ;
        SLINE_head := SLINE_head + edt79.Text+',' ;
        SLINE_head := SLINE_head +trim( edit1.Text)+',';
        SLINE_head := SLINE_head +trim(medit1.Text)+',';
      //  datetimetostring(stime,'yyyy-mm-dd HH:nn:ss',Now);
        SLINE_head := SLINE_head + stime+',' ;    //时间
      end else begin

        if (edt62.Text<>'24')and(Readchis('Comm Para','BarcodeCSVposCol')<>'2') then begin             //非指定第2列，则默认第1列
         
          if StrToInt('0'+Label7.hint)=2 then           ////用条码还是用序号                          //clearbarcode=2，条码用编号代替
            SLINE_head := SLINE_head +IntToStr(TestNum)+','
          else
            SLINE_head := SLINE_head +trim(edit1.Text)+',';            //2017-12-20加trim
          SLINE_head := SLINE_head + stime+',' ;    //时间在条码之后
        end else begin
          SLINE_head := SLINE_head + stime+',' ;    //早期：时间在条码之前

          if StrToInt('0'+Label7.hint)=2 then                          //clearbarcode=2，条码用编号代替
            SLINE_head := SLINE_head +IntToStr(TestNum)+','
          else
            SLINE_head := SLINE_head +trim(edit1.Text)+',';            //2017-12-20加trim
        end;
        datetimetostring(sntime,'yyyy-mm-dd HH:nn:ss',Now);

        if (readchis('Comm Para','OSpointPos')<>'OFF') then
        begin
          btn33.Hint:='';PP:=0;
          stempljk:=mmo2String[10];
          pp:=Pos('OPEN',stempljk);
          while pp>0 do
          begin
              btn33.Hint:=btn33.Hint+'['+Copy(stempljk,pp+6,3)+'-' +Copy(stempljk,pp+10,3)+']';
              Delete(stempljk,1,pp+4-1);
              pp:=Pos('OPEN',stempljk);
          end;
          {  注销 by ljk 2023-10-11
          for i:=1 to 5 do
          begin
            if PosEx('OPEN',mmo2String[10],pp)>0 then
            begin
              pp:=PosEX('OPEN',mmo2String[10],PP);
              btn33.Hint:=btn33.Hint+'['+Copy(mmo2String[10],pp+6,3)+'-' +Copy(mmo2String[10],pp+10,3)+']';
              pp:=pp+1;
            end;
          end;
          if PosEx('OPEN',mmo2String[10],pp)>0 then btn33.Hint:=btn33.Hint+'...';
          }

          btn34.Hint:='';PP:=0;
          stempljk:=mmo2String[10];
          pp:=Pos('SHORT',stempljk);
          while pp>0 do
          begin
              btn34.Hint:=btn34.Hint+'['+Copy(stempljk,pp+6,3)+'-' +Copy(stempljk,pp+10,3)+']';
              Delete(stempljk,1,pp+5-1);
              pp:=Pos('SHORT',stempljk);
          end;

         {
          for i:=1 to 5 do
          begin
            if PosEx('SHORT',mmo2String[10],pp)>0 then
            begin
              pp:=PosEX('SHORT',mmo2String[10],PP);
              btn34.Hint:=btn34.Hint+'['+Copy(mmo2String[10],pp+6,3)+'-' +Copy(mmo2String[10],pp+10,3)+']';
              pp:=pp+1;
            end;
          end;
          if PosEx('SHORT',mmo2String[10],pp)>0 then btn34.Hint:=btn34.Hint+'...';
          }
        end;

        if StrToInt(edt81.Text)>0 then begin                            //早期tesla机台：测试log数据处理提前到预处理函数<---早期8700仪器用法，tesla之前！【0B00】上传模式用
          if (edt62.Text='24')or(Form_line.edt62.text='25')  then begin      //第2文件格式数据准备：jaguar第一次扩展 ；tesla改造

            if(edt62.Text='24') then begin                                   //tesla要求时间在前
              if (rbuf[4]=0) then begin                                   
                sline_sn:=  sntime+ ',' +trim(edit1.Text)+ ',' +'PASS' ;
              end else begin
                sline_sn:=  sntime+ ',' +trim(edit1.Text)+ ',' +'FAIL' ;
              end;
              zzoflag:=0;
              for i:=1 to StrToInt(edt81.Text) do begin
                   if Condutor[i]='0.00' then begin
                      zzoflag:=i;
                      break;
                   end;
                   if Condutor[i]='0.000' then begin
                      zzoflag:=i;
                      break;
                   end;
                   if Condutor[i]='0.0000' then begin
                      zzoflag:=i;
                      break;
                   end;
              end;
              cboflag:=0;
           
              if Pos('OPEN',mmo2String[10])>0 then begin
                sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('OPEN',mmo2String[10])+6,3);
                sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('OPEN',mmo2String[10])+10,3);
                sline_sn:=  sline_sn+',OPEN';
              end else  if Pos('SHORT',mmo2String[10])>0 then begin
                sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('SHORT',mmo2String[10])+6,3);
                sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('SHORT',mmo2String[10])+10,3);
                sline_sn:=  sline_sn+',SHORT';
              end else  if zzoflag>0 then begin
                if ( Pos('X COND',mmo2String[10])>0 )and ( Pos('X COND',mmo2String[10])+32<Pos(Condutor[zzoflag],mmo2String[10]) ) then begin      //同一行，X COND且0.000判定为ZZO
                  sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('X COND',mmo2String[10])+7 ,3);
                  sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('X COND',mmo2String[10])+11,3);
                  sline_sn:=  sline_sn+',cbo';
                end else if ( Pos('XCOND',mmo2String[10])>0 )and ( Pos('XCOND',mmo2String[10])+32<Pos(Condutor[zzoflag],mmo2String[10]) ) then begin      //8761仪器
                  sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('XCOND',mmo2String[10])+6 ,3);
                  sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('XCOND',mmo2String[10])+10,3);
                  sline_sn:=  sline_sn+',cbo';
                end else  if ( Pos('>6.0',mmo2String[10])>0 )and( Pos('>6.0',mmo2String[10])+32<Pos(Condutor[zzoflag],mmo2String[10]) ) then begin
                  sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('>6.0',mmo2String[10])-4,3);
                  sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('>6.0',mmo2String[10])-8,3);
                  sline_sn:=  sline_sn+',cbo';
                end else begin
                  sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos(Condutor[zzoflag],mmo2String[10])-4,3);
                  sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos(Condutor[zzoflag],mmo2String[10])-8,3);
                  sline_sn:=  sline_sn+',zzo';
                end;
              end else  if cboflag>0 then begin
                  sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos(Condutor[cboflag],mmo2String[10])-4,3);
                  sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos(Condutor[cboflag],mmo2String[10])-8,3);
                  sline_sn:=  sline_sn+',cbo';
              end else  if Pos('X COND',mmo2String[10])>0 then begin
                sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('X COND',mmo2String[10])+7 ,3);
                sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('X COND',mmo2String[10])+11,3);
                sline_sn:=  sline_sn+',cbo';
              end else  if Pos('XCOND',mmo2String[10])>0 then begin               //8761仪器
                sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('XCOND',mmo2String[10])+6 ,3);
                sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('XCOND',mmo2String[10])+10,3);
                sline_sn:=  sline_sn+',cbo';
              end else  if Pos('>6.0',mmo2String[10])>0 then begin                
                sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('>6.0',mmo2String[10])-4,3);
                sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('>6.0',mmo2String[10])-8,3);
                sline_sn:=  sline_sn+',cbo';
              end else  if (rbuf[4]=0) then begin
                sline_sn:=  sline_sn+ ',0' +',0'+ ',PASS';
              end else  if Pos('INSU',mmo2String[10])>0 then begin
                sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('INSU',mmo2String[10])+6,3);
                sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('INSU',mmo2String[10])+6,3);
                sline_sn:=  sline_sn+',1kvs';
              end else
                sline_sn:=  sline_sn+ ',0' +',0'+ ',0';

              if(rbuf[4]=0) then  begin
                for i:=1 to StrToInt(edt81.Text) do begin
                  sline_sn:= sline_sn+','+Condutor[i] ;
                end;
                if Pos('INSU',mmo2String[10])>0 then begin
                  sline_sn:= sline_sn+',' ;    //error
                end else
                  sline_sn:= sline_sn+',ok' ;  //right
              end;
            end else begin                                                 //早期jaguar格式：mflex通用要求，条码在前；
              if (rbuf[4]=0) then begin                                     //结果OK
                sline_sn:=  trim(edit1.Text)+ ',' +sntime+ ',' +'PASS' + ',' +'FCT'+ ','+inttostr(rbuf[11]);
              end else begin
                sline_sn:=  trim(edit1.Text)+ ',' +sntime+ ',' +'FAIL' + ',' +'FCT'+ ','+inttostr(rbuf[11]);
              end;
              for i:=1 to NameCount do      //遍历所有数据，主要是NTC测量
              begin
                dd:=(rbuf[12+2*i]*256+rbuf[13+2*i]);
                ff:=dd / strtofloat(ScaleZu[i]);
                sline_sn:=  sline_sn+','+ copy(floattostr(ff),0,7);
              end;
              for i:=1 to StrToInt(edt81.Text) do begin
                sline_sn:= sline_sn+','+Condutor[i] ;
              end;
            end;
          end else begin                          //淘汰！早期格式！
            if (rbuf[4]=0) then begin                                     //结果OK
              sline_sn:= 'TTime='+ sntime+ ';' +'BarCode='+trim(edit1.Text)+ ';' +'Result=PASS' + ';' +'ErrorCode=0'      + ';' +'WorkOrder='+trim(edt78.Text)+ ';';
            end else begin
              sline_sn:= 'TTime='+ sntime+ ';' +'BarCode='+trim(edit1.Text)+ ';' +'Result=FAIL' + ';' +'ErrorCode=OPENERR'+ ';' +'WorkOrder='+trim(edt78.Text)+ ';';
            end;
          end;
        end;                                                    //正威tesla特殊要求文件格式
      end;

      //-------------------结果及测量值的处理----------------------------------------------

      if (rbuf[4]=0)              //结果OK
      then begin
        SLINE_head := SLINE_head +'PASS'+',';
      end else begin
        SLINE_head := SLINE_head +'FAIL'+',';
      end;

      if (grp1.Tag=2) then begin
        if (edt62.Text<>'24')and(edt62.text<>'25') then begin    //早期tesla第一文件格式数据； （非jaguar） ，四线电阻值名称
          for i:=1 to StrToInt(edt81.Text) do begin
              SLINE_head:= SLINE_head+Condutor[i]+',' ;
              SLINE_head1:= SLINE_head1+Condutor[i]+#9 ;
          end;
        end;
      end;
      mmo6.Lines[0]:=  Copy(SLINE_head,0,35);     //tesla四线专用  1024);
    end;
  end;

  procedure DataInMemoDisp();
  var
    i,j:Integer;
  begin
    with Form_line do begin
      bb:=1;
      if rbuf[3]=$EE then     //CSV格式联板则先处理MEMO显示数据，仅显示用。
      begin
          zuptr:=1;             //表头指针，当前显示指针
          for i:=bb to (len_ss div 2) do          //遍历所有数据,用i指示，实际显示指针为zuptr
          begin
            if (OnePCSNamePos[zuptr]=(i-1) ) or (edt74.Text<>'1') then    //如果启动单PCS测试（含有'#'的名称，且联板测试，不在memo1显示）
            begin
              dd:=(rbuf[10+4*i]*256+rbuf[11+4*i]);
              if (Copy(ModeZu[i],2,1)='S')or(Copy(ModeZu[i],2,1)='O')or(Copy(ModeZu[i],2,1)='K')  then begin
                if dd<1 then begin
                  SLINE_head1:=SLINE_head1+'right'+#9 ;
                end else begin
                  SLINE_head1:=SLINE_head1+'error'+#9 ;
                end;
              end else if (Copy(ModeZu[i],2,2)='XH')or(Copy(ModeZu[i],2,2)='YH') then begin      //16进制显示
                SLINE_head1:= SLINE_head1+'0x'+inttohex(word(dd),2)+#9 ;
              end else begin
                ff:=dd / strtofloat(ScaleZu[zuptr]);
                SLINE_head1:= SLINE_head1+copy(floattostr(ff),0,7)+#9 ;
              end;
              if (zuptr<UpStepMax) then zuptr:=zuptr+1;
            end;
          end;

          DispErrlist(len_ss);
      end else begin        //【0B00]模式，单PCS数据结果处理并显示；SLINE_head1仅用于显示和保存
        for i:=bb to len_ss do
        begin
          dd:=(rbuf[12+2*i]*256+rbuf[13+2*i]);
          if(edt48.Text='1')and (i<=2) then begin //barcode check=1: 核对使能
              if i=1 then begin
                //SLINE_head:= SLINE_head+edit1.Text+',' ;
                SLINE_head1:= SLINE_head1+edit1.Text+#9 ;
              end;
              if i=2 then begin
                //SLINE_head:= SLINE_head+medit1.Text+',' ;
                SLINE_head1:= SLINE_head1+medit1.Text+#9 ;
              end;
          end else if ( NameStrZu[i]='OPEN')or(NameStrZu[i]='SHOR') then begin
            if dd=0 then begin
              //SLINE_head:= SLINE_head+'right'+',' ;
              SLINE_head1:= SLINE_head1+'right'+#9 ;
            end else begin
              //SLINE_head:= SLINE_head+'error'+',' ;
              SLINE_head1:= SLINE_head1+'error'+#9 ;
            end;
          end else begin
            ff:=dd / strtofloat(ScaleZu[i]);  //'100');//
            SLINE_head1:= SLINE_head1+copy(floattostr(ff),0,7)+#9 ;
          end;
        end;
      end;
    end;
  end;
  procedure saveOnePCSresult();          //逐步转移到多PCB保存功能里！；<--yijia,早期tesla,早期weixin,早期通用
  var
    i,j:Integer;
    Teslastr:string;
  begin
    with Form_line do begin
        try
          assignfile(f, edt2.Text + save_file_name);      //ExtractFilePath(application.ExeName)
          Append(f);                 //application.ProcessMessages;
          begin                        //=2(tesla),=11(通用）.....
            if(edt62.Text='24') then begin
                for i:=1 to StrToInt(edt81.Text) do begin
                    SLINE_head:= SLINE_head+Condutor[i]+',' ;
                    SLINE_head1:= SLINE_head1+Condutor[i]+#9 ;
                end;  //ts9.Show;  只显示list
                if rbuf[4]<>0 then begin          //绝缘项目 Pos('INSU',mmo2String[10])>0
                  SLINE_head:= SLINE_head +',' ;      //error
                  SLINE_head1:=SLINE_head1+''+#9 ;    //error
                end else begin
                  SLINE_head:= SLINE_head +'ok,' ;      //right
                  SLINE_head1:=SLINE_head1+'ok'+#9 ;    //right
                end;
                Teslastr:=SLINE_head;
                if len_ss<2 then len_ss:=0;             //少于2个上传项目，不显示
            end;
            for i:=bb to len_ss do
            begin
              dd:=(rbuf[12+2*i]*256+rbuf[13+2*i]);
              if(edt48.Text='1')and (i<=2) then begin //barcode check=1: jiahe核对使能
                  if i=1 then begin
                    SLINE_head:= SLINE_head+edit1.Text+',' ;
                    //SLINE_head1:= SLINE_head1+edit1.Text+#9 ;
                  end;
                  if i=2 then begin
                    SLINE_head:= SLINE_head+medit1.Text+',' ;
                    //SLINE_head1:= SLINE_head1+medit1.Text+#9 ;
                  end;
              end else if ( NameStrZu[i]='OPEN')or(NameStrZu[i]='SHOR') then begin
                if dd=0 then begin
                  SLINE_head:= SLINE_head+'right'+',' ;
                  //SLINE_head1:= SLINE_head1+'right'+#9 ;
                end else begin
                  SLINE_head:= SLINE_head+'error'+',' ;
                  //SLINE_head1:= SLINE_head1+'error'+#9 ;
                end;
              end else begin
                 ff:=dd / strtofloat(ScaleZu[i]);  //'100');//
                 if (grp1.Tag=2)       //jaguar用
                    and( (NameStrZu[i]='T1')or(NameStrZu[i]='T2')or(NameStrZu[i]='T3')or(NameStrZu[i]='T4')or(NameStrZu[i]='T5')  //写入范围区间，已淘汰！
                     or(NameStrZu[i]='T6')or(NameStrZu[i]='T7')or(NameStrZu[i]='T8')or(NameStrZu[i]='T9')or(NameStrZu[i]='T10')  )
                   then begin
                     dd:=(dd+200) div StrToInt(ScaleZu[i]);         //补偿2.00摄氏度，用作四舍五入；
                     dd:=dd div 5 ;
                     if(dd<2) then dd:=2;
                     if(dd>8) then  dd:=8;

                    SLINE_head:= SLINE_head+copy(floattostr(ff),0,7)+readchis('Name->Redef', IntToStr(dd)+'^Crange', '') +',';    //'[125.5--155.6],'
                 end else
                    SLINE_head:= SLINE_head+copy(floattostr(ff),0,7)+',' ;
              end;

            end;
            if (grp1.Tag=2) then  begin              //=2:tesla等机种写入内容有区别
              if (edt62.Text='24') then begin
                if not DirectoryExists(edt2.Text+lbl183.Caption ) then          //汇总数据文件先备份
                try
                  begin
                    CreateDir(edt2.Text+lbl183.Caption );  //创建目录
                  end;
                except  //finally
                    stat1.Panels[4].Text:='Cannot Create '+edt2.Text+lbl183.Caption;
                    Exit;
                end;
                if not DirectoryExists(edt2.Text+lbl183.Caption+lbl90.hint+'\' ) then
                try
                  begin
                    CreateDir(edt2.Text+lbl183.Caption+lbl90.hint+'\' );  //创建目录
                  end;
                except  //finally
                    stat1.Panels[4].Text:='Cannot Create '+edt2.Text+lbl183.Caption+lbl90.hint+'\' ;
                    Exit;
                end;
                if not FileExists(edt2.Text+lbl183.Caption + save_file_name) then
                  CopyFile(PChar(edt2.Text + save_file_name), PChar(edt2.Text+lbl183.Caption + save_file_name), True);

                 writeln(f, Teslastr);              //tesla改造不用SLINE_head；提供给客户看，备注信息不添加
              end else if(edt62.text='25')  then begin               //测试实时生成文件<----//实际不存在='25'（四连片2BU0） 情况
                for i:=1 to StrToInt(edt81.Text) do begin
                    SLINE_head:= SLINE_head+Condutor[i]+',' ;
                    SLINE_head1:= SLINE_head1+Condutor[i]+#9 ;
                end;  //ts9.Show;  只显示list
                writeln(f, SLINE_head+mmo2String[10]+',Version:'+label9.caption);       //jaguar 增加 四线制设备 备注信息添加
              end else
                writeln(f, SLINE_head+mmo2String[10]+',Version:'+label9.caption);       //淘汰<-- 早期tesla(='22') 增加 四线制设备 备注信息添加
            end else  begin
              if ts13.TabVisible then
                 SLINE_head:=SLINE_head+#9+Form_line.mmo16.lines[0] ;
              writeln(f, SLINE_head);     //测量值+SN序列号
            end;
          end;
          if StrToInt(edt38.text)mod 10 >=2 then       //uplocal =4： 写入多行结果  分别为NGLIST,OPEN NG,SHORT NG
          begin
            writeln(f, ','+','+mmo2String[6]);        //NGLIST
            if StrToInt(edt38.text)mod 10 >=3 then writeln(f, ','+','+mmo2String[7]);  //OPEN NG
            if StrToInt(edt38.text)mod 10 >=4 then writeln(f, ','+','+mmo2String[8]);  //SHORT ng
          end;
        finally
          Closefile(f);
        end;
        if (grp1.Tag=2)and (edt62.Text='24') then begin                     //=2:tesla改造后写入内容有区别
          try
            assignfile(f, edt2.Text +lbl183.Caption + save_file_name);          //备份路径 ExtractFilePath(application.ExeName)
            Append(f); //                //application.ProcessMessages;
            writeln(f, SLINE_head+mmo2String[10]+',Version:'+label9.caption)    //必须增加8700原始数据，否则无法求证 if(len_ss>2) then
          finally
            Closefile(f);
          end;
        end;
    end;
  end;
  procedure saveNPCSresult();
  var
    i,j:Integer;
    str1:string;
    ResultList:TStringList;
 
    procedure teslaLog;
    var i:SmallInt;
    begin
      with Form_line do begin                            //【2BU0】上传模式用
        if StrToInt('0'+Label7.hint)=2 then  begin              //clearbarcode=2，条码用编号代替
          sline_sn:=  sntime+ ','  + Trim(Redits[j].Value);     //StringReplace(Trim(Redits[j].Value), ':', ' ', [rfReplaceAll])  //新ET设备用！
        end else
          sline_sn:=  sntime+ ',' +trim(edit1.Text);

        if (rbuf[4]=0) then begin                                //不使用pre函数的字符串
          sline_sn:= sline_sn+ ',' +'PASS' ;
        end else begin
          sline_sn:= sline_sn+ ',' +'FAIL' ;
        end;
        zzoflag:=0;
        SLINE_head:= sline_sn+ ',' ;

        cboflag:=0;
        if Pos('OPEN',mmo2String[10])>0 then begin                    //存放NG项目和2个点位；
          sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('OPEN',mmo2String[10])+6,3);
          sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('OPEN',mmo2String[10])+10,3);
          sline_sn:=  sline_sn+',OPEN';
        //  Label11.Caption:='open';
        //  Label11.Font.Color:=clRed;
        end else  if Pos('SHORT',mmo2String[10])>0 then begin
          sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('SHORT',mmo2String[10])+6,3);
          sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('SHORT',mmo2String[10])+10,3);
          sline_sn:=  sline_sn+',SHORT';
        //  Label11.Caption:='short';
        //  Label11.Font.Color:=clYellow;
        end else  if zzoflag>0 then begin
        //  Label11.Caption:='cbo';
        //  Label11.Font.Color:=clPurple;
          if ( Pos('X COND',mmo2String[10])>0 )and ( Pos('X COND',mmo2String[10])+32<Pos(Condutor[zzoflag],mmo2String[10]) ) then begin      //同一行，X COND且0.000判定为ZZO
            sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('X COND',mmo2String[10])+7 ,3);
            sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('X COND',mmo2String[10])+11,3);
            sline_sn:=  sline_sn+',cbo';
          end else if ( Pos('XCOND',mmo2String[10])>0 )and ( Pos('XCOND',mmo2String[10])+32<Pos(Condutor[zzoflag],mmo2String[10]) ) then begin      //8761仪器
            sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('XCOND',mmo2String[10])+6 ,3);
            sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('XCOND',mmo2String[10])+10,3);
            sline_sn:=  sline_sn+',cbo';
          end else  if ( Pos('>6.0',mmo2String[10])>0 )and( Pos('>6.0',mmo2String[10])+32<Pos(Condutor[zzoflag],mmo2String[10]) ) then begin
            sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('>6.0',mmo2String[10])-4,3);
            sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('>6.0',mmo2String[10])-8,3);
            sline_sn:=  sline_sn+',cbo';
          end else begin
            sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos(Condutor[zzoflag],mmo2String[10])-4,3);
            sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos(Condutor[zzoflag],mmo2String[10])-8,3);
            sline_sn:=  sline_sn+',zzo';
          //  Label11.Caption:='zzo';
          //  Label11.Font.Color:=clBlue;
          end;
        end else  if cboflag>0 then begin
            sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos(Condutor[cboflag],mmo2String[10])-4,3);
            sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos(Condutor[cboflag],mmo2String[10])-8,3);
            sline_sn:=  sline_sn+',cbo';
        end else  if Pos('X COND',mmo2String[10])>0 then begin
          sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('X COND',mmo2String[10])+7 ,3);
          sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('X COND',mmo2String[10])+11,3);
          sline_sn:=  sline_sn+',cbo';
        //  Label11.Caption:='cbo';
        //  Label11.Font.Color:=clPurple;
        end else  if Pos('XCOND',mmo2String[10])>0 then begin               //8761仪器
          sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('XCOND',mmo2String[10])+6 ,3);
          sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('XCOND',mmo2String[10])+10,3);
          sline_sn:=  sline_sn+',cbo';
        //  Label11.Caption:='cbo';
        //  Label11.Font.Color:=clPurple;
        end else  if Pos('>6.0',mmo2String[10])>0 then begin
          sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('>6.0',mmo2String[10])-4,3);
          sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('>6.0',mmo2String[10])-8,3);
          sline_sn:=  sline_sn+',cbo';
        //  Label11.Caption:='cbo';
        //  Label11.Font.Color:=clPurple;
        end else  if (rbuf[4]=0) then begin
          sline_sn:=  sline_sn+ ',0' +',0'+ ',PASS';
        end else  if Pos('INSU',mmo2String[10])>0 then begin
          sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('INSU',mmo2String[10])+6,3);
          sline_sn:=  sline_sn+',' +Copy(mmo2String[10],Pos('INSU',mmo2String[10])+6,3);
          sline_sn:=  sline_sn+',1kvs';
        //  Label11.Caption:='INSUL';
        //  Label11.Font.Color:=clOlive;
        end else  if Pos('HIPOT',mmo2String[10])>0 then begin
        //  Label11.Caption:='HIPOT';
        //  Label11.Font.Color:=clFuchsia;
      {由主板判断  end else  if Pos('NTC',mmo2String[10])>0 then begin
          Label11.Caption:='NTC';
          Label11.Font.Color:=clPurple;    }
        end else if(rbuf[4]<>0) then begin
          for i:=1 to (len_ss div 2) do                       //遍历所有数据，寻找NGLIST
          begin
            if (rbuf[12+4*i]=j+1)or(StrToInt(edt59.Text)*strtoint(edt60.Text)=1) then begin   //PCS号相等或者单PCS;
               if (rbuf[13+4*i] and $01)=$01 then begin         //NG
                  sline_sn:=  sline_sn+',0'+ ',0'+','+NameStrZu[zuptr];
                  Break;
               end;
               if (zuptr<UpStepMax) then zuptr:=zuptr+1;
            end;
          end;
        end else
          sline_sn:=  sline_sn+ ',0' +',0'+ ',0';

        zuptr:=1;
        for i:=1 to (len_ss div 2) do      //遍历所有数据，放在一行存放
        begin
          if (rbuf[12+4*i]=j+1)or(StrToInt(edt59.Text)*strtoint(edt60.Text)=1) then        //当前连片或者单PCS,数据指示
          begin
            if (zuptr=1) and (NameStrZu[zuptr]='*ET') then begin             //*ET数据项目
               if j=1 then begin
                  str1:= chk26.caption;
                  str1:= StringReplace(str1, char(#9), '-', [rfReplaceAll]);
                  str1:= StringReplace(str1, ',', '-', [rfReplaceAll]);
               end else  if j=2 then begin
                  str1:= chk27.caption;
                  str1:= StringReplace(str1, char(#9), '-', [rfReplaceAll]);
                  str1:= StringReplace(str1, ',', '-', [rfReplaceAll]);
               end else if j=3 then begin
                  str1:= chk28.caption;
                  str1:= StringReplace(str1, char(#9), '-', [rfReplaceAll]);
                  str1:= StringReplace(str1, ',', '-', [rfReplaceAll]);
               end else begin   //       if j=0 then begin   //从0开始；
                  str1:= chk25.caption;
                  str1:= StringReplace(str1, char(#9), '-', [rfReplaceAll]);
                  str1:= StringReplace(str1, ',', '-', [rfReplaceAll]);
               end;

              SLINE_head:= SLINE_head+str1+',' ;
              if(rbuf[4]=0) then
                sline_sn:= sline_sn+','+str1 ;

              if (zuptr<UpStepMax) then zuptr:=zuptr+1;
              Continue;
            end;
            dd:=(rbuf[10+4*i]*256+rbuf[11+4*i]);              //rbuf[14]为第一个  {SLINE_head:= SLINE_head+inttostr(dd)+','; }
            ff:=dd / strtofloat(ScaleZu[zuptr]);              //'100');//
            if (NameStrZu[i]='*EEP') then begin               //EEPROM测试数据
              SLINE_head:= SLINE_head+mmo2String[17]+',' ;
            end else if (NameStrZu[i]='OPEN') then begin               //OPEN显示点位
              if(readchis('Comm Para','OSpointPos')<>'OFF')and(btn33.Hint<>'') then
                SLINE_head:= SLINE_head+btn33.Hint+','
              else
                SLINE_head:= SLINE_head+copy(floattostr(ff),0,7)+',' ;
            end else if (NameStrZu[i]='SHOR')or(NameStrZu[i]='SHORT') then begin               //显示点位
              if (readchis('Comm Para','OSpointPos')<>'OFF')and(btn34.Hint<>'') then
                SLINE_head:= SLINE_head+btn34.Hint+','
              else
                SLINE_head:= SLINE_head+copy(floattostr(ff),0,7)+',' ;
            end else
              SLINE_head:= SLINE_head+copy(floattostr(ff),0,7)+',' ;  //1.整单文件用字符串

            if(rbuf[4]=0) then
              sline_sn:= sline_sn+','+copy(floattostr(ff),0,7) ;      //2.条码文件用字符串

            if (zuptr<UpStepMax) then zuptr:=zuptr+1;
          end;
        end;
        if edt163.Visible then  begin
          SLINE_head := SLINE_head +'治具ID:'+edt163.Text+',';
        //数据对不起
          sline_sn:= sline_sn+','+'治具ID:'+edt163.Text;
        end;
        
        if not DirectoryExists(edt2.Text+lbl183.Caption ) then          //汇总数据文件先备份
        try
          begin
            CreateDir(edt2.Text+lbl183.Caption );  //创建目录
          end;
        except  //finally
            stat1.Panels[4].Text:='Cannot Create '+edt2.Text+lbl183.Caption;
            Exit;
        end;
        if not DirectoryExists(edt2.Text+lbl183.Caption+lbl90.hint+'\' ) then
        try
          begin
            CreateDir(edt2.Text+lbl183.Caption+lbl90.hint+'\' );  //创建目录
          end;
        except  //finally
            stat1.Panels[4].Text:='Cannot Create '+edt2.Text+lbl183.Caption+lbl90.hint+'\' ;
            Exit;
        end;
        if not FileExists(edt2.Text+lbl183.Caption + save_file_name) then
          CopyFile(PChar(edt2.Text + save_file_name), PChar(edt2.Text+lbl183.Caption + save_file_name), True);

        try
          assignfile(f, edt2.Text + save_file_name);      //ExtractFilePath(application.ExeName)
          Append(f); //
          writeln(f, SLINE_head);                    //tesla改造不用SLINE_head；提供给客户看，备注信息不添加
        finally
          Closefile(f);
        end;

        try
          assignfile(f, edt2.Text +lbl183.Caption + save_file_name);          //备份路径 ExtractFilePath(application.ExeName)
          Append(f); //                //application.ProcessMessages;
          writeln(f, SLINE_head+mmo2String[10]+',Version:'+label9.caption)    //必须增加8700原始数据，否则无法求证 if(len_ss>2) then
        finally
          Closefile(f);
        end;
//------------------tesla条码文件生成及表头----------------------------
        if (lbl2.Caption<>'filename')and( StrToInt(edt59.Text)*strtoint(edt60.Text)<=1)  then
        begin
          try
            begin
              datetimetostring(stime,'yyyymmddhhnnss',Now);
              if StrToInt(edt81.Text)>0 then
                lbl180.caption:=edt77.Text+'\Tesla_'+DelSpecialChar(Trim(edit1.Text)) //StringReplace(Trim(edit1.Text), ':', '', [rfReplaceAll])
                      +'_'+stime +'_'+IdIPWatch1.LocalIP    //冒号删除掉
              else begin
                if StrToInt('0'+Label7.hint)=2 then  begin              //clearbarcode=2，条码用编号代替   //新ET设备用！
                    lbl180.caption:=edt77.Text+'\'+Readchis('Comm Para','ProjectName')+DelSpecialChar(Trim(Redits[0].Value)) //StringReplace(Trim(Redits[0].Value), ':', '', [rfReplaceAll]) 、
                      +'_'+stime +'_'+IdIPWatch1.LocalIP;
                end else
                    lbl180.caption:=edt77.Text+'\'+Readchis('Comm Para','ProjectName')+DelSpecialChar(Trim(edit1.Text)) //StringReplace(Trim(edit1.Text), ':', '', [rfReplaceAll])
                      +'_'+stime +'_'+IdIPWatch1.LocalIP;
              end;
              if (rbuf[4]=0)then              //结果OK

                lbl180.caption:=lbl180.caption+'_ICT_PASS'+ '.csv'
              else
                lbl180.caption:=lbl180.caption+'_ICT_FAIL'+ '.csv';
              assignfile(f, edt157.text+lbl180.caption);      //增加条码命名的Log文件；
            end;
            rewrite(f);                           //application.ProcessMessages;

            begin                                      //tesla改造后格式
               writeln(f, 'Customer PN'+','+ edt77.Text);
               writeln(f, 'Supplier Lot Code'+','+ edt78.Text);
               writeln(f, lbl92.Caption+','+ edt79.Text);
               writeln(f, lbl93.Caption+','+ edt80.Text);
               writeln(f, 'TestType'+',ICT');

              SLINE_head:='Time'+','+'Serial Barcode'+','+'Result'+',Location1'+',Location2'+',DefectCode,';    //tesla专用 ，时间在前
              for i:=1 to UpStepMax do
              begin
                if NameStrZu[i]<>'' then SLINE_head := SLINE_head +NameStrZu[i]+',' ;     //名称
              end;
              writeln(f, SLINE_head);           //网络名称
            end;

            writeln(f, sline_sn);                         //专用格式
          finally
            Closefile(f);
          end;
          
          lbl181.Caption:='24';               //上传映射盘标志；
          btn2Click(Form_line);

        end;
      end;
    end;
    procedure SMTlog;         //  （edt62.text='25')
    var i:SmallInt;
        slljk:TStringList;
        stemp:string;
        k:Integer;
    begin
      //1.SLINE_head为早期主文件存放数据格式，从起始（另起）到保存结束；
        //2.如有第2文件保存，则使用SLINE_head2早期文件格式；贯穿所有格式；barcodeFile=0则不使用SLINE_head2；
        //3.如要显示，则使用SLINE_head1
      with Form_line do begin
        SLINE_head :='';
        datetimetostring(stime,'yyyy-mm-dd HH:nn:ss',Now);
        btn115Click(Form_line);                                  //guid

        if (Readchis('Comm Para','BarcodeCSVposCol')='2') then      //---时间在前，仅teslat文件格式专用！
          SLINE_head := SLINE_head + stime+',' ;

        //-------------用条码还是用序号 ------------------------------

        if StrToInt('0'+Label7.hint)=2 then  begin              //clearbarcode=2，条码用编号代替
          SLINE_head := SLINE_head +Trim(Redits[j].Value)+','; //新ET设备用！
        end else begin
          if (Label7.hint='11')and (j=1) then      //双条码，第二个条码选择
            SLINE_head :=SLINE_head + trim(medit1.Text)+','
          else
            SLINE_head :=SLINE_head + trim(edit1.Text)+',';
        end;

        if (Readchis('Comm Para','BarcodeCSVposCol')<>'2') then           //时间在条码之后，通用格式！
          SLINE_head := SLINE_head + stime+',' ;

        if Rleds[j].ActiveColor=clgreen then
        begin
          SLINE_head := SLINE_head +'PASS'+',';
        end else begin
          SLINE_head := SLINE_head +'FAIL'+',';
        end;

        begin   //导通电阻由测试主板解析；

          //----如下语句适用=23早期jaguar和赋值给SLINE_head2-----=25有重新定义SLINE_head----------------------------
          SLINE_head := SLINE_head + Readchis('Model Para','TestType')+',';
          SLINE_head := SLINE_head + edt77.Text+',';
          SLINE_head := SLINE_head +Readchis('Model Para','Line')+',';
          SLINE_head := SLINE_head +IdIPWatch1.LocalIP+',';
          SLINE_head := SLINE_head + edt80.Text+',';          //机台，治具
          SLINE_head := SLINE_head + edt79.Text+',';          //作业员
          SLINE_head := SLINE_head + 'N'+',';                  //OnLine，在线

          SLINE_head2 := SLINE_head ;                         //2.SLINE_head2为第2分支（本地条码文件用！），格式要同SQL数据格式；

          begin        //SMT单文件保存，必须另外起始;第2分支SLINE_head2保留
            if not DirectoryExists(edt2.Text+lbl183.Caption ) then                      //汇总数据文件先备份
            try
              begin
                CreateDir(edt2.Text+lbl183.Caption );  //创建目录
              end;
            except  //finally
                stat1.Panels[4].Text:='Cannot Create '+edt2.Text+lbl183.Caption;
                Exit;
            end;
            if not DirectoryExists(edt2.Text+lbl183.Caption+lbl90.hint+'\' ) then
            try
              begin
                CreateDir(edt2.Text+lbl183.Caption+lbl90.hint+'\' );  //创建备份目录
              end;
            except  //finally
                stat1.Panels[4].Text:='Cannot Create '+edt2.Text+lbl183.Caption+lbl90.hint+'\' ;
                Exit;
            end;
            if not FileExists(edt2.Text+lbl183.Caption + save_file_name) then        //拷贝表头文件
              CopyFile(PChar(edt2.Text + save_file_name), PChar(edt2.Text+lbl183.Caption + save_file_name), True);

            if not DirectoryExists(edt157.Text+lbl90.hint ) then      //单PCS文件夹生成
            try
              begin
                CreateDir(edt157.Text+lbl90.hint );  //创建目录
              end;
            except  //finally
                stat1.Panels[4].Text:='Cannot Create '+edt157.Text+lbl90.hint;
                Exit;
            end;
            if(edt6.hint<>'UpSinglePCS') then begin                   //生成临时文件夹
              if not DirectoryExists(edt2.Text+ 'temp\' ) then            //汇总数据文件临时目录
              try
                begin
                  CreateDir(edt2.Text+ 'temp\' );  //创建目录
                end;
              except  //finally
                  stat1.Panels[4].Text:='Cannot Create '+edt2.Text+ 'temp\';
                  Exit;
              end;
              assignfile(f, edt2.Text+ 'temp\'+ save_file_name);
              if not DirectoryExists(edt2.Text+'temp\'+lbl90.hint+'\' ) then
              try
                begin
                  CreateDir(edt2.Text+'temp\'+lbl90.hint+'\' );  //创建备份目录
                end;
              except  //finally
                  stat1.Panels[4].Text:='Cannot Create '+edt2.Text+'temp\'+lbl90.hint+'\' ;
                  Exit;
              end;
              if not FileExists(edt2.Text+'temp\' + save_file_name) then        //拷贝表头文件
                CopyFile(PChar(edt2.Text + save_file_name), PChar(edt2.Text+'temp\' + save_file_name), True);

              if not DirectoryExists(edt157.Text+ 'temp\' ) then            //汇总数据文件临时目录
              try
                begin
                  CreateDir(edt157.Text+ 'temp\' );  //创建目录
                end;
              except
                  stat1.Panels[4].Text:='Cannot Create '+edt157.Text+ 'temp\';
                  Exit;
              end;
              assignfile(f, edt157.Text+ 'temp\'+ save_file_name);
              if not DirectoryExists(edt157.Text+'temp\'+lbl90.hint+'\' ) then
              try
                begin
                  CreateDir(edt157.Text+'temp\'+lbl90.hint+'\' );  //创建备份目录
                end;
              except  //finally
                  stat1.Panels[4].Text:='Cannot Create '+edt157.Text+'temp\'+lbl90.hint+'\' ;
                  Exit;
              end;
            end;
            SLINE_head := edt77.Text+',';                         //
            SLINE_head := SLINE_head + edt78.Text+',';            //LotNo

            if StrToInt('0'+Label7.hint)=2 then  begin              //clearbarcode=2:继承从4PCs机台存放条码的方式！
              SLINE_head := SLINE_head +Trim(Redits[j].Value) +',';//StringReplace(Trim(Redits[j].Value), ':', ' ', [rfReplaceAll])  ;
            end else begin
              if (Label7.hint='11')and (j=1) then      //双条码，第二个条码选择
                SLINE_head :=SLINE_head + trim(medit1.Text)+','
              else
                SLINE_head :=SLINE_head + trim(edit1.Text)+',';
            end;

            if Rleds[j].ActiveColor=clgreen then
            begin
              SLINE_head := SLINE_head +'PASS'+',';
            end else begin
              SLINE_head := SLINE_head +'FAIL'+',';
            end;
            SLINE_head := SLINE_head + isvnsgmntclck1.Hint+',' ;      //start time
            SLINE_head := SLINE_head + stime+',' ;                    //end time
            if edt163.Visible then
              SLINE_head := SLINE_head +edt163.Text+','               //治具载板上二维码；
            else
              SLINE_head := SLINE_head + edt80.Text+',';              //机台编号    ，客串机台名称；
            SLINE_head := SLINE_head + edt79.Text+',';                //作业员

            zuptr:=1;
            Rleds[j].Hint:='';
            for i:=1 to (len_ss div 2) do                       //遍历所有数据，寻找NGLIST
            begin
              if (rbuf[12+4*i]=j+1)or(StrToInt(edt59.Text)*strtoint(edt60.Text)=1) then begin   //PCS号相等或者单PCS;
                 if (rbuf[13+4*i] and $01)=$01 then begin         //NG
                    SLINE_head := SLINE_head +NameStrZu[zuptr]+'/';
                    Rleds[j].Hint:=Rleds[j].Hint+NameStrZu[zuptr]+'/';
                 end;
                 if (zuptr<UpStepMax) then zuptr:=zuptr+1;
              end;
            end;
            if(StrToInt(edt59.Text)*strtoint(edt60.Text)=1)and(StrToInt(edt81.Text)>0) then begin //早期jaguar单PCS //会出现异常数据 <-- and(Pos('MACHINE:',mmo2String[10])<1)
              if Pos('OPEN',mmo2String[10])>0 then   begin
                SLINE_head := SLINE_head +'OPEN/';
                Rleds[j].Hint:=Rleds[j].Hint+'OPEN/';
              end;
              if Pos('SHORT',mmo2String[10])>0 then    begin
                SLINE_head := SLINE_head +'SHORT/';
                Rleds[j].Hint:=Rleds[j].Hint+'SHORT/';
              end;
              if Pos('X COND',mmo2String[10])>0 then     begin
                SLINE_head := SLINE_head +'COND/';
                Rleds[j].Hint:=Rleds[j].Hint+'COND/';
              end;
              if Pos('XCOND',mmo2String[10])>0 then     begin        //8761仪器
                SLINE_head := SLINE_head +'COND/';
                Rleds[j].Hint:=Rleds[j].Hint+'COND/';
              end;
              if Pos('>6.0',mmo2String[10])>0 then      begin
                SLINE_head := SLINE_head +'COND/';
                Rleds[j].Hint:=Rleds[j].Hint+'COND/';
              end;
              if Pos('X INSU',mmo2String[10])>0 then    begin
                SLINE_head := SLINE_head +'INSU/';
                Rleds[j].Hint:=Rleds[j].Hint+'INSU/';
              end;
            end;
            if Rleds[j].Hint='' then
              Rleds[j].Hint:='null'
            else if(StrToInt(edt59.Text)*strtoint(edt60.Text)=1) then begin
              if j=0 then lbl73.Caption:= lbl73.Caption  +':'+ Rleds[j].Hint;
              if j=1 then lbl185.Caption:=lbl185.Caption +':'+ Rleds[j].Hint;
              if j=2 then lbl186.Caption:=lbl186.Caption +':'+ Rleds[j].Hint;
              if j=3 then lbl187.Caption:=lbl187.Caption +':'+ Rleds[j].Hint;
            end;
            SLINE_head := SLINE_head +',';                     //NGLIST后面的分号；
          end;

        end;


        zuptr:=1;
        for i:=1 to (len_ss div 2) do      //遍历所有数据，放在一行存放
        begin
          if (rbuf[12+4*i]=j+1)or(StrToInt(edt59.Text)*strtoint(edt60.Text)=1) then        //当前连片或者单PCS,数据指示
          begin
            if (zuptr=1) and (NameStrZu[zuptr]='*ET') then begin             //*ET数据项目
               if j=1 then begin
                  str1:= chk26.caption;
                  str1:= StringReplace(str1, char(#9), '-', [rfReplaceAll]);
                  str1:= StringReplace(str1, ',', '-', [rfReplaceAll]);
               end else  if j=2 then begin
                  str1:= chk27.caption;
                  str1:= StringReplace(str1, char(#9), '-', [rfReplaceAll]);
                  str1:= StringReplace(str1, ',', '-', [rfReplaceAll]);
               end else if j=3 then begin
                  str1:= chk28.caption;
                  str1:= StringReplace(str1, char(#9), '-', [rfReplaceAll]);
                  str1:= StringReplace(str1, ',', '-', [rfReplaceAll]);
               end else begin   //       if j=0 then begin   //从0开始；
                  str1:= chk25.caption;
                  str1:= StringReplace(str1, char(#9), '-', [rfReplaceAll]);
                  str1:= StringReplace(str1, ',', '-', [rfReplaceAll]);

               //   dd:=(rbuf[10+4*i]*256+rbuf[11+4*i]);
               //   ff:=dd / strtofloat(ScaleZu[zuptr]);
               //   str1:=copy(floattostr(ff),0,7);
               end;
               SLINE_head:= SLINE_head+str1+',' ;
               if (zuptr<UpStepMax) then zuptr:=zuptr+1;
               Continue;
            end;
            dd:=(rbuf[10+4*i]*256+rbuf[11+4*i]);              //rbuf[14]为第一个  {SLINE_head:= SLINE_head+inttostr(dd)+','; }
            ff:=dd / strtofloat(ScaleZu[zuptr]);              //'100');//
            if (NameStrZu[i]='*EEP') then begin               //EEPROM测试数据
              SLINE_head:= SLINE_head+mmo2String[17]+',' ;
            end else if (NameStrZu[i]='OPEN') then begin               //OPEN显示点位
              if(readchis('Comm Para','OSpointPos')<>'OFF')and(btn33.Hint<>'') then
                SLINE_head:= SLINE_head+btn33.Hint+','
              else
                SLINE_head:= SLINE_head+copy(floattostr(ff),0,7)+',' ;
            end else if (NameStrZu[i]='SHOR')or(NameStrZu[i]='SHORT') then begin               //显示点位
              if (readchis('Comm Para','OSpointPos')<>'OFF')and(btn34.Hint<>'') then
                SLINE_head:= SLINE_head+btn34.Hint+','
              else
                SLINE_head:= SLINE_head+copy(floattostr(ff),0,7)+',' ;
            end else if(StrToInt(edt59.Text)*strtoint(edt60.Text)=1)
              and(  (NameStrZu[i]='T1')or(NameStrZu[i]='T2')or(NameStrZu[i]='T3')or(NameStrZu[i]='T4')or(NameStrZu[i]='T5')   //写入范围区间，已淘汰！
                  or(NameStrZu[i]='T6')or(NameStrZu[i]='T7')or(NameStrZu[i]='T8')or(NameStrZu[i]='T9')or(NameStrZu[i]='T10')  )
            then begin
               dd:=(dd+200) div StrToInt(ScaleZu[i]);         //补偿2.00摄氏度，用作四舍五入；
               dd:=dd div 5 ;
               if(dd<2) then dd:=2;
               if(dd>8) then  dd:=8;

              SLINE_head:= SLINE_head+copy(floattostr(ff),0,7)+readchis('Name->Redef', IntToStr(dd)+'^Crange', '') +',';    //'[125.5--155.6],'
            end else
              SLINE_head:= SLINE_head+copy(floattostr(ff),0,7)+',' ;
            if (zuptr<UpStepMax) then zuptr:=zuptr+1;
          end;
        end;
        if(StrToInt(edt59.Text)*strtoint(edt60.Text)=1)
          and(mmo2String[10]<>'')and(StrToInt(edt81.Text)>0)  then begin           //淘汰！早期jaguar 单PCS用的8700; //会出现异常数据<-- and(Pos('MACHINE:',mmo2String[10])<1)

          for i:=1 to StrToInt(edt81.Text) do begin
            SLINE_head:= SLINE_head+Condutor[i]+',' ;
          end;

          if Pos('OPEN',mmo2String[10])>0 then   begin
            SLINE_head := SLINE_head +'fail,';
          end else
            SLINE_head := SLINE_head +'pass,';
          if Pos('SHORT',mmo2String[10])>0 then    begin
            SLINE_head := SLINE_head +'fail,';
          end else
            SLINE_head := SLINE_head +'pass,';
          if Pos('X INSU',mmo2String[10])>0 then    begin               //1KV绝缘项目
            SLINE_head := SLINE_head +'fail,';
          end else
            SLINE_head := SLINE_head +'pass,';
        end;

        try
          assignfile(f, edt2.Text + save_file_name);      //集总文件
          Append(f); //rewrite(f);                        
          if ts13.TabVisible then                         //专用：多组SN条码，例如：SIM卡条码
             SLINE_head:=SLINE_head+#9+Form_line.mmo16.lines[j+1];
          writeln(f, SLINE_head)
        finally
          Closefile(f);
        end;
        begin             //jaguar写入备份文件，含有8761数据 ;表头有版本号；
          try
            assignfile(f, edt2.Text +lbl183.Caption + save_file_name);
            Append(f); //
            //application.ProcessMessages;
            writeln(f, SLINE_head+mmo2String[10])                       //必须增加8761原始数据，否则无法求证 if(len_ss>2) then
          finally
            Closefile(f);
          end;
          if(edt6.hint<>'UpSinglePCS')and(lbl2.hint<>Trim(Redits[j].Value))   //生成临时文件夹  //非指定的ERROR条码，才保存记录；
          then begin   
            if (btn130.Caption='MES联机')and (
                    ((j=0)and(chk25.Color=Clred) )or( (j=1)and(chk26.Color=Clred) )
                  or((j=2)and(chk27.Color=Clred) )or( (j=3)and(chk28.Color=Clred) ) )then     //联机且MES错误不上传
            begin

            end else begin                                              //正常则保存临时文件夹
              try
                assignfile(f, edt2.Text +'temp\' + save_file_name);
                Append(f); //
                //application.ProcessMessages;
                writeln(f, SLINE_head)                       //临时文件
              finally
                Closefile(f);
              end;
            end;
          end;
        end;

        if(lbl2.Caption<>'filename') then begin
          try
            zuptr:=1;
            for i:=1 to (len_ss div 2) do      //遍历所有数据，放在一行存放;  *本地条码文件
            begin
              if (rbuf[12+4*i]=j+1)or(StrToInt(edt59.Text)*strtoint(edt60.Text)=1) then        //当前连片数据指示
              begin
                dd:=(rbuf[10+4*i]*256+rbuf[11+4*i]);   //rbuf[14]为第一个
                if (StdValZu[i]='30005') then begin         //无符号数指示标志
                  if word(dd)<65000 then
                    ff:=dd / strtofloat(ScaleZu[zuptr])
                  else
                    ff:=dd;
                end else begin
                  if dd<30000 then
                    ff:=dd / strtofloat(ScaleZu[zuptr])
                  else
                    ff:=dd;
                end;
                if (NameStrZu[i]='*EEP') then begin                     //EEPROM测试数据要存放本地！
                  SLINE_head2:= SLINE_head2+NameStrZu[zuptr]+':'+mmo2String[17]+'*,';
                end else
                  SLINE_head2:= SLINE_head2+NameStrZu[zuptr]+':'+ copy(floattostr(ff),0,7)+'*'+ Unitstrzu[zuptr]+',' ;
                if (zuptr<UpStepMax) then zuptr:=zuptr+1;
              end;
            end;
            if((StrToInt(edt59.Text)*strtoint(edt60.Text)=1))and(StrToInt(edt81.Text)>0) then begin      //单PCS;
               for i:=1 to StrToInt(edt81.Text) do begin
                  SLINE_head2:= SLINE_head2+NameStrZu[zuptr]+':'+Condutor[i]+'*'+ Unitstrzu[zuptr]+',' ;
                  if (zuptr<UpStepMax) then zuptr:=zuptr+1;
               end;
            end;

            SLINE_head := SLINE_head2 ;         //开始本地条码文件存放<---集总文件生成完成！    //格式要同SQL数据格式；

            begin

              edt177.Visible:=true;
              edt177.Text:='';
              DMLJK.MyAddlog('1.begine.dt177---------------------------------');
              edt177.Text:=edt177.Text+'{"header": "'+'serialNumber'
                              +'","upperLimit":null' + ',"lowerLimit":null ' +',"measureUnit":null'
                              +',"value": "'+Trim(Redits[j].Value) +'"},';  //StringReplace(Trim(Redits[j].Value), ':', ' ', [rfReplaceAll])
              edt177.Text:=edt177.Text+'{"header": "'+'startTime'
                              +'","upperLimit":null' + ',"lowerLimit":null ' +',"measureUnit":null'
                              +',"value": "'+isvnsgmntclck1.Hint  +'"},';
              edt177.Text:=edt177.Text+'{"header": "'+'attribute1'
                              +'","upperLimit":null' + ',"lowerLimit":null ' +',"measureUnit":null'
                              +',"value": "'+btn115.Hint  +'"},';
              edt177.Text:=edt177.Text+'{"header": "'+'Product'
                              +'","upperLimit":null' + ',"lowerLimit":null ' +',"measureUnit":null'
                              +',"value": "'+edt77.Text  +'"},';
              edt177.Text:=edt177.Text+'{"header": "'+'Lot No'
                              +'","upperLimit":null' + ',"lowerLimit":null ' +',"measureUnit":null'
                              +',"value": "'+edt78.Text  +'"},';
              edt177.Text:=edt177.Text+'{"header": "'+'Test Pass/Fail Status'
                              +'","upperLimit":null' + ',"lowerLimit":null ' +',"measureUnit":null'
                              +',"value": "';
              if Rleds[j].ActiveColor=clgreen then  edt177.Text:=edt177.Text+'PASS"},' else edt177.Text:=edt177.Text+'FAIL"},';

              edt177.Text:=edt177.Text+'{"header": "'+'EndTime'
                              +'","upperLimit":null' + ',"lowerLimit":null ' +',"measureUnit":null'
                              +',"value": "'+stime  +'"},';
              edt177.Text:=edt177.Text+'{"header": "'+'Fixture ID'
                              +'","upperLimit":null' + ',"lowerLimit":null ' +',"measureUnit":null'
                              +',"value": "'+edt163.Text  +'"},';
              edt177.Text:=edt177.Text+'{"header": "'+'Operator'
                              +'","upperLimit":null' + ',"lowerLimit":null ' +',"measureUnit":null'
                              +',"value": "'+edt79.Text +'"},';
              edt177.Text:=edt177.Text+'{"header": "'+'List of Failing Tests'
                              +'","upperLimit":null' + ',"lowerLimit":null ' +',"measureUnit":null'
                              +',"value": "'+Rleds[j].Hint  +'"},';
              edt177.Text:=edt177.Text+'{"header": "'+'Station'
                              +'","upperLimit":null' + ',"lowerLimit":null ' +',"measureUnit":null'
                              +',"value": "'+edt80.Text  +'"},';
              edt177.Text:=edt177.Text+'{"header": "'+'TestSystem'
                              +'","upperLimit":null' + ',"lowerLimit":null ' +',"measureUnit":null'
                              +',"value": "'+'hFCT9+PC'+label9.Caption  +'"},';
            //以上是12个汇总项目
              DMLJK.MyAddlog(edt177.Text);
              DMLJK.MyAddlog('1.end.dt177---------------------------------');
              zuptr:=1;
              for i:=1 to (len_ss div 2) do      //遍历所有数据，生成上传字符串
              begin
                 DMLJK.MyAddlog('组信息------------');
                 DMLJK.MyAddlog('i：'+IntToStr(i)+' to:'+IntToStr(len_ss div 2));
                 DMLJK.MyAddlog('zuptr：'+IntToStr(zuptr)+' j:'+IntToStr(j));
                 DMLJK.MyAddlog('NameStrZu[zuptr]：'+NameStrZu[zuptr]);
                 DMLJK.MyAddlog('UpperZu[zuptr]：'+UpperZu[zuptr]);

                if (rbuf[12+4*i]=j+1)or(StrToInt(edt59.Text)*strtoint(edt60.Text)=1) then        //当前连片数据指示
                begin
                  dd:=(rbuf[10+4*i]*256+rbuf[11+4*i]);   //rbuf[14]为第一个
                  if (StdValZu[i]='30005') then begin         //无符号数指示标志
                    if word(dd)<65000 then
                      ff:=dd / strtofloat(ScaleZu[zuptr])
                    else
                      ff:=dd;
                  end else begin
                    if dd<30000 then
                      ff:=dd / strtofloat(ScaleZu[zuptr])
                    else
                      ff:=dd;
                  end;
                  if zuptr>1 then  edt177.Text:=edt177.Text+',';
                  edt177.Text:=edt177.Text+'{"header": "'+NameStrZu[zuptr]
                              +'","upperLimit": "'+UpperZu[zuptr]+'","lowerLimit": "'+LowerZu[zuptr]
                              +'","measureUnit": "'+Unitstrzu[zuptr]+'","value": "' ;

                  if (NameStrZu[zuptr]='OPEN')and(readchis('Comm Para','OSpointPos')<>'OFF')and(btn33.Hint<>'') then
                  begin               //OPEN显示点位
                    //ljk 原来的 edt177.Text:=edt177.Text+btn33.Hint;
                     stemp:=btn33.Hint;
                     stemp:=StringReplace(stemp,'[','',[rfReplaceAll]);
                     slljk:=TStringList.Create;
                     DMLJK.strToList(stemp,slljk);
                      DMLJK.MyAddlog('--slljk--:');
                      DMLJK.MyAddlog(slljk.Text);
                     stemp:='';
                      case j of
                         0:begin
                               for k:=0 to slljk.Count-1 do
                               begin
                                 if (Pos('A',slljk[k])>0) or (Pos('B',slljk[k])>0) then
                                 begin
                                 if stemp<>'' then
                                 stemp:=stemp+'['+slljk[k]+']'
                                 else
                                 stemp:='['+slljk[k]+']';
                                 end;
                               end;
                           end;
                         1:begin
                               for k:=0 to slljk.Count-1 do
                               begin
                                 if (Pos('C',slljk[k])>0) or (Pos('D',slljk[k])>0) then
                                 begin
                                 if stemp<>'' then
                                 stemp:=stemp+'['+slljk[k]+']'
                                 else
                                 stemp:='['+slljk[k]+']';
                                 end;
                               end;
                           end;
                         2:begin
                               for k:=0 to slljk.Count-1 do
                               begin
                                 if (Pos('E',slljk[k])>0) or (Pos('F',slljk[k])>0) then
                                 begin
                                 if stemp<>'' then
                                 stemp:=stemp+'['+slljk[k]+']'
                                 else
                                 stemp:='['+slljk[k]+']';
                                 end;
                               end;
                           end;
                         3:begin
                               for k:=0 to slljk.Count-1 do
                               begin
                                 if (Pos('G',slljk[k])>0) or (Pos('H',slljk[k])>0) then
                                 begin
                                 if stemp<>'' then
                                 stemp:=stemp+'['+slljk[k]+']'
                                 else
                                 stemp:='['+slljk[k]+']';
                                 end;
                               end;
                           end;
                      end;
                     if stemp<>'' then
                     edt177.Text:=edt177.Text+stemp;
                     slljk.Free;
                  end
                  else
                  if ((NameStrZu[zuptr]='SHOR')or(NameStrZu[zuptr]='SHORT'))and(readchis('Comm Para','OSpointPos')<>'OFF')and(btn34.Hint<>'') then
                  begin               //显示点位
                    //edt177.Text:=edt177.Text+btn34.Hint;
                     stemp:=btn34.Hint;
                     stemp:=StringReplace(stemp,'[','',[rfReplaceAll]);
                     slljk:=TStringList.Create;
                     DMLJK.strToList(stemp,slljk);
                     DMLJK.MyAddlog('--slljk--:');
                     DMLJK.MyAddlog(slljk.Text);
                     stemp:='';
                      case j of
                         0:begin
                               for k:=0 to slljk.Count-1 do
                               begin
                                 if (Pos('A',slljk[k])>0) or (Pos('B',slljk[k])>0) then
                                 begin
                                 if stemp<>'' then
                                 stemp:=stemp+'['+slljk[k]+']'
                                 else
                                 stemp:='['+slljk[k]+']';
                                 end;
                               end;
                           end;
                         1:begin
                               for k:=0 to slljk.Count-1 do
                               begin
                                 if (Pos('C',slljk[k])>0) or (Pos('D',slljk[k])>0) then
                                 begin
                                 if stemp<>'' then
                                 stemp:=stemp+'['+slljk[k]+']'
                                 else
                                 stemp:='['+slljk[k]+']';
                                 end;
                               end;
                           end;
                         2:begin
                               for k:=0 to slljk.Count-1 do
                               begin
                                 if (Pos('E',slljk[k])>0) or (Pos('F',slljk[k])>0) then
                                 begin
                                 if stemp<>'' then
                                 stemp:=stemp+'['+slljk[k]+']'
                                 else
                                 stemp:='['+slljk[k]+']';
                                 end;
                               end;
                           end;
                         3:begin
                               for k:=0 to slljk.Count-1 do
                               begin
                                 if (Pos('G',slljk[k])>0) or (Pos('H',slljk[k])>0) then
                                 begin
                                 if stemp<>'' then
                                 stemp:=stemp+'['+slljk[k]+']'
                                 else
                                 stemp:='['+slljk[k]+']';
                                 end;
                               end;
                           end;
                      end;
                     if stemp<>'' then
                     edt177.Text:=edt177.Text+stemp;
                     slljk.Free;
                  end
                  else
                  begin
                    edt177.Text:=edt177.Text+copy(floattostr(ff),0,7);
                  end;

                  edt177.Text:=edt177.Text+'"}';

                  if (zuptr<UpStepMax) then zuptr:=zuptr+1;
                end;
              end;
              DMLJK.MyAddlog(edt177.Text);
              DMLJK.MyAddlog('2.end.dt177---------------------------------');
              if((StrToInt(edt59.Text)*strtoint(edt60.Text)=1))and(StrToInt(edt81.Text)>0) then begin      //单PCS;
                for i:=1 to StrToInt(edt81.Text) do begin
                  if zuptr>1 then  edt177.Text:=edt177.Text+',';
                  edt177.Text:=edt177.Text+'{"header": "'+NameStrZu[zuptr]
                              +'","upperLimit": "'+UpperZu[zuptr]+'","lowerLimit": "'+LowerZu[zuptr]
                              +'","measureUnit": "'+Unitstrzu[zuptr]+'","value": "'+Condutor[i]
                              +'"}';
                  if (zuptr<UpStepMax) then zuptr:=zuptr+1;
                end;
              end;
              DMLJK.MyAddlog(edt177.Text);
              DMLJK.MyAddlog('3.end.dt177---------------------------------');
              if ( chk25.Checked  ) and (j=0) then begin  //MES系统校验过或关闭校验！，兼容单PCS使用；
                  if (Readchis('Server','MESfun')='21')and( (lbl2.hint=Trim(Redits[j].Value))or(chk25.Color=Clred) ) then begin //扫码错误或者校验异常不上传MES
                    mmo19.Lines.Add('工位1条码校验错误，不执行ET测试结果上传');
                  end else begin
                    edt136.Text:=Redits[0].Value;
                    if Rleds[0].ActiveColor=clGreen then
                      edt137.Text:='PASS' //OK'
                    else
                      edt137.Text:='FAIL';
                    edt165.Text:=copy(Rleds[j].Hint,0,30);
                    btn114Click(Form_line);
                  end;
              end;
              if chk26.Checked and (j=1) then begin                   //MES系统使能
                  if (Readchis('Server','MESfun')='21')and ( (lbl2.hint=Trim(Redits[j].Value))or(chk26.Color=Clred) ) then begin
                    mmo19.Lines.Add('工位2条码校验错误，不执行ET测试结果上传');
                  end else begin

                    edt136.Text:=Redits[1].Value;
                    if Rleds[1].ActiveColor=clGreen then
                      edt137.Text:='PASS' //'OK'
                    else
                      edt137.Text:='FAIL';
                    edt165.Text:=copy(Rleds[j].Hint,0,30);
                    btn114Click(Form_line);
                  end;
              end;
              if chk27.Checked and (j=2) then begin                   //MES系统使能
                  if (Readchis('Server','MESfun')='21')and ( (lbl2.hint=Trim(Redits[j].Value))or(chk27.Color=Clred) ) then  begin
                    mmo19.Lines.Add('工位3条码校验错误，不执行ET测试结果上传');
                  end else begin

                    edt136.Text:=Redits[2].Value;
                    if Rleds[2].ActiveColor=clGreen then
                      edt137.Text:='PASS' //'OK'
                    else
                      edt137.Text:='FAIL';
                    edt165.Text:=copy(Rleds[j].Hint,0,30);
                    btn114Click(Form_line);
                  end;
              end;
              if chk28.Checked and (j=3) then begin                   //MES系统使能
                  if (Readchis('Server','MESfun')='21')and ( (lbl2.hint=Trim(Redits[j].Value))or(chk28.Color=Clred) ) then begin
                    mmo19.Lines.Add('工位4条码校验错误，不执行ET测试结果上传');
                  end else begin

                    edt136.Text:=Redits[3].Value;
                    if Rleds[3].ActiveColor=clGreen then
                      edt137.Text:='PASS' //'OK'
                    else
                      edt137.Text:='FAIL';
                    edt165.Text:=copy(Rleds[j].Hint,0,30);
                    btn114Click(Form_line);
                  end;
              end;
            end;
            if (Readchis('Server','barcodeFile')='0') then begin
              lbl180.caption:='禁止条码命名的文件(barcodeFile=0)';
              lbl180.Enabled:=false;
              edt157.Enabled:=False;
              btn125.Enabled:=False;
            end else begin
              lbl180.Enabled:=True;
              edt157.Enabled:=True;
              btn125.Enabled:=True;
              try
                begin
                  datetimetostring(stime,'yyyymmddhhnnss',Now);
                  lbl180.caption:=edt77.Text+'\'+edt77.Text+'_'+DelSpecialChar(Trim(Redits[j].Value)) //StringReplace(Trim(Redits[j].Value), ':', ' ', [rfReplaceAll])
                          +'_'+stime +'_'+IdIPWatch1.LocalIP+'_ICT';
                  if Rleds[j].ActiveColor=clgreen then  begin
                    lbl180.caption := lbl180.caption + '_PASS.csv';
                  end else begin
                    lbl180.caption := lbl180.caption + '_FAIL.csv';
                  end;
                  assignfile(f, edt157.text+lbl180.caption);      //增加条码命名的txt文件； jaguar多连片

                end;
                rewrite(f);  // Append(f);                       //application.ProcessMessages;
                writeln(f,'SerialNumber,TestTime,TestResult,TestType,PartNumber,Line,IP_Address,Machine,Operator,OnLine,Other1,Other2');
                writeln(f, SLINE_head);                         //专用格式
              finally
                Closefile(f);
              end;
              if(edt6.hint<>'UpSinglePCS')                      //单PCS文件仅复制到temp临时文件夹
              then begin
                 if not CopyFile(PChar(edt157.text+lbl180.caption), PChar(edt157.Text  +'temp\' + lbl180.caption), True) then  //如果文件已存在则返回错误
                    stat1.Panels[4].Text:='Copy File Failed!'
                 else begin
                    stat1.Panels[4].Text:='Copy File Succeed!';
                 end;
              end;
            end;
          finally
          end;
          if(StrToInt(edt6.Text)>0) then begin
            lbl181.Caption:='25';               //生成条码文件后，上传映射盘标志；
            btn2Click(Form_line);
          end;
        end;
      end;
    end;
  begin
    with Form_line do begin
       for j:=0 to StrToInt(edt59.Text)*strtoint(edt60.Text)-1 do      //搜索有输出结果的连片序号；如果只有1连片则单独处理；
       begin
         if ( Readchis('Comm Para','DoubleBarcodeStart')='1' )and (Label7.hint='11') then begin       // 双条码有任一个无效，则不保存并显示红灯
             if(medit1.Color= clred)or(medit1.Color= clred) then begin  //有一个条码不符合规则
               Rleds[0].ActiveColor:=clRed;
               Rleds[1].ActiveColor:=clRed;
              // iLedRound1.Active:=True;
               iLedRound1.ActiveColor:=clRed;
              // iLedRound2.Active:=True;
               iLedRound2.ActiveColor:=clRed;
               iLabel1.Font.Color:=clRed;
               iLabel1.Caption:='FAIL';
               iLabel2.Font.Color:=clRed;
               iLabel2.Caption:='FAIL';
               break;
             end;
         end;
         if j>0 then  TestNum:=TestNum+1;        //序号依次加一
         if Rleds[j].Active= TRUE then
         begin
            zuptr:=1;
            begin      //tesla和jaguar    前几项与通用的不一样
                
                if(edt62.Text='24')then begin
                  teslaLog;
                  if(edt53.hint='9000')and(Stat1.tag=9000)then begin
                    CreatAnewCsv(Stat1.tag,0);              //每次都生成条码命名文件
                    zuptr:=1;
                    saveCarLedlog(Stat1.tag);
                  end;
                  Exit;
                end else if(edt62.Text='25')then
                  SMTlog;
                  
            end;
            if(edt53.hint='9000')and(Stat1.tag=9000)then begin
              CreatAnewCsv(Stat1.tag,0);              //每次都生成条码命名文件
              zuptr:=1;
              saveCarLedlog(Stat1.tag);
            end;

            if(edt74.Text='10') then begin     //强制生成一条数据
              break;
            end;
         end;

       end;
    end;
  end;

begin                  //-------------保存CSV文件入口----------------
  with Form_line do begin
  //-----------1.------预处理，软件开启后第一次收到log，可生成文件！--------------------------------------------------------
    if NewLogFileFlag=1 then begin                   //应该生成新Log文件（不管文件名是否存在）//文件夹生成或者软件开启标志
        if StrToInt(edt38.text)mod 10 >0 then begin    //uplocal
            CreatAnewCsv(1,0)
        end;
    end;

    //-----如果不存在文件，第2次可生成文件！----------------------------------------------
    if (Label10.caption<>'Label10') then     //有文件名存在：从FCT主板获取到机种名
    begin

        save_file_name :=Label10.caption;     //默认文件名处理完成
        TeslaJiaheDataPre();                              //tesla 或者jiahe ，早期数据的前面几个项目 + 后期第二文件数据准备；


        mmo2.Lines[1]:='测试结果：'+ Copy(SLINE_head,0,1024);
//-^^^^^^^^^^^^^^^^^^^----以上pre预处理客户定义 开头几项参数，---------在界面显示一行-----------------------------------------

//--------2.---------------以下处理结果数据部分,仅显示用---------------------------------------------------------------
        SLINE_head1:='result-'+#9;                  //用于在MEMO1里显示

        DataInMemoDisp();                           //要保存的数据，SLINE_head1仅先显示一下
        mmo2.Lines[3]:= Copy(SLINE_head1,0,1024);   //结果 显示，TAB分隔
        mmo2.Lines[1]:=mmo2.Lines[1];               //使能 显示区域在开始位置
  //------------------------以上处理memo显示----------------------------------------------

  //----------3.--------------以下开始处理连片数据，以及数据保存---及单PCS保存用的数据（后面可以直接保存不用二次处理）其中多连片的开头几项重新开始------------------------------------------
        //mmo2.Lines[3]:= '-';      //空的话，不执行。必须有个空格
        if (Label10.caption<>'Label10')and isfileinuse(edt2.Text + Label10.caption) then  begin   //存在文件且被占用
          Application.MessageBox('LogFile in use！Log文件被占用，不能写入数据！',
                '警告', MB_OK + MB_ICONWARNING);
        end else begin
          if rbuf[3]<>$EE then      //非 联板保存，
          begin            //单PCS测试数据，前面已经处理完成！ 注：连片测试单步会导致接收数据错误，从而进到这里！
            saveOnePCSresult();
          end else begin             //rbuf[3]=$EE 连片测试，每PCS都要保存
            saveNPCSresult();
          end;
        end;
        ////--------------------------第二路径，第3路径文件保存-----------------------
    end;
  end;
end;


end.
