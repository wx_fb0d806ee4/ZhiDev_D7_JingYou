{  描述：     数据接收保存，生成文件，保存文件；
  文件名：	  unit_global_var...
  应用语言:   delphi
  版本：			V1.0
  苏州周至电子科技有限公司http://zhzhi.cn 
  Copyright(C) Jin 2013   All rights reserved
  作者：Jin
  建立日期：2019-05-09
}
unit Unit0_globalVariant;

interface

uses
  Windows, Messages, SysUtils,Variants, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ActnList, NB30, registry, shellapi, dbclient, inifiles, db, adodb,
  JvHidControllerClass,FileCtrl, StdCtrls;

type
  TmenuParam = record // 用来传递菜单信息
    flag: word; // 标志位
    parm1: Dword;
    parm2: Dword;
    Text: string;
  end;

  xyedit=array[0..75] of Tedit;     //64个有效，其他预留
  xylabel=array[0..75] of Tlabel;   //160

type
  Rec_data = record     /////数据库记录格式
    NUMB: Integer;
    MODELS: String;
    STATION: Integer;
    BARCODE:string;
    RESULT:string;
    CRATE_DATE:TDateTime;
    TES_EMP:string;
    BASE_LOT:string;
  end;

  TRemTime = record
    Remtime: boolean;
    BeginTime: Tdatetime;
    EndTime:   Tdatetime;
  end;

  TTablekind = (tkReal, tkRun, tktrend, tkRunResult); // 表类型

const
  HexChar: array [0 .. 15] of string = ('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');
 
const
  ScreenWidth: Integer = 1024;
  ScreenHeight: Integer = 768;

  MAX_BUFFLEN=16384;                //12000
  VER630_MAXSTEP=9990+5;            //设计支持9990个步骤，而FCT6最小支持900步骤，最大支持4500步骤
  END_STEP=9999;                    //结束步骤标志
  VIRLCOFFSET:SmallInt=10000;
  UpStepMax=1000*2;                 //占用加载的时间明显！可上传的最大步骤数，2022年增加一倍（可匹配MCU一个机种可占用原来2个机种存储空间！）


  MAXPINS =2047*2;                  //与MCU短路群数组长度对应，超4095点只能存一次短路群，系统最大点数后几个数组系统占用！
                                    //最大点数不能取2的n次方（bit写0与点数少的不容易兼容！）//考虑MCU从0开始编号（实际对应2048,匹配MCU空间）   注意+1必须为2的N次方
                                    //支持2次开短路，点数最大4094；取整4090
                                    //支持1次开短路，点数最大8188；取整8180
  SHORTPINS:SmallInt =256*2;        //翻倍可达到8188点<--短路群页数（一页16个点，最多4096点匹配MCU空间！）    128;// (MAXPINS+1)/16;//8; 超过1000点不能学习二次短路群 //32;    //: Integer    32对应255点，二次开短路总需页数。


var
  modbusfuntion:SmallInt;   //03功能码设置不同的通讯功能
  modbusfun05:Byte;
  modbusfun16,modbusfun16int,modbusfun16len,modbusfun06,modbusfun05dat,modbusfun02,TestNum:Integer;
  modbusfun16str,viewstring,viewstring2:string;
  modbusfun03,modbusfun03dat,modbusfun06dat:SmallInt;
  GroupNowNum,working,SysExplainNum :SmallInt;
  // -----------------
  P2exeFileName: string;
Var
//---------------与下位机项目的变量------------------------------------
  CurrentDevice: TJvHidDevice;

  VIRLCROWS:Smallint=70;            //对启动时间影响大，最大64每扇区 =65可自适应行数  ,=65固件版本决定行数
  StepNowFresh:SmallInt=0;          //  PowerOnCreatNewFile:SmallInt=1;  //默认：非FCT5;
  RepeatN:SmallInt=0;
  StationNowNum:SmallInt=0;          //当前工位；生成窗口时定义；
  row_diff:SmallInt;
  barcodeSelT,ImportVer,TxtOSPins:Integer;
  rbuf,sbuf:array[1..MAX_BUFFLEN] of byte;
  //如下数据指针从1..开始
  NameStrZu,UnitStrZu,ScaleZu,UpperZu,LowerZu,Condutor:array [1..UpStepMax] of String;  //目前？？？显示，循环变量 尽量用 zuptr！<--多连片则只存放单PCS的表头；用config只能按顺序设置；
  ModeZu:array [1..UpStepMax] of String;                                 //目前用 i和zuptr？？？模式，循环变量 ，
  SpecValZu,StdValZu,HpointZu,LpointZu:array [1..UpStepMax] of String;   //设置，循环变量 尽量用 i！<--多连片则只存放所有参数，互相有区别；
  OnePCSNamePos:array [1..UpStepMax] of SmallInt;                               //存放多连片之单PCS项目（可非连续）所在位置指针，存放值从0开始；
  lv1NameZu,ErrlistZu,ErrChartZu:array [1..UpStepMax] of SmallInt;                        //在测试界面统计用！
  zu1edit,zu2edit,zu3edit,zu4edit,zu5edit,zu6edit,zu7edit,zu8edit,zu9edit,zu10edit
  ,zu11edit,zu12edit,zu13edit,zu14edit,zu15edit,zu16edit,zu17edit,zu18edit,zu19edit,zu20edit,sysedit:xyedit;
  syslabel,xyout:xylabel;

  /////////////--------------------函数------------------------------------
  function    IsFileInUse(fName: string): boolean;
  function    IsNumberic(Vaule:String):Boolean;
  function    IsSignedNumberic(Vaule:String):Boolean;
  function    IsSignedNumbericHex(Vaule:String):Boolean;
  function    IsFloatNum(Vaule:String):Boolean;
  function    IsxFloatNum(Vaule:String):Boolean;
  function    IsHexNum(Vaule:String):Boolean;
  function    DelSpecialChar(Vaule:String):string;
//  procedure   createRHexowEdit(row:SmallInt);
  procedure   send_crcdata(len_send:integer;add_crc:SmallInt);
  procedure   printdata(len_ss:integer);          //function  rxd_crc(start_temp,len_temp:integer):integer;
  procedure   LV1MemoDisp(len_ss:SmallInt);
  procedure   createRowEdit(row:SmallInt);
  function    SelectMessageBox(Text: string; CaptionNum: Integer;
    Flags: Integer): Integer;

  procedure   addToLog(str: string); overload;    //procedure   addToLog(DebugStr: string; Debug: boolean);overload  ;

  function    GetMonths(btime, etime: Tdatetime): TStrings;

  // 将一个记录中的数据插入到目标数据集中。byWho表示按哪一个得字段为准：1：cdsSRC；2：cdsDES
  procedure   InsertRecords(cdsSRC: Tclientdataset; var cdsDEST: Tclientdataset;
    byWho: Integer);

  function    Readchis(section: string; indent: string): string; overload;

  function    Readchis(section: string; indent: string; default: string): string;
        overload;
  procedure   WriteChis(section: string; indent: string; value: string); // 写ini
  function    Readchis2(section: string; indent: string): string; overload;
  procedure   WriteChis2(section: string; indent: string; value: string);
  function    Readchis3(section: string; indent: string): string; overload;
  procedure   WriteChis3(section: string; indent: string; value: string);
  function    Readchis4(section: string; indent: string): string; overload;
  procedure   WriteChis4(section: string; indent: string; value: string);  // function ChisKeyExits(Section:string;Indent:string;Acreate:boolean):boolean;
  procedure   FillDataset(dataset: Tdataset; timeFieldname: string;
    divSec: Integer);       // 补数 divsec 时间间隔妙 divsec<60

  procedure   SendMessageData(AHandle: THandle; AStr: String);
  procedure   SendMessageDataToMain( AStr: String);
  function    rxd_crc(start_temp,len_temp:integer):integer;
  function    uart_crc(start_temp,len_temp:integer):integer;
  function    GetNameUnitFromConfig(firstClrFlag:integer):integer;        //  function GetNameUnitForTesla(firstClrFlag:integer):integer;

  function    GetTimemS:string;
  procedure   DispErrlist(len_ss:SmallInt);

implementation

uses StepCopy,main,OSstudy,FindPoint,PNenter
    ,LogHeadTips,Unit2_Communication,LoadFile,Unit4_logFileCode;

  function GetTimemS:string;
  var
    currentTime:TSystemTime;
    year, month, day, hour, minute, second, millisecond: string;        //datetime: string;
  begin
    GetSystemTime(currentTime);
    //year:= IntToStr(currentTime.wYear);
    //month:= IntToStr(currentTime.wMonth);
    //day:= IntToStr(currentTime.wDay);
    hour:= IntToStr(currentTime.wHour + 8);   if Length(hour)=1 then hour:='0'+hour;
    minute:= IntToStr(currentTime.wMinute);   if Length(minute)=1 then minute:='0'+minute;
    second:= IntToStr(currentTime.wSecond);   if Length(second)=1 then second:='0'+second;
    millisecond:= IntToStr(currentTime.wMilliseconds);
    result:='[' + hour + ':' + minute + ':' + second + ':' + millisecond+']';
  end;
  procedure DispErrlist(len_ss:SmallInt);
  var
    zuptr,bb:SmallInt;
    i,j:Integer;
    ff:Real;
    dd:SmallInt;
  begin
    with Form_line do begin
      if Readchis('Result','CloseErrlist')='' then begin           //没有禁止ERRlist显示；

        if not Rleds[0].Active then  begin       //Rleds[0].ActiveColor:=clBlue;
          Rleds[0].Active:=True;                 //设置true用于单PCS数据保存 //CSV格式太复杂，不能作为通用连片用
          if (rbuf[4]=0)   then
            Rleds[0].ActiveColor:=clgreen
          else
            Rleds[0].ActiveColor:=clred;
        end;

        if StrToInt(edt59.Text)*strtoint(edt60.Text)=1 then begin
          for i:=1 to  NameCount do begin            //清空所有项目信息
            ErrlistZu[i]:=0; lv1NameZu[i]:=0;
          end;
          zuptr:=1;
          for i:=1 to (len_ss div 2) do begin
             if (zuptr<=UpStepMax) then begin
               if (rbuf[13+4*i] and $01)=$01 then begin         //NG
                  ErrlistZu[zuptr]:=1;
                  ErrChartZu[i]:=ErrChartZu[i]+1;
                  if(ErrChartZu[i]>series1.Tag) then begin
                    if series2.Tag<>i then begin
                      series9.Tag:=series7.Tag;
                      series7.Tag:=series5.Tag;
                      series5.Tag:=series3.Tag;
                      series3.Tag:=series1.Tag;
                    end;
                    series1.Tag:=ErrChartZu[i]; series1.Title:=NameStrZu[zuptr];
                    series2.Tag:=i;
                  end else if(ErrChartZu[i]>series3.Tag) then begin
                    if series4.Tag<>i then begin
                      series9.Tag:=series7.Tag;
                      series7.Tag:=series5.Tag;
                      series5.Tag:=series3.Tag;
                    end;
                    series3.Tag:=ErrChartZu[i]; series3.Title:=NameStrZu[zuptr];
                    series4.Tag:=i;
                  end else if(ErrChartZu[i]>series5.Tag) then begin
                    if series6.Tag<>i then begin
                      series9.Tag:=series7.Tag;
                      series7.Tag:=series5.Tag;
                    end;
                    series5.Tag:=ErrChartZu[i]; series5.Title:=NameStrZu[zuptr];
                    series6.Tag:=i;
                  end else if(ErrChartZu[i]>series7.Tag) then begin
                    if series8.Tag<>i then begin
                      series9.Tag:=series7.Tag;
                    end;
                    series7.Tag:=ErrChartZu[i]; series7.Title:=NameStrZu[zuptr];
                    series8.Tag:=i;
                  end else if(ErrChartZu[i]>series9.Tag) then begin
                    series9.Tag:=ErrChartZu[i]; series9.Title:=NameStrZu[zuptr];
                    series10.Tag:=i;
                  end;
               end else
                  ErrlistZu[zuptr]:=0;
               lv1NameZu[zuptr]:=i;                    //界面显示组对应mode组的位置
             end;
             if (zuptr<UpStepMax) then zuptr:=zuptr+1;
          end;
        end else begin                      //多连片数据
          for i:=1 to  UpStepMax do begin            //清空所有项目信息
            ErrlistZu[i]:=0; lv1NameZu[i]:=0;
          end;
          zuptr:=1;
          //edt59.Tag:=0;
          for j:=0 to StrToInt(edt59.Text)*strtoint(edt60.Text)-1 do
          begin
            for i:=1 to (len_ss div 2) do      //遍历所有数据，
            begin
              if rbuf[12+4*i]=j+1 then
              begin
                 if (zuptr<=UpStepMax) then begin
                   if (rbuf[13+4*i] and $01)=$01 then begin         //NG
                      ErrlistZu[zuptr]:=1;
                      ErrChartZu[i]:=ErrChartZu[i]+1;
                      if(ErrChartZu[i]>series1.Tag) then begin
                        if series2.Tag<>i then begin
                          series9.Tag:=series7.Tag;
                          series7.Tag:=series5.Tag;
                          series5.Tag:=series3.Tag;
                          series3.Tag:=series1.Tag;
                        end;
                        series1.Tag:=ErrChartZu[i]; series1.Title:=NameStrZu[zuptr];
                        series2.Tag:=i;
                      end else if(ErrChartZu[i]>series3.Tag) then begin
                        if series4.Tag<>i then begin
                          series9.Tag:=series7.Tag;
                          series7.Tag:=series5.Tag;
                          series5.Tag:=series3.Tag;
                        end;
                        series3.Tag:=ErrChartZu[i]; series3.Title:=NameStrZu[zuptr];
                        series4.Tag:=i;
                      end else if(ErrChartZu[i]>series5.Tag) then begin
                        if series6.Tag<>i then begin
                          series9.Tag:=series7.Tag;
                          series7.Tag:=series5.Tag;
                        end;
                        series5.Tag:=ErrChartZu[i]; series5.Title:=NameStrZu[zuptr];
                        series6.Tag:=i;
                      end else if(ErrChartZu[i]>series7.Tag) then begin
                        if series8.Tag<>i then begin
                          series9.Tag:=series7.Tag;
                        end;
                        series7.Tag:=ErrChartZu[i]; series7.Title:=NameStrZu[zuptr];
                        series8.Tag:=i;
                      end else if(ErrChartZu[i]>series9.Tag) then begin
                        series9.Tag:=ErrChartZu[i]; series9.Title:=NameStrZu[zuptr];
                        series10.Tag:=i;
                      end;
                   end else
                      ErrlistZu[zuptr]:=0;
                   //if (j>0)and(edt59.Tag>0) then
                   //   lv1NameZu[zuptr]:=( (zuptr-1) mod edt59.Tag )+1
                   //else
                      lv1NameZu[zuptr]:=i;        //界面显示组对应mode组的位置
                 end;
                 //if j=0 then  edt59.Tag:=edt59.Tag+1;              //统计单PCS项目数量；
                 if (zuptr<UpStepMax) then zuptr:=zuptr+1;         //lv1排列顺序
              end;
            end;
          end;
        end;
  
        series2.Title:='-';
        series4.Title:='-';
        series6.Title:='-';
        series8.Title:='-';
        series10.Title:='-';
        series1.XValues[0]:=series1.Tag;
        series3.XValues[0]:=series3.Tag;
        series5.XValues[0]:=series5.Tag;
        series7.XValues[0]:=series7.Tag;
        series9.XValues[0]:=series9.Tag;
        series10.Clear;

        lv1.Clear;                            //清空
        lv1.Columns.Clear;

        lv1.Columns.Add;                       //增加5项列表头
        lv1.Columns.Add;
        lv1.Columns.Add;
        lv1.Columns.Add;
        lv1.Columns.Add;

        lv1.Columns.Items[0].Caption:='项目名';
        lv1.Columns.Items[1].Caption:='测量值';
        lv1.Columns.Items[2].Caption:='上限';
        lv1.Columns.Items[3].Caption:='下限';
        lv1.Columns.Items[4].Caption:='单位';

        lv1.Columns.Items[0].Width:=lv1.Width div 6;
        lv1.Columns.Items[1].Width:=lv1.Width div 6;
        lv1.Columns.Items[2].Width:=lv1.Width div 6;
        lv1.Columns.Items[3].Width:=lv1.Width div 6;
        lv1.Columns.Items[4].Width:=lv1.Width div 6;

        for j:=0 to StrToInt(edt59.Text)*strtoint(edt60.Text)-1 do
        begin
          zuptr:=1;                       //表头指针，每一PCS都从1开始；多连片必须有相同的测试项目；
          if StrToInt(edt59.Text)*strtoint(edt60.Text)=1 then begin
             for i:=1 to   (len_ss div 2) do begin            //添加所有项目信息   NameCount
               with lv1.items.add do
               begin
                  caption:=NameStrZu[zuptr];//IntToStr(i);        //subitems.add(NameStrZu[i]);
                  dd:=(rbuf[10+4*i]*256+rbuf[11+4*i]);
                  ff:=dd / strtofloat(ScaleZu[zuptr]);       
                  if (readchis('Comm Para','OSpointPos')<>'OFF')and ( (NameStrZu[zuptr]='OPEN')or(NameStrZu[zuptr]='SHOR')or(NameStrZu[zuptr]='SHORT') ) then begin
                    if (NameStrZu[zuptr]='OPEN') then begin
                      if (btn33.Hint<>'') then subitems.add(btn33.Hint)
                      else subitems.add(copy(floattostr(ff),0,7));
                    end else if (btn34.Hint<>'') then  begin
                      subitems.add(btn34.Hint)
                    end else
                      subitems.add(copy(floattostr(ff),0,7));

                    if (Copy(ModeZu[i],2,1)='S') then begin
                      subitems.add('>='+UpperZu[zuptr]);
                      subitems.add('-');
                    end else if (Copy(ModeZu[i],2,1)='O')or (Copy(ModeZu[i],2,1)='K') then begin
                      subitems.add('-');
                      subitems.add('<='+LowerZu[zuptr]);
                    end else begin
                      subitems.add(UpperZu[zuptr]);
                      subitems.add(LowerZu[zuptr]);
                    end;
                  end else if  (Copy(ModeZu[i],2,1)='S')or(Copy(ModeZu[i],2,1)='O')or(Copy(ModeZu[i],2,1)='K')  then begin
                    if dd<1 then begin
                      subitems.add('right');
                    end else begin
                      subitems.add('error');
                    end;
                    if (Copy(ModeZu[i],2,1)='S') then begin
                      subitems.add('>='+UpperZu[zuptr]);
                      subitems.add('-');
                    end else begin
                      subitems.add('-');
                      subitems.add('<='+LowerZu[zuptr]);
                    end;
                  end else if (Copy(ModeZu[i],2,2)='XH')or(Copy(ModeZu[i],2,2)='YH') then begin      //16进制显示
                    subitems.add('0x'+inttohex(dd,2));
                    subitems.add('0x'+inttohex(StrToInt(UpperZu[zuptr]),2));
                    subitems.add('0x'+inttohex(StrToInt(LowerZu[zuptr]),2));
                  end else begin
                    subitems.add(copy(floattostr(ff),0,7));
                    if UpperZu[zuptr]='30009' then subitems.add('*') else subitems.add(UpperZu[zuptr]);
                    if LowerZu[zuptr]='30009' then subitems.add('*') else subitems.add(LowerZu[zuptr]);
                  end;
                  subitems.add(UnitStrZu[zuptr]);
               end;
               if (zuptr<UpStepMax) then zuptr:=zuptr+1;
             end;
          end else begin                      //多连片

            for i:=1 to (len_ss div 2) do      //遍历所有数据，
            begin

              if rbuf[12+4*i]=j+1 then        //分PCS显示,用zuptr循环！
              begin
                with lv1.items.add do
                begin
                    if j>0 then
                      caption:=NameStrZu[zuptr]+'#'+IntToStr(j+1)
                    else
                      caption:=NameStrZu[zuptr];

                    dd:=(rbuf[10+4*i]*256+rbuf[11+4*i]);
                    if (StdValZu[i]='30005') then begin         //无符号数指示标志
                      if word(dd)<65000 then
                        ff:=word(dd) / strtofloat(ScaleZu[zuptr])
                      else
                        ff:=word(dd);
                    end else begin
                      if dd<30000 then
                        ff:=word(dd) / strtofloat(ScaleZu[zuptr])
                      else
                        ff:=word(dd);
                    end;
                    if //(NameStrZu[zuptr]='OPEN')or(NameStrZu[zuptr]='SHOR')or
                        (Copy(ModeZu[i],2,1)='S')or(Copy(ModeZu[i],2,1)='O')or(Copy(ModeZu[i],2,1)='K')  then begin
                      if dd<1 then begin
                        subitems.add('right');
                      end else begin
                        subitems.add('error');
                      end;
                      if (Copy(ModeZu[i],2,1)='S') then begin
                        subitems.add('>='+UpperZu[zuptr]);
                        subitems.add('-');
                      end else begin
                        subitems.add('-');
                        subitems.add('<='+LowerZu[zuptr]);
                      end;
                    end else if (Copy(ModeZu[i],2,2)='XH')or(Copy(ModeZu[i],2,2)='YH') then begin      //16进制显示
                      subitems.add('0x'+inttohex(dd,2));
                      subitems.add('0x'+inttohex(StrToInt(UpperZu[zuptr]),2));
                      subitems.add('0x'+inttohex(StrToInt(LowerZu[zuptr]),2));
                    end else begin
                      subitems.add(copy(floattostr(ff),0,7));
                      if UpperZu[zuptr]='30009' then subitems.add('*') else subitems.add(UpperZu[zuptr]);
                      if LowerZu[zuptr]='30009' then subitems.add('*') else subitems.add(LowerZu[zuptr]);
                    end;
                    subitems.add(UnitStrZu[zuptr]);
                end;

                if (zuptr<UpStepMax) then zuptr:=zuptr+1;
              end;
            end;
          end;


        end;
        if not ts7.Showing and TabSheet2.TabVisible then 
          ts6.Show;
      end;
    end;
  end;
  procedure LV1MemoDisp(len_ss:SmallInt);
  var
    zuptr,bb:SmallInt;
    i,j,dd:Integer;
    ff:Real;
  begin
    with Form_line do begin
      bb:=1;
      if rbuf[3]=$EE then     //CSV格式联板则先处理MEMO显示数据，仅显示用。
      begin
          zuptr:=1;             //表头指针，当前显示指针
          for i:=bb to (len_ss div 2) do          //遍历所有数据,用i指示，实际显示指针为zuptr
          begin
            if (OnePCSNamePos[zuptr]=(i-1) ) or (edt74.Text<>'1') then    //如果启动单PCS测试（含有'#'的名称，且联板测试，不在memo1显示）
            begin
              dd:=(rbuf[10+4*i]*256+rbuf[11+4*i]);               //SLINE_head:= SLINE_head+inttostr(dd)+',' ;         {SLINE_head1:= SLINE_head1+inttostr(dd)+#9 ;   }
              ff:=dd / strtofloat(ScaleZu[zuptr]);  //'100');    //SLINE_head1:= SLINE_head1+copy(floattostr(ff),0,7)+#9 ;

              if (zuptr<UpStepMax) then zuptr:=zuptr+1;
            end;
          end;
          
          DispErrlist(len_ss);

      end else begin        //单PCS数据结果处理并显示；SLINE_head1仅用于显示和保存
 
      end;
    end;
  end;

procedure printdata(len_ss:integer);
var
 //   f: TextFile;
 //   save_file_name: string;
    s,  SLINE_head,SLINE_head1: string;
    i,j,bb,len: integer;
    zuptr,dd:SmallInt;
    ff:Real;
begin
  bb:=0;
  with Form_line do begin
    if (edt62.Text='25') then  begin      //jaguar项目
      if (StrToInt(edt11.Text)mod 10)=1 then begin        //短消息打印
        if (rbuf[4]=0)             //结果OK
        then begin
           mmo5.lines[0]:='PASS';       //RS:
        end else begin
           mmo5.lines[0]:='FAIL';
        end;
        mmo5.lines[1]:=edt79.Text;
        s:=DateToStr(Now);
        mmo5.lines[2]:=copy(s,3,2)+copy(s,6,2)+copy(s,9,2);     //打印年月日；
        bb:=bb+1;
        s:=TimeToStr(Now);
        mmo5.lines[3]:=copy(s,0,5);
        bb:=3;
        if rbuf[3]=$EE then begin                //高级显示，只打印O/S TEMP,NTC ；  //CSV格式联板则先处理MEMO显示数据，仅显示用。
          for i:=1 to (len_ss div 2) do          //遍历所有数据,用i指示，实际显示指针为zuptr
          begin
            if(Copy(NameStrZu[i],0,4)='NTC1')then begin
                dd:=(rbuf[10+4*i]*256+rbuf[11+4*i]);
                bb:=bb+1;
                ff:=dd / strtofloat(ScaleZu[i]);
                mmo5.lines[bb+0]:='T'+copy(floattostr(ff)+'     ',0,5);
                for j:=1 to (len_ss div 2) do          //遍历所有数据,用i指示，实际显示指针为zuptr
                begin
                  if(NameStrZu[j]='T1')then begin
                      dd:=(rbuf[10+4*j]*256+rbuf[11+4*j]);
                      bb:=bb+1;
                      ff:=dd / strtofloat(ScaleZu[j]);
                      mmo5.lines[bb+0]:=copy(floattostr(ff)+'     ',0,4)+ Unitstrzu[j];
                  end;
                end;
            end;
            if(Copy(NameStrZu[i],0,4)='NTC2')then begin
                dd:=(rbuf[10+4*i]*256+rbuf[11+4*i]);
                bb:=bb+1;
                ff:=dd / strtofloat(ScaleZu[i]);
                mmo5.lines[bb+0]:='T'+copy(floattostr(ff)+'     ',0,5);
                for j:=1 to (len_ss div 2) do          //遍历所有数据,用i指示，实际显示指针为zuptr
                begin
                  if(NameStrZu[j]='T2')then begin
                      dd:=(rbuf[10+4*j]*256+rbuf[11+4*j]);
                      bb:=bb+1;
                      ff:=dd / strtofloat(ScaleZu[j]);
                      mmo5.lines[bb+0]:=copy(floattostr(ff)+'     ',0,4)+ Unitstrzu[j];
                  end;
                end;
            end;
          end;
        end;
      end else begin
        if (rbuf[4]=0)              //结果OK
        then begin
           mmo5.lines[0]:='结果: PASS';       //RS:
        end else begin
           mmo5.lines[0]:='结果: FAIL';
        end;
        mmo5.lines[1]:=edt79.Text;
        s:=DateToStr(Now);
        mmo5.lines[2]:=s;
        bb:=bb+1;
        s:=TimeToStr(Now);
        mmo5.lines[3]:=s;
        bb:=3;
        if rbuf[3]=$EE then begin                //高级显示，只打印O/S TEMP,NTC ；  //CSV格式联板则先处理MEMO显示数据，仅显示用。
          for i:=1 to (len_ss div 2) do          //遍历所有数据,用i指示，实际显示指针为zuptr
          begin
            dd:=(rbuf[10+4*i]*256+rbuf[11+4*i]);
             if(Copy(NameStrZu[i],0,3)='NTC')or(NameStrZu[i]='T1')then begin
                bb:=bb+1;
                ff:=dd / strtofloat(ScaleZu[i]);
                mmo5.lines[bb+0]:=NameStrZu[i]+'-'+copy(floattostr(ff)+'     ',0,5)+ Unitstrzu[i];
             end;
          end;
        end;
      end;
    end else begin
      if (StrToInt(edt11.Text)mod 10)=1 then begin        //短消息打印
        s:=DateToStr(Now);
        mmo5.lines[0]:=s;
        bb:=bb+1;
        s:=TimeToStr(Now);
        mmo5.lines[1]:=s;
        if (Label7.hint='1')and(readchis('Result','PrintBarcode')='1') then  begin //printBarcode控制是否打印
          bb:=bb+1;
          mmo5.lines[bb]:=Trim(edit1.Text);
        end;
      end else begin
        datetimetostring(s,'yyyy-mm-dd hh:nn:ss',Now);
        mmo5.lines[0]:=s;

        if (Label7.hint='1')and(readchis('Result','PrintBarcode')='1') then  begin
          bb:=bb+1;
          mmo5.lines[bb]:=Trim(edit1.Text);
        end else
          mmo5.lines[1]:='';
      end;
      for i:=bb+1 to 20 do
        mmo5.lines[i]:='';

      if rbuf[3]=$EE then begin                //高级显示，只打印O/S TEMP,NTC ；  //CSV格式联板则先处理MEMO显示数据，仅显示用。
        for i:=1 to (len_ss div 2) do          //遍历所有数据,用i指示，实际显示指针为zuptr
        begin
          dd:=(rbuf[10+4*i]*256+rbuf[11+4*i]);
          if (StrToInt(edt11.Text)mod 10)=1 then begin    //短信息 打印
            if ( NameStrZu[i]='OPEN')or(NameStrZu[i]='SHOR') then begin
              bb:=bb+1;
              if dd=0 then
                mmo5.lines[bb+0]:=NameStrZu[i]+'-'+copy('right',0,5)
              else
                mmo5.lines[bb+0]:=NameStrZu[i]+'-'+copy('error',0,5);
            end else if(Copy(NameStrZu[i],0,3)='NTC')or(NameStrZu[i]='temp')or(NameStrZu[i]='Temp')or(NameStrZu[i]='TEMP')then begin
              bb:=bb+1;
              ff:=dd / strtofloat(ScaleZu[i]);
              mmo5.lines[bb+0]:=NameStrZu[i]+'-'+copy(floattostr(ff)+'     ',0,5);
            end;
          end else begin
            if ( NameStrZu[i]='OPEN')or(NameStrZu[i]='SHOR') then begin
              bb:=bb+1;
              if dd=0 then  mmo5.lines[bb+0]:=NameStrZu[i]+'- '+copy('right',0,6)+':'+ Unitstrzu[i]
              else  mmo5.lines[bb+0]:=NameStrZu[i]+'- '+copy('error',0,6)+':'+ Unitstrzu[i];
            end else if(Copy(NameStrZu[i],0,3)='NTC')or(NameStrZu[i]='temp')or(NameStrZu[i]='Temp')or(NameStrZu[i]='TEMP')then begin
              bb:=bb+1;
              ff:=dd / strtofloat(ScaleZu[i]);
              mmo5.lines[bb+0]:=NameStrZu[i]+'- '+copy(floattostr(ff)+'      ',0,7)+':'+ Unitstrzu[i];
            end;
          end;

        end;
      end else begin             //早期0B00数据上传；
        for i:=1 to len_ss do    //数据结果行数  除去开短路，再开始   可以用不到
        begin
          dd:=(rbuf[12+2*i]*256+rbuf[13+2*i]);
          if (StrToInt(edt11.Text)mod 10)=1 then begin    //短信息打印
            if ( NameStrZu[i]='OPEN')or(NameStrZu[i]='SHOR') then begin
              if dd=0 then  mmo5.lines[bb+i]:=NameStrZu[i]+'-'+copy('right',0,5)
              else  mmo5.lines[bb+i]:=NameStrZu[i]+'-'+copy('error',0,5);
            end else begin
              ff:=dd / strtofloat(ScaleZu[i]);
              mmo5.lines[bb+i]:=NameStrZu[i]+'-'+copy(floattostr(ff)+'     ',0,5);
            end;
          end else begin
            if ( NameStrZu[i]='OPEN')or(NameStrZu[i]='SHOR') then begin
              if dd=0 then  mmo5.lines[bb+i]:=NameStrZu[i]+'- '+copy('right',0,6)+':'+ Unitstrzu[i]
              else  mmo5.lines[bb+i]:=NameStrZu[i]+'- '+copy('error',0,6)+':'+ Unitstrzu[i];
            end else begin
              ff:=dd / strtofloat(ScaleZu[i]);
              mmo5.lines[bb+i]:=NameStrZu[i]+'- '+copy(floattostr(ff)+'      ',0,7)+':'+ Unitstrzu[i];
            end;
          end;
        end;
      end;
      if(strtoint(edt159.text))>0 then      //最后一个输出结果,最大行数在Matrix卡下
      begin
        if (rbuf[4]=0)              //结果OK
        then begin
           mmo5.lines[strtoint(edt159.text)-1]:='结果: PASS';       //RS:
        end else begin
           mmo5.lines[strtoint(edt159.text)-1]:='结果: FAIL';
        end;
      end;
    end;
  end;
end;

function   isfileinuse(fname   :   string   )   :   boolean;
var
    hfileres   :   hfile;     
begin
    result   :=   false;
    if   not   fileexists(fname)   then      //不存在，也认为被占用
    begin result   := True;    exit;
    end;
    hfileres   :=   createfile(pchar(fname),   generic_read   or   generic_write,0,   nil,   open_existing,file_attribute_normal,   0);
    result   :=   (hfileres   =   invalid_handle_value);
    if   not   result   then     
        closehandle(hfileres);
end;



procedure send_crcdata(len_send:integer;add_crc:SmallInt);

var
  i,crc_temp:integer;
  commflg:boolean;
  UsbBuf: array [0..64] of Byte;
  Written: Cardinal;
  ToWrite: Cardinal;
  Str: string;
  Err: DWORD;
begin
    if Assigned(CurrentDevice)and(Form_line.tmr5.Tag>9999) then      //出现过错误，需在0.5秒再检查一遍
      Form_line.tmr5.Tag:=50;

    Viewstring:='';
    commflg:=False;
    if Form_line.N34.Checked then begin
        sbuf[1]:=sbuf[1] or $80 ;                   //级联透传地址标志：D7位写1，CRC计算在内！
    end;
    if add_crc=2 then begin
      crc_temp:=uart_crc(1,len_send);
      sbuf[len_send+1]:=crc_temp mod 256;
      sbuf[len_send+2]:=crc_temp div 256;
    end;
    if Form_line.N34.Checked then begin              //PC下发标志$20和串口号，不参加CRC计算！
        if (Form_line.N34.Tag mod 100>0) then begin  //级联透传
           sbuf[1]:=$20+Form_line.N34.Tag mod 100;                  //$21代表PC机发送给主控板
           sbuf[2]:=sbuf[2]+(Form_line.N34.Tag div 1000)*$20;        //串口号（2.3.4.6）+命令
        end else begin
           sbuf[1]:=$21;                            //默认：PC下发标志$20+串口3有二级级联板；
        end;
    end;
    //-------------------发送数据解析；-------------------------------------
    if form_line.rb5.Checked and( not Form_line.chk24.checked or (Form_line.chk24.Tag>9)               //非刷新不显示
      or (modbusfuntion>0)or (modbusfun16>0)or (modbusfun16int>0) or (modbusfun02>0)or   (modbusfun03>0) or (modbusfun05>0)or (modbusfun06>0)
      or ( (sbuf[2] mod $20=$05)and(sbuf[3]=$50) ) )   //响应指令
    then begin
        if Form_line.chk24.checked then                 //有非刷新指令
            Form_line.chk24.Tag:=1;
        for i:=1 to len_send+add_crc do  begin
            viewstring:=viewstring+inttohex(sbuf[i],2)+' ';
        end;
        viewstring:='Send:'+viewstring;
      //----解析命令说明--------------------------
        case sbuf[2]mod $20 of
            $01:begin
                  viewstring:=viewstring+'[读取：测试板读写信息]';
                end;
            $02:begin
                  viewstring:=viewstring+'[读取：测试板只读状态信息]';
                end;
            $03: begin
                case sbuf[3] of
                  $10: begin
                      viewstring:=viewstring+'[读取：系统参数]';
                    end;
                  $18..$28: begin
                      viewstring:=viewstring+'[读取：短路群]';
                    end;
                  $30..$4F:
                  begin
                    viewstring:=viewstring+'[读取：测试步骤]';
                    end;
                else
                  viewstring:=viewstring+'[读取]';
                end;
            end;
            $05: begin
                case sbuf[3] of
                  $00: begin
                       case sbuf[4] of
                          $00: begin
                              viewstring:=viewstring+'[导入、导出 复位命令]';
                          end;
                          $01: begin
                              viewstring:=viewstring+'[找点命令]';
                          end;
                          $05: begin
                              viewstring:=viewstring+'[一次短路群学习命令]';
                          end;
                          $06: begin
                              viewstring:=viewstring+'[启二次短路群学习命令]';
                          end;
                          $08: begin
                              viewstring:=viewstring+'[保存系统参数命令]';
                          end;
                          $09: begin
                              viewstring:=viewstring+'[保存短路群命令]';
                          end;
                          $0B: begin
                              viewstring:=viewstring+'[保存步骤命令]';
                          end;
                          $0C: begin
                              viewstring:=viewstring+'[单步命令]';
                          end;
                          $0D: begin
                              viewstring:=viewstring+'[上翻页命令]';
                          end;
                          $0E: begin
                              viewstring:=viewstring+'[下翻页命令]';
                          end;
                          $10: begin
                              viewstring:=viewstring+'[启动命令]';
                          end;
                          $11: begin
                              viewstring:=viewstring+'[复位命令]';
                          end;
                       else
                          viewstring:=viewstring+'[无说明]';
                       end;
                  end;
                  $50: begin
                       case sbuf[4] of
                          $00: begin
                              viewstring:=viewstring+'[上传PC数据已保存]';
                          end;
                           $01: begin
                              viewstring:=viewstring+'[USB通讯正常]';
                          end;
                           $02: begin
                              viewstring:=viewstring+'[已收到上传状态]';
                          end;
                       else
                          viewstring:=viewstring+'[无说明]';
                       end;
                  end;
                else
                  viewstring:=viewstring+'[无说明]';
                end;
            end;
            $06: begin
                case sbuf[3] of
                  $00:begin
                      case sbuf[4] of
                        $41: begin
                             viewstring:=viewstring+'[测试程序选择]';
                        end;
                      else
                        viewstring:=viewstring+'[无说明]';
                      end;
                  end;
                else
                  viewstring:=viewstring+'[无说明]';
                end;
            end;
        else
           viewstring:=viewstring+'[无说明]';
        end;

      //-------ASCII吗显示-------------------
        //viewstring:=viewstring+'@';
        for i:=1 to len_send+add_crc do  begin
            viewstring:=viewstring+char(sbuf[i]);
        end;
        if form_line.chk38.Checked then
          viewstring:=GetTimemS+viewstring;
        form_line.Memo1.Lines.add(viewstring);
        Viewstring:='';
    end;
    //-------------------通过不同接口输出-----------------------------------
    if Assigned(CurrentDevice)and(Form_line.edt96.Text='1')and (not Form_line.ts18.Showing or not Form_line.ts3.Showing ){(Form_line.shp4.Brush.Color<>clGreen)} then begin //使能HIDen，且串口助手未开启
        ToWrite := CurrentDevice.Caps.OutputReportByteLength;
        for i := 1 to 64 do
          UsbBuf[i]:=sbuf[i];
        UsbBuf[0]:=0;
        UsbBuf[64 ]:=64;
        if not CurrentDevice.WriteFile(UsbBuf, ToWrite, Written) then
        begin
          Err := GetLastError;
          Form_line.AddToHistory(Format('WRITE ERROR: %s (%x)', [SysErrorMessage(Err), Err]));
          CurrentDevice := nil;
          //ShowMessage('USB write Error!通信错误，请重连USB！');
          Form_line.lbl194.Visible:=True;Form_line.lbl194.Caption:='USB通信错误';Form_line.lbl194.Font.Color:=clRed;
          if (modbusfuntion=$3F)or(modbusfuntion=$41)or(modbusfuntion=$44) then
            modbusfuntion:=$44
          else if (modbusfuntion=$4F)or(modbusfuntion=$51)or(modbusfuntion=$54) then
            modbusfuntion:=$54;            //复位
          //else                          //  modbusfuntion:=0;
          
          Form_line.Stat1.Panels[0].Text :='Port:USB ERROR';
          Form_line.Stat1.Panels[1].Text :=Format('WRITE ERROR: %s (%x)', [SysErrorMessage(Err), Err]);
          usb_busyT:=31;
        end else  begin
          Str := Format('W %.2x  ', [UsbBuf[0]]);
          for i := 1 to Written-1 do
            Str := Str + Format('%.2x ', [UsbBuf[i]]);
          Form_line.AddToHistory(Str);
          commflg:=True;
        end;
     end else if(not Form_line.SpeedButton12.Enabled) then begin   //串口开启
       if Form_line.Comm1.baudrate<=9600 then begin
          Form_line.tmr1.interval:=100+Form_line.tmr1.interval;
       end else  if Form_line.Comm1.baudrate<=38400 then begin
          Form_line.tmr1.interval:=50+Form_line.tmr1.interval;
       end;
       if (form_line.comm1.writecommdata(@sbuf,len_send+add_crc))  then
       begin
          commflg:=True;
       end;
       if commflg then  begin
          for i:=1 to len_send+add_crc do  begin
             viewstring:=viewstring+inttohex(sbuf[i],2)+' ';
          end;
          viewstring:='Send:'+viewstring;
          if form_line.chk38.Checked then
            viewstring:=GetTimemS+viewstring;
          if not form_line.rb5.Checked  then   //串口调试因考虑数据解析显示，故此处不显示发送内容
            form_line.Memo1.Lines.add(viewstring);
       end else if not commflg then  begin      //发送不成功，关闭串口
          Form_line.SpeedButton12.Enabled:=True;
          Form_line.Comm1.StopComm;
          messagedlg('Comm Send Fail!',mterror,[mbyes],0);      //tmr1.Enabled:=False;
       end;
    end else if true then begin
        for i:=1 to len_send+add_crc do  begin
            viewstring:=viewstring+inttohex(sbuf[i],2)+' ';
        end;
        viewstring:='Send:'+viewstring;
        if form_line.chk38.Checked then
          viewstring:=GetTimemS+viewstring;
        if not form_line.rb5.Checked then
          form_line.Memo1.Lines.add(viewstring);
    end;;
end;

procedure createRowEdit(row:SmallInt);
var i,temp:SmallInt;

begin
  ///////////////////生产步骤 表格 仅作参考！
  try
    for i:=0 to VIRLCROWS  do         //[0]备用
    begin
     // xyinput[i].free;xyspeed[i].free;xylength[i].free;xycircle[i].free;
    //  xystatue[i].free;xyalarm[i].free;xymachine[i].free;
     // norecnum[i]:=0;
     // speedstatue[i]:=0;
      xyout[i].free;
      zu1edit[i].Free;zu2edit[i].Free;zu3edit[i].Free;zu4edit[i].Free;zu5edit[i].Free;
      zu6edit[i].Free;zu7edit[i].Free;zu8edit[i].Free;zu9edit[i].Free;zu10edit[i].Free;
      zu11edit[i].Free;zu12edit[i].Free;zu13edit[i].Free;zu14edit[i].Free;zu15edit[i].Free;
      zu16edit[i].Free;  zu17edit[i].Free;  zu18edit[i].Free;zu19edit[i].Free;zu20edit[i].Free;     // 用到19个
    end;
  finally

    temp:=0;
    for i:=1 to VIRLCROWS do
    begin
      if i=60 then begin
        Form_line.btn121.top:=30+52*strtoint(Form_line.edt121.Text)-40;
        Form_line.btn121.BringToFront;    //Break;
        Form_line.btn121.Enabled:=false;
      end;
      
      zu1edit[i+40*temp]:=tedit.create(Form_line);      //序号
      zu1edit[i+40*temp].parent:=Form_line.panel1;
      zu1edit[i+40*temp].width:=50;
      zu1edit[i+40*temp].Left:=0;
      zu1edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      zu1edit[i+40*temp].OnClick:= Form_line.stepIntClick;

      zu2edit[i+40*temp]:=tedit.create(Form_line);      //名称
      zu2edit[i+40*temp].parent:=Form_line.panel1;
      zu2edit[i+40*temp].width:=60;
      zu2edit[i+40*temp].Left:=55;
      zu2edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      zu2edit[i+40*temp].Font.Name:=Form_line.edt121.font.Name;
      //zu2edit[i].CharCase := ecUpperCase;
      zu2edit[i+40*temp].OnClick:= Form_line.stepNamestrClick;

      zu3edit[i+40*temp]:=tedit.create(Form_line);      //标准值
      zu3edit[i+40*temp].parent:=Form_line.panel1;
      zu3edit[i+40*temp].width:=57;
      zu3edit[i+40*temp].Left:=115;
      zu3edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      zu3edit[i+40*temp].OnClick:= Form_line.stepIntClick;


      zu4edit[i+40*temp]:=tedit.create(Form_line);      //测量值
      zu4edit[i+40*temp].parent:=Form_line.panel1;
      zu4edit[i+40*temp].width:=64;
      zu4edit[i+40*temp].Left:=173;
      zu4edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      if readchis('Comm Para', 'DebugMode')='2' then
        zu4edit[i+40*temp].OnClick:= Form_line.stepIntClick
      else
        zu4edit[i+40*temp].Enabled:=False;//zu4edit[i+40*temp].OnClick:= IntClick;
      zu4edit[i+40*temp].Color:=clCream;//clYellow;
      zu4edit[i+40*temp].Font.Style:=[fsbold];

      zu5edit[i+40*temp]:=tedit.create(Form_line);      //上限%
      zu5edit[i+40*temp].parent:=Form_line.panel1;
      zu5edit[i+40*temp].width:=57;
      zu5edit[i+40*temp].Left:=240;
      zu5edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      zu5edit[i+40*temp].OnClick:= Form_line.stepIntClick;

      zu6edit[i+40*temp]:=tedit.create(Form_line);      //下限%
      zu6edit[i+40*temp].parent:=Form_line.panel1;
      zu6edit[i+40*temp].width:=57;
      zu6edit[i+40*temp].Left:=300;
      zu6edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      zu6edit[i+40*temp].OnClick:= Form_line.stepIntClick;

      zu7edit[i+40*temp]:=tedit.create(Form_line);      //高点
      zu7edit[i+40*temp].parent:=Form_line.panel1;
      zu7edit[i+40*temp].width:=55;
      zu7edit[i+40*temp].Left:=360;
      zu7edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      zu7edit[i+40*temp].OnClick:= Form_line.stepIntClick;


      zu8edit[i+40*temp]:=tedit.create(Form_line);      //低点
      zu8edit[i+40*temp].parent:=Form_line.panel1;
      zu8edit[i+40*temp].width:=55;
      zu8edit[i+40*temp].Left:=420;
      zu8edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      zu8edit[i+40*temp].OnClick:= Form_line.stepIntClick;

      zu9edit[i+40*temp]:=tedit.create(Form_line);      //延时
      zu9edit[i+40*temp].parent:=Form_line.panel1;
      zu9edit[i+40*temp].width:=55;
      zu9edit[i+40*temp].Left:=480;
      zu9edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      zu9edit[i+40*temp].OnClick:= Form_line.stepIntClick;

      zu10edit[i+40*temp]:=tedit.create(Form_line);      //模式
      zu10edit[i+40*temp].parent:=Form_line.panel1;
      zu10edit[i+40*temp].width:=65;
      zu10edit[i+40*temp].Left:=535;
      zu10edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      zu10edit[i+40*temp].Font.Style:=zu10edit[i+40*temp].Font.Style+[fsBold];
      zu10edit[i+40*temp].Font.Name:=Form_line.edt121.font.Name;
      zu10edit[i+40*temp].OnClick:= Form_line.stepModestrClick;

      zu11edit[i+40*temp]:=tedit.create(Form_line);      //K
      zu11edit[i+40*temp].parent:=Form_line.panel1;
      zu11edit[i+40*temp].width:=55;
      zu11edit[i+40*temp].Left:=600;
      zu11edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      zu11edit[i+40*temp].OnClick:= Form_line.stepIntClick;

      zu12edit[i+40*temp]:=tedit.create(Form_line);      //B
      zu12edit[i+40*temp].parent:=Form_line.panel1;
      zu12edit[i+40*temp].width:=57;
      zu12edit[i+40*temp].Left:=660;
      zu12edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      zu12edit[i+40*temp].OnClick:= Form_line.stepIntClick;

      zu13edit[i+40*temp]:=tedit.create(Form_line);      //上限%2
      zu13edit[i+40*temp].parent:=Form_line.panel1;
      zu13edit[i+40*temp].width:=55;
      zu13edit[i+40*temp].Left:=720;
      zu13edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      zu13edit[i+40*temp].OnClick:= Form_line.stepIntClick;


      zu14edit[i+40*temp]:=tedit.create(Form_line);      //下限%2
      zu14edit[i+40*temp].parent:=Form_line.panel1;
      zu14edit[i+40*temp].width:=55;
      zu14edit[i+40*temp].Left:=780;
      zu14edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      zu14edit[i+40*temp].OnClick:= Form_line.stepIntClick;

      zu15edit[i+40*temp]:=tedit.create(Form_line);      //高点 2
      zu15edit[i+40*temp].parent:=Form_line.panel1;
      zu15edit[i+40*temp].width:=55;
      zu15edit[i+40*temp].Left:=840;
      zu15edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      zu15edit[i+40*temp].OnClick:= Form_line.stepIntClick;


      zu16edit[i+40*temp]:=tedit.create(Form_line);      //低点 2
      zu16edit[i+40*temp].parent:=Form_line.panel1;
      zu16edit[i+40*temp].width:=55;
      zu16edit[i+40*temp].Left:=900;
      zu16edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      zu16edit[i+40*temp].OnClick:= Form_line.stepIntClick;

      zu17edit[i+40*temp]:=tedit.create(Form_line);      //备注
      zu17edit[i+40*temp].parent:=Form_line.panel1;
      zu17edit[i+40*temp].width:=250;
      zu17edit[i+40*temp].Left:=960;
      zu17edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      zu17edit[i+40*temp].Font.Name:=Form_line.edt121.font.Name;
      zu17edit[i+40*temp].OnClick:= Form_line.stepNoteClick;

      zu1edit[i+40*temp].OnKeydown:= Form_line.stepParaKeydown;
      zu2edit[i+40*temp].OnKeydown:= Form_line.stepParaKeydown;
      zu3edit[i+40*temp].OnKeydown:= Form_line.stepParaKeydown;
      zu4edit[i+40*temp].OnKeydown:= Form_line.stepParaKeydown;
      zu5edit[i+40*temp].OnKeydown:= Form_line.stepParaKeydown;
      zu6edit[i+40*temp].OnKeydown:= Form_line.stepParaKeydown;
      zu7edit[i+40*temp].OnKeydown:= Form_line.stepParaKeydown;
      zu8edit[i+40*temp].OnKeydown:= Form_line.stepParaKeydown;
      zu9edit[i+40*temp].OnKeydown:= Form_line.stepParaKeydown;
      zu10edit[i+40*temp].OnKeydown:= Form_line.stepParaKeydown;
      zu11edit[i+40*temp].OnKeydown:= Form_line.stepParaKeydown;
      zu12edit[i+40*temp].OnKeydown:= Form_line.stepParaKeydown;
      zu13edit[i+40*temp].OnKeydown:= Form_line.stepParaKeydown;
      zu14edit[i+40*temp].OnKeydown:= Form_line.stepParaKeydown;
      zu15edit[i+40*temp].OnKeydown:= Form_line.stepParaKeydown;
      zu16edit[i+40*temp].OnKeydown:= Form_line.stepParaKeydown;
      zu17edit[i+40*temp].OnKeydown:= Form_line.stepParaKeydown;

      zu1edit[i+40*temp].OnContextPopup:= Form_line.stepModePopup;
      zu2edit[i+40*temp].OnContextPopup:= Form_line.stepModePopup;
      zu3edit[i+40*temp].OnContextPopup:= Form_line.stepModePopup;
      zu4edit[i+40*temp].OnContextPopup:= Form_line.stepModePopup;
      zu5edit[i+40*temp].OnContextPopup:= Form_line.stepModePopup;
      zu6edit[i+40*temp].OnContextPopup:= Form_line.stepModePopup;
      zu7edit[i+40*temp].OnContextPopup:= Form_line.stepModePopup;
      zu8edit[i+40*temp].OnContextPopup:= Form_line.stepModePopup;
      zu9edit[i+40*temp].OnContextPopup:= Form_line.stepModePopup;
      zu10edit[i+40*temp].OnContextPopup:= Form_line.stepModePopup;
      zu11edit[i+40*temp].OnContextPopup:= Form_line.stepModePopup;
      zu12edit[i+40*temp].OnContextPopup:= Form_line.stepModePopup;
      zu13edit[i+40*temp].OnContextPopup:= Form_line.stepModePopup;
      zu14edit[i+40*temp].OnContextPopup:= Form_line.stepModePopup;
      zu15edit[i+40*temp].OnContextPopup:= Form_line.stepModePopup;
      zu16edit[i+40*temp].OnContextPopup:= Form_line.stepModePopup;
      zu17edit[i+40*temp].OnContextPopup:= Form_line.stepModePopup;
      zu1edit[i+40*temp].ReadOnly:=true;
      zu2edit[i+40*temp].ReadOnly:=true;
      zu3edit[i+40*temp].ReadOnly:=true;
      zu4edit[i+40*temp].ReadOnly:=true;
      zu5edit[i+40*temp].ReadOnly:=true;
      zu6edit[i+40*temp].ReadOnly:=true;
      zu7edit[i+40*temp].ReadOnly:=true;
      zu8edit[i+40*temp].ReadOnly:=true;
      zu9edit[i+40*temp].ReadOnly:=true;
      zu10edit[i+40*temp].ReadOnly:=true;
      zu11edit[i+40*temp].ReadOnly:=true;
      zu12edit[i+40*temp].ReadOnly:=true;
      zu13edit[i+40*temp].ReadOnly:=true;
      zu14edit[i+40*temp].ReadOnly:=true;
      zu15edit[i+40*temp].ReadOnly:=true;
      zu16edit[i+40*temp].ReadOnly:=true;
      zu17edit[i+40*temp].ReadOnly:=true;


      zu18edit[i+40*temp]:=tedit.create(Form_line);      //单位测量值s2
      zu18edit[i+40*temp].parent:=Form_line.panel1;
      zu18edit[i+40*temp].width:=120;
      zu18edit[i+40*temp].Left:=1220;
      zu18edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      zu18edit[i+40*temp].Enabled:=False;
      zu18edit[i+40*temp].OnClick:= Form_line.ExpertClick;  //
      zu18edit[i+40*temp].Color:=clYellow;

      zu19edit[i+40*temp]:=tedit.create(Form_line);      //测量值误差
      zu19edit[i+40*temp].parent:=Form_line.panel1;
      zu19edit[i+40*temp].width:=80;
      zu19edit[i+40*temp].Left:=1350;
      zu19edit[i+40*temp].Top:=30+I*strtoint(Form_line.edt121.Text)-40;
      zu19edit[i+40*temp].Enabled:=False;//
      zu19edit[i+40*temp].OnClick:= Form_line.ExpertClick;;
      zu19edit[i+40*temp].Color:=clYellow;

      if(Readchis('Result','top5')<>'0')  then begin
        zu1edit[i+40*temp].ShowHint:=True;
        zu2edit[i+40*temp].ShowHint:=True;
        zu3edit[i+40*temp].ShowHint:=True;
        zu4edit[i+40*temp].ShowHint:=True; zu4edit[i+40*temp].Enabled:=true;
        zu5edit[i+40*temp].ShowHint:=True;
        zu6edit[i+40*temp].ShowHint:=True;
        zu7edit[i+40*temp].ShowHint:=True;
        zu8edit[i+40*temp].ShowHint:=True;
        zu9edit[i+40*temp].ShowHint:=True;
        zu10edit[i+40*temp].ShowHint:=True;
        zu11edit[i+40*temp].ShowHint:=True;
        zu12edit[i+40*temp].ShowHint:=True;
        zu13edit[i+40*temp].ShowHint:=True;
        zu14edit[i+40*temp].ShowHint:=True;
        zu15edit[i+40*temp].ShowHint:=True;
        zu16edit[i+40*temp].ShowHint:=True;
        zu17edit[i+40*temp].ShowHint:=True;
        zu18edit[i+40*temp].ShowHint:=True;
        zu19edit[i+40*temp].ShowHint:=True;
      end;
    end;

      i:=0;
      xyout[0+6*i]:=tlabel.create(Form_line);
      xyout[0+6*i].parent:=Form_line.panel2;
      xyout[0+6*i].width:=40;
      xyout[0+6*i].Left:=zu1edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //0;
      xyout[0+6*i].Top:=80;
      xyout[0+6*i].Font.Size:=8+2;
      xyout[0+6*i].Caption:='Nu序号';
      xyout[0+6*i].Font.Name:='宋体';

      xyout[1+6*i]:=tlabel.create(Form_line);
      xyout[1+6*i].parent:=Form_line.panel2;
      xyout[1+6*i].width:=40;
      xyout[1+6*i].Left:=zu2edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //60;
      xyout[1+6*i].Top:=80;
      xyout[1+6*i].Font.Size:=8+2;
      xyout[1+6*i].Caption:='Na步骤名'+'';
      xyout[1+6*i].Font.Name:='宋体';

      xyout[2+6*i]:=tlabel.create(Form_line);
      xyout[2+6*i].parent:=Form_line.panel2;
      xyout[2+6*i].width:=40;
      xyout[2+6*i].Left:=zu3edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //120;
      xyout[2+6*i].Top:=80;
      xyout[2+6*i].Font.Size:=8+1;
      xyout[2+6*i].Caption:='St标准值-'+'';
      xyout[2+6*i].Font.Name:='宋体';

      xyout[3+6*i]:=tlabel.create(Form_line);
      xyout[3+6*i].parent:=Form_line.panel2;
      xyout[3+6*i].width:=40;
      xyout[3+6*i].Left:=zu4edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //180;
      xyout[3+6*i].Top:=80;
      xyout[3+6*i].Font.Size:=8+1;
      xyout[3+6*i].Caption:='Me测控值-'+'';
      xyout[3+6*i].Font.Name:='宋体';

      xyout[4+6*i]:=tlabel.create(Form_line);
      xyout[4+6*i].parent:=Form_line.panel2;
      xyout[4+6*i].width:=40;
      xyout[4+6*i].Left:=zu5edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //240;
      xyout[4+6*i].Top:=80;
      xyout[4+6*i].Font.Size:=8+2;
      xyout[4+6*i].Caption:='Up上限%-'+'';
      xyout[4+6*i].Font.Name:='宋体';

      xyout[5+6*i]:=tlabel.create(Form_line);
      xyout[5+6*i].parent:=Form_line.panel2;
      xyout[5+6*i].width:=40;
      xyout[5+6*i].Left:=zu6edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //300;
      xyout[5+6*i].Top:=80;
      xyout[5+6*i].Font.Size:=8+2;
      xyout[5+6*i].Caption:='Lo下限%-'+'';
      xyout[5+6*i].Font.Name:='宋体';

      xyout[6+6*i]:=tlabel.create(Form_line);
      xyout[6+6*i].parent:=Form_line.panel2;
      xyout[6+6*i].width:=40;
      xyout[6+6*i].Left:=zu7edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //360;
      xyout[6+6*i].Top:=80;
      xyout[6+6*i].Font.Size:=8+2;
      xyout[6+6*i].Caption:='Hp高点'+'';
      xyout[6+6*i].Font.Name:='宋体';

      xyout[7+6*i]:=tlabel.create(Form_line);
      xyout[7+6*i].parent:=Form_line.panel2;
      xyout[7+6*i].width:=40;
      xyout[7+6*i].Left:=zu8edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //420;
      xyout[7+6*i].Top:=80;
      xyout[7+6*i].Font.Size:=8+2;
      xyout[7+6*i].Caption:='Lp低点'+'';
      xyout[7+6*i].Font.Name:='宋体';

      xyout[8+6*i]:=tlabel.create(Form_line);
      xyout[8+6*i].parent:=Form_line.panel2;
      xyout[8+6*i].width:=40;
      xyout[8+6*i].Left:=zu9edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //480;
      xyout[8+6*i].Top:=80;
      xyout[8+6*i].Font.Size:=8+2;
      xyout[8+6*i].Caption:='Tm前延时'+'';
      xyout[8+6*i].Font.Name:='宋体';

      xyout[9+6*i]:=tlabel.create(Form_line);
      xyout[9+6*i].parent:=Form_line.panel2;
      xyout[9+6*i].width:=40;
      xyout[9+6*i].Left:=zu10edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //540;
      xyout[9+6*i].Top:=80;
      xyout[9+6*i].Font.Size:=12;
      xyout[9+6*i].Font.Style:=xyout[9+6*i].Font.Style+[fsBold];
      xyout[9+6*i].Caption:='Mo模式'+'';
      xyout[9+6*i].Font.Name:='宋体';

      xyout[10+6*i]:=tlabel.create(Form_line);
      xyout[10+6*i].parent:=Form_line.panel2;
      xyout[10+6*i].width:=40;
      xyout[10+6*i].Left:=zu11edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //600;
      xyout[10+6*i].Top:=80;
      xyout[10+6*i].Font.Size:=8+2;
      xyout[10+6*i].Caption:='K比例%'+'';
      xyout[10+6*i].Font.Name:='宋体';

      xyout[11+6*i]:=tlabel.create(Form_line);
      xyout[11+6*i].parent:=Form_line.panel2;
      xyout[11+6*i].width:=40;
      xyout[11+6*i].Left:=zu12edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //660;
      xyout[11+6*i].Top:=80;
      xyout[11+6*i].Font.Size:=8+2;
      xyout[11+6*i].Caption:='B偏移-'+'';
      xyout[11+6*i].Font.Name:='宋体';

      xyout[12+6*i]:=tlabel.create(Form_line);
      xyout[12+6*i].parent:=Form_line.panel2;
      xyout[12+6*i].width:=40;
      xyout[12+6*i].Left:=zu13edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //720;
      xyout[12+6*i].Top:=80;
      xyout[12+6*i].Font.Size:=8;
      xyout[12+6*i].Caption:='Ps平均/次'+'';
      xyout[12+6*i].Font.Name:='宋体';

      xyout[13+6*i]:=tlabel.create(Form_line);
      xyout[13+6*i].parent:=Form_line.panel2;
      xyout[13+6*i].width:=40;
      xyout[13+6*i].Left:=zu14edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //780;
      xyout[13+6*i].Top:=80;
      xyout[13+6*i].Font.Size:=8;
      xyout[13+6*i].Caption:='Cs联板号'+'';
      xyout[13+6*i].Font.Name:='宋体';

      xyout[14+6*i]:=tlabel.create(Form_line);
      xyout[14+6*i].parent:=Form_line.panel2;
      xyout[14+6*i].width:=40;
      xyout[14+6*i].Left:=zu15edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //840;
      xyout[14+6*i].Top:=80;
      xyout[14+6*i].Font.Size:=8+2;
      xyout[14+6*i].Caption:='H2高点'+'';
      xyout[14+6*i].Font.Name:='宋体';

      xyout[15+6*i]:=tlabel.create(Form_line);
      xyout[15+6*i].parent:=Form_line.panel2;
      xyout[15+6*i].width:=40;
      xyout[15+6*i].Left:=zu16edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //900;
      xyout[15+6*i].Top:=80;
      xyout[15+6*i].Font.Size:=8+2;
      xyout[15+6*i].Caption:='L2低点'+'';
      xyout[15+6*i].Font.Name:='宋体';

      xyout[16+6*i]:=tlabel.create(Form_line);
      xyout[16+6*i].parent:=Form_line.panel2;
      xyout[16+6*i].width:=40;
      xyout[16+6*i].Left:=zu17edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //960;
      xyout[16+6*i].Top:=80;
      xyout[16+6*i].Font.Size:=8+2;
      xyout[16+6*i].Caption:='Notes备注(可中文)'+'';
      xyout[16+6*i].Font.Name:='宋体';

      xyout[17+6*i]:=tlabel.create(Form_line);
      xyout[17+6*i].parent:=Form_line.panel2;
      xyout[17+6*i].width:=40;
      xyout[17+6*i].Left:=zu18edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //1220;
      xyout[17+6*i].Top:=80;
      xyout[17+6*i].Font.Size:=8+2;
      xyout[17+6*i].Caption:='m测量值(！专家预判)'+'';
      xyout[17+6*i].Font.Name:='宋体';

      xyout[18+6*i]:=tlabel.create(Form_line);
      xyout[18+6*i].parent:=Form_line.panel2;
      xyout[18+6*i].width:=40;
      xyout[18+6*i].Left:=zu19edit[1].Left-form_line.panel1.HorzScrollBar.Position;  //1350;
      xyout[18+6*i].Top:=80;
      xyout[18+6*i].Font.Size:=8+2;
      xyout[18+6*i].Caption:='Err%误差'+'';

    for i:=0 to 13 do
    begin
      syslabel[i]:=tlabel.create(Form_line);
      syslabel[i].parent:=Form_line.grp3;
      syslabel[i].width:=40;
      syslabel[i].Left:=60*i;
      syslabel[i].Top:=25;
      syslabel[i].Font.Size:=8;
    end;
    syslabel[0].Caption:='$对比度';
    syslabel[1].Caption:='$找点PINS';
    syslabel[2].Caption:='$找点阀值';
    syslabel[3].Caption:='O/S*PINS';
    syslabel[4].Caption:='O/S*阀值';
    syslabel[5].Caption:='@固件版本';
    syslabel[6].Caption:='485地址';
    syslabel[7].Caption:='O/S*总时';
    syslabel[8].Caption:='O/S*F1点';
    syslabel[9].Caption:='O/S*充时';
    syslabel[10].Caption:='O/S*F2点';
    syslabel[11].Caption:='F2延时';
    syslabel[12].Caption:='机台';
    syslabel[13].Caption:='机种名';
    for i:=0 to 13 do
    begin
        sysedit[i]:=tedit.create(Form_line);      //序号
        sysedit[i].parent:=Form_line.grp3;
        sysedit[i].width:=58;
        sysedit[i].Left:=60*i;
        sysedit[i].Top:=60;
        sysedit[i].Font.Size:=13;
        sysedit[i].Font.Style:=sysedit[i].Font.Style+ [fsbold];
        sysedit[i].OnClick:= Form_line.sysIntClick;

        sysedit[i].OnContextPopup:= Form_line.sysParaPopup;
        sysedit[i].ReadOnly:=true;
        sysedit[i].OnKeydown:= Form_line.sysParaKeydown;
    end;
   // sysedit[0].Color:=clAqua;
   // sysedit[1].Color:=clAqua;
   // sysedit[2].Color:=clAqua;
    sysedit[0].Enabled:=False;    //对比度
    sysedit[6].Enabled:=False;    //485地址
    sysedit[8].Enabled:=False;    //F1点
    sysedit[11].Enabled:=False;   //F2点延时
    sysedit[0].Color:=clSkyBlue;
    sysedit[6].Color:=clSkyBlue;//clGreen;
    sysedit[8].Color:=clSkyBlue;
    sysedit[11].Color:=clSkyBlue;

    sysedit[5].Enabled:=False;    //固件版本号
    sysedit[5].Color:=clYellow;
    sysedit[5].Visible:=False;    //仅存放主版本号，用于固件版本的判断；
    Form_line.edt130.Enabled:=sysedit[5].Enabled;
    Form_line.edt130.Color:=sysedit[5].Color;
    Form_line.edt130.Left:=sysedit[5].Left;
    Form_line.edt130.Top:=sysedit[5].Top;
    Form_line.edt130.parent:=sysedit[5].parent;
    Form_line.edt130.width:=sysedit[5].width+5;
    Form_line.edt130.Height:=sysedit[5].Height;

    sysedit[12].Color:=clGreen;
    sysedit[13].Color:=clGreen;

    sysedit[13].Width:=100;    //sysedit[13].CharCase := ecUpperCase;
    sysedit[13].OnClick:=Form_line.sysStrClick;

  end;
end;

//firstClrFlag & 01,则先清空所有名称。
//firstClrFlag & 02,更新名称,[Name->Redef].Name有高优先级。
//firstClrFlag & 10,则先清空所有单位。
//firstClrFlag & 20,更新单位,[Name->Redef].Unit有高优先级。

//firstClrFlag & 0100,则先清空所有下限。
//firstClrFlag & 0200,更新下限,[Name->Redef].Lower有高优先级。
//firstClrFlag & 1000,则先清空所有上限。
//firstClrFlag & 2000,更新上限,[Name->Redef].Upper有高优先级。
//原则上，config命名优先级高于FCT主板
//两处主要的应用流程：
//1.文件表头生成时先读取config.ini有无定义；
//2.接收到FCT主板返回的Name和Unit数据，如果字符串为空则替代；
//3.文件表头更新，如果config.ini有定义则优先选用
//4.更新Namecount须谨慎！！！！
//5.随数组变大，执行时间会变长，影响USB加载的时间！
function GetNameUnitFromConfig(firstClrFlag:integer):integer;
var
    nrf_dat_config: Tinifile;
    ini_path: string;
    i,temp,ppos:integer;
    Getlens:SmallInt;
begin
    ini_path := ExtractFilePath(application.ExeName) + 'config.ini';
    try
      nrf_dat_config := Tinifile.Create(ini_path);    //关联INI文件    ////机种参数

      if (form_line.edt77.text='') then
        form_line.edt77.text:='-';   //不能为空

      if (firstClrFlag=$02)or(firstClrFlag=$20) then    //加载时调用，减少执行时间！
        Getlens:=NameCount
      else
        Getlens:=UpStepMax;
      temp:=0;    //开启软件，从config读取名称数量
      if (nrf_dat_config.Readstring('Model Para', 'enPN', '')<>'') then begin   //多机种设置，参数分组
        for i:=1 to Getlens do
        begin
          if (firstClrFlag and $01)<>0 then   NameStrZu[i]:='';
          if (firstClrFlag and $10)<>0 then   UnitStrZu[i]:='';

          if ( (firstClrFlag and $02)<>0 ) and(nrf_dat_config.Readstring(form_line.edt77.text, 'Name'+inttostr(i), '')<>'') then      //不为空
          begin
              NameStrZu[i]:= nrf_dat_config.Readstring(form_line.edt77.text, 'Name'+inttostr(i), '');
              temp:=temp+1;
          end;
          if ( (firstClrFlag and $02)<>0 ) and(nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Name', '')<>'') then      //不为空
          begin
              NameStrZu[i]:= nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Name', '');
          end;

          if ( (firstClrFlag and $20)<>0 ) and(nrf_dat_config.Readstring(form_line.edt77.text, 'Unit'+inttostr(i), '')<>'') then      //不为空
          begin
              UnitStrZu[i]:= nrf_dat_config.Readstring(form_line.edt77.text, 'Unit'+inttostr(i), '');
          end;
          if ( (firstClrFlag and $20)<>0 ) and(nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Unit', '')<>'') then      //不为空
          begin
              UnitStrZu[i]:= nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Unit', '');
          end;

          if ( (firstClrFlag and $20)<>0 ) and(nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Scale', '')<>'') then      //不为空
          begin
              ScaleZu[i]:= nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Scale', '');
          end;


          if (firstClrFlag and $0100)<>0 then   LowerZu[i]:='';
          if (firstClrFlag and $1000)<>0 then   UpperZu[i]:='';

          if ( (firstClrFlag and $0200)<>0 ) and(nrf_dat_config.Readstring(form_line.edt77.text, 'Lower'+inttostr(i), '')<>'') then      //不为空
          begin
              LowerZu[i]:= nrf_dat_config.Readstring(form_line.edt77.text, 'Lower'+inttostr(i), '');
          end;
          if ( (firstClrFlag and $0200)<>0 ) and(nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Lower', '')<>'') then      //不为空
          begin
              LowerZu[i]:= nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Lower', '');
          end;

          if ( (firstClrFlag and $2000)<>0 ) and(nrf_dat_config.Readstring(form_line.edt77.text, 'Upper'+inttostr(i), '')<>'') then      //不为空
          begin
              UpperZu[i]:= nrf_dat_config.Readstring(form_line.edt77.text, 'Upper'+inttostr(i), '');
          end;
          if ( (firstClrFlag and $2000)<>0 ) and(nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Upper', '')<>'') then      //不为空
          begin
              UpperZu[i]:= nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Upper', '');
          end;
        end;
        if (Form_line.edt62.Text='24')or(Form_line.edt62.text='25') then begin
          if(nrf_dat_config.Readstring(form_line.edt77.text, 'conductorPos', '')<>'') then
            ppos:=strtoint(nrf_dat_config.Readstring(form_line.edt77.text, 'conductorPos', ''))
          else
            ppos:=temp+1;
        end;
      end else begin
        ////机种参数
        for i:=1 to Getlens do
        begin
          if (firstClrFlag and $01)<>0 then   NameStrZu[i]:='';
          if (firstClrFlag and $10)<>0 then   UnitStrZu[i]:='';

          if ( (firstClrFlag and $02)<>0 ) and(nrf_dat_config.Readstring('Model Para', 'Name'+inttostr(i), '')<>'') then      //不为空
          begin
              NameStrZu[i]:= nrf_dat_config.Readstring('Model Para', 'Name'+inttostr(i), '');
              //SLINE_head := SLINE_head +NameStrZu[i]+sepator; //',' ;
              temp:=temp+1;
          end;
          if ( (firstClrFlag and $02)<>0 ) and(nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Name', '')<>'') then      //不为空
          begin
              NameStrZu[i]:= nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Name', '');
          end;

          if ( (firstClrFlag and $20)<>0 ) and(nrf_dat_config.Readstring('Model Para', 'Unit'+inttostr(i), '')<>'') then      //不为空
          begin
              UnitStrZu[i]:= nrf_dat_config.Readstring('Model Para', 'Unit'+inttostr(i), '');
              //SLINE_head2 := SLINE_head2 +UnitStrZu[i]+sepator; //',' ;
          end;
          if ( (firstClrFlag and $20)<>0 ) and(nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Unit', '')<>'') then      //不为空
          begin
              UnitStrZu[i]:= nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Unit', '');
          end;

          if ( (firstClrFlag and $20)<>0 ) and(nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Scale', '')<>'') then      //不为空
          begin
              ScaleZu[i]:= nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Scale', '');
          end;

          if (firstClrFlag and $0100)<>0 then   LowerZu[i]:='';
          if (firstClrFlag and $1000)<>0 then   UpperZu[i]:='';

          if ( (firstClrFlag and $0200)<>0 ) and(nrf_dat_config.Readstring('Model Para', 'Lower'+inttostr(i), '')<>'') then      //不为空
          begin
              LowerZu[i]:= nrf_dat_config.Readstring('Model Para', 'Lower'+inttostr(i), '');
          end;
          if ( (firstClrFlag and $0200)<>0 ) and(nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Lower', '')<>'') then      //不为空
          begin
              LowerZu[i]:= nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Lower', '');
          end;

          if ( (firstClrFlag and $2000)<>0 ) and(nrf_dat_config.Readstring('Model Para', 'Upper'+inttostr(i), '')<>'') then      //不为空
          begin
              UpperZu[i]:= nrf_dat_config.Readstring('Model Para', 'Upper'+inttostr(i), '');
          end;
          if ( (firstClrFlag and $2000)<>0 ) and(nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Upper', '')<>'') then      //不为空
          begin
              UpperZu[i]:= nrf_dat_config.Readstring('Name->Redef', NameStrZu[i]+'->Upper', '');
          end;
        end;
        if (Form_line.edt62.Text='24')or(Form_line.edt62.text='25') then begin
            if(nrf_dat_config.Readstring('Model Para', 'conductorPos', '')<>'') then
              ppos:=strtoint(nrf_dat_config.Readstring('Model Para', 'conductorPos', ''))
            else
              ppos:=temp+1;
        end;

      end;
      
      if (Form_line.edt62.text='25') then begin              //jaguar专用格式   //Tesla没有trace
        for i:=1 to StrToInt(Form_line.edt81.Text) do        //ppos代表前面是NTC项目，由MCU定义；
        begin
          if NameStrZu[ppos+i-1]='' then         //没有定义导线名称
            NameStrZu[ppos+i-1]:= nrf_dat_config.Readstring('Name->Redef', 'TRACE->Name', '')+inttostr(i);
          if(nrf_dat_config.Readstring('Name->Redef', 'TRACE->Unit', '')<>'')then
            UnitStrZu[ppos+i-1]:= nrf_dat_config.Readstring('Name->Redef', 'TRACE->Unit', '');
          if(nrf_dat_config.Readstring('Name->Redef', 'TRACE->Scale', '')<>'')then
            ScaleZu[ppos+i-1]:= nrf_dat_config.Readstring('Name->Redef', 'TRACE->Scale', '');
          if(nrf_dat_config.Readstring('Name->Redef', 'TRACE->Lower', '')<>'')then
            LowerZu[ppos+i-1]:= nrf_dat_config.Readstring('Name->Redef', 'TRACE->Lower', '');
          if(nrf_dat_config.Readstring('Name->Redef', 'TRACE->Upper', '')<>'')then
            UpperZu[ppos+i-1]:= nrf_dat_config.Readstring('Name->Redef', 'TRACE->Upper', '');
        end;
        if (StrToInt(Form_line.edt81.Text)>0)and(Form_line.edt62.text<>'25') then
          NameStrZu[ppos+StrToInt(Form_line.edt81.Text)+1]:='Err...';
      end;

      if (StrToInt(Form_line.edt81.Text)>0)and(Form_line.edt62.Text='24') then begin              //不会执行！Tesla专用格式
        ppos:=0;
        NameStrZu[ppos+StrToInt(Form_line.edt81.Text)+1]:='1KV绝缘';            //只有导线数量和绝缘！
        UnitStrZu[ppos+StrToInt(Form_line.edt81.Text)+1]:='MOhm';
        LowerZu[ppos+StrToInt(Form_line.edt81.Text)+1]:='10';
        UpperZu[ppos+StrToInt(Form_line.edt81.Text)+1]:='9999';
      end;
      if (StrToInt(Form_line.edt81.Text)>0)and(Form_line.edt62.Text='25') then begin       //jaguar单PCS专用格式
        //ppos:=0;
        NameStrZu[ppos+StrToInt(Form_line.edt81.Text)+1]:='OPEN';            //只有导线数量和绝缘！
        UnitStrZu[ppos+StrToInt(Form_line.edt81.Text)+1]:='Ohm';
        LowerZu[ppos+StrToInt(Form_line.edt81.Text)+1]:='0';
        UpperZu[ppos+StrToInt(Form_line.edt81.Text)+1]:='0';
        NameStrZu[ppos+StrToInt(Form_line.edt81.Text)+2]:='SHORT';            //只有导线数量和绝缘！
        UnitStrZu[ppos+StrToInt(Form_line.edt81.Text)+2]:='Ohm';
        LowerZu[ppos+StrToInt(Form_line.edt81.Text)+2]:='0';
        UpperZu[ppos+StrToInt(Form_line.edt81.Text)+2]:='0';
        NameStrZu[ppos+StrToInt(Form_line.edt81.Text)+3]:='INSU';            //只有导线数量和绝缘！  1KV绝缘
        UnitStrZu[ppos+StrToInt(Form_line.edt81.Text)+3]:='MOhm';
        LowerZu[ppos+StrToInt(Form_line.edt81.Text)+3]:='100';
        UpperZu[ppos+StrToInt(Form_line.edt81.Text)+3]:='3000';
      end;
    finally
      nrf_dat_config.free;
    end;
    result:=temp;      //配置文件中步骤名称数量
end;
//function GetNameUnitForTesla(firstClrFlag:integer):integer;             //多机种可选，从选中的机种下面提取名称、单位

function uart_crc(start_temp,len_temp:integer):integer;
var
  i,j,crc:integer;
begin
   crc:=65535;
   for i:=start_temp to len_temp do
     begin
     crc:=crc xor sbuf[i];
     for j:= 1 to 8 do
        begin
        if crc<>((crc div 2)*2) then
            begin
            crc:=crc div 2;
            crc:=crc xor 40961;
            end
        else
           begin
           crc:=crc div 2;
           end;
        end;
     end;
  uart_crc:=crc;
end;
function rxd_crc(start_temp,len_temp:integer):integer;
var
  i,j,crc:integer;
begin
   crc:=65535;
   for i:=start_temp to len_temp do
     begin
     crc:=crc xor rbuf[i];
     for j:= 1 to 8 do
        begin
        if crc<>((crc div 2)*2) then
            begin
            crc:=crc div 2;
            crc:=crc xor 40961;
            end
        else
           begin
           crc:=crc div 2;
           end;
        end;
     end;
   rxd_crc:=crc;
end;
function IsNumberic(Vaule:String):Boolean;   //判断Vaule是不是无符号数字
var
i:integer;
begin
  result:=true;   //设置返回值为 是（真）
  Vaule:=trim(Vaule);  //去空格
  if length(Vaule)<1 then begin     //
    result:=false;  //返回值 不是（假）
  end;
  for i:=1 to length(Vaule) do  //准备循环
  begin
    if not (Vaule[i] in ['0'..'9']) then  //如果Vaule的第i个字不是0-9中的任一个
    begin
      result:=false;  //返回值 不是（假）
      exit;  //退出函数
    end;
  end;
end;
function IsSignedNumberic(Vaule:String):Boolean;   //判断Vaule是不是有符号数字
var
i:integer;
begin
  result:=false;   //设置返回值为 是（真）
  Vaule:=trim(Vaule);  //去空格
  for i:=1 to length(Vaule) do  //准备循环
  begin
    if (i=1)and( (Vaule[i]='+')or(Vaule[i]='-') ) then           //符号继续
      Continue;
    if not (Vaule[i] in ['0'..'9']) then  //如果Vaule的第i个字不是0-9中的任一个
    begin
      result:=false;  //返回值 不是（假）
      exit;  //退出函数
    end else
      result:=true;
  end;
end;
function IsSignedNumbericHex(Vaule:String):Boolean;   //判断Vaule是不是有符号数字
var
i:integer;
begin
  result:=false;   //设置返回值为 是（真）
  Vaule:=trim(Vaule);  //去空格
  for i:=1 to length(Vaule) do  //准备循环
  begin
    if (i=1)and( (Vaule[i]='+')or(Vaule[i]='-')or(Vaule[i]='x')or(Vaule[i]='X') ) then           //符号继续
      Continue;
    if not (Vaule[i] in ['0'..'9']) then  //如果Vaule的第i个字不是0-9中的任一个
    begin
      result:=false;  //返回值 不是（假）
      exit;  //退出函数
    end else
      result:=true;
  end;
end;
function IsFloatNum(Vaule:String):Boolean;   //判断Vaule是不是整数，或浮点数
var
i:integer;
begin
  result:=false;   //设置返回值为 是（真）
  Vaule:=trim(Vaule);  //去空格
  for i:=1 to length(Vaule) do  //准备循环
  begin
    if (i=1)and( (Vaule[i]='+')or(Vaule[i]='-') ) then           //符号继续
      Continue;
    if not (Vaule[i] in ['0'..'9','.']) then  //如果Vaule的第i个字不是.,0-9中的任一个
    begin
      result:=false;  //返回值 不是（假）
      exit;  //退出函数
    end else
      result:=true;
  end;
end;
function IsxFloatNum(Vaule:String):Boolean;   //判断Vaule是不是 十进制，或者x开头的16进制数
var
i:integer;
begin
  result:=false;   //设置返回值为 是（真）
  Vaule:=trim(Vaule);  //去空格
  for i:=1 to length(Vaule) do  //准备循环
  begin
    if (i=1)and( (Vaule[i]='+')or(Vaule[i]='-')or(Vaule[i]='x')or(Vaule[i]='X') ) then           //符号继续
      Continue;
    if ( (Vaule[1]='x')or(Vaule[1]='X') ) then begin
      if not (Vaule[i] in ['0'..'9','a'..'f','A'..'F','.']) then begin
        result:=false;
        exit;
      end else
        result:=true;
    end else begin
      if not (Vaule[i] in ['0'..'9','.']) then  //如果Vaule的第i个字不是.,0-9中的任一个
      begin
        result:=false;
        exit;
      end else
        result:=true;
    end;
  end;
end;
function IsHexNum(Vaule:String):Boolean;   //判断Vaule是不是16进制数
var
i:integer;
begin
  result:=false;                  //设置返回值为 是（真）
  Vaule:=trim(Vaule);             //去空格
  for i:=1 to length(Vaule) do    //准备循环
  begin
    if (i=1)and( (Vaule[i]='x')or(Vaule[i]='X') ) then           //符号继续
      Continue;
    if not (Vaule[i] in ['0'..'9','A'..'F','a'..'f']) then      //如果Vaule的第i个字不是16进制字符
    begin
      result:=false;
      exit;
    end else
      result:=true;
  end;
end;
function IsUpperCase(Vaule:String):Boolean;   //判断Vaule 是不是大写字母
var
i:integer;
begin
result:=true;  //设置返回值为 是
Vaule:=trim(Vaule);   //去空格
  for i:=1 to length(Vaule) do   //准备循环
    begin
      if not (Vaule[i] in ['A'..'Z']) then  //如果Vaule的第i个字不是A-Z中的任一个
        begin
          result:=false;  //返回值 不是
          exit;  //退出函数
        end;
    end;
end;
function IsLowerCase(Vaule:String):Boolean;  //判断Vaule 是不是小写字母
var
i:integer;
begin 
result:=true;   //设置返回值为 是
Vaule:=trim(Vaule);   //去空格
  for i:=1 to length(Vaule) do   //准备循环
    begin
      if not (Vaule[i] in ['a'..'z']) then   //如果Vaule的第i个字不是a-z中的任一个
        begin
          result:=false;   //返回值 不是
          exit;   //退出函数
        end;
    end;
end;
function IsEnCase(Vaule:String):boolean;    //判断Vaule 是不是字母
var
i:integer;
begin 
result:=true;   //设置返回值为 是
Vaule:=trim(Vaule);   //去空格
  for i:=1 to length(Vaule) do   //准备循环
    begin
      if (not (Vaule[i] in ['A'..'Z'])) or
         (not (Vaule[i] in ['a'..'z'])) then   //如果Vaule的第i个字不是A-Z或者a-z中的任一个
        begin
          result:=false;   //返回值 不是
          exit;   //退出函数
        end;
    end;
end;
function DelSpecialChar(Vaule:String):string;    //文件名 不能含有以下9种特殊字符：?*:"<>\/|
var
  ss:string;
begin
    ss:=StringReplace(Vaule, '?', ' ', [rfReplaceAll]);
    ss:=StringReplace(ss, '*', ' ', [rfReplaceAll]);
    ss:=StringReplace(ss, ':', ' ', [rfReplaceAll]);
    ss:=StringReplace(ss, '"', ' ', [rfReplaceAll]);
    ss:=StringReplace(ss, '<', ' ', [rfReplaceAll]);
    ss:=StringReplace(ss, '>', ' ', [rfReplaceAll]);
    ss:=StringReplace(ss, '\', ' ', [rfReplaceAll]);
    ss:=StringReplace(ss, '/', ' ', [rfReplaceAll]);
    result:=StringReplace(ss, '|', ' ', [rfReplaceAll]);
end;
procedure SendMessageDataToMain( AStr: String);
begin
  //SendMessageData(MainFormHandle,AStr);
end;

function Readchis(section: string; indent: string): string; // 读chisini
var
  chis: Tinifile;
begin
  chis := Tinifile.Create(extractfilepath(application.ExeName) + '\config.ini');
  result := chis.ReadString(section, indent, '');
  chis.free;
end;


function Readchis(section: string; indent: string; default: string): string;
var
  chis: Tinifile;
begin
  chis := Tinifile.Create(extractfilepath(application.ExeName) + '\config.ini');
  result := chis.ReadString(section, indent, default);
  chis.free;
end;

procedure WriteChis(section: string; indent: string; value: string); // 写ini
var
  chis: Tinifile;
begin
  chis := Tinifile.Create(extractfilepath(application.ExeName) + '\config.ini');
  chis.WriteString(section, indent, value);
  chis.free;
end;

function Readchis2(section: string; indent: string): string; // 读chisini
var
  chis: Tinifile;
begin
  chis := Tinifile.Create(extractfilepath(application.ExeName) + '\2\config.ini');
  result := chis.ReadString(section, indent, '');
  chis.free;
end;

procedure WriteChis2(section: string; indent: string; value: string); // 写ini
var
  chis: Tinifile;
begin
  chis := Tinifile.Create(extractfilepath(application.ExeName) + '\2\config.ini');
  chis.WriteString(section, indent, value);
  chis.free;
end;
function Readchis3(section: string; indent: string): string; // 读chisini
var
  chis: Tinifile;
begin
  chis := Tinifile.Create(extractfilepath(application.ExeName) + '\3\config.ini');
  result := chis.ReadString(section, indent, '');
  chis.free;
end;

procedure WriteChis3(section: string; indent: string; value: string); // 写ini
var
  chis: Tinifile;
begin
  chis := Tinifile.Create(extractfilepath(application.ExeName) + '\3\config.ini');
  chis.WriteString(section, indent, value);
  chis.free;
end;
function Readchis4(section: string; indent: string): string; // 读chisini
var
  chis: Tinifile;
begin
  chis := Tinifile.Create(extractfilepath(application.ExeName) + '\4\config.ini');
  result := chis.ReadString(section, indent, '');
  chis.free;
end;

procedure WriteChis4(section: string; indent: string; value: string); // 写ini
var
  chis: Tinifile;
begin
  chis := Tinifile.Create(extractfilepath(application.ExeName) + '\4\config.ini');
  chis.WriteString(section, indent, value);
  chis.free;
end;
//=================以下函数用于参考=============================================
{ }

procedure SendMessageData(AHandle: THandle; AStr: String);
var
 sData: TCopyDataStruct;
begin
   sData.cbData := Length(AStr) + 1;
   //为传递的数据分配内存
   GetMem(sData.lpData,sData.cbData );
   try
    StrCopy(sData.lpData,PChar(AStr));
    //发送WM_COPYDATA消息
    SendMessage(AHandle,WM_COPYDATA,0,Cardinal(@sData));
   finally
    FreeMem(sData.lpData); //释放资源
   end;
end;
{
procedure addToLog(DebugStr: string; Debug: boolean);
begin
  if Debug then
    addToLog(DebugStr);
end; }


procedure FillDataset(dataset: Tdataset; timeFieldname: string;
  divSec: Integer);
var

  btime, etime: Tdatetime;
  // bdate,edate : Tdate;
  recordvalues: array of variant;
  i: Integer;
  OffsetSec: Integer; // 负的 随机数
  BreakTime: Integer; // 断线时间间隔 second
begin
  dataset.First;
  BreakTime := strtoint(Readchis('chis', 'NoLineMinues', '10'));

  if dataset.RecordCount <= 1 then // one record
    exit
  else
  begin
    btime := Tdatetime(dataset.fieldbyname(timeFieldname).value);
    // bdate := Tdate(dataset.fieldbyname(timeFieldname).value);
  end;
  dataset.Next;
  etime := Tdatetime(dataset.fieldbyname(timeFieldname).value);
  setlength(recordvalues, dataset.fieldcount);

  while not dataset.Eof do
  begin
    etime := Tdatetime(dataset.fieldbyname(timeFieldname).value); //

    if etime > (btime + strtotime('00:00:' + inttostr(divSec))) then // 需要补数
    begin
      dataset.RecNo := dataset.RecNo - 1;
      for i := 0 to dataset.fieldcount - 1 do // 取得需要补的值
      begin
        recordvalues[i] := dataset.Fields[i].value;
      end; // end for
      dataset.RecNo := dataset.RecNo + 1;
      while etime >= btime + strtotime('00:00:' + inttostr(divSec)) do
      // 补数 补多条数
      begin
        if etime > (btime + BreakTime / (60 * 24)) then
          break; // 时间间隔 大于Breaktime 分钟 表示 数据中断

        dataset.Insert;
        for i := 0 to dataset.fieldcount - 1 do // 填充数据
        begin
          if lowercase(dataset.Fields[i].FieldName) = lowercase(timeFieldname)
          then // 时间字段
          begin
            // randomize;
            // sleep(10);//不然 取出拉的 随机数据 相同
            OffsetSec := -3; // Random(10)-10;
            dataset.Fields[i].value := Tdatetime(btime) +
              strtotime('00:00:' + inttostr(divSec + OffsetSec));

          end
          else
            dataset.Fields[i].value := recordvalues[i];
        end; // end for
        dataset.Post;
        btime := Ttime(dataset.fieldbyname(timeFieldname).value);
        // dataset.Next;
        // etime := Ttime(dataset.FieldByName(timeFieldname).value);
      end; // end while
    end; // end if
    btime := Ttime(dataset.fieldbyname(timeFieldname).value);
    dataset.Next;
  end;
end;

procedure InsertRecords(cdsSRC: Tclientdataset; var cdsDEST: Tclientdataset;
  byWho: Integer);
var
  i: Integer;
begin
  // if cdsSRC.fieldcount=0 then
  if cdsDEST.fieldcount = 0 THEN
  begin
    cdsDEST.Data := cdsSRC.Data;
  end
  else
  begin
    cdsSRC.First;
    while not cdsSRC.Eof do
    begin
      cdsDEST.Append;
      if byWho = 1 then
      begin
        for i := 0 to cdsSRC.fieldcount - 1 DO
        begin
          // 不检查目标字段是否存在
          try
            cdsDEST.fieldbyname(cdsSRC.Fields[i].FieldName).value :=
              cdsSRC.Fields[i].value;
          except
            ;
          end;
        end
      end
      else
      begin
        for i := 0 to cdsDEST.fieldcount - 1 DO
        begin
          // 不检查目标字段是否存在
          try
            cdsDEST.Fields[i].value :=
              cdsSRC.fieldbyname(cdsDEST.Fields[i].FieldName).value;
          except
            ;
          end;
        end
      end;
      cdsDEST.Post;
      cdsSRC.Next;
    END;
  end;
end;



function SelectMessageBox(Text: string; CaptionNum: Integer;
  Flags: Integer): Integer;
begin
  result := 0;
  //gv_inMessageBox := 1;
  //Gv_HasDigForm := True; // helei
  if CaptionNum = 1 then
  begin
    case Flags of
      1:
        result := application.MessageBox(pchar(Text), '提示',
          MB_OK + MB_ICONEXCLAMATION + MB_DEFBUTTON1);
      2:
        result := application.MessageBox(pchar(Text), '提示',
          MB_OKCANCEL + MB_ICONEXCLAMATION + MB_DEFBUTTON2);
      3:
        result := application.MessageBox(pchar(Text), '提示',
          MB_YESNO + MB_ICONEXCLAMATION + MB_DEFBUTTON2);
    end;
  end
  else if CaptionNum = 2 then
  begin
    case Flags of
      1:
        result := application.MessageBox(pchar(Text), '错误',
          MB_OK + MB_ICONERROR + MB_DEFBUTTON1);
      2:
        result := application.MessageBox(pchar(Text), '错误',
          MB_OKCANCEL + MB_ICONERROR + MB_DEFBUTTON2);
      3:
        result := application.MessageBox(pchar(Text), '错误',
          MB_YESNO + MB_ICONERROR + MB_DEFBUTTON2);
    end;
  end;
 // gv_inMessageBox := 0;
 // Gv_HasDigForm := false;
end;

procedure addToLog(str: string);
var
  LogFileName: string;
  f: Integer;
  y, m, d: word;
  mstr, dstr: string;
  LogFile: Textfile;
begin
  if Readchis('result','editlog')<>'0' then begin
    try
      LogFileName :=ExtractFilePath(application.ExeName)+'edit01.log';
      if not FileExists(LogFileName) then  begin    // 不存在
        //f := FileCreate(LogFileName);
        //fileclose(f);
        try
          AssignFile(LogFile, LogFileName);
          Rewrite(LogFile);
          //WriteLn(LogFile, datetimetostr(now) + #9 + str);
        finally
          closefile(LogFile);
        end;
      end;
      try
        AssignFile(LogFile, LogFileName);
        Append(LogFile);
        WriteLn(LogFile, datetimetostr(now) + #9+form_line.IdIPWatch1.LocalIP+#9
                +form_line.edt77.text+'('+sysedit[13].Text+')-' +sysedit[12].Text+#9
                +form_line.edt78.text+#9
                +'operator:'+form_line.edt79.text+#9
                +form_line.edt80.text+#9
                +form_line.edt164.text+#9
                + str);
      finally
        closefile(LogFile);
      end;
    Except
    end;
  end;
end;

function GetRealSql(ASerialNo, begin_date, end_date: string): string;
var
  i, ArrayCount: Integer;
  sqlStr, sSubSql, sTableName: string;
  monthStrs: TStrings;
  strTest: string;
begin
  try

    sqlStr := '';
    sSubSql :=
      ' where c.serial_no=a.serial_no and a.alarm_status=b.name  and a.alarm_status=b.name   and a.serial_no in (';

    sSubSql := sSubSql + ASerialNo + ')';

    sSubSql := sSubSql + ' and a.happen_date >=' + '''' + begin_date + '''';
    sSubSql := sSubSql + ' and a.happen_date <=' + '''' + end_date + '''';

    monthStrs := TStrings.Create;
    monthStrs := GetMonths(strtodatetime(begin_date), strtodatetime(end_date));
    for i := 0 to monthStrs.Count - 1 do
    begin

      sTableName := 'kj_real' + monthStrs.Strings[i] + '  a';
      sqlStr := sqlStr +
        ' (select describe=c.describe+''(''+str(c.fds_no,3,0)+c.channel_type+str(c.channel_no,2,0)+'')'','
        + ' a.*,case a.proff_flag when 1 then ''√'' else ''  '' end,b.fore_color,b.back_color,b.describe from zd_alarm_color b,kj_sensor_define c,'
        + sTableName + sSubSql + ')';
      if i < monthStrs.Count - 1 then
        sqlStr := sqlStr + '  union ';

    end;
    sqlStr := sqlStr + '  order by a.happen_date';

    // addtolog(sqlStr);
    result := sqlStr;

  except
    on e: Exception do
    begin
      addToLog(e.Message + sqlStr);
      result := '';

    end;
  end;
end;

function GetMonths(btime, etime: Tdatetime): TStrings;
var
  monthStrs: TStrings;
  year, month, day, Year1, Month1, Day1: word;
  ystr, mstr: string;
begin
  monthStrs := TStringList.Create;
  decodedate(btime, year, month, day);
  decodedate(etime, Year1, Month1, Day1);
  if ((year <> Year1) or (month <> Month1)) then
  begin
    ystr := inttostr(year);
    ystr := Copy(ystr, 3, 2);
    mstr := inttostr(month);
    if month < 10 then
      mstr := '0' + mstr;
    monthStrs.Add(ystr + mstr);

    ystr := inttostr(Year1);
    ystr := Copy(ystr, 3, 2);
    mstr := inttostr(Month1);
    if Month1 < 10 then
      mstr := '0' + mstr;
    monthStrs.Add(ystr + mstr);
  end
  else
  begin
    ystr := inttostr(Year1);
    ystr := Copy(ystr, 3, 2);
    mstr := inttostr(Month1);
    if Month1 < 10 then
      mstr := '0' + mstr;
    monthStrs.Add(ystr + mstr);
  end;
  result := monthStrs;
end;

end.
