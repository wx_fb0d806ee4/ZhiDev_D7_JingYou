{  描述：    主启动问题
  文件名：	 main
  应用语言:  delphi7
  版本：		 V1.0
  Copyright(C) Jin 2013   All rights reserved
  作者：Jin
  建立日期：2019-05-09

 注意：
 1.没有串口，debug会报错
 2.启用socket，debug会报错
 3.不支持CSVen=5数据格式：不通用的功能先删除掉，等需要再加进来。
 4.更早期的应用必须用原程序，二次开发软件不支持；
//----2020-05-27-----v545.02-------------------------------------
//1.保留维信数据保存功能：edt47.text='2'(grp1.Tag=2)  ,='3'(grp33.Tag=3) 这2种数据格式！
  2.保留基本的测试程序调试
  3.只保留串口调试，方便开发软件的安装

//----2020-05-27-----v545.03-------------------------------------
  1.客户需求单独文件？
  2.通信用modbus结构？
  3.去除不容易理解的变量？
  4.删除MachineNumCheck相关的代码！
//----2020-05-30-----v545.07-------------------------------------
  1.删除txt格式数据：tab容易错位；删除csv=1:不通用
  2.删除USB通信：开发复杂

  
  3.saveflag改成形参 少于2个字有提示！
//----2020-06-13-----v546.07-------------------------------------
  1.增加USB调试

2022-6-15 增加
1.步骤名和备注第1个字符的+-
2.ctrl+空格加新步骤
3.复制多1行

v547.07
1. 05功能码执行后，强制row_num:=VIRLCROWS-2;   从倒数第2行开始刷新
2.透传要有醒目的提示
3.条码中的9种特殊字符替代为空格；
4.BZV0条码规则扩展+4+12；
v547.10
1. 解决v547.05之后版本接收到sys参数会设置row_num:=0的bug; //否则会继续读状态和勾选参数

v548.02
 1.输入888999同时把助手界面开启！
 2.加载完成后清命令
v549.01
 1.tesla数据文件最后增加治具ID；
 2.if ( (hh div 60)=16)and(Length(zu17edit[StepNowFresh+1].text)>4) then begin       //长备注允许右键复制
   end else begin
v550.30
 1.下限30009改成0；
v550.31
 1. xx冲突<--增加UDPpORT=9000本地交互；
 
******集成到通用软件，不再更新***************
需处理通信、备份、和log文件单元；
 }
unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Grids, ValEdit, Mask, Buttons, SPComm,Unit0_globalVariant,
  ComCtrls, IniFiles,FileCtrl,strUtils,
  sEdit, sSpinEdit, sMaskEdit, sCustomComboEdit,
  sToolEdit, DBCtrls, Printers, ActnList, jpeg,
  iComponent, iVCLComponent, iCustomComponent, iPlotComponent, iPlot,
  IdUDPBase, IdUDPServer, IdBaseComponent, IdComponent, IdIPWatch, ScktComp,
  IdAntiFreezeBase, IdAntiFreeze,
  IdTCPConnection, IdTCPClient,
  IdSocketHandle,IdUDPClient, IdHTTP,HTTPApp,
  Menus, ToolWin, sScrollBox, iLed, iLedRound, sMemo, iLabel,
  iEditCustom, iEdit, iLedArrow, sLabel, iLedMatrix , RzLaunch
  ,shellapi, sRadioButton, sBitBtn, acProgressBar, sListView, sSplitter, TLHelp32,
  acPNG, acImage, iSevenSegmentDisplay, iSevenSegmentClock, acPathDialog,
  pngimage,IdMultipartFormData,IdSMTP,IdMessage, IdMessageClient, TeEngine,
  Series, TeeProcs, Chart, OleCtrls, VCFI, TeeFunci, JvHidControllerClass;

const
  xlUnderlineStyleSingle = 2;
  xlCenter = -4108;
  xlBottom = -4107;
type
  TReport = packed record
    ReportID: Byte;
    Bytes: array [0..63] of Byte;
  end;
  TForm_line = class(TForm)
    Comm1: TComm;
    memo1: TMemo;
    SpeedButton12: TSpeedButton;
    SpeedButton13: TSpeedButton;
    stat1: TStatusBar;
    tmr1: TTimer;
    tmr2: TTimer;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    Label2: TLabel;
    Label1: TLabel;
    GroupBox1: TGroupBox;
    GroupBox3: TGroupBox;
    edt2: TEdit;
    btn3: TButton;
    GroupBox5: TGroupBox;
    grp2: TGroupBox;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    lbl11: TLabel;
    lbl12: TLabel;
    lbl13: TLabel;
    lbl14: TLabel;
    lbl15: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    lbl17: TLabel;
    lbl18: TLabel;
    lbl19: TLabel;
    lbl20: TLabel;
    lbl21: TLabel;
    lbl22: TLabel;
    lbl23: TLabel;
    lbl24: TLabel;
    lbl25: TLabel;
    Label10: TLabel;
    lbl16: TLabel;
    Label11: TLabel;
    ts1: TTabSheet;
    grp3: TGroupBox;
    btn16: TBitBtn;
    btn14: TBitBtn;
    btn15: TBitBtn;
    chk1: TCheckBox;
    chk2: TCheckBox;
    chk3: TCheckBox;
    chk4: TCheckBox;
    BitBtn1: TBitBtn;
    TabSheet4: TTabSheet;
    btn23: TSpeedButton;
    btn24: TSpeedButton;
    lbl28: TLabel;
    dlgOpen1: TOpenDialog;
    lbl29: TLabel;
    btn2: TButton;
    grp1: TGroupBox;
    edt1: TEdit;
    btn4: TButton;
    edt6: TEdit;
    lbl2: TLabel;
    edt38: TEdit;
    lbl38: TLabel;
    btn8: TButton;
    lbl43: TLabel;
    lbl44: TLabel;             
    lbl45: TLabel;
    lbl46: TLabel;
    lbl47: TLabel;
    lbl48: TLabel;
    lbl49: TLabel;
    lbl50: TLabel;
    lbl51: TLabel;
    lbl52: TLabel;
    lbl53: TLabel;
    lbl54: TLabel;
    lbl55: TLabel;
    lbl56: TLabel;
    lbl57: TLabel;
    lbl58: TLabel;
    lbl59: TLabel;
    lbl60: TLabel;
    ts3: TTabSheet;
    DevListBox: TListBox;
    HistoryListBox: TListBox;
    grp6: TGroupBox;
    grp9: TGroupBox;
    SaveBtn: TSpeedButton;
    ClearBtn: TSpeedButton;
    ReadBtn: TSpeedButton;
    WriteBtn: TSpeedButton;
    SaveDialog: TSaveDialog;
    edit1: TMaskEdit;
    chk5: TCheckBox;
    dlgPnt1: TPrintDialog;
    dlgPntSet1: TPrinterSetupDialog;
    lbl63: TLabel;
    GpcResult: TPageControl;
    ts4: TTabSheet;
    mmo2: TMemo;
    tmr3: TTimer;
    edt45: TEdit;
    lbl64: TLabel;
    ts6: TTabSheet;
    tmr4: TTimer;
    btn28: TButton;
    grp10: TGroupBox;
    img3: TImage;
    tmr5: TTimer;
    IdIPWatch1: TIdIPWatch;
    shp2: TShape;
    btn27: TBitBtn;
    btn29: TButton;
    chk7: TCheckBox;
    chk8: TCheckBox;
    chk9: TCheckBox;
    mm1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    Panel1: TScrollBox;
    x1: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N18: TMenuItem;
    q1: TMenuItem;
    f1: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N12: TMenuItem;
    N22: TMenuItem;
    Panel2: TPanel;
    edt55: TEdit;              //不使用
    D1: TMenuItem;
    S2: TMenuItem;
    N26: TMenuItem;
    N27: TMenuItem;
    N28: TMenuItem;
    I1: TMenuItem;
    N29: TMenuItem;
    H1: TMenuItem;
    S3: TMenuItem;
    D2: TMenuItem;
    M2: TMenuItem;
    B1: TMenuItem;
    btn17: TSpeedButton;
    btn18: TSpeedButton;
    N30: TMenuItem;
    O1: TMenuItem;
    P1: TMenuItem;
    U1: TMenuItem;
    N31: TMenuItem;
    P2: TMenuItem;
    S4: TMenuItem;
    O2: TMenuItem;
    N24: TMenuItem;
    P3: TMenuItem;
    P4: TMenuItem;
    U2: TMenuItem;
    D4: TMenuItem;
    I2: TMenuItem;
    D5: TMenuItem;
    N33: TMenuItem;
    F2: TMenuItem;
    C1: TMenuItem;
    edt47: TEdit;
    lbl27: TLabel;
    ts7: TTabSheet;
    Rled1: TiLedRound;
    scrlbx2: TScrollBox;
    edt59: TsEdit;
    edt60: TsEdit;
    lbl68: TLabel;
    lbl72: TLabel;
    I3: TMenuItem;
    lbl73: TLabel;
    ildrw1: TiLedArrow;
    N36: TMenuItem;
    C2: TMenuItem;
    lbl78: TLabel;
    edt62: TEdit;
    ts8: TTabSheet;
    lbl79: TLabel;
    lbl80: TLabel;
    lbl81: TLabel;
    lbl82: TLabel;
    edt4: TEdit;
    edt63: TEdit;
    edt64: TEdit;
    edt65: TEdit;
    edt66: TEdit;
    lbl83: TLabel;
    grp17: TGroupBox;
    N37: TMenuItem;
    N38: TMenuItem;
    edt69: TEdit;
    edt70: TEdit;
    lbl75: TLabel;
    lbl76: TLabel;
    edt71: TEdit;
    lbl84: TLabel;
    edt74: TEdit;
    lbl88: TLabel;
    chk10: TCheckBox;
    mmo5: TMemo;
    edt75: TEdit;
    btn55: TButton;
    lbl89: TLabel;
    ts9: TTabSheet;
    mmo6: TMemo;
    edt76: TEdit;
    grp18: TGroupBox;
    lbl90: TLabel;
    lbl91: TLabel;
    lbl92: TLabel;
    lbl93: TLabel;
    edt77: TEdit;
    edt78: TEdit;
    edt79: TEdit;
    edt80: TEdit;
    btn56: TSpeedButton;
    edt81: TEdit;
    lbl94: TLabel;
    edt82: TEdit;
    N39: TMenuItem;
    btn1: TSpeedButton;
    edt7: TEdit;
    lbl6: TLabel;
    lbl41: TLabel;
    lbl42: TLabel;
    edt8: TEdit;
    edt9: TEdit;
    edt10: TEdit;
    lbl98: TLabel;
    edt11: TEdit;
    edt14: TEdit;
    edt15: TEdit;
    lbl114: TLabel;
    ts12: TTabSheet;
    j1: TMenuItem;
    edt96: TEdit;
    lbl119: TLabel;
    grp23: TGroupBox;
    mmo13: TMemo;
    L2: TMenuItem;
    mmo14: TMemo;
    mmo15: TMemo;
    N42: TMenuItem;
    N43: TMenuItem;
    ts13: TTabSheet;
    mmo16: TMemo;
    lbl133: TLabel;
    edt111: TEdit;
    btn75: TSpeedButton;
    lbl134: TLabel;
    edt112: TEdit;
    lbl135: TLabel;
    medit1: TMaskEdit;
    lbl137: TLabel;
    lbl139: TLabel;
    lbl140: TLabel;
    btn77: TBitBtn;
    btn78: TBitBtn;
    pgc1: TPageControl;
    ts15: TTabSheet;
    ts16: TTabSheet;
    grp28: TGroupBox;
    pnl1: TPanel;
    iLedRound1: TiLedRound;
    iLedRound2: TiLedRound;
    iLabel1: TiLabel;
    iLabel2: TiLabel;
    edt120: TEdit;
    lbl148: TLabel;
    C4: TMenuItem;
    N34: TMenuItem;
    edt121: TEdit;
    N47: TMenuItem;
    btn92: TButton;
    tmr6: TTimer;
    grp30: TGroupBox;
    edt48: TEdit;
    lbl151: TLabel;
    edt123: TEdit;
    edt124: TEdit;
    edt125: TEdit;
    edt126: TEdit;
    grp31: TGroupBox;
    spl1: TSplitter;
    spl2: TSplitter;
    grp32: TGroupBox;
    spl3: TSplitter;
    lbl155: TLabel;
    lbl156: TLabel;
    edt130: TEdit;
    grp34: TGroupBox;
    rb3: TsRadioButton;
    rb4: TsRadioButton;
    edt134: TsDecimalSpinEdit;
    lbl160: TsLabel;
    U3: TMenuItem;
    btn98: TButton;
    edt135: TEdit;
    btn100: TButton;
    lbl161: TLabel;
    L6: TMenuItem;
    sprgrsbr1: TsProgressBar;
    lv1: TsListView;
    grp35: TGroupBox;
    grp36: TGroupBox;
    grp37: TGroupBox;
    grp39: TGroupBox;
    btn101: TBitBtn;
    sprgrsbr2: TsProgressBar;
    btn12: TBitBtn;
    btn13: TBitBtn;
    mmo3: TMemo;
    grp41: TGroupBox;
    btn114: TButton;
    mmo19: TMemo;
    edt73: TEdit;
    edt136: TEdit;
    edt137: TEdit;
    btn115: TButton;
    btn116: TButton;
    lbl164: TLabel;
    btn118: TButton;
    edt139: TEdit;
    edt140: TEdit;
    edt141: TEdit;
    edt83: TEdit;
    edt142: TEdit;
    edt145: TEdit;
    lbl168: TLabel;
    lbl169: TLabel;
    C5: TMenuItem;
    btn121: TButton;
    lbl170: TLabel;
    lbl177: TLabel;
    lbl178: TLabel;
    edt153: TEdit;
    edt154: TEdit;
    edt155: TEdit;
    edt156: TEdit;
    lbl179: TLabel;
    lbl180: TLabel;
    btn125: TButton;
    edt157: TEdit;
    lbl181: TLabel;                                                             //上传网盘标志符号；
    sbtbtn1: TBitBtn;
    edt158: TEdit;
    lbl182: TLabel;
    rb5: TsRadioButton;
    lbl183: TLabel;
    Redit1: TiEdit;
    edt159: TEdit;
    lbl184: TLabel;
    lbl185: TLabel;
    lbl186: TLabel;
    lbl187: TLabel;
    chk24: TCheckBox;
    edt163: TEdit;
    chk25: TCheckBox;
    chk27: TCheckBox;
    chk26: TCheckBox;
    chk28: TCheckBox;
    btn129: TButton;
    lbl188: TLabel;
    edt164: TEdit;
    edt165: TEdit;
    btn130: TBitBtn;
    chk29: TCheckBox;
    A1: TMenuItem;
    N50: TMenuItem;
    N51: TMenuItem;
    spthdlg1: TsPathDialog;
    chk30: TCheckBox;
    chk31: TCheckBox;
    ts23: TTabSheet;
    chk32: TCheckBox;
    chk33: TCheckBox;
    chk34: TCheckBox;
    chk35: TCheckBox;
    chk36: TCheckBox;
    mmo22: TMemo;
    mmo23: TMemo;
    mmo24: TMemo;
    mmo25: TMemo;
    mmo26: TMemo;
    edt168: TEdit;
    edt169: TEdit;
    lbl192: TLabel;
    grp43: TGroupBox;
    grp44: TGroupBox;
    grp45: TGroupBox;
    edt170: TEdit;
    edt171: TEdit;
    edt172: TEdit;
    edt173: TEdit;
    edt174: TEdit;
    edt175: TEdit;
    lbl191: TLabel;
    lbl193: TLabel;
    edt176: TEdit;
    btn138: TButton;
    btn139: TBitBtn;
    isvnsgmntclck1: TiSevenSegmentClock;
    Btn120: TButton;
    edt177: TEdit;
    edt178: TEdit;
    edt179: TEdit;
    edt180: TEdit;
    M3: TMenuItem;
    N52: TMenuItem;
    N53: TMenuItem;
    N54: TMenuItem;
    P5: TMenuItem;
    btn143: TBitBtn;
    chk38: TCheckBox;
    edt181: TEdit;
    edt182: TEdit;
    edt183: TEdit;
    edt184: TEdit;
    lbl136: TLabel;
    grp46: TGroupBox;
    btn25: TsBitBtn;
    btn26: TsBitBtn;
    Chart1: TChart;
    Series1: THorizBarSeries;
    Series2: THorizBarSeries;
    Series3: THorizBarSeries;
    Series4: THorizBarSeries;
    Series5: THorizBarSeries;
    Series6: THorizBarSeries;
    Series7: THorizBarSeries;
    Series8: THorizBarSeries;
    Series9: THorizBarSeries;
    Series10: THorizBarSeries;
    btn144: TButton;
    s8: TMenuItem;
    N59: TMenuItem;
    N60: TMenuItem;
    pgc2: TPageControl;
    ts20: TTabSheet;
    grp16: TGroupBox;
    btn9: TButton;
    btn10: TButton;
    btn20: TButton;
    btn21: TButton;
    edt5: TsDecimalSpinEdit;
    grp38: TGroupBox;
    btn102: TButton;
    btn103: TButton;
    btn104: TButton;
    btn105: TButton;
    btn106: TButton;
    btn107: TButton;
    btn108: TButton;
    btn109: TButton;
    btn110: TButton;
    btn111: TButton;
    btn73: TBitBtn;
    btn74: TBitBtn;
    ts18: TTabSheet;
    grp29: TGroupBox;
    mmo17: TMemo;
    mmo18: TMemo;
    pnl2: TPanel;
    rb1: TRadioButton;
    chk18: TCheckBox;
    chk19: TCheckBox;
    rb2: TRadioButton;
    edt122: TsDecimalSpinEdit;
    btn91: TButton;
    btn90: TButton;
    grp40: TGroupBox;
    btn113: TSpeedButton;
    shp4: TShape;
    lbl87: TLabel;
    lbl162: TLabel;
    btn112: TSpeedButton;
    cbb2: TComboBox;
    cbb3: TComboBox;
    ts19: TTabSheet;
    grp11: TGroupBox;
    lbl166: TLabel;
    btn32: TBitBtn;
    btn31: TBitBtn;
    btn30: TBitBtn;
    btn33: TBitBtn;
    btn34: TBitBtn;
    btn35: TBitBtn;
    btn36: TBitBtn;
    btn37: TBitBtn;
    btn38: TBitBtn;
    btn39: TBitBtn;
    btn40: TBitBtn;
    btn41: TBitBtn;
    btn42: TBitBtn;
    btn43: TBitBtn;
    btn46: TBitBtn;
    chk6: TCheckBox;
    edt143: TEdit;
    grp47: TGroupBox;
    lbl69: TLabel;
    lbl70: TLabel;
    lbl71: TLabel;
    lbl167: TLabel;
    edt49: TEdit;
    edt51: TEdit;
    edt50: TEdit;
    edt144: TEdit;
    btn44: TBitBtn;
    btn145: TButton;
    edt99: TEdit;
    btn146: TButton;
    edt185: TEdit;
    btn147: TButton;
    btn148: TButton;
    lbl150: TLabel;
    edt40: TEdit;
    edt42: TEdit;
    HidCtl: TJvHidDeviceController;
    lbl194: TLabel;
    ts2: TTabSheet;
    idpclnt1: TIdUDPClient;
    UDPip: TLabel;
    edt100: TEdit;
    UDPport: TLabel;
    edt53: TEdit;
    mmo9: TMemo;
    idpsrvr1: TIdUDPServer;
    N5: TMenuItem;
/////////////通用函数
    procedure FormCreate(Sender: TObject);
    procedure Comm1ReceiveData(Sender: TObject; Buffer: Pointer;   BufferLength: Word);
    procedure SpeedButton12Click(Sender: TObject);
    procedure SpeedButton13Click(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure tmr2Timer(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btn12Click(Sender: TObject);
    procedure btn13Click(Sender: TObject);
    procedure btn14Click(Sender: TObject);
    procedure btn15Click(Sender: TObject);
    procedure chk1Click(Sender: TObject);
    procedure chk2Click(Sender: TObject);
    procedure chk3Click(Sender: TObject);
    procedure chk4Click(Sender: TObject);
    procedure btn17Click(Sender: TObject);
    procedure btn18Click(Sender: TObject);
    procedure btn20Click(Sender: TObject);
    procedure btn21Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btn24Click(Sender: TObject);
    procedure btn23Click(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure TabSheet4Show(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

    procedure FormShow(Sender: TObject);
    procedure btn25Click(Sender: TObject);
    procedure btn26Click(Sender: TObject);
    procedure chk5Click(Sender: TObject);
    procedure TabSheet2Hide(Sender: TObject);
    procedure btn27Click(Sender: TObject);
    procedure tmr3Timer(Sender: TObject);
    procedure edt45Click(Sender: TObject);
    procedure tmr4Timer(Sender: TObject);
    procedure btn28Click(Sender: TObject);
    procedure btn29Click(Sender: TObject);
    procedure tmr5Timer(Sender: TObject);
//    procedure edt48Click(Sender: TObject);
    procedure btn30Click(Sender: TObject);
    procedure btn31Click(Sender: TObject);
    procedure btn32Click(Sender: TObject);
    procedure btn33Click(Sender: TObject);
    procedure btn34Click(Sender: TObject);
    procedure btn35Click(Sender: TObject);
    procedure btn36Click(Sender: TObject);
    procedure btn37Click(Sender: TObject);
    procedure btn38Click(Sender: TObject);
    procedure btn39Click(Sender: TObject);
    procedure btn40Click(Sender: TObject);
    procedure btn41Click(Sender: TObject);
    procedure btn42Click(Sender: TObject);
    procedure btn43Click(Sender: TObject);
    procedure btn44Click(Sender: TObject);
    procedure btn46Click(Sender: TObject);
    procedure chk7Click(Sender: TObject);
    procedure chk8Click(Sender: TObject);
    procedure chk9Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N20Click(Sender: TObject);
    procedure Panel1MouseWheelDown(Sender: TObject; Shift: TShiftState;    MousePos: TPoint; var Handled: Boolean);
    procedure Panel1MouseWheelUp(Sender: TObject; Shift: TShiftState;     MousePos: TPoint; var Handled: Boolean);
    procedure N21Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure btn9Click(Sender: TObject);
    procedure btn10Click(Sender: TObject);
    procedure D1Click(Sender: TObject);
    procedure N27Click(Sender: TObject);
    procedure H1Click(Sender: TObject);
    procedure S3Click(Sender: TObject);
    procedure N31Click(Sender: TObject);
    procedure P2Click(Sender: TObject);
    procedure O1Click(Sender: TObject);
    procedure P1Click(Sender: TObject);
    procedure S4Click(Sender: TObject);
    procedure B1Click(Sender: TObject);
    procedure TabSheet4Hide(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure I2Click(Sender: TObject);
    procedure D5Click(Sender: TObject);
    procedure N33Click(Sender: TObject);
    procedure I3Click(Sender: TObject);
    procedure C2Click(Sender: TObject);
    procedure N38Click(Sender: TObject);
    procedure btn55Click(Sender: TObject);
    procedure edit1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btn56Click(Sender: TObject);
    procedure tmr6Timer(Sender: TObject);
    procedure ts1Show(Sender: TObject);
    procedure btn61Click(Sender: TObject);
    procedure btn59Click(Sender: TObject);
    procedure btn62Click(Sender: TObject);
    procedure j1Click(Sender: TObject);
    procedure N19Click(Sender: TObject);
    procedure L2Click(Sender: TObject);
    procedure mmo13DblClick(Sender: TObject);
    procedure mmo14DblClick(Sender: TObject);
    procedure N42Click(Sender: TObject);
    procedure N43Click(Sender: TObject);
    procedure edt103Change(Sender: TObject);
    procedure edt106Change(Sender: TObject);
    procedure edt102Change(Sender: TObject);
    procedure edt109Change(Sender: TObject);
    procedure btn73Click(Sender: TObject);
    procedure btn74Click(Sender: TObject);
    procedure btn75Click(Sender: TObject);
    procedure medit1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btn77Click(Sender: TObject);
    procedure btn78Click(Sender: TObject);
    procedure btn80Click(Sender: TObject);
    procedure btn81Click(Sender: TObject);
    procedure btn82Click(Sender: TObject);
    procedure btn83Click(Sender: TObject);
    procedure btn84Click(Sender: TObject);
    procedure btn85Click(Sender: TObject);
    procedure btn86Click(Sender: TObject);
    procedure btn87Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure C4Click(Sender: TObject);
    procedure N34Click(Sender: TObject);
    procedure N47Click(Sender: TObject);
    procedure btn90Click(Sender: TObject);
    procedure btn91Click(Sender: TObject);
    procedure btn92Click(Sender: TObject);
    procedure chk18Click(Sender: TObject);
    procedure edt122Change(Sender: TObject);
//    procedure btn94Click(Sender: TObject);
//    procedure btn95Click(Sender: TObject);
//    procedure btn93Click(Sender: TObject);
    procedure rb2Click(Sender: TObject);
    procedure rb1Click(Sender: TObject);
    procedure edt134Change(Sender: TObject);
    procedure btn98Click(Sender: TObject);
    procedure U3Click(Sender: TObject);
    procedure btn100Click(Sender: TObject);
    procedure btn125Click(Sender: TObject);
    procedure sbtbtn1Click(Sender: TObject);
    procedure lv1AdvancedCustomDrawItem(Sender: TCustomListView;
      Item: TListItem; State: TCustomDrawState; Stage: TCustomDrawStage;
      var DefaultDraw: Boolean);
    procedure ts16Show(Sender: TObject);
    procedure ts15Show(Sender: TObject);
    procedure btn101Click(Sender: TObject);
    procedure btn102Click(Sender: TObject);
    procedure btn103Click(Sender: TObject);
    procedure btn104Click(Sender: TObject);
    procedure btn105Click(Sender: TObject);
    procedure btn106Click(Sender: TObject);
    procedure btn107Click(Sender: TObject);
    procedure btn108Click(Sender: TObject);
    procedure btn109Click(Sender: TObject);
    procedure btn110Click(Sender: TObject);
    procedure btn111Click(Sender: TObject);
    procedure cbb3Change(Sender: TObject);
    procedure cbb2Change(Sender: TObject);
    procedure mmo18KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure mmo17Change(Sender: TObject);
    procedure btn114Click(Sender: TObject);
    procedure btn115Click(Sender: TObject);
    procedure btn116Click(Sender: TObject);
    procedure btn118Click(Sender: TObject);
    procedure btn120Click(Sender: TObject);
    procedure C5Click(Sender: TObject);
    procedure btn129Click(Sender: TObject);
    procedure btn130Click(Sender: TObject);
    procedure A1Click(Sender: TObject);
    procedure Label7DblClick(Sender: TObject);
    procedure lbl79DblClick(Sender: TObject);
    procedure N51Click(Sender: TObject);
    procedure N50Click(Sender: TObject);
    procedure chk32Click(Sender: TObject);
    procedure chk33Click(Sender: TObject);
    procedure chk34Click(Sender: TObject);
    procedure chk35Click(Sender: TObject);
    procedure chk36Click(Sender: TObject);
    procedure ts23Show(Sender: TObject);
    procedure grp43DblClick(Sender: TObject);
    procedure btn138Click(Sender: TObject);
    procedure btn139Click(Sender: TObject);
    procedure btn140Click(Sender: TObject);
    procedure btn141Click(Sender: TObject);
    procedure mmo28Click(Sender: TObject);
    procedure btn112Click(Sender: TObject);
    procedure M3Click(Sender: TObject);
    procedure N52Click(Sender: TObject);
    procedure N53Click(Sender: TObject);
    procedure P5Click(Sender: TObject);
//    procedure A2Click(Sender: TObject);
    procedure btn143Click(Sender: TObject);
    procedure C7Click(Sender: TObject);
    procedure ts18Show(Sender: TObject);
    procedure btn144Click(Sender: TObject);
    procedure pnl1DblClick(Sender: TObject);
    procedure grp28DblClick(Sender: TObject);
    procedure lv1DblClick(Sender: TObject);
    procedure s8Click(Sender: TObject);
    procedure btn146Click(Sender: TObject);
    procedure btn147Click(Sender: TObject);
    procedure btn148Click(Sender: TObject);
    procedure lbl140DblClick(Sender: TObject);
    procedure lbl82DblClick(Sender: TObject);
    procedure lbl80DblClick(Sender: TObject);
    procedure N59Click(Sender: TObject);
    procedure stepModePopup(Sender: TObject; MousePos: TPoint;var Handled: Boolean);
    procedure sysParaPopup(Sender: TObject; MousePos: TPoint;var Handled: Boolean);
    procedure N60Click(Sender: TObject);
    procedure edt45ContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure edt45KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure HidCtlRemoval(HidDev: TJvHidDevice);
    procedure HidCtlArrival(HidDev: TJvHidDevice);
    procedure HidCtlDeviceChange(Sender: TObject);

    procedure HidCtlDeviceDataError(HidDev: TJvHidDevice; Error: Cardinal);

    function HidCtlEnumerate(HidDev: TJvHidDevice;
      const Idx: Integer): Boolean;
    procedure ClearBtnClick(Sender: TObject);
    procedure WriteBtnClick(Sender: TObject);
    procedure ReadBtnClick(Sender: TObject);
    procedure SaveBtnClick(Sender: TObject);
    procedure DevListBoxClick(Sender: TObject);
    procedure N5Click(Sender: TObject);

  private
    { Private declarations }

  public
    { Public declarations }
    CLose_1:    integer;          //登录窗口是否按了直接 CLOSE按键 ；其它窗口必须加前缀；
    procedure   ShowRead(HidDev: TJvHidDevice; ReportID: Byte;const Data: Pointer; Size: Word);
    procedure   PrintClickProcess(len:Integer);
    procedure   AddToHistory(Str: string);

    function    DeviceName(HidDev: TJvHidDevice): string;

    procedure   sysIntClick(Sender:   TObject);
    procedure   CommandIntClick(Sender:   TObject);
    procedure   stepIntClick(Sender:   TObject);
    procedure   sysStrClick(Sender:   TObject);
    procedure   stepNameStrClick(Sender:   TObject);
    procedure   stepModeStrClick(Sender:   TObject);
    procedure   stepParaKeydown(Sender:TObject; var Key: Word; Shift: TShiftState);
    procedure   sysParaKeydown(Sender:TObject; var Key: Word; Shift: TShiftState);
    procedure   stepNoteClick(Sender:   TObject);
    procedure   ExpertClick(Sender:   TObject);
    function    UpServerOneLog(SLINE_head:string): Boolean;      // function    UpServerOneLog(len_ss:string): Boolean;
  end;

var
  Form_line: TForm_line;
//  fieldvalues:Rec_data;     //数据库表格式
  inportF,exportF,LogFile:textfile;
  i,send_len,zerocount:Byte;
  row_num,Freshcycles:Smallint;
  mmo2String:array[0..21] of string;//TStringList;
  mmo2StringCSV:array[0..11] of string;
  NameCount,usb_busyT,comandbusy,rbuf_usbptr:integer;
  shortbuff:array[0..2*MAXPINS] of Integer;       //4094*2短路群
  PassOverT,NGOverT,UploadSerT,inportflag,inportNewFileflag,shortptr,PassCount,NGcount,TripCount:integer;  //  speedstatue:array[0..160] of Byte;
  Rleds:array[0..200] of TiLedRound;
//  Powerlabels:array[0..200] of TLabel;                  //,Rlabels
//  Powerlabelscolor:array[0..200]of Byte;
  Redits:array[0..200] of TiEdit;                       //  xy2machine:xybutton;
implementation

uses FindPoint, OSstudy,
  StepCopy,  ModfyN, MoveNstep, InsertNstep, DeleteNstep,
  InsertCopyNstep,
  PNenter, Unit1_MainCreateShowlCode,Unit2_Communication,Unit4_logFileCode,
  LoadFile, LogHeadTips, UnitDM;

{$R *.dfm}

/////////////窗口生产初始化
procedure TForm_line.FormCreate(Sender: TObject);
var //temp:byte;
    temp16,i,j:SmallInt;
    nrf_dat_config: Tinifile;
    ini_path: string;

begin                    //变量在生成时就执行
  PageControl1.TabIndex:=2-1;    //最先进入PCset界面；

  NameCount:=0;
  GpcResult.TabIndex:=0;
  modbusfuntion:=0; UploadSerT:=0;
  PassOverT:=0;NGOverT:=0;
  PassCount:=0;NGcount:=0;
  TripCount:=0;  lbl134.Caption:=inttostr(tripcount);     //单次 计数  ，达到最大数后停止测试
  modbusfun05:=0;modbusfun05dat:=$FF;modbusfun06:=0;modbusfun16:=0;modbusfun16int:=0;modbusfun16len:=0;
  modbusfun02:=0; modbusfun03:=0;modbusfun03dat:=$00;
  working:=0;  inportNewFileflag:=$00;

  Series1.AddX(0);
  Series2.AddX(0); series2.Title:='';
  Series3.AddX(0);
  Series4.AddX(0); series4.Title:='';
  Series5.AddX(0);
  Series6.AddX(0);series6.Title:='';
  Series7.AddX(0);
  Series8.AddX(0);series8.Title:='';
  Series9.AddX(0);
  Series10.AddX(0);series10.Title:='';

  /////////////读配置文件
  ini_path := ExtractFilePath(application.ExeName) + 'config.ini';
  try
    nrf_dat_config := Tinifile.Create(ini_path);    //关联INI文件
  //-----------------[Comm Para]---------------------------------------------------
    if(nrf_dat_config.Readstring('Comm Para', 'CSVen', '')='2') then begin	//后面有同样操作；
      Rled1.Width:=100;
      Rled1.Height:=100;
      Redit1.Width:=300;
    end;
    if nrf_dat_config.Readstring('Comm Para', 'FormLeft', '')<>'' then begin    //多PC软件系统，此设定窗口位置

      if(Readchis2('Comm Para','UpLocal')='')then begin              //非第1工位用软件
        if Pos('\2\',ExtractFilePath(application.ExeName))>0 then begin
          Form_line.Left:=SCREEN.WIDTH div 2;
          if nrf_dat_config.Readstring('Comm Para', 'FormLeft', '')='4' then
            Form_line.Top:=0;
          lbl177.Caption:='工位2 ：';  StationNowNum:=2;
        end;
        if Pos('\3\',ExtractFilePath(application.ExeName))>0 then begin
          Form_line.Left:=0;
          Form_line.Top:=SCREEN.Height div 2;
          lbl177.Caption:='工位3 ：';  StationNowNum:=3;
        end;
        if Pos('\4\',ExtractFilePath(application.ExeName))>0 then begin
          Form_line.Left:=SCREEN.WIDTH div 2;
          Form_line.Top:=SCREEN.Height div 2;
          lbl177.Caption:='工位4 ：';  StationNowNum:=4;
        end;
      end else begin                                                 //第1 工位           
        if nrf_dat_config.Readstring('Comm Para', 'FormLeft', '')='4' then
          Form_line.Top:=0;

        Form_line.Left:=0;
        lbl177.Caption:='工位1 ：';

      end;
      if nrf_dat_config.Readstring('Comm Para', 'FormLeft', '')='4' then
        Form_line.Height:=SCREEN.Height div 2;
      Form_line.Width:=SCREEN.WIDTH div 2;
      grp18.Width:=200;
    end else
      lbl177.Caption:='';
    if nrf_dat_config.Readstring('Comm Para', 'FormTop', '')<>'' then begin    //双PC软件系统，此设定窗口位置
      Form_line.Top:=strtoint(nrf_dat_config.Readstring('Comm Para', 'FormTop', ''));
    end;

    edt100.Text:=IdIPWatch1.LocalIP;
    if(nrf_dat_config.Readstring('Comm Para', 'UDPport', '')<>'')  then begin   //因为防火墙缘故，软件启动时必须立即配置！
      edt53.Text:=nrf_dat_config.Readstring('Comm Para', 'UDPport', '');
      if (edt53.Text='9001')or(edt53.Text='9002')or(edt53.Text='9003')or(edt53.Text='9004') then begin
         edt53.tag:=StrToInt(edt53.Text);
         edt53.Text:='9000';

         idpsrvr1.DefaultPort:=StrToInt(edt53.Text);
         idpsrvr1.Active:=true;
         if edt53.tag>=9002 then begin
            Stat1.Panels[3].Text :='主UDP=9000,从UDP='+IntToStr(edt53.Tag);
         end else
            Stat1.Panels[3].Text :='主UDP=9000,从UDP='+IntToStr(9001);
         edt135.Visible:=true;
         btn100.Visible:=true;
         lbl161.Visible:=True;

         if nrf_dat_config.Readstring('Comm Para', 'PathSNfile', '')<>'' then      //不为空
           begin
              edt135.Text:=nrf_dat_config.Readstring('Comm Para', 'PathSNfile', '');
              if not DirectoryExists(edt135.Text) then                            //FileExists
                edt135.Text:=ExtractFilePath(application.ExeName) ;
           end
         else
            edt135.Text:=ExtractFilePath(application.ExeName) ;

         if IsNumberic(nrf_dat_config.Readstring('Comm Para', 'UDPoverTimes', ''))  then begin
            idpsrvr1.Tag:=10*StrToInt(nrf_dat_config.Readstring('Comm Para', 'UDPoverTimes', ''));
         end else
            idpsrvr1.Tag:=50;       //默认：5秒接收UDP数据超时
         Stat1.Tag:=9000;
      //   u4.Enabled:=True;
      end;
    end else begin
      edt53.Text:='9000';             //监听默认9000：立即读会报错<--nrf_dat_config.Readstring('Comm Para', 'UDPport', '');
      idpsrvr1.DefaultPort:=StrToInt(edt53.Text);
    end;
                                                                                //小于9000：保留！
    if (edt53.Text='9000') then begin                                           //9000~9049：二次开发启用
      edt53.hint:=edt53.Text;
    end else if( IsNumberic(edt53.Text)and( StrToInt(edt53.Text)>=9050) )then begin  //大于9050：实际应用端口
      edt53.tag:=StrToInt(edt53.Text);
      edt53.hint:=edt53.Text;
      idpsrvr1.DefaultPort:=StrToInt(edt53.Text);                               //UDP默认端口必须在此设置一次！

       idpsrvr1.Active:=true;
       Stat1.Panels[3].Text :='监听UDP='+IntToStr(edt53.Tag);
    //   Stat1.Tag:=9050;
    //  mmo10.Text:='sn1234567890abcedefg';           //模拟条形码发送
    end;

    temp16:=StrToInt('0'+nrf_dat_config.Readstring('Comm Para', 'PageRow', ''));
    if(temp16>=10)and (temp16<VIRLCROWS) then VIRLCROWS:=temp16        //10--69可指定定义
    else if (temp16<10) then VIRLCROWS:=66;                            //自适应根据固件版本确定行数

    if nrf_dat_config.Readstring('Comm Para', 'TextHeight', '')<>'' then        //此设定debug编辑框高度
      edt121.Text:=nrf_dat_config.Readstring('Comm Para', 'TextHeight', '');
    CreateRowEdit(VIRLCROWS);

  //-----------------[result]---------------------------------------------------
    if nrf_dat_config.Readstring('result', 'Row', '')<>'' then        //此两个参数影响软件启动时间
        edt59.Text:=nrf_dat_config.Readstring('result', 'Row', '');
    if edt59.Text='0' then edt59.Text:='1';      //不能为0
    if nrf_dat_config.Readstring('result', 'column', '')<>'' then
        edt60.Text:=nrf_dat_config.Readstring('result', 'column', '');
    if edt60.Text='0' then edt60.Text:='1';     //不能为0

    if nrf_dat_config.Readstring('result', 'Group', '')<>'' then
        edt145.Text:=nrf_dat_config.Readstring('result', 'Group', '');


    if nrf_dat_config.Readstring('result', 'colMirror', '')='1' then
        ildrw1.Style:=ilasLeft;

    if nrf_dat_config.Readstring('result', 'UpOnePCSName', '')<>'' then        //上传联板所有测试项目名称
        edt74.Text:=nrf_dat_config.Readstring('result', 'UpOnePCSName', '');

    if nrf_dat_config.Readstring('result', 'enPrint', '')<>'' then        //自动打印使能 ,并设置大小
       edt75.Text:=nrf_dat_config.Readstring('result', 'enPrint', '')
    else begin
       btn55.Visible:=False;
       btn28.Visible:=false;
       btn29.Visible:=false;
       btn27.Enabled:=false;
    end;
    if nrf_dat_config.Readstring('result', 'PrintVHdir', '')<>'' then        //设置 打印机方向+ + 空行数+信息长短
        edt11.Text:=nrf_dat_config.Readstring('result', 'PrintVHdir', '');
    if nrf_dat_config.Readstring('result', 'PrintRow', '')<>'' then           //此两个参数影响软件启动时间
        edt159.Text:=nrf_dat_config.Readstring('result', 'PrintRow', '');
    if nrf_dat_config.Readstring('result', 'PrintMaxRows', '')<>'' then        //打印最大行数，高优先级；
        edt159.Text:=nrf_dat_config.Readstring('result', 'PrintMaxRows', '');
    if edt159.Text<>'0' then  begin
      edt159.Visible:=True;
      lbl184.Visible:=true;
    end;

    if nrf_dat_config.Readstring('result', 'OddRowOffset', '')<>'' then
        edt69.Text:=nrf_dat_config.Readstring('result', 'OddRowOffset', '');
    if nrf_dat_config.Readstring('result', 'OddColOffset', '')<>'' then
        edt70.Text:=nrf_dat_config.Readstring('result', 'OddColOffset', '');
    if nrf_dat_config.Readstring('result', 'OddColVOffset', '')<>'' then
        edt70.Text:=nrf_dat_config.Readstring('result', 'OddColVOffset', '');
    if nrf_dat_config.Readstring('result', 'OddColHOffset', '')<>'' then
        edt112.Text:=nrf_dat_config.Readstring('result', 'OddColHOffset', '');
    if nrf_dat_config.Readstring('result', 'AllColHOffset', '')<>'' then
        edt120.Text:=nrf_dat_config.Readstring('result', 'AllColHOffset', '');

    if nrf_dat_config.Readstring('result', 'OddRowDown', '')<>'' then
        edt71.Text:=nrf_dat_config.Readstring('result', 'OddRowDown', '');

    if StrToInt(edt59.Text)*strtoint(edt60.Text) >High(Rleds) then      //必须小于200个
    begin
      edt59.Text:='10';
      edt60.Text:='20';    //列
    end;

  //-----------------[Model Para]---------------------------------------------------
    if nrf_dat_config.Readstring('Model Para', 'FromMcuPNMinSize', '')<>'' then
        edt158.Text:=nrf_dat_config.Readstring('Model Para', 'FromMcuPNMinSize', '');
  //-----------------other---------------------------------------------------

    MatrixLed_createProcess(0);
    if(nrf_dat_config.Readstring('Comm Para', 'CSVen', '')='2') then begin   //tesla
      Rled1.Width:=100;
      Rled1.Height:=100;
      Redit1.Width:=300;
      if StrToInt(edt59.Text)*strtoint(edt60.Text)=1 then begin
        lbl73.Visible:=True;
        lbl73.Top:=Redits[0].Top+50;
        lbl73.Left:=Redits[0].Left+100;
        chk25.Left:=Redits[0].Left;
        chk25.Top:=Redits[0].Top+100;
      end else if StrToInt(edt59.Text)*strtoint(edt60.Text)>3 then begin
        lbl73.Visible:=True;
        lbl73.Top:=Redits[0].Top+50;
        lbl73.Left:=Redits[0].Left+100;
        lbl185.Visible:=True;
        lbl185.Top:=Redits[1].Top+50;
        lbl185.Left:=Redits[1].Left+100;
        lbl186.Visible:=True;
        lbl186.Top:=Redits[2].Top+50;
        lbl186.Left:=Redits[2].Left+100;
        lbl187.Visible:=True;
        lbl187.Top:=Redits[3].Top+50;
        lbl187.Left:=Redits[3].Left+100;

        chk25.Left:=Redits[0].Left;
        chk26.Left:=Redits[1].Left;
        chk27.Left:=Redits[2].Left;
        chk28.Left:=Redits[3].Left;
        chk25.Top:=Redits[0].Top+100;
        chk26.Top:=Redits[1].Top+100;
        chk27.Top:=Redits[2].Top+100;
        chk28.Top:=Redits[3].Top+100;
      end;
    end;

  finally
    nrf_dat_config.free;
  end;
end;
procedure   TForm_line.CommandIntClick(Sender:   TObject);
var str: string;
begin
  if InputQuery('Pls Enter Command!', '111', str) then
  begin        // str := InputBox('输入整数', '123', '000000');
     if str='' then str:='0';       //edt27.Text:=str;
     modbusfun06dat:=StrToInt(str);
     modbusfun06:=$1080;
  end;
end;
procedure   TForm_line.stepIntClick(Sender:   TObject);
var str,tip,demostr: string;
    ii,hh,jj,stepline:Integer;

  function HexChar(c: Char): Byte;
  begin
    case c of
      '0'..'9':  Result := Byte(c) - Byte('0');
      'a'..'f':  Result := (Byte(c) - Byte('a')) + 10;
      'A'..'F':  Result := (Byte(c) - Byte('A')) + 10;
    else
      Result := 0;
    end;
  end;

begin
   ii:=TEdit(Sender).Top+panel1.VertScrollBar.Position ;
   hh:=TEdit(Sender).Left+panel1.HorzScrollBar.Position ;
   StepNowFresh:=(ii-10)div strtoint(Form_line.edt121.Text);   //刷新开始行数
   stepline:=StepNowFresh+1;
   
   tip:='Enter Int输入<十进制> 或 <十六进制> 整数';demostr:= '1234 或 xABCD   :';
  
   if InputQuery(tip, demostr , str) then
   begin
     if ((hh+10) div 60)=10 then begin
      if str='' then str:='100';                //比例K默认100% ；2021-12-20改
     end else begin
      if str='' then str:='0';
     end;
     if tip='Enter Int输入十六进制数' //( ( (Copy(zu10edit[stepline].Text,2,2)='AC')or (Copy(zu10edit[stepline].Text,2,2)='NC') )and( (((hh+10) div 60)=7-1)or(((hh+10) div 60)=15-1) ) )
      //or( ( (Copy(zu10edit[stepline].Text,2,2)='YH') )and( (((hh+10) div 60)=3-1)or(((hh+10) div 60)=4-1)or(((hh+10) div 60)=15-1)or(((hh+10) div 60)=16-1) )  )
      //or( ( (Copy(zu10edit[stepline].Text,2,2)='NL') )and( (((hh+10) div 60)=15-1) )  )
     then begin
         if not IsHexNum(str)  then ShowMessage('err! pls enter number,错误！请输入16进制数字')
         else begin
           //HexChar('1');
           modbusfun06dat:=0;//StrToInt(StringReplace(str, '.', '', [rfReplaceAll]));
           for jj:=0 to Length(str) do begin
             if (str[jj]='x')or(str[jj]='X') then Continue;
             modbusfun06dat:=modbusfun06dat*16+HexChar(str[jj]);
           end;
           modbusfun06:=$3000+ ( (ii-10)div strtoint(Form_line.edt121.Text) )*32+((hh+10) div 60)+1;   //TEdit(Sender).Top  modbus地址加一，
           if (hh div 60)>9 then  modbusfun06:= modbusfun06+1;
           addToLog('第'+inttostr( (StrToInt(form_line.edt45.text)div 50)*50 +StepNowFresh +1 )+'步骤的第'+inttostr(((hh+10) div 60)+1)+'个【参数】修改为：'+str );
         end;
     end else begin
       if IsxFloatNum(str) then
          modbusfun06dat:=StrToInt(StringReplace(str, '.', '', [rfReplaceAll]));
       if(TEdit(Sender).Left=0) then begin
         if (str=' ')or(str='  ') then begin                       //序号填写一个空格:删略切换
              modbusfun06dat:=(StrToInt(form_line.edt45.text)div 50)*50 +StepNowFresh +1;
              modbusfun06:=$3000+( (ii-10)div strtoint(Form_line.edt121.Text) )*32;
              addToLog('第'+inttostr( (StrToInt(form_line.edt45.text)div 50)*50 +StepNowFresh +1 )+'步骤的第'+inttostr(((hh+10) div 60)+1)+'个【参数】修改为：'+inttostr(modbusfun06dat) );
         end else begin
           if not IsxFloatNum(str)  then ShowMessage('err! pls enter number,错误！请输入数字')
           else begin
              modbusfun06:=$3000+( (ii-10)div strtoint(Form_line.edt121.Text) )*32;
              addToLog('第'+inttostr( (StrToInt(form_line.edt45.text)div 50)*50 +StepNowFresh +1 )+'步骤的第'+inttostr(((hh+10) div 60)+1)+'个【参数】修改为：'+inttostr(modbusfun06dat) );
           end;
         end;
       end else  begin
         if not IsxFloatNum(str)  then ShowMessage('err! pls enter number,错误！请输入数字')
         else begin
           modbusfun06:=$3000+ ( (ii-10)div strtoint(Form_line.edt121.Text) )*32+((hh+10) div 60)+1;   //TEdit(Sender).Top  modbus地址加一，
           if (hh div 60)>9 then  modbusfun06:= modbusfun06+1;                                         //模式之后的参数
           addToLog('第'+inttostr( (StrToInt(form_line.edt45.text)div 50)*50 +StepNowFresh +1 )+'步骤的第'+inttostr(((hh+10) div 60)+1)+'个【参数】修改为：'+inttostr(modbusfun06dat) );
         end;
       end;
     end;
   end;

end;
procedure   TForm_line.sysIntClick(Sender:   TObject);
var
  str: string;
begin
  if InputQuery('Pls Enter Integer输入整数!', '123', str) then
  begin
     if str='' then str:='0';
     if not IsFloatNum(str)  then ShowMessage('err! pls enter number,错误！请输入数字')
     else begin
       modbusfun06dat:=StrToInt(StringReplace(str, '.', '', [rfReplaceAll]));
       modbusfun06:=$10A0+(TEdit(Sender).Left div 60)*1;
       SysExplainNum:=9 +(TEdit(Sender).Left div 60) ;
       addToLog('系统参数第'+inttostr( 1+ (TEdit(Sender).Left div 60))+'个【参数】，修改为：'+modbusfun16str );
       StepNowFresh:= VIRLCROWS-1;
     end;
  end;
 //ShowMessage(str); //显示输入的内容
end;
procedure   TForm_line.stepNameStrClick(Sender:   TObject);    //名称
var str: string;
    ii,hh:SmallInt;
begin

  if InputQuery('Pls Enter 4 char!', 'abAB', str) then
  begin //str := InputBox('输入4位字符', 'abcd', '0000');
     for ii:=1 to length(str) do begin
       if  Byte(str[ii]) >$7F then  str[ii]:='*';
     end;
     modbusfun16str:=Trim(str);//仅去除空格   AnsiUpperCase(str);
     modbusfun16len:=2;

     hh:=TEdit(Sender).Left+panel1.HorzScrollBar.Position ;
     modbusfun16:=$3000+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(Form_line.edt121.Text) )*32+((hh+10) div 60);
    
     StepNowFresh:=(TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(Form_line.edt121.Text);   //刷新开始行数
     addToLog('第'+inttostr( (StrToInt(form_line.edt45.text)div 50)*50 +StepNowFresh +1)+'步骤的【步骤名】修改为：'+modbusfun16str );
  end;

end;
procedure   TForm_line.stepModeStrClick(Sender:   TObject);    //模式
var str: string;
    ii,hh:SmallInt;
begin

  if InputQuery('Pls Enter 4 char!', 'AB', str) then
  begin //str := InputBox('输入4位字符', 'abcd', '0000');
     for ii:=1 to length(str) do begin
       if  Byte(str[ii]) >$7F then  str[ii]:='*';
     end;
     if length(str)<4 then str :=str+'0';       //补足4个字符；
     if length(str)<4 then str :=str+'0';
     if length(str)<4 then str :=str+'0';
     if length(str)<4 then str :=str+'0';

     modbusfun16str:=AnsiUpperCase(str);
     modbusfun16len:=2;
     hh:=TEdit(Sender).Left+panel1.HorzScrollBar.Position ;
     modbusfun16:=$3000+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(Form_line.edt121.Text) )*32+((hh+10) div 60)+1;

     StepNowFresh:=(TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(Form_line.edt121.Text);   //刷新开始行数
     addToLog('第'+inttostr( (StrToInt(form_line.edt45.text)div 50)*50 +StepNowFresh +1)+'步骤的【模式】修改为：'+modbusfun16str );
  end;

end;

procedure   TForm_line.stepNoteClick(Sender:   TObject);
  var str: string;
begin
  if InputQuery('Pls Enter 16 char!', 'ab中文', str) then
  begin
     modbusfun16str:=str;      //ZU17EDIT
     modbusfun16len:=$08;
     modbusfun16:=$3012+((TEdit(Sender).Top+panel1.VertScrollBar.Position-10) div strtoint(Form_line.edt121.Text) ) *32 ;
     StepNowFresh:=(TEdit(Sender).Top+panel1.VertScrollBar.Position-10) div strtoint(Form_line.edt121.Text);   //刷新开始行数
     addToLog('第'+inttostr( (StrToInt(form_line.edt45.text)div 50)*50 +StepNowFresh+1 )+'步骤的[备注]修改为：'+modbusfun16str );
  end;
end;
procedure   TForm_line.ExpertClick(Sender:   TObject);

begin
  if(Readchis('Result','top5')<>'0') then begin
    StepNowFresh:=(TEdit(Sender).Top+panel1.VertScrollBar.Position-10) div strtoint(Form_line.edt121.Text);   //刷新开始行数
  //ExpertNotes( (StrToInt(form_line.edt45.text)div 50)*50 +StepNowFresh,0 );
  end;
end;
procedure   TForm_line.sysStrClick(Sender:   TObject);
var str: string;

begin
  if InputQuery('Pls Enter 6 char!', 'ab中文', str) then
  begin
     modbusfun16:=$10A0+(TEdit(Sender).Left div 60)*1;
     SysExplainNum:=9 +(TEdit(Sender).Left div 60) ;
     modbusfun16str:=Trim(str);//仅去除空格，不改变小写字符   AnsiUpperCase(str);
     modbusfun16len:=3;
     addToLog('系统参数[机种名]修改为：'+modbusfun16str );
     StepNowFresh:= VIRLCROWS-1;
  end;
end;

procedure TForm_line.Comm1ReceiveData(Sender: TObject; Buffer: Pointer;
  BufferLength: Word);
var temp :byte;
    i:Integer;

    lanrbuf: array of Char;
begin        //串口接收到数据
  temp:=0;

  viewstring:='';
  if not rb4.Checked then       //非ASCII格式显示
    viewstring:='  receivie:'+viewstring;
  move(buffer^,pchar(@rbuf)^,bufferlength);
  if form_line.chk38.Checked then
    viewstring:=GetTimemS+viewstring;
  for i:=1 to bufferlength do
  begin
    if rb4.Checked then
      viewstring:=viewstring+char(rbuf[i])
    else
      viewstring:=viewstring+inttohex(rbuf[i],2)+' ';
    temp:=temp+1;
    if temp=100 then
    begin
      temp:=0;
      if not rb5.Checked then
        form_line.Memo1.Lines.add(viewstring);
      viewstring:='';
    end;
  end;
  if not rb5.Checked then
    form_line.Memo1.Lines.add(viewstring);
  if(rbuf[2]=$05)and(rbuf[3]=$00)and( (rbuf[4]=$10)or(rbuf[4]=$11) )      //复位启动后面跟F9的话！
    and(rbuf[10]=$03) and(rbuf[11]=$F9)and(bufferlength>11) then begin
      for i:=9 to bufferlength do begin                                   //扔掉前8个字节！
        rbuf[i-8]:=rbuf[i];
      end;
  end;
  rxdprocess(bufferlength,0);
  usb_busyT:=1;           //必须为1以下，否则刷新很慢；
end;

procedure TForm_line.SpeedButton12Click(Sender: TObject);
begin
  try
      Comm1.CommName := combobox1.Text;
      Comm1.BaudRate := strtoint(combobox2.Text);
      form_line.SpeedButton12.Enabled:=false;     btn112.Enabled:=False;     //runing...
      form_line.SpeedButton13.Enabled:=true;      btn113.Enabled:=true;
      form_line.Comm1.StartComm;
      sleep(20);
      shp2.Brush.Color := clGreen;         shp4.Brush.Color := clGreen;    //开启串口助手标志
      //Form_line.tmr1.Enabled:=True;
      Stat1.Panels[0].Text := 'Port:'+combobox1.Text;
      Stat1.Panels[1].Text :='Baut'+combobox2.Text + 'bps';
  except
      form_line.Comm1.StopComm;
      form_line.SpeedButton13.Enabled:=false;  btn113.Enabled:=False;
      form_line.SpeedButton12.Enabled:=true;   btn112.Enabled:=True;
      shp2.Brush.Color := clSilver;            shp4.Brush.Color := clSilver;
      messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);   //     fswitch.StatusBar1.Panels[4].Text:='通讯状态：错误';
  end;
end;

procedure TForm_line.SpeedButton13Click(Sender: TObject);
begin
   form_line.chk18.Checked:=False;
   form_line.SpeedButton13.Enabled:=false;  btn113.Enabled:=False;
   form_line.SpeedButton12.Enabled:=true;   btn112.Enabled:=true;
   form_line.Comm1.StopComm;
   shp2.Brush.Color := clSilver;     shp4.Brush.Color := clSilver;
   //Form_line.tmr1.Enabled:=False;
end;
//////////////定时发送串口指令，唯一发送命令入口--------------------------------------------------------------
procedure TForm_line.tmr1Timer(Sender: TObject);
var temp8 :byte;
    btemp:Array[0..3] of byte;
    save_file_name,SLINE_head,str:string;  //,s
    Ansi_s,AnsiSLINE_head:String;
  //  strs:array [0..32] of string; //
    strs:TStrings;
    i,j,temp16,line:Integer;
begin

    if( (FormLoad.Showing                                        //加载界面，可以后台执行导入步骤
         or( (TabSheet4.Showing  and Form_line.Active)            //debug界面
              or (modbusfun16>0)or (modbusfun16int>0) or (modbusfun02>0)or   (modbusfun03>0) or (modbusfun05>0)or (modbusfun06>0) )  //命令可用，可同时启用，单需要借用SBUF的命令优先级高！
        )
      and( (  ( Assigned(CurrentDevice)  //使能usb调试
              or not SpeedButton12.Enabled  )    //串口设备可用
              and (usb_busyT=0)         //设备不忙 ，通信时延自适应
           )
           or (modbusfuntion=$44)or(modbusfuntion=$54)           //通信错误，提示信息；
         )
      ) then
    begin
      if FormLogHead.Showing and (modbusfun16int<>$0021) then
        FormLogHead.Close;
      DebugWin_txdProcess(0);

    end else begin
       sbuf[1]:=1;
    end;
end;

procedure TForm_line.tmr2Timer(Sender: TObject);
var j:SmallInt;
begin
  if edt45.Focused then edt45.Color:=clSkyBlue else edt45.Color:=clWindow;
   mmo28Click(Self);
  if xyout[00].Left<>zu1edit[1].Left then begin
      //if zu1edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[00].Left:=zu1edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[00].Left:=2000;
      //if zu2edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[01].Left:=zu2edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[01].Left:=2000;
      //if zu3edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[02].Left:=zu3edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[02].Left:=2000;
      //if zu4edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[03].Left:=zu4edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[03].Left:=2000;
      //if zu5edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[04].Left:=zu5edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[04].Left:=2000;
      //if zu6edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[05].Left:=zu6edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[05].Left:=2000;
      //if zu7edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[06].Left:=zu7edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[06].Left:=2000;
      //if zu8edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[07].Left:=zu8edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[07].Left:=2000;
      //if zu9edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[08].Left:=zu9edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[08].Left:=2000;
      //if zu10edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[09].Left:=zu10edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[09].Left:=2000;
      //if zu11edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[10].Left:=zu11edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[10].Left:=2000;
      //if zu12edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[11].Left:=zu12edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[11].Left:=2000;
      //if zu13edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[12].Left:=zu13edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[12].Left:=2000;
      //if zu14edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[13].Left:=zu14edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[13].Left:=2000;
      //if zu15edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[14].Left:=zu15edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[14].Left:=2000;
      //if zu16edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[15].Left:=zu16edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[15].Left:=2000;
      //if zu17edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[16].Left:=zu17edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[16].Left:=2000;
      //if zu18edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[17].Left:=zu18edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[17].Left:=2000;
      //if zu19edit[1].Left>=form_line.panel1.HorzScrollBar.Position then
      xyout[18].Left:=zu19edit[1].Left;//-form_line.panel1.HorzScrollBar.Position;// else xyout[18].Left:=2000;
  end;
  if isvnsgmntclck1.Enabled then begin
    if isvnsgmntclck1.Tag>0 then begin           //0.5秒一次中断；两次中断计数一次；
      if isvnsgmntclck1.Seconds>=59 then begin
        if isvnsgmntclck1.Minutes<59 then begin
          isvnsgmntclck1.Seconds:=0;
          isvnsgmntclck1.Minutes:=isvnsgmntclck1.Minutes+1;
        end;
      end else begin
        isvnsgmntclck1.Seconds:=isvnsgmntclck1.Seconds+1;
      end;
      isvnsgmntclck1.Tag:=0;
    end else
      isvnsgmntclck1.Tag:=isvnsgmntclck1.Tag+1;
  end;
  if not FormPN.Showing then  begin
    Form_line.Caption := Readchis('Comm Para', 'Company')+'-'+lbl90.hint;

    Form_line.Caption :=Form_line.Caption +'-AT'+Label9.Caption
                          +'     '+datetostr(Date)+'    '+TimeToStr(Time)
                          +'     -     '+form_line.lbl29.Caption
                          +'     -     '+form_line.lbl28.Caption
                          ;


    if (barcodeSelT>0) and TabSheet2.TabVisible then
    begin
      if (barcodeSelT=1) and (TabSheet2.Showing) then    //条码输入时间到，设置全选中状态以便重新输入条码！
      begin
        if (Label7.hint='11') {and edit1.Enabled and grp36.Visible} then
          if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then  edit1.SetFocus ;   //双条码框
        edit1.SelectAll;
      end;
      if (Label7.hint<>'11')
        or (trim(medit1.Text)<>'')    //双条码模式，条码非空    //or ( length( trim(medit1.Text) ) >=strtoint(edt55.text) )       //满足最小长度
      then      //双产品时 且第二个条码不为空
        barcodeSelT:=barcodeSelT-1;
    end;
  end;
   ///////////////////////////PASS NG显示超时 --------------------------------------------------------
  if PassOverT=1 then
  begin
    if  Trim(Label11.Caption)='PASS' then                         //不适用双工位同步系统
    begin
      Label11.Font.Color:=clWindowText;
      Label11.Caption:='Ready'; Label11.Font.Size:=150;
      Lbl169.Caption:=''; lbl169.Font.Size:=10;
      if (strtoint(edt59.Text)>0) and (strtoint(edt60.Text)>0) then
      begin
        for J := 0 to StrToInt(edt59.Text)-1 do
          begin
          for I := 0 to StrToInt(edt60.Text)-1 do
            with Rleds[J*StrToInt(edt60.Text) + I] do
            begin
              Active:=False;
              ActiveColor:=clGreen;
              //AutoInactiveColor:=False;
            end;
          end;
      end;
    end;
  end;
  if PassOverT>0 then PassOverT:=PassOverT-1;

  if NGOverT=1 then
  begin
    if  (Trim(Label11.Caption)='NG') or (Trim(Label11.Caption)='FAIL')then    //不适用双工位同步系统
        begin
          Label11.Font.Color:=clWindowText;
          Label11.Caption:='Ready';    Label11.Font.Size:=150;
          Lbl169.Caption:='';lbl169.Font.Size:=10;
          if (strtoint(edt59.Text)>0) and (strtoint(edt60.Text)>0) then
          begin
            for J := 0 to StrToInt(edt59.Text)-1 do
              begin
              for I := 0 to StrToInt(edt60.Text)-1 do
                with Rleds[J*StrToInt(edt60.Text) + I] do
                begin
                  Active:=False;
                  ActiveColor:=clGreen;
                  //AutoInactiveColor:=False;
                end;
              end;
          end;
        end;
  end;
  if NGOverT >0 then NGOverT:=NGOverT-1;
///////////----不常用-----------------////////////////数据文件备份服务器映射盘，如果时间（分钟）到则自动备份上传！
  UploadSerT:=UploadSerT+1;
  if (UploadSerT>=60*StrToInt(edt6.Text)) and (StrToInt(edt6.Text)>0) and (Label10.caption<>'Label10')
  then  begin
    UploadSerT:=0;
    lbl181.Caption:='1';btn2Click(self);
  end;
end;

//FCT5不能上传测试状态，不能上传Loading数据 ，必须在PC软件设置如下区别
//设置CommOpen=5，从而禁止由主板数据更新Log文件头几行，禁止Loading数据，禁止读取长机种名，
//设置PassOverT=5,从而实现PC定时复位结果标志；
procedure TForm_line.btn1Click(Sender: TObject);         //保存参数
var
  nrf_dat_config: Tinifile;
  ini_path: string;
begin
  try
    ini_path := ExtractFilePath(application.ExeName) + 'config.ini';
    nrf_dat_config := Tinifile.Create(ini_path);
  //------------------------------------[Result]组机种参数---------------------------------------------------------------------
    if nrf_dat_config.Readstring('result', 'PassOverT', '')='' then begin    //默认
      edt40.Text:= '0';
      nrf_dat_config.WriteString('result', 'PassOverT',edt40.Text );     //FCT5主板必须由上位机通过超时来关闭显示结果；
    end;
    if nrf_dat_config.Readstring('result', 'NGOverT', '')='' then begin    //默认
      edt42.Text:= '0';
      nrf_dat_config.WriteString('result', 'NGOverT', edt42.Text);
    end;

  //------------------------------------[Server]组机种参数---------------------------------------------------------------------
    nrf_dat_config.WriteString('Server', '*', '*');

  //------------------------------------[Comm Para]组机种参数---------------------------------------------------------------------
    //Stat1.Panels[0].Text := 'Comm'+combobox1.Text;   //Stat1.Panels[1].Text := 'Baut'+combobox2.Text + 'bps';
    //add by sunny  写Ini档通讯参数选择&误差设定      
    Comm1.CommName := combobox1.Text;
    Comm1.BaudRate := strtoint(combobox2.Text);
  //  nrf_dat_config.WriteString('单位信息', '单位', Label5.Caption);
    nrf_dat_config.WriteString('Comm Para', 'Path', edt2.Text);
    nrf_dat_config.WriteString('Comm Para', 'CommOpen', groupbox1.hint);          //保存串口使能位；

    if GroupBox1.Visible then begin
      nrf_dat_config.WriteString('Comm Para', 'Port', ComboBox1.Text);
      nrf_dat_config.WriteString('Comm Para', 'Baud', ComboBox2.Text);
    end;

    if nrf_dat_config.Readstring('Comm Para', 'Uplocal', '')='' then begin    //默认
      edt38.Text:='1';
      nrf_dat_config.WriteString('Comm Para', 'Uplocal', edt38.Text);
    end;
    if Readchis('Comm Para','ChildFolder')='' then begin
      edt62.Text:='2';
      nrf_dat_config.WriteString('Comm Para', 'ChildFolder', '2');    //没有设置过则默认：2
    end;
    if nrf_dat_config.Readstring('Comm Para', 'DebugMode', '')='' then begin    //默认
      lbl79.Color:=clGray;//edt61.Text:='1';
      nrf_dat_config.WriteString('Comm Para', 'DebugMode', '1');
    end;

    if edt1.Text<>'' then begin
      nrf_dat_config.WriteString('Comm Para', 'PathServer', edt1.Text);
      nrf_dat_config.WriteString('Comm Para', 'UploadServerT', edt6.Text);
    end;
    if edt135.Visible then
      nrf_dat_config.WriteString('Comm Para', 'PathSNfile', edt135.Text);
    if edt157.Visible then
      nrf_dat_config.WriteString('Comm Para', 'TeslaSNPath', edt157.Text);
  //------------------------------------[Name->Redef]组机种参数---------------------------------------------------------------------
    nrf_dat_config.WriteString('Name->Redef', '*', '*');
  //------------------------------------[Model Para]组机种参数---------------------------------------------------------------------
    nrf_dat_config.WriteString('Model Para', '*', '*');
    if Readchis('Model Para','BarCodeEdit')<>'' then begin           //启用条码自锁功能
      if (StrToInt('0'+trim(Form_line.edt82.Text) ) >0)and(StrToInt('0'+trim(Form_line.edt82.Text) ) <99) then begin     //enPN>0,通用！
        nrf_dat_config.WriteString(FormPN.cbb1.Text,'BarCodeLen',edt134.Text);
      end else
      begin
        nrf_dat_config.WriteString('Model Para','BarCodeLen',edt134.Text);
      end;
      edt7.Text:=edt134.Text;            //不需锁死 edt134.Enabled:=False;           //修改完条码后锁死编辑框；
    end;
  except
  end;
  
  nrf_dat_config.free;
  btn1.Font.Color:=clDefault;
end;

const  SELDIRHELP   =   1000;
procedure TForm_line.btn3Click(Sender: TObject);         //选择文件夹
var 
    Dir:   string;
begin
    Dir   :=  '';
    if   SelectDirectory(Dir,   [sdAllowCreate,   sdPerformCreate,   sdPrompt],SELDIRHELP)   then
    begin
        edt2.Text   :=   Dir+'\';
        btn1.Font.Color:=clRed;
        Label10.Caption:='Label10';
        lbl90.hint:='modal';
    end;
end;


procedure TForm_line.btn12Click(Sender: TObject);
var ii:SmallInt;
begin
  ts15.Show;
  if(VIRLCROWS>=50) then begin
    for ii:=1 to 50 do begin        //颜色还原
      zu1edit[ii].Color:=clWhite;
      zu2edit[ii].Color:=clWhite;
      zu3edit[ii].Color:=clWhite;

      zu5edit[ii].Color:=clWhite;
      zu6edit[ii].Color:=clWhite;
      zu7edit[ii].Color:=clWhite;
      zu8edit[ii].Color:=clWhite;
      zu9edit[ii].Color:=clWhite;
      zu10edit[ii].Color:=clWhite;
      zu11edit[ii].Color:=clWhite;
      zu12edit[ii].Color:=clWhite;
      zu13edit[ii].Color:=clWhite;
      zu14edit[ii].Color:=clWhite;
      zu15edit[ii].Color:=clWhite;
      zu16edit[ii].Color:=clWhite;

    end;
  end;
  modbusfun05:=13; modbusfun05dat:=$FF;
end;

procedure TForm_line.btn13Click(Sender: TObject);
var ii:SmallInt;
begin
  ts15.Show;
  if(VIRLCROWS>=50) then begin
    for ii:=1 to 50 do begin        //颜色还原
      zu1edit[ii].Color:=clWhite;
      zu2edit[ii].Color:=clWhite;
      zu3edit[ii].Color:=clWhite;

      zu5edit[ii].Color:=clWhite;
      zu6edit[ii].Color:=clWhite;
      zu7edit[ii].Color:=clWhite;
      zu8edit[ii].Color:=clWhite;
      zu9edit[ii].Color:=clWhite;
      zu10edit[ii].Color:=clWhite;
      zu11edit[ii].Color:=clWhite;
      zu12edit[ii].Color:=clWhite;
      zu13edit[ii].Color:=clWhite;
      zu14edit[ii].Color:=clWhite;
      zu15edit[ii].Color:=clWhite;
      zu16edit[ii].Color:=clWhite;

    end;
  end;
  modbusfun05:=14; modbusfun05dat:=$FF;
end;

procedure TForm_line.btn14Click(Sender: TObject);
begin
  btn16.Tag:=5;
  if btn14.Font.Color=clRed then begin
    modbusfun05:=8;  modbusfun05dat:=$FF;
    addToLog('SaveSys,保存系统参数' );
  end else if btn16.Font.Color=clRed then begin
    modbusfun05:=11; modbusfun05dat:=$FF;
    addToLog('SaveStep,保存当前页步骤' );
  end else begin
    modbusfun05:=8;  modbusfun05dat:=$FF;
    addToLog('SaveSys,保存系统参数' );
  end;
end;

procedure TForm_line.btn15Click(Sender: TObject);
begin
  btn15.Tag:=2;       //单步按下，用于working...是单步还是自动
   modbusfun05:=12; modbusfun05dat:=$FF;
end;

procedure TForm_line.chk1Click(Sender: TObject);
begin
  if not chk1.Focused then Exit;      //只有鼠标点击触发
  SysExplainNum:=1;//-1; btn141Click(Self);
  btn14.SetFocus;
  modbusfun05:=32;
  if chk1.Checked then modbusfun05dat:=$FF else modbusfun05dat:=0;
end;

procedure TForm_line.chk2Click(Sender: TObject);
begin
  if not chk2.Focused then Exit;
  SysExplainNum:=2;//-1; btn141Click(Self);
  btn14.SetFocus;
  modbusfun05:=33;
  if chk2.Checked then modbusfun05dat:=$FF else modbusfun05dat:=0;
end;

procedure TForm_line.chk3Click(Sender: TObject);
begin
  SysExplainNum:=3;//-1; btn141Click(Self);
  modbusfun05:=34;
  if chk3.Checked then modbusfun05dat:=$FF else modbusfun05dat:=0;
end;

procedure TForm_line.chk4Click(Sender: TObject);
begin
  SysExplainNum:=4;//-1; btn141Click(Self);
  modbusfun05:=35;
  if chk4.Checked then modbusfun05dat:=$FF else modbusfun05dat:=0;
end;

procedure TForm_line.btn17Click(Sender: TObject);
begin
     modbusfun06dat:=StrToInt('0'+edt45.Text)+1;
     modbusfun06:=$10B0;

end;

procedure TForm_line.btn18Click(Sender: TObject);
begin

   modbusfun06dat:=StrToInt('0'+edt45.Text);
   if   modbusfun06dat>0 then modbusfun06dat:=modbusfun06dat-1;
   modbusfun06:=$10B0;
end;



procedure TForm_line.btn20Click(Sender: TObject);
begin
    modbusfun16int:=$5000;
    modbusfun16len:=StrToInt(edt5.Text);
    //sbuf[1]:=1;
    sbuf[2]:=$03;
    sbuf[3]:=$50;   sbuf[4]:=$00;
    sbuf[5]:=$00;   sbuf[6]:=StrToInt(edt5.Text);

end;

procedure TForm_line.btn21Click(Sender: TObject);
begin
  modbusfun02:=$5000;

  sbuf[2]:=$02;   sbuf[3]:=$50;
  sbuf[4]:=$00;    sbuf[5]:=$00;
  sbuf[6]:=$20;
end;

procedure TForm_line.BitBtn1Click(Sender: TObject);
var str,password:string;
    nrf_dat_config: Tinifile;
    ini_path: string;
begin
    ini_path := ExtractFilePath(application.ExeName) + 'config.ini';
    try
       nrf_dat_config := Tinifile.Create(ini_path);    //关联INI文件

       if nrf_dat_config.Readstring('Comm Para', 'PassWord', '')<>'' then      //不为空
        begin
          password:=nrf_dat_config.Readstring('Comm Para', 'PassWord', '');
        end
        else password:='999';
    finally
      nrf_dat_config.free;
    end;

   if InputQuery('LogIn Window!', 'Pls Enter Password', str) then
   begin
     if //(inputbox('登录窗口’,'请输入账号','')='MyName') and (inputbox('登录窗口','请输入密码','')='999')
          (Str=password) or (Str='137760') then//'999' then
     begin
        Showmessage('Debug Window Open!');
        if lbl79.Color=clgreen then         //不能回到监控界面
        begin
            TabSheet2.TabVisible:=False;    //不用关闭组监控界面 TabSheet3.TabVisible:=False;
        end;
        TabSheet4.TabVisible:=true;
        ts3.TabVisible:=True;
        TabSheet4.Enabled:=True;
        PageControl1.TabIndex:=3-1;
        N9.Enabled:=True;
        N10.Enabled:=TRUE;
        N50.Enabled:=True;
        N51.Enabled:=True;
        A1.Enabled:=True;

     end
     else   begin
        Showmessage('Enter Error！');
        TabSheet4.TabVisible:=False;
        ts3.TabVisible:=False;
        TabSheet4.Enabled:=False;
         N9.Enabled:=False;
         N10.Enabled:=False;
         N50.Enabled:=False;
         N51.Enabled:=False;
         A1.Enabled:=False;
          // Application.Terminate;
     end;
   end;
end;

procedure TForm_line.btn24Click(Sender: TObject);
var
  f: TextFile;
  save_file_name,SLINE_head,s: string;
begin
  ts23.Show;                        //显示checklist项目；

  if (StrToInt('0'+sysedit[5].Text)=0) or (working >0) then      //没有读到版本号，不执行
    begin
      if(working >0) then  lbl28.Caption:='MCU is busying, Pls press RESET!请按复位再导出'
      else lbl28.Caption:='Pls retry to click!请重新上电，重启软件试一次！';
    end
  else begin
    if dlgOpen1.Execute then
    begin
      lbl28.Caption:='Start Export Data to PC';
      lbl28.Font.Color:=clRed;
      if btn24.Tag >100 then begin         //读取到最大步骤数
        sprgrsbr1.Max:= (btn24.Tag*100) div 90;
      end else
        sprgrsbr1.Max:=100;
      sprgrsbr1.Position:=0;

      if ExtractFileExt(dlgOpen1.FileName)='' then     //没有扩展名，则默认加上 .txt
        save_file_name := dlgOpen1.FileName+'.txt'
      else
        save_file_name := dlgOpen1.FileName;
      lbl29.Caption:=save_file_name;
      try
        assignfile(f, save_file_name);
        rewrite(f); Sleep(50);
        //application.ProcessMessages;
        SLINE_head:='';
        s := '序号N';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '0'+sysedit[5].Text;//改为版本号<-'名称'  ,导入文件时会判断，不能含字母
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
         s := '标准值V';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '测控值S';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '上限Up%';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '下限Lo%';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '高点H';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '低点L';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '前延时T';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '模式M';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
         s := '比例K%';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '偏移B';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '平均P次';//上限%2';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '联板号L';//下限%2';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
         s := '高点H2';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := '低点L2';
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;
        s := 'Notes备注->>>';//   +'>>>>>';    //zu17edit
        //if (pos(',', s) <> 0) or (pos('"', s) <> 0) then s := AnsiQuotedStr(s, '"');
        s := s + #9;
        SLINE_head := SLINE_head + s;

        s := '固件:v'+edt130.Text;
        s := s + #9;
        SLINE_head := SLINE_head + s;


        
        writeln(f, SLINE_head);
        modbusfuntion:=$3F;
      finally
        Closefile(f);
      end;
    end else begin
      A1.Checked:=False;    //停止所有机种导入
      N50.Checked:=false;
      N51.Checked:=false;
    end;
  end;
end;

procedure TForm_line.btn23Click(Sender: TObject);
var
  f: TextFile;
  save_file_name: string;
  TxtFile:TStringList;
begin
  if (StrToInt('0'+sysedit[5].Text)=0) or (working >0) then begin     //没有读到版本号，不执行

      if(working >0) then  lbl28.Caption:='MCU is busying, Pls press RESET!请按复位再导入'
      else     lbl28.Caption:='Pls retry to click!请重新上电，重启软件试一次！';
    
  end else  if dlgOpen1.Execute then begin
    lbl28.Caption:='Start Import Data to MCU';
    lbl28.Font.Color:=clRed;
    sprgrsbr1.Position:=0;

    save_file_name := dlgOpen1.FileName;
    TxtFile:=TStringList.Create;
    TxtFile.LoadFromFile(save_file_name);
    sprgrsbr1.Max:=1+TxtFile.Count;
    sprgrsbr1.Position:=0;
    FormLoad.sprgrsbr1.Max:=1+TxtFile.Count;
    FormLoad.sprgrsbr1.Position:=0;
    TxtFile.Destroy;

    lbl29.Caption:=save_file_name;
    try
      assignfile(f, save_file_name);
      //rewrite(f);
      Reset(f);
      //application.ProcessMessages;
      modbusfuntion:=$4F;
    finally
      Closefile(f);
    end;
  end else begin
    A1.Checked:=False;    //停止所有机种导入
    N50.Checked:=false;
    N51.Checked:=false;
  end;
end;

procedure TForm_line.TabSheet2Show(Sender: TObject);   //基本监控、测试界面
begin
  if(Readchis('Result','top5')='0')  then Chart1.Hide;
  //2022-1-8关闭，操作不方便 if shp4.Brush.Color = clGreen  then SpeedButton13Click(Self);  //自动关闭串口助手
//if TabSheet4.TabVisible then
  ts1.Hide;     //comm报错会默认ts1.showing
  if StrToInt(edt75.Text)>0 then
    GpcResult.TabIndex:=1
  else
    GpcResult.TabIndex:=0;
  if(TabSheet2.Showing)and edit1.Enabled and grp36.Visible  then    Edit1.SetFocus;

  tmr3.Enabled:=true;       //定时读取主板信息，以及Result界面显示  //通过定时器执行不同的开机读取指令      ，可启动//读上限下限 及短路群
  if groupbox1.hint<>'5' then begin         //FCT5不支持加载和读取长文件名  //Label11.Font.Color:=clWindowText;label11.Caption:='Loading...'; Label11.Font.Size:=150;
    Lbl169.Caption:='';lbl169.Font.Size:=10;
  end;
  if Form_line.sbtbtn1.Visible then
    Form_line.sbtbtn1.Enabled:=False;
  //移动到tmr3中断内<-- tmr4.Enabled:=true; tmr4.Interval:=2000+1;      //光标定位在条码框
end;

procedure TForm_line.TabSheet4Show(Sender: TObject);
begin

  N13.Visible:=True;
  S2.Visible:=True;
  x1.Visible:=True;
  N15.Visible:=True;
  N15.Enabled:=True;
  
  N13.Enabled:=True;
  S2.Enabled:=True;
  x1.Enabled:=True;

  N9.Enabled:=True;   //导入
  N10.Enabled:=True;   //导出
  N50.Enabled:=True;
  N51.Enabled:=True;
  A1.Enabled:=True;

  grp3.Color:=clBtnFace;
  Panel1.Color:=clBtnFace;
  //zu1edit[1].SetFocus;
  if btn13.Enabled then btn13.setfocus;
  if edt45.Enabled then edt45.setfocus;

  pgc1.TabIndex:=0;        //默认：显示系统参数卡；
end;

procedure TForm_line.btn2Click(Sender: TObject);    //备份到服务器
begin
  bakToServerDisk();
end;

procedure TForm_line.btn4Click(Sender: TObject);
var 
    Dir:   string;
begin
    Dir   :=  '';
    if   SelectDirectory(Dir,   [sdAllowCreate,   sdPerformCreate,   sdPrompt],SELDIRHELP)   then
    begin
        edt1.Text   :=   Dir+'\';
        btn1.Font.Color:=clRed;
        edt1.Color:=clWindow;
    end;
end;

procedure TForm_line.FormClose(Sender: TObject; var Action: TCloseAction);
var
    handle2:Thandle;
    f: TextFile;
    save_file_name,ss: string;
    shandle:PAnsiChar;
    var MyHandle:THandle;  
    function FindTask(ExeFileName: string): Boolean;     //查找正在运行的进程
    const
      PROCESS_TERMINATE=$0001;
    var
      ContinueLoop: BOOL;
      FSnapshotHandle: THandle;
      FProcessEntry32: TProcessEntry32;
    begin
      result := False;
      FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
      FProcessEntry32.dwSize := Sizeof(FProcessEntry32);
      ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);
      while integer(ContinueLoop) <> 0 do
      begin
      if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) = UpperCase(ExtractFileName(ExeFileName))) or
        (UpperCase(FProcessEntry32.szExeFile) = UpperCase(ExeFileName))) then
      begin
        Result := True;
        Break;
      end;
        ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
      end;
      CloseHandle(FSnapshotHandle);
    end;

    function KillTask(ExeFileName: string): Integer;   //关闭正在运行的进程
    const
      PROCESS_TERMINATE = $0001;
    var
      ContinueLoop: BOOL;
      FSnapshotHandle: THandle;
      FProcessEntry32: TProcessEntry32;
    begin
      Result := 0;
      FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
      FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
      ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);

      while Integer(ContinueLoop) <> 0 do
      begin
        if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) =UpperCase(ExeFileName))
          or (UpperCase(FProcessEntry32.szExeFile) = UpperCase(ExeFileName)) ) then
          Result := Integer(TerminateProcess(OpenProcess(PROCESS_TERMINATE,BOOL(0),FProcessEntry32.th32ProcessID), 0));
        ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
      end;
      CloseHandle(FSnapshotHandle);
    end;
begin

  if (Readchis('result', 'CloseReport')<>'1')and(Readchis('result', 'CloseReport')<>'11') then begin
      if (grp1.Tag<>2) and (StrToInt('0'+edt38.text)mod 10 >0) then //tesla和jiahe不保存
        SaveCsvReport(0);
  end;

  if (StrToInt('0'+edt6.Text)>0)and (Label10.caption<>'Label10') then      //如果上传映射服务器，则在关闭时上传一次
  begin
    lbl181.Caption:='10';btn2Click(self);
  end;

  if(Readchis2('Comm Para','UpLocal')<>'')then begin                    //关闭其它从属程序；
    if FindTask('2calline.exe') then
      KillTask('2calline.exe');
    if FindTask('3calline.exe') then
      KillTask('3calline.exe');
    if FindTask('4calline.exe') then
      KillTask('4calline.exe');
    if FindTask('5calline.exe') then
      KillTask('5calline.exe');
    if FindTask('6calline.exe') then
      KillTask('6calline.exe');
    if FindTask('7calline.exe') then
      KillTask('7calline.exe');
    if FindTask('8calline.exe') then
      KillTask('8calline.exe');

    if FindTask('calline.exe') then
      KillTask('calline.exe');
  end;
end;


procedure TForm_line.FormShow(Sender: TObject);
var temp:byte;
    i:SmallInt;
    nrf_dat_config: Tinifile;
    save_file_name,ini_path,sst: string;
begin                   //显示才处理配置文件相关

  ReadConfig_iniProcess(0);                  //读取总的机种参数

  //--------串口自动识别------
  zerocount:=0;
  Comm1.CommName := combobox1.Text;
  Comm1.BaudRate := strtoint(combobox2.Text);
  if StrToInt(groupbox1.hint)>0 then  begin              //commopen使能
     try
        form_line.SpeedButton12.Enabled:=false;
        form_line.SpeedButton13.Enabled:=true;
        form_line.Comm1.StartComm;
        shp2.Brush.Color := clGreen; shp4.Brush.Color := clGreen;
        sleep(20);          
     except
       for temp:=1 to 9  do
       begin
         Comm1.CommName := 'com'+inttostr(temp);
         try
            form_line.SpeedButton12.Enabled:=false;  btn112.Enabled:=False;
            form_line.SpeedButton13.Enabled:=true;   btn113.Enabled:=true;
            form_line.Comm1.StartComm;
            shp2.Brush.Color := clGreen; shp4.Brush.Color := clGreen;
         except
            form_line.SpeedButton13.Enabled:=false;  btn113.Enabled:=False;
            form_line.SpeedButton12.Enabled:=true;   btn112.Enabled:=true;
            shp2.Brush.Color := clSilver;
         end;
         if shp2.Brush.Color = clGreen then
         begin
            combobox1.Text:=Comm1.CommName;
            cbb3.Text:= combobox1.Text;
            Break;
         end;
       end;
       if shp2.Brush.Color = clSilver then
          messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm1~9 is exist！仅支持COM1~COM9端口',mterror,[mbyes],0);   
     end;
     if shp2.Brush.Color = clSilver then
     begin
        ts1.Visible:=true;
        Stat1.Panels[0].Text :='Port:'+ 'COMM error!';
     end else begin
        Stat1.Panels[0].Text := 'Port:'+combobox1.Text;
        Stat1.Panels[1].Text :='Baut:'+combobox2.Text + 'bps';
     end;

  end else begin       //GroupBox1.Enabled:=False;
    GroupBox1.Visible:=False;
  end;

  if StrToInt(Form_line.edt82.Text)>=99 then begin         //可能用串口通信，必须放串口之后！//enPN jaguar等启用PC存放程序
    modbusfun06:=$0041;
    if FormPN.tag>0 then
        modbusfun06dat:=FormPN.tag-1
    else
        modbusfun06dat:=48-1;
    if edt82.Tag=102 then
       modbusfun06dat:=modbusfun06dat+$1000;               //双向加载校验复位信号
    form_line.tmr1Timer(form_line);
    Sleep(100);
  end;

  PNbarcode_fileCreateProcess(0);             //有调用Load和 PN界面显示，然后读取机种下面参数

  if Readchis('Comm Para','barcodeDisRepeat')='1' then
    Lbl140.Color:=clGreen
  else
    Lbl140.Color:=clGray;

  /////////////////因为tabsheet2显示会和生成文件冲突，所以移动到后面显示  ----------------------
  if Readchis('result','ParaSetsDisp')='1' then Form_line.grp31.Visible:=True;
  if lbl79.Color=clGray then    //debugMode，开机进入
  begin
      memo1.Visible:=True ;
      TabSheet4.TabVisible:=True;
      TabSheet2.TabVisible:=False;
      PageControl1.TabIndex:=3-1;       //调试界面
      grp3.Visible:=True;
  end else begin
    TabSheet4.TabVisible:=False;
  // -------------处理选项卡开机界面，不同场合开机界面不同----------------     ts9.TabVisible:=False;   //ts10.TabVisible:=False;
    begin
      TabSheet2.TabVisible:=True;
      PageControl1.TabIndex:=2-1;   //pcset界面<--监控界面
    end;
    memo1.Visible:=True;          //False;   //同学命令显示
    grp3.Visible:=True;           //False;      //OS 系统参数框
  end;
  //--------------232通信 基本参数---------------------------------------------
  row_num:=8;                                 //从第8行开始读
  rbuf_usbptr:=0; usb_busyT:=0; comandbusy:=0;

  //---------------------------------------------------------------------------
  if lbl79.Color=clGreen then    //非debug界面
  begin
    TabSheet2.TabVisible:=True;              //监控界面<--pcset界面
    PageControl1.TabIndex:=0;
  end;
  if edt53.hint='9000' then begin            //备用：二次开发
    P2exeFileName := ExtractFilePath(application.ExeName) +copy(ExtractFileName(application.ExeName),0,Pos('.exe',ExtractFileName(application.ExeName))-1)+'.a.exe' ;

    if FileExists(P2exeFileName) then  begin
      WinExec(PChar(P2exeFileName), SW_NORMAL); //P2exefile.Execute;
      if edt53.tag>=9002 then begin
        Stat1.Panels[3].Text :='主UDP=9000,从UDP='+IntToStr(edt53.Tag);
      end else
        Stat1.Panels[3].Text :='主UDP=9000,从UDP='+IntToStr(9001);
      Stat1.Tag:=9000;
    //  u4.Enabled:=True;
      tmr4.Enabled:=true;                  //定时：光标定位到debug界面
    end;
  end;

  if(Readchis2('Comm Para','UpLocal')<>'')then begin                            //先启动：此软件为第1工位（主软件） ;存在有其它工位；
    P2exeFileName := ExtractFilePath(application.ExeName) +'3\'+ExtractFileName(application.ExeName) ;  //'calline.exe';//IncludeTrailingBackslash(AppOptions.JFlashPath) + AppOptions.JFlashExe;
      {$WARN SYMBOL_PLATFORM ON}
    if not FileExists(P2exeFileName) then  begin
       P2exeFileName := ExtractFilePath(application.ExeName) +'3\3'+ExtractFileName(application.ExeName) ;
    end;
    if FileExists(P2exeFileName) then  begin
      writechis3('Comm Para', 'Path', edt2.Text);
      writechis3('Model Para','Operator',edt79.Text);
      writechis3('Model Para','BarCodeLen',edt134.Text);
      WinExec(PChar(P2exeFileName), SW_NORMAL); //P2exefile.Execute;
    end;

    P2exeFileName := ExtractFilePath(application.ExeName) +'4\'+ExtractFileName(application.ExeName) ;  //'calline.exe';//IncludeTrailingBackslash(AppOptions.JFlashPath) + AppOptions.JFlashExe;
      {$WARN SYMBOL_PLATFORM ON}
    if not FileExists(P2exeFileName) then  begin
       P2exeFileName := ExtractFilePath(application.ExeName) +'4\4'+ExtractFileName(application.ExeName) ;
    end;
    if FileExists(P2exeFileName) then  begin
      writechis4('Comm Para', 'Path', edt2.Text);
      writechis4('Model Para','Operator',edt79.Text);
      writechis4('Model Para','BarCodeLen',edt134.Text);
      WinExec(PChar(P2exeFileName), SW_NORMAL); //P2exefile.Execute;
    end;

    P2exeFileName := ExtractFilePath(application.ExeName) +'2\'+ExtractFileName(application.ExeName) ;  //'calline.exe';//IncludeTrailingBackslash(AppOptions.JFlashPath) + AppOptions.JFlashExe;
      {$WARN SYMBOL_PLATFORM ON}
    if not FileExists(P2exeFileName) then  begin
       P2exeFileName := ExtractFilePath(application.ExeName) +'2\2'+ExtractFileName(application.ExeName) ;
       if not FileExists(P2exeFileName) then  begin
          Application.MessageBox('No P2 software;没有第2工位软件！', PChar(Application.Title),
            MB_OK + MB_ICONWARNING);              // Exit;
       end;
    end;
    if FileExists(P2exeFileName) then  begin
      writechis2('Comm Para', 'Path', edt2.Text);
      writechis2('Model Para','Operator',edt79.Text);
      writechis2('Model Para','BarCodeLen',edt134.Text);
      WinExec(PChar(P2exeFileName), SW_NORMAL); //P2exefile.Execute;
    end;
  end;
end;

procedure TForm_line.btn25Click(Sender: TObject);
begin
  modbusfun05:=16; modbusfun05dat:=$FF;
end;

procedure TForm_line.btn26Click(Sender: TObject);                  //“复位”按钮
begin
  if (modbusfuntion=$50)or (modbusfuntion=$51)or (modbusfuntion=$52) then  modbusfuntion:=$53      //导入结束
  else if (modbusfuntion=$40)or (modbusfuntion=$41)or (modbusfuntion=$42) then  modbusfuntion:=$43 //导出结束
  else modbusfun05:=17; modbusfun05dat:=$FF;
end;

procedure TForm_line.chk5Click(Sender: TObject);
begin
  SysExplainNum:=5;//-1; btn141Click(Self);
  modbusfun05:=36;
  if chk5.Checked then modbusfun05dat:=$FF else modbusfun05dat:=0;

end;

procedure TForm_line.TabSheet2Hide(Sender: TObject);
begin
  GpcResult.Visible:=False;      //用于下次调用show函数
end;

procedure TForm_line.btn27Click(Sender: TObject);
begin
  PrintClickProcess(0);
end;


procedure TForm_line.tmr3Timer(Sender: TObject);
begin
  if TabSheet2.TabVisible then begin              //测试界面读取全部参数---参考基准
    GpcResult.Visible:=True;
    begin                   //通用用法!, //关闭上下限更新文件表头 if inportNewFileflag=0 then安捷利防止刷新原来的文件头格式----
      if Form_line.groupbox1.hint<>'5' then begin         //FCT5不支持加载和读取长文件名
        modbusfun06dat:=$00F6;                        //读上限下限 及短路群
        modbusfun06:=$0001;
        tmr4.Enabled:=true; tmr4.Interval:=2000+1;
      end;
    end;
    tmr3.Enabled:=False;    //tmr3之延时小于500，会导致上面的指令不能完全执行（枚举过程需要时间！）
    if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then       Edit1.SetFocus;
  end;
end;

procedure TForm_line.edt45Click(Sender: TObject);
var str:string;
begin
  if InputQuery('Enter Int', '1234', str) then begin
     if str='' then str:='0';
     if StrToInt(str)>4980 then  modbusfun06dat:=4980     //最大第980步
     else modbusfun06dat:=StrToInt(str);
     modbusfun06:=$10B0;   //等同
  end;

end;

procedure TForm_line.tmr4Timer(Sender: TObject);      //光标定位。开启软件计时定时器 ，
begin
  begin
    if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then   Edit1.SetFocus;
  end;

  begin
    if(label11.Caption='Loading...')and TabSheet2.Showing then begin      //仅测试界面允许加载<--- not TabSheet4.Showing debug界面必须禁止加载
      modbusfun06dat:=$00F6;   //读上限下限 及短路群
      modbusfun06:=$0001;
    end else
      tmr4.Enabled:=False;
  end;
end;

procedure TForm_line.btn28Click(Sender: TObject);
begin
  dlgPntSet1.Execute;//选择输出的打印机以及其他打印控制选项
end;


//定义report过程，使真实打印和打印愈来愈览共用此过程
//自定义Report过程
procedure Report (Acanvas:Tcanvas);
var
    sx,sy,sy0,sx0:real;//开始位置
    dpi,dpm:real; //每毫米的点数
   // Arect: Trect;
Begin
    Dpi := GetDeviceCaps (Printer.Handle,LogPixelsX);
    Dpm:=trunc(dpi/25.4); //每毫米的点数
    Sx:=100; sx0:=10; sy0:=10; sy:=sy0;
    with Acanvas do
    try
      font.name:='宋体';
      font.size:=18;
      textout(trunc (60*dpm),trunc (sy*dpm),'Test...99999999999999');
      sy:=10;
      textout(trunc (60*dpm),trunc (sy*dpm),'Test...');

    finally
      Free;
    End;

End;
procedure TForm_line.N51Click(Sender: TObject);
var str:string;
begin
  if (StrToInt('0'+sysedit[5].Text)=0) or (working >0) then      //没有读到版本号，不执行
    begin
      if(working >0) then  lbl28.Caption:='MCU is busying, Pls press RESET!请按复位再导入'
      else     lbl28.Caption:='Pls retry to click!请重新上电，重启软件试一次！';
    end
  else begin         //允许导入
    if InputQuery('Import Window!', 'Pls Enter Password 请输入密码', str) then
    begin
      if (Str='888999') then begin
        if not spthdlg1.Execute then  begin
          Exit;
        end else begin
          
          str:='24';
          N51.Checked:=TRUE;A1.Checked:=false; N50.Checked:=False;
          if InputQuery('Import Window!', 'Pls Enter MaxPN 请输入机种程序导出数N=', str) then
          begin
            N51.Tag:=strtoint(str);
          end else
            N51.Tag:=0;
          A1.Tag:=0;
          if(strtoint('0'+sysedit[5].text)<930) then begin
            str:='0';
            if InputQuery('Import Window!', 'Pls Enter Delay(S) 因固件版本太低，需增加延时（秒）=', str) then
            begin
              spthdlg1.Tag:=strtoint(str);
            end else
              spthdlg1.Tag:=0;
          end;          
          modbusfun06:=$0041;    //选择机种
          modbusfun06dat:=A1.Tag+$2000;
          //btn23Click(Self);
        end;
      end;
    end;
  end;
end;

procedure TForm_line.N50Click(Sender: TObject);
var str:string;
begin
  if (StrToInt('0'+sysedit[5].Text)=0) or (working >0) then      //没有读到版本号，不执行
    begin
      if(working >0) then  lbl28.Caption:='MCU is busying, Pls press RESET!请按复位再导入'
      else     lbl28.Caption:='Pls retry to click!请重新上电，重启软件试一次！';
    end
  else begin         //允许导入
    if InputQuery('Import Window!', 'Pls Enter Password 请输入密码', str) then
    begin
      if (Str='888999') then begin
        if not spthdlg1.Execute then  begin
          Exit;
        end else begin

          str:='24';
          if InputQuery('Import Window!', 'Pls Enter MaxPN 请输入机种程序导入数N=', str) then
          begin
            N50.Tag:=strtoint(str);
          end else
            N50.Tag:=0;
          A1.Tag:=0;
          if(strtoint('0'+sysedit[5].text)<930) then begin
            str:='0';
            if InputQuery('Import Window!', 'Pls Enter Delay(S) 因固件版本太低，需增加延时（秒）=', str) then
            begin
              spthdlg1.Tag:=strtoint(str);
            end else
              spthdlg1.Tag:=0;
          end;
          N50.Checked:=TRUE;A1.Checked:=false; N51.Checked:=False;
          modbusfun06:=$0041;    //选择机种
          modbusfun06dat:=A1.Tag+$2000;
          //btn23Click(Self);
        end;
      end;
    end;
  end;
end;

procedure TForm_line.btn29Click(Sender: TObject);
Var mymetafile:Tmetafile;
   // Ifprt:Boolean;
Begin
    //是false则打印预览，true则打印
    if chk10.Checked then         //ifprt
    begin
      printer.BeginDoc;
      //report(printer.canvas);  //将打印机画布对象传给report过程
      printer.enddoc;          //如果是打印则结束打印
    end else begin
      MyMetafile:=Tmetafile.Create;           //创建图元文件

      //以打印机句柄创建图元文件画布对象（Tmetafilecanvas）,并调用report过程
      report (TmetafileCanvas.Create(MyMetafile,printer.Handle));
      img3.Canvas.StretchDraw (img3.canvas.cliprect,MyMetafile); //如果是打印预览，则在image1上显示出来
    end;
end;

procedure TForm_line.tmr5Timer(Sender: TObject);      //总的通信（usb为主）超时计数
begin
   if Label11.Tag<999999 then
      Label11.Tag:=label11.Tag+5;

   if tmr5.Tag=1 then begin              //error之后读取没有返回，重新连接
      if Assigned(CurrentDevice) then  begin
        usb_busyT:=31;
        Form_line.Stat1.Panels[0].Text :='Port:USB ERROR';
      end else begin
                                       //串口error无法控制
      end;
   end;
   if tmr5.Tag>0 then
     tmr5.Tag:=tmr5.Tag-1;
  if (usb_busyT=11)and(
      (Form_line.Stat1.Panels[0].Text='Port:USB ERROR')or(Form_line.Stat1.Panels[0].Text='Port:USB ERROR.')or(Form_line.Stat1.Panels[0].Text='Port:USB ERROR..')
    ) then begin
    usb_busyT:=31;  tmr5.Tag:=999999;
    if (Form_line.Stat1.Panels[0].Text='Port:USB ERROR') then
      Form_line.Stat1.Panels[0].Text:='Port:USB ERROR.'
    else if (Form_line.Stat1.Panels[0].Text='Port:USB ERROR.') then
      Form_line.Stat1.Panels[0].Text:='Port:USB ERROR..'
    else
      Form_line.Stat1.Panels[0].Text:='Port:USB ERROR...';
    HidCtlDeviceChange(self);
  end;

  if usb_busyT>0 then
    usb_busyT:=usb_busyT-1;
  if comandbusy>0 then
  begin
    comandbusy:=comandbusy-1;
  end;
end;


procedure TForm_line.btn30Click(Sender: TObject);
begin
      modbusfun06dat:=$00F2;
      modbusfun06:=$0001;
end;

procedure TForm_line.btn31Click(Sender: TObject);
begin
      modbusfun06dat:=$00F1;    //读找点结果
      modbusfun06:=$0001;
end;

procedure TForm_line.btn32Click(Sender: TObject);
begin
      modbusfun06dat:=$00F0;    //读找点结果
      modbusfun06:=$0001;
end;

procedure TForm_line.btn33Click(Sender: TObject);
begin
      modbusfun06dat:=$00F3;
      modbusfun06:=$0001;
end;

procedure TForm_line.btn34Click(Sender: TObject);
begin
      modbusfun06dat:=$00F4;
      modbusfun06:=$0001;
end;

procedure TForm_line.btn35Click(Sender: TObject);
begin
      modbusfun06dat:=$00F5;
      modbusfun06:=$0001;
end;

procedure TForm_line.btn36Click(Sender: TObject);
begin
      modbusfun06dat:=$00F6;
      modbusfun06:=$0001;
end;

procedure TForm_line.btn37Click(Sender: TObject);
begin
      modbusfun06dat:=$00F7;
      modbusfun06:=$0001;
end;

procedure TForm_line.btn38Click(Sender: TObject);
begin
      modbusfun06dat:=$00F8;
      modbusfun06:=$0001;
end;

procedure TForm_line.btn39Click(Sender: TObject);
begin
      modbusfun06dat:=$00F9;
      modbusfun06:=$0001;
end;

procedure TForm_line.btn40Click(Sender: TObject);
begin
      modbusfun06dat:=$00E0;
      modbusfun06:=$0001;
end;

procedure TForm_line.btn41Click(Sender: TObject);
begin
      modbusfun06dat:=$00E1;
      modbusfun06:=$0001;
end;

procedure TForm_line.btn42Click(Sender: TObject);
var i:Integer;
begin
    modbusfun16int:=$10A0;
    modbusfun16len:=$10;  //18
    //sbuf[1]:=1;
    sbuf[2]:=$10;
    sbuf[3]:=$10;
    sbuf[4]:=$A0;
    sbuf[5]:=$00;
    sbuf[6]:=$10;       //18
    sbuf[7]:=2*sbuf[6];
    sbuf[8]:=$80;     //ngokenabel      (StrToInt(sysedit[0].Text)+1) div 256;
    sbuf[9]:=$10;     //lcd显示参数-对比度     (StrToInt(sysedit[0].Text)+1) mod 256;
    for i:=1 to 12 do   //12
    begin
          sbuf[8+2*i]:=StrToInt(sysedit[i].Text) div 256;
          sbuf[9+2*i]:=StrToInt(sysedit[i].Text) mod 256;
    end;
    sbuf[34]:=byte('1');sbuf[35]:=byte(' ');sbuf[36]:=byte(' ');sbuf[37]:=byte(' ');
    sbuf[38]:=byte(' ');sbuf[39]:=byte(' ');
end;

procedure TForm_line.btn43Click(Sender: TObject);
begin
    modbusfun16int:=$1800;
    modbusfun16len:=$10;
    //sbuf[1]:=1;
    sbuf[2]:=$10;
    sbuf[3]:=$18;
    sbuf[4]:=$00;
    sbuf[5]:=$00;
    sbuf[6]:=$10;
    sbuf[7]:=2*sbuf[6];

    sbuf[8+2*0]:=$8000 div 256;
    sbuf[9+2*0]:=$8000 mod 256;
    sbuf[8+2*1]:=$0002 div 256;
    sbuf[9+2*1]:=$0002 mod 256;
    sbuf[8+2*2]:=$C001 div 256;
    sbuf[9+2*2]:=$C001 mod 256;
    sbuf[8+2*3]:=$8FFF div 256;
    sbuf[9+2*3]:=$8FFF mod 256;

end;

procedure TForm_line.btn44Click(Sender: TObject);
var
    i:SmallInt;
    temp8:Byte;
    tempstr:string;
begin
    modbusfun16int:=$2F00;
    modbusfun16len:=$12;
    //sbuf[1]:=1;
    sbuf[2]:=$10;

    sbuf[8]:=StrToInt(edt143.Text)div 256;sbuf[9]:=StrToInt(edt143.Text)mod 256; //序号
    sbuf[10]:=byte(' ');sbuf[11]:=byte('0');sbuf[12]:=byte(' ');sbuf[13]:=byte(' ');
    for i:=2 to 8 do
    begin
     sbuf[10+2*i]:=0;sbuf[11+2*i]:=0;
    end;
    sbuf[22]:=StrToInt(edt49.Text) div 256;
    sbuf[23]:=StrToInt(edt49.Text);
    sbuf[24]:=StrToInt(edt50.Text) div 256;
    sbuf[25]:=StrToInt(edt50.Text);
    sbuf[26]:=StrToInt(edt51.Text) div 256;
    sbuf[27]:=StrToInt(edt51.Text);

    tempstr:=Copy(edt144.text,0,4);
    Move(tempstr[1],sbuf[28],4);
    if tempstr='' then begin
      sbuf[28]:=byte('0');
      sbuf[29]:=byte('0');
      sbuf[30]:=byte('0');
      sbuf[31]:=byte('0');
    end;
    temp8:=sbuf[28]; sbuf[28]:=sbuf[29];sbuf[29]:=temp8;
    temp8:=sbuf[30]; sbuf[30]:=sbuf[31];sbuf[31]:=temp8;

    // sbuf[28]:=byte('R');sbuf[29]:=byte('0');sbuf[30]:=byte('0');sbuf[31]:=byte('0');
    for i:=10 to 15 do begin
          sbuf[12+2*i]:=0;sbuf[13+2*i]:=0;
    end;
    sbuf[33]:=100;     //比例
end;

procedure TForm_line.btn46Click(Sender: TObject);
begin
    modbusfun16int:=$10A0;
    modbusfun16len:=$10;   //18
    //sbuf[1]:=1;
    sbuf[2]:=$03;
    sbuf[3]:=$10;
    sbuf[4]:=$A0;
    sbuf[5]:=$00;
    sbuf[6]:=$18;
end;


procedure TForm_line.chk7Click(Sender: TObject);
begin
  SysExplainNum:=6;//-1; btn141Click(Self);
  modbusfun05:=37;
  if chk7.Checked then modbusfun05dat:=$FF else modbusfun05dat:=0;
end;

procedure TForm_line.chk8Click(Sender: TObject);
begin
  if not chk8.Focused then Exit;
  SysExplainNum:=7;//-1; btn141Click(Self);
  btn14.SetFocus;
  modbusfun05:=38;
  if chk8.Checked then modbusfun05dat:=$FF else modbusfun05dat:=0;

end;

procedure TForm_line.chk9Click(Sender: TObject);
begin
  SysExplainNum:=8; //btn141Click(Self);
  modbusfun05:=39;
  if chk9.Checked then modbusfun05dat:=$FF else modbusfun05dat:=0;

end;

procedure TForm_line.N11Click(Sender: TObject);
begin
  Form_line.Close;
end;



procedure TForm_line.N8Click(Sender: TObject);
begin
  modbusfun05:=1; modbusfun05dat:=$FF;
  formfind.showmodal;
end;



procedure TForm_line.N20Click(Sender: TObject);
begin
  modbusfun05:=2; modbusfun05dat:=$FF;
  formOS.showmodal;
end;

procedure TForm_line.Panel1MouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
 // if(Panel1.VertScrollBar.Position   +  strtoint(Form_line.edt121.Text) )< (VIRLCROWS *strtoint(Form_line.edt121.Text) ) then
    Panel1.VertScrollBar.Position   :=   Panel1.VertScrollBar.Position   +  strtoint(Form_line.edt121.Text);// 10;
end;

procedure TForm_line.Panel1MouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Panel1.VertScrollBar.Position   >=  strtoint(Form_line.edt121.Text) then
    Panel1.VertScrollBar.Position   :=   Panel1.VertScrollBar.Position   -  strtoint(Form_line.edt121.Text);// 10;
end;

procedure TForm_line.N21Click(Sender: TObject);
begin
  ts3.TabVisible:=True;
  PageControl1.TabIndex:=5-1;
end;

procedure TForm_line.N3Click(Sender: TObject);
var str:string;
begin
  if lbl79.Color=clGray then        //直接可切换到monitor
  begin
      TabSheet2.TabVisible:=True;
      PageControl1.TabIndex:=0;   //监控界面
  end else if InputQuery('Goto Monitor Window!', 'Pls Enter Password', str) then
  begin
     if  (Str='888') or (Str='137760') then  begin
         Showmessage('Monitor Window Open!');

         TabSheet2.TabVisible:=True;
         PageControl1.TabIndex:=0;   //监控界面

         TabSheet4.TabVisible:=False;      //关闭debug、USB和相关菜单项目
         ts3.TabVisible:=False;
         TabSheet4.Enabled:=False;
         N9.Enabled:=False;
         N10.Enabled:=False;
         N50.Enabled:=False;
         N51.Enabled:=False;
         A1.Enabled:=False;
     end else   begin
         Showmessage('Enter Error！');
     end;
  end;
end;


procedure TForm_line.btn9Click(Sender: TObject);
begin
 modbusfun05:=16; modbusfun05dat:=$FF;

end;

procedure TForm_line.btn10Click(Sender: TObject);
begin
 modbusfun05:=17; modbusfun05dat:=$FF;

end;
function TForm_line.UpServerOneLog(SLINE_head:string): Boolean;     //上传一条记录，返回是否成功！
var
    i: integer;     
    strs:TStrings;
begin
  Result := False;
  strs:=TStringList.Create;
  strs.Delimiter:=#9;               //  strs.QuoteChar := ' ';
  strs.DelimitedText:=SLINE_head;   //查分不完全正确。连续的空格认识是TAB

end;


procedure TForm_line.D1Click(Sender: TObject);
begin
  formstepCopy.btn1.Visible:=True;
  formstepCopy.btn2.Visible:=False;
  formstepCopy.showmodal;
end;

procedure TForm_line.N27Click(Sender: TObject);
begin
  modbusfun05:=1; modbusfun05dat:=$FF;
  formfind.showmodal;
end;

procedure TForm_line.H1Click(Sender: TObject);
begin
     modbusfun06dat:=StrToInt('0'+edt45.Text);
     modbusfun06:=$2F20;
     addToLog('交换第'+edt45.Text+'步骤的高低点' );

end;

procedure TForm_line.S3Click(Sender: TObject);
begin
     modbusfun06dat:=StrToInt('0'+edt45.Text);
     modbusfun06:=$2F21;
    addToLog('删略第'+edt45.Text+'步骤的序号' );
end;

procedure TForm_line.N31Click(Sender: TObject);
var str:string;
begin
  str:=N3.Hint;
  if InputQuery('Pls Enter 4 char!查询名称（大写）', 'abc', str) then  begin
     N3.Hint:=str;
     modbusfun16str:=AnsiUpperCase(str);
     modbusfun16len:=2;
     modbusfun16:=$2F40;
  end;
end;

procedure TForm_line.P2Click(Sender: TObject);
var  str:string;
begin
  str:=P2.Hint;
  if InputQuery('Enter Int!查询测试点所在步骤', '1234', str) then begin
      P2.Hint:=str;
     if str='' then str:='0';
     if IsNumberic(sysedit[1].Text) and (StrToInt(sysedit[1].Text)>=768+1+3328) then begin      //大于4095，点数可翻倍！
       if StrToInt(str)>MAXPINS*2 then
          modbusfun06dat:=MAXPINS*2
       else
          modbusfun06dat:=StrToInt(str);
     end else if StrToInt(str)>MAXPINS then
        modbusfun06dat:=MAXPINS     //最大第4094步
     else
        modbusfun06dat:=StrToInt(str);
     modbusfun06:=$2F41;
  end;
end;

procedure TForm_line.O1Click(Sender: TObject);
begin
     modbusfun06dat:=StrToInt('0'+edt45.Text);
     if   modbusfun06dat>0 then modbusfun06dat:=modbusfun06dat-1;
     modbusfun06:=$2F60;
end;

procedure TForm_line.P1Click(Sender: TObject);
begin
     modbusfun06dat:=StrToInt('0'+edt45.Text);
     if   modbusfun06dat>0 then modbusfun06dat:=modbusfun06dat-1;
     modbusfun06:=$2F61;
end;

procedure TForm_line.S4Click(Sender: TObject);
var str:string;
begin
  str:=S4.Hint;
  if InputQuery('Enter Int!查询步骤序号', '1234', str) then begin
      S4.Hint:=str;
     if str='' then str:='0';
     if StrToInt(str)>4980 then  modbusfun06dat:=4980     //最大第980步
     else modbusfun06dat:=StrToInt(str);
     modbusfun06:=$2F42;   //等同 10B0
  end;
end;


procedure TForm_line.B1Click(Sender: TObject);
begin
  formModfyNstep.showmodal;
end;

procedure TForm_line.TabSheet4Hide(Sender: TObject);
begin
  N13.Visible:=false;

  S2.Visible:=false;
  x1.Visible:=false;
  N15.Visible:=False;

  N15.Enabled:=False;
  N13.Enabled:=False;
  S2.Enabled:=False;
  x1.Enabled:=False;

  N9.Enabled:=False;
  N10.Enabled:=False;
  N50.Enabled:=False;
  N51.Enabled:=False;
  A1.Enabled:=False;
end;


procedure TForm_line.ComboBox2Change(Sender: TObject);
begin
   cbb2.Text:=combobox2.Text;
   Comm1.BaudRate := strtoint(combobox2.Text);
   btn1.Font.Color:=clRed;

   SpeedButton12.Enabled:=true;
   btn112.Enabled:=true;

   form_line.Comm1.StopComm;
   shp2.Brush.Color := clSilver;
   shp4.Brush.Color := clSilver;
end;

procedure TForm_line.I2Click(Sender: TObject);
begin
  if IsNumberic(Form_line.edt45.Text) then begin
    FormInsertN.edt1.Text:=Form_line.edt45.Text;
  end;
  FormInsertN.showmodal;
end;

procedure TForm_line.D5Click(Sender: TObject);
begin
  if IsNumberic(Form_line.edt45.Text) then begin
    FormDeleteN.edt1.Text:=Form_line.edt45.Text;
    FormDeleteN.edt2.Text:=Form_line.edt45.Text;
  end;

  formDeleteN.showmodal;
end;

procedure TForm_line.N33Click(Sender: TObject);
begin

  formMoveN.showmodal;
end;


procedure TForm_line.I3Click(Sender: TObject);
begin
  if IsNumberic(Form_line.edt45.Text) then begin
    FormInsertCopyN.edt1.Text:=IntToStr(StrToInt(Form_line.edt45.Text)+1);
  end;
  FormInsertCopyN.showmodal;
end;


procedure TForm_line.C2Click(Sender: TObject);
var str,password:string;
begin
     if readchis('Comm Para', 'PassWord')<>'' then  begin    //不为空
        password:=readchis('Comm Para', 'PassWord');
     end else
        password:='999';
     str:='0';
     if InputQuery('测试计数清零', '按"确定"清空计数,输入其它先请输入:密码+ok/ng     ', str) then begin
       if (Str=password+'ok') or (Str=password+'OK') then  begin
         str:='0';
         if InputQuery('设置测试OK计数', '请输入...', str) then begin
            modbusfun16int:=$2F65;
            modbusfun16len:=$02;
            sbuf[2]:=$10;
            sbuf[8]:=StrToInt(str) shr 0;
            sbuf[9]:=StrToInt(str) shr 8;
            sbuf[10]:=StrToInt(str) shr 16;
            sbuf[11]:=StrToInt(str) shr 24;
         end;
       end else if (Str=password+'ng') or (Str=password+'NG') then  begin
         str:='0';
         if InputQuery('设置测试NG计数', '请输入...', str) then begin
            modbusfun16int:=$2F66;
            modbusfun16len:=$02;
            sbuf[2]:=$10;
            sbuf[8]:=StrToInt(str) shr 0;
            sbuf[9]:=StrToInt(str) shr 8;
            sbuf[10]:=StrToInt(str) shr 16;
            sbuf[11]:=StrToInt(str) shr 24;
         end;
       end else if (str='')or(str='0') then  begin
          modbusfun06dat:=0;
          modbusfun06:=$2F63;
       end else  begin
          Showmessage('密码错误，禁止设置计数！');
       end;
     end;
end;

procedure TForm_line.N38Click(Sender: TObject);
begin
  if pnl1.Height=188 then begin  //btn120.Caption='X'当前开启则关闭    pgc1.Visible then begin
    pnl1.Height:=22;
    btn120.Caption:='^^显示';
    N38.Caption:='Open SysWin 开启系统参数显示';
  end else begin
    pnl1.Height:=188;
    btn120.Caption:='X 隐藏';
    N38.Caption:='Close SysWin 关闭系统参数显示';    // grp23.Visible:=False;
  end;
end;


procedure TForm_line.btn55Click(Sender: TObject);
var
  Device: array[0..255] of char;
  Driver: array[0..255] of char;
  Port: array[0..255] of char;
  hDMode: THandle;
  PDMode: PDEVMODE;
begin
//设置打印机
  try
  Printer.PrinterIndex := Printer.PrinterIndex;
  Printer.GetPrinter(Device, Driver, Port, hDMode);
  if hDMode <> 0 then
  begin
    pDMode := GlobalLock(hDMode);
    if pDMode <> nil then
    begin
        //设定自定义纸张
        pDMode^.dmFields := pDMode^.dmFields or
          dm_PaperSize or
          DM_PAPERWIDTH or
          DM_PAPERLENGTH;

        pDMode^.dmPaperSize := 0;
        pDMode^.DMPAPERWIDTH:= 40;//width*10 ;
        pDMode^.DMPAPERLENGTH:=30;//height*10;

      GlobalUnlock(hDMode);
    end;
  end ;
  Printer.PrinterIndex := Printer.PrinterIndex;
  except
    showmessage('没有默认选择打印机，或默认打印机不可使！');
    exit;
  end;
end;

procedure TForm_line.edit1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if Key=$0D then begin
    if Readchis('result','TipCodeEnd')<>'' then
      MessageBox(Form_line.Handle, PAnsiChar(AnsiString(Readchis('result','TipCodeEnd'))),'TipCodeEnd' ,MB_ICONINFORMATION+MB_OkCancel);

    if (Readchis('result','BarcodeStart')='1' )               //用条码中的回车字符来启动测试
      //and ( Length(Trim(edit1.Text))>=StrToInt(edt55.Text) )  //大于最小长度
      and(Trim(edit1.Text)<>'')    //条码不为空
    then  begin
       modbusfun05:=16; modbusfun05dat:=$FF;
    end;
    if ( Label7.hint='11' )               //双产品测试       //and( Length(Trim(edit1.Text))>=StrToInt(edt55.Text) )  //大于最小长度
      and( Trim(edit1.Text)<>'')    //条码不为空
    then  begin
      if(TabSheet2.Showing)and medit1.Enabled and grp36.Visible  then  medit1.SetFocus;
    end;
    if (Readchis('Model Para','BarCodeEdit')<>'')and(Readchis('Server','MESprogram')='')and(Readchis('Server','MESprogramdefault')='') then begin     //丽清启用MES，输入密码或者导入导出结束，按ENTER会条码报警；
      if Length(Trim(edit1.Text))<>StrToInt(edt7.Text) then begin       //目的是什么？？？ 手动输入提示长度吗？
        edit1.Color:=clRed;
        edit1.SelectAll;
        ts8.Show;
        edt7.Color:=clRed;
      end else begin
        edit1.Color:=clWindow;
        edt7.Color:=clWindow;
        edit1.Enabled:=False;                        
      end;
    end;
    if readchis('Comm Para', 'barcodeSelT')='Enter' then begin
        if TabSheet2.TabVisible and grp36.Visible  then begin
          if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then edit1.SetFocus;
          edit1.SelectAll;
        end;
    end;
  end;
  if readchis('Comm Para', 'barcodeSelT')<>'Enter' then
    barcodeSelT :=2*strtoint(edt76.Text);
end;

procedure TForm_line.btn56Click(Sender: TObject);
var
    str,password:string;
begin
  if (Readchis('Server','MESprogram')<>'')then begin
     if readchis('Comm Para', 'PassWord')<>'' then  begin    //不为空
        password:=readchis('Comm Para', 'PassWord');
     end else
        password:='999';

     if InputQuery('上传测试程序到MES系统', '请输入密码', str) then
     begin
       if (Str=password) or (Str='137760') then  begin
            FormLoad.Caption:='导出测试程序，并上传MES系统';
            FormLoad.btn3.Visible:=False;
            FormLoad.btn4.Visible:=true; FormLoad.btn4.Enabled:=false;
            FormLoad.lv1.Visible:=False;
            formLOad.ShowModal;
       end else  begin
          Showmessage('密码错误，禁止上传！');
       end;
       if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then  edit1.SetFocus;
     end;
  end else if (Readchis('Server','MESprogramdefault')<>'') then begin
     Showmessage('MES脱机状态，不能上传！请先切换到MES联机');
  end else
    formPN.show;
end;

procedure TForm_line.tmr6Timer(Sender: TObject);
begin
  btn91Click(Self);
end;

procedure TForm_line.ts1Show(Sender: TObject);
begin
  if Copy(stat1.Panels[4].Text,1,7)<>'Succuss' then stat1.Panels[4].Text := '-';

end;

procedure TForm_line.btn61Click(Sender: TObject);
begin
  Close;
end;

procedure TForm_line.btn59Click(Sender: TObject);
begin
  modbusfun05:=17; modbusfun05dat:=$FF;
end;


procedure TForm_line.btn62Click(Sender: TObject);
begin

  comandbusy:=220  div 5;
end;


procedure TForm_line.j1Click(Sender: TObject);
begin
  C2Click(Self);
end;

procedure TForm_line.N19Click(Sender: TObject);
begin
  begin
    if grp17.Visible then begin
      grp17.Visible:=False;  lbl169.Left:=0;Label11.Left:=8;
      N19.Caption:='Yield on 良率显示';
    end else begin
      grp17.Visible:=True;   lbl169.Left:=120;Label11.Left:=128;  
      N19.Caption:='Yield off 关闭良率显示';
    end;
  end;
end;

procedure TForm_line.L2Click(Sender: TObject);
begin
  if grp28.Visible=True then begin
    grp28.Visible:=False;
    if not grp3.Visible then
      pnl1.Visible:=false;
    L2.Caption:='Open SysButton 显示系统按钮';
  end  else begin
    grp28.Visible:=true;
    pnl1.Visible:=true;
    L2.Caption:='Close SysButton 关闭系统按钮';
  end;
end;

procedure TForm_line.mmo13DblClick(Sender: TObject);
begin
      modbusfun06dat:=$00F3;
      modbusfun06:=$0001;
end;

procedure TForm_line.mmo14DblClick(Sender: TObject);
begin
      modbusfun06dat:=$00F4;
      modbusfun06:=$0001;
end;

procedure TForm_line.N42Click(Sender: TObject);
begin
    modbusfun16int:=$2F00;
    modbusfun16len:=$12;
    //sbuf[1]:=1;
    sbuf[2]:=$10;

    sbuf[8]:=0;sbuf[9]:=0; //序号
    sbuf[10]:=byte(' ');sbuf[11]:=byte('0');sbuf[12]:=byte(' ');sbuf[13]:=byte(' ');
    for i:=2 to 8 do
    begin
     sbuf[10+2*i]:=0;sbuf[11+2*i]:=0;
    end;
    sbuf[22]:=StrToInt(edt49.Text) div 256;
    sbuf[23]:=StrToInt(edt49.Text);
    sbuf[28]:=byte('Q');sbuf[29]:=byte('1');sbuf[30]:=byte('0');sbuf[31]:=byte('0');
    for i:=10 to 15 do
      begin
          sbuf[12+2*i]:=0;sbuf[13+2*i]:=0;
      end;
    sbuf[33]:=100;     //比例
end;

procedure TForm_line.N43Click(Sender: TObject);
begin
   modbusfun16int:=$2F00;
    modbusfun16len:=$12;
    //sbuf[1]:=1;
    sbuf[2]:=$10;

    sbuf[8]:=0;sbuf[9]:=0; //序号
    sbuf[10]:=byte(' ');sbuf[11]:=byte('0');sbuf[12]:=byte(' ');sbuf[13]:=byte(' ');
    for i:=2 to 8 do
    begin
     sbuf[10+2*i]:=0;sbuf[11+2*i]:=0;
    end;
    sbuf[22]:=StrToInt(edt50.Text) div 256;
    sbuf[23]:=StrToInt(edt50.Text);
    sbuf[28]:=byte('Q');sbuf[29]:=byte('1');sbuf[30]:=byte('0');sbuf[31]:=byte('0');
    for i:=10 to 15 do
      begin
          sbuf[12+2*i]:=0;sbuf[13+2*i]:=0;
      end;
    sbuf[33]:=100;     //比例
end;



procedure TForm_line.edt103Change(Sender: TObject);
begin
  btn1.Font.Color:=clRed;
end;

procedure TForm_line.edt106Change(Sender: TObject);
begin
  btn1.Font.Color:=clRed;

end;

procedure TForm_line.edt102Change(Sender: TObject);
begin
  btn1.Font.Color:=clRed;

end;

procedure TForm_line.edt109Change(Sender: TObject);
begin
  btn1.Font.Color:=clRed;

end;

procedure TForm_line.btn73Click(Sender: TObject);
begin
    modbusfun16int:=$0041;
    modbusfun16len:=1;      //sbuf[1]:=1;
    sbuf[2]:=$03;
    sbuf[3]:=$00;   sbuf[4]:=$00;
    sbuf[5]:=$41;   sbuf[6]:=1;

end;

procedure TForm_line.btn74Click(Sender: TObject);
var str:string;
begin
   if InputQuery('测试程序切换', '请输入1~24之间的一个数:', str) then
   begin
     if IsNumberic(str) then
     begin
        modbusfun06:=$0041;    //选择机种
        if(StrToInt(str)<=240)and(StrToInt(str)>0) then
          modbusfun06dat:=StrToInt(str)-1
        else
          modbusfun06dat:=0;
     end else  begin
        Showmessage('输入非数字，不能切换！');
     end;
   end;

end;

procedure TForm_line.btn75Click(Sender: TObject);
begin
  edt111.Color:=clWindow;
  TripCount:=0;
  lbl134.Caption:=inttostr(tripcount);
  Label11.Font.Color:=clWindowText;Label11.Caption:='Ready'; Label11.Font.Size:=150; Lbl169.Caption:='';lbl169.Font.Size:=10;
  Label11.Font.Color:=clwindow;
end;

procedure TForm_line.medit1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=$0D then begin
    if ( Label7.hint='11' )               //双产品测试     //and( Length(Trim(medit1.Text))>=StrToInt(edt55.Text) )  //大于最小长度
      and( Trim(medit1.Text)<>'')    //条码不为空
    then  begin
      if(TabSheet2.Showing) and edit1.Enabled  and grp36.Visible then  edit1.SetFocus;
    end;
  end;
  barcodeSelT :=2*strtoint(edt76.Text);
end;

procedure TForm_line.btn77Click(Sender: TObject);
begin
  modbusfun05:=29; modbusfun05dat:=$FF;
end;

procedure TForm_line.btn78Click(Sender: TObject);
begin
  modbusfun05:=30; modbusfun05dat:=$FF;
end;


procedure TForm_line.btn80Click(Sender: TObject);
begin
        modbusfun06:=$0041;    //选择机种
        modbusfun06dat:=0;
end;

procedure TForm_line.btn81Click(Sender: TObject);
begin
        modbusfun06:=$0041;    //选择机种
        modbusfun06dat:=1;

end;

procedure TForm_line.btn82Click(Sender: TObject);
begin
        modbusfun06:=$0041;    //选择机种
        modbusfun06dat:=2;

end;

procedure TForm_line.btn83Click(Sender: TObject);
begin
        modbusfun06:=$0041;    //选择机种
        modbusfun06dat:=3;

end;

procedure TForm_line.btn84Click(Sender: TObject);
begin
        modbusfun06:=$0041;    //选择机种
        modbusfun06dat:=4;

end;

procedure TForm_line.btn85Click(Sender: TObject);
begin
        modbusfun06:=$0041;    //选择机种
        modbusfun06dat:=5;

end;

procedure TForm_line.btn86Click(Sender: TObject);
begin
        modbusfun06:=$0041;    //选择机种
        modbusfun06dat:=6;

end;

procedure TForm_line.btn87Click(Sender: TObject);
begin
        modbusfun06:=$0041;    //选择机种
        modbusfun06dat:=7;
end;


procedure TForm_line.ComboBox1Change(Sender: TObject);
begin
   btn1.Font.Color:=clRed;

   cbb3.Text:=ComboBox1.Text;
   form_line.SpeedButton12.Enabled:=true;
   btn112.Enabled:=true;

   form_line.Comm1.StopComm;
   shp2.Brush.Color := clSilver;
   shp4.Brush.Color := clSilver;
end;


procedure TForm_line.C4Click(Sender: TObject);
begin
  formstepCopy.btn2.Visible:=True;
  formstepCopy.btn1.Visible:=False;
  formstepCopy.showmodal;
end;

procedure TForm_line.N34Click(Sender: TObject);
var str:string;
begin
  if N34.Checked then begin
     N34.Checked:=False ;   TabSheet4.Caption:='Debug调试';
    //  if Readchis('Comm Para','COMMdelay')<>'' then
    //  else
        edt14.Text:='0'+Readchis('Comm Para','COMMdelay');
     edt15.Text:='0';
  end else begin
    if N34.Hint='' then
      str:='2001'
    else
      str:=N34.Hint;
    if InputQuery('Import Window!', 'Pls Enter MachineNum 请输入透传主板编号（串口号c+级联序号nn）', str) then
    begin
      if str='0' then
          str:='3001';     //默认：串口3，有一级级联板；
       N34.Tag:=strtoint(str);
       N34.Hint :=str;
       N34.Checked:=true;  TabSheet4.Caption:='Debug透传:'+str;    //透传调试，延时必须加长；

       edt14.Text:='20';    //透传通讯后，增加20  =0可以正常导入导出
       edt15.Text:='60';    //USB延时：透传9600时（50可以导入导出，40不能正常导入导出）,透传38400时（20可以导入导出，=0不能正常导入导出）
       btn12Click(Self);
    end else
      N34.Tag:=0;
  end;
end;

procedure TForm_line.N47Click(Sender: TObject);
begin
 modbusfun05:=19; modbusfun05dat:=$FF;
end;

procedure TForm_line.btn90Click(Sender: TObject);
begin
  mmo17.Clear;
end;

procedure TForm_line.btn91Click(Sender: TObject);       //调试助手：定时发送数据
var ii,i,j:SmallInt;
    temp,s,viewstring:string;
begin
    if shp4.Brush.Color <> clGreen then begin
      messagedlg('Open Comm Fail! 没有启动串口！',mterror,[mbyes],0);
      Exit;
    end;
    fillchar(sbuf, sizeof(sbuf), 0);
    send_len:=Length(mmo17.Text);

    if rb1.Checked then begin         //16进制
      s:=Trim(mmo17.Text);
      repeat
        i := pos(' ', s);
        j := length(s);
        if i > 0 then
          s := copy(s, 1, i - 1) + copy(s, i + 1, j - i);
      until i = 0;
      temp:=s;

      send_len:=Length(temp);
      send_len:=(1+send_len) div 2;
      hextobin(pchar(temp),pchar(@sbuf)+0, send_len);      //中间有空格会导致发送错误？？？
    end;
    if rb2.Checked and (send_len>0) then    begin     //ASCII文本格式
      temp:=mmo17.Text;
      for ii:=1 to  send_len do
        sbuf[ii]:= Byte(temp[ii]);
    end;

    try
      if chk19.Checked then begin
        send_crcdata(send_len,2);
        j:=send_len+2;
      end else begin
        send_crcdata(send_len,0);
        j:=send_len;
      end;
    except
        messagedlg('Open Comm Fail!串口不存在!Pls Check the Comm is exist .',mterror,[mbyes],0);
    end;
    if chk18.Checked then
        tmr6.Enabled:=True;

    viewstring:='';
    if rb5.Checked then begin                         //串口全解析时，此处也显示发送内容
      for i:=1 to j do  begin
         viewstring:=viewstring+inttohex(sbuf[i],2)+' ';       
      end;
      viewstring:='Send:'+viewstring;
      if form_line.chk38.Checked then
        viewstring:=GetTimemS+viewstring;
      form_line.Memo1.Lines.add(viewstring);      //串口调试则显示发送内容
    end;
    Form_line.chk24.Tag:=10;                     //强制 显示解析数据
end;

procedure TForm_line.btn92Click(Sender: TObject);
begin
  memo1.Clear;
end;

procedure TForm_line.chk18Click(Sender: TObject);
begin
  tmr6.Enabled:=false;
  tmr6.Interval:=StrToInt(edt122.Text);
end;

procedure TForm_line.edt122Change(Sender: TObject);
begin
  if StrToInt(edt122.Text)>=10 then
    tmr6.Interval:=StrToInt(edt122.Text);

end;







procedure TForm_line.rb2Click(Sender: TObject);
var i,j:SmallInt;
    s,temp:string;
begin

    s:=Trim(mmo17.Text);
    repeat
      i := pos(' ', s);
      j := length(s);
      if i > 0 then
        s := copy(s, 1, i - 1) + copy(s, i + 1, j - i);
    until i = 0;
    temp:=s;

    send_len:=Length(temp);
    send_len:=(1+send_len) div 2;   //字符个数
    hextobin(pchar(temp),pchar(@sbuf)+0, send_len);      //中间有空格会导致发送错误

    s:='';
    for i:=1 to  send_len do
      s:= s+char(sbuf[i]);

    mmo17.Text:=s;
    if rb1.Checked then mmo18.Enabled:=true else mmo18.Enabled:=False;
end;

procedure TForm_line.rb1Click(Sender: TObject);
var i:SmallInt;
    AStr,s:string;
begin
  AStr:=mmo17.Text;
  for i:=1 to length(AStr)  do
  begin
    //ch:=AStr[i];
    s:=s+IntToHex(Ord(AStr[i]),2)+' ';
  end;
  mmo17.Text:=s;
  if rb1.Checked then mmo18.Enabled:=true else mmo18.Enabled:=False;
end;

procedure TForm_line.edt134Change(Sender: TObject);
begin
  // edt7.Text:=edt134.Text;
   btn1.Font.Color:=clRed;
end;

procedure TForm_line.PrintClickProcess(len:Integer);
var
  prntext:system.text;// 将prntext 声明为一个在system 程序单元中定义的文本文件
//  line:Integer;
  ii:SmallInt;
  s:string;
begin
  with Form_line do begin
    AssignPrn(prntext);   // 将prnsetup分配给打印机
    if (StrToInt(edt11.Text)div 1000)=1 then        //强制 设置打印机为横向打印
      Printer.Orientation:=poLandscape
    else  if (StrToInt(edt11.Text)div 1000)=2 then  //强制 设置打印机为纵向打印
      Printer.Orientation:=poPortrait ;
    Rewrite(prntext);       // 调用rewrite 函数，为输出打开已分配的文件
    if(StrToInt(edt75.Text) mod 1000) >1 then begin
      Printer.Canvas.Font:=mmo5.Font;     // 把当前Memo1的字体指定给打印对象的Canvas 的字体属性
      Printer.Canvas.Font.Size:=StrToInt(edt75.Text) mod 1000;
    end;
    for ii:=1 to ( StrToInt(edt11.Text)div 10 )mod 10   do                //十位填写：空行
    begin
      Writeln(prntext,'--');
    end;
    if (readchis('result','PrintBlankLines')<>'') then begin
      for ii:=1 to StrToInt(readchis('result','PrintBlankLines') )  do   //空行
      begin
        Writeln(prntext,' .');
      end;
    end;

    if StrToInt(edt159.Text)>0 then
    begin
      for ii:=0 to StrToInt(edt159.Text)-1   do
      begin
        if (readchis('result','PrintFontSizeL'+inttostr(1+ii))<>'')
          and IsNumberic(readchis('result','PrintFontSizeL'+inttostr(1+ii))) then
        begin
          Printer.Canvas.Font.Size:=StrToInt(readchis('result','PrintFontSizeL'+inttostr(1+ii))) mod 1000;
          Writeln(prntext,mmo5.lines[ii]);
          if(StrToInt(edt75.Text) mod 1000) >1 then begin
            Printer.Canvas.Font.Size:=StrToInt(edt75.Text) mod 1000;
          end;
        end else
          Writeln(prntext,mmo5.lines[ii]);
      end;
    end;
    System.Close(prntext);
  end;
end;

procedure TForm_line.btn98Click(Sender: TObject);
var
  str,password:string;
begin
   if readchis('Comm Para', 'ResetPassWord')<>'' then  begin        //ResetPassWord不为空
      password:=readchis('Comm Para', 'ResetPassWord');
   end else if readchis('Comm Para', 'PassWord')<>'' then  begin    //PassWord不为空
      password:=readchis('Comm Para', 'PassWord');
   end else
      password:='999';

   if  InputQuery('Reset复位治具', '请输入密码', str) then
   begin
     if (Str=password) or (Str='137760') then  begin
        if (modbusfuntion=$50)or (modbusfuntion=$51)or (modbusfuntion=$52) then  modbusfuntion:=$53      //导入结束
        else if (modbusfuntion=$40)or (modbusfuntion=$41)or (modbusfuntion=$42) then  modbusfuntion:=$43 //导出结束
        else modbusfun05:=17; modbusfun05dat:=$FF;
     end else  begin
        Showmessage('密码错误，不能复位！');
     end;
   end;
   
   if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then  edit1.SetFocus;

end;

procedure TForm_line.U3Click(Sender: TObject);
begin
  edit1.Enabled:=true;
  if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then  edit1.SetFocus;
  edit1.SelectAll;
end;



procedure TForm_line.btn100Click(Sender: TObject);
var
    Dir:   string;
begin
    Dir   :=  '';
    if   SelectDirectory(Dir,   [sdAllowCreate,   sdPerformCreate,   sdPrompt],SELDIRHELP)   then
    begin
        edt135.Text   :=   Dir+'\';
        btn1.Font.Color:=clRed;
    end;
end;
procedure TForm_line.btn125Click(Sender: TObject);
var
    Dir:   string;
begin
    Dir   :=  '';
    if   SelectDirectory(Dir,   [sdAllowCreate,   sdPerformCreate,   sdPrompt],SELDIRHELP)   then
    begin
        edt157.Text   :=   Dir+'\';
        btn1.Font.Color:=clRed;
    end;
end;
procedure TForm_line.sbtbtn1Click(Sender: TObject);
var str,password:string;
begin
  if Form_line.edt82.Text='99' then begin                          //jaguar等 启用PC存放程序  enPN   demo

     if readchis('Comm Para', 'LoadPassWord')<>'' then  begin
       password:=readchis('Comm Para', 'LoadPassWord');
       if  InputQuery('Load加载程序', '请输入密码', str) then
       begin
         if (Str=password) or (Str='137760') then  begin

         end else  begin
            Showmessage('密码错误，不能复位！');
            Exit;
         end;
       end;
     end;
     if ( (label11.Caption<>'Ready')and(label11.Caption<>'PN错误') ) or (working >0) then  begin    //没有通讯读到版本号或者工作忙，不执行
        btn26Click(Form_line);              //加载之前先复位？
        tmr1Timer(Self);
        Application.ProcessMessages;       //定时发送复位命令
        Sleep(100);
        Application.ProcessMessages;       //接收复位命令回传
        Sleep(200);
        Application.ProcessMessages;        //接收复位后ready命令；
        Sleep(200);
        Application.ProcessMessages;        //接收复位后ready命令；
     end;
     formLOad.BorderStyle:=bsNone;
     formLOad.btn3.Visible:=false;    //加载按钮禁止！
     formLOad.edt4.Visible:=true;     //可扫工单条码，加载程序；
     formLOad.ShowModal;
  end else begin
     formLOad.ShowModal;
  end;
end;

procedure TForm_line.lv1AdvancedCustomDrawItem(Sender: TCustomListView;
  Item: TListItem; State: TCustomDrawState; Stage: TCustomDrawStage;
  var DefaultDraw: Boolean);
begin

  if ErrlistZu[Item.Index+1]>0  then sender.Canvas.Brush.Color:=clRed;
end;

procedure TForm_line.ts16Show(Sender: TObject);
begin
  modbusfun06dat:=$00F3;
  modbusfun06:=$0001;
  if pnl1.Height=22 then begin   //btn120.Caption<>'X'
    pnl1.Height:=188;
    btn120.Caption:='X 隐藏';
    N38.Caption:='Close SysWin 关闭系统参数显示';
  end;
end;

procedure TForm_line.ts15Show(Sender: TObject);
begin
  modbusfun06dat:=$00F4;
  modbusfun06:=$0001;
  if pnl1.Height=22 then begin    //btn120.Caption<>'X'
    pnl1.Height:=188;
    btn120.Caption:='X 隐藏';
    N38.Caption:='Close SysWin 关闭系统参数显示';
  end;
end;

procedure TForm_line.btn101Click(Sender: TObject);
var
  save_file_name: string;
  ResultList : TStringList;
  strs:TStrings;
  ii,jj,temp16:SmallInt;
begin
  if dlgOpen1.Execute then
  begin
    sprgrsbr2.Position:=0;
    btn101.Caption:='Start Conver....开始转换';
    btn101.Font.Color:=clRed;
    save_file_name := dlgOpen1.FileName;
    ResultList := TStringList.Create;
    try
      ResultList.LoadFromFile(save_file_name);
      sprgrsbr2.Max:=ResultList.Count;
      for ii:=0 to ResultList.Count-1 do begin
        sprgrsbr2.Position:=sprgrsbr2.Position+1  ;
        strs:=TStringList.Create;
        strs.Delimiter:=#9;
        strs.CommaText:=ResultList[ii];
        if ii=0 then begin
           temp16:=strs.Count;
        end else if (strs.Count>temp16)and(temp16>1) then begin
          ResultList[ii]:='';
          for jj:=0 to temp16-1 do begin
            ResultList[ii]:= ResultList[ii]+StrS[jj]+#9;  //Copy(ResultList[ii],0,Length(ResultList[ii])-length(StrS[strs.Count-1]));
          end;
        end;
        strs.Destroy;
      end;
      ResultList.SaveToFile(edt2.Text+ExtractFileName(dlgOpen1.FileName)+'-New.txt');// copy(save_file_name,0,Length(save_file_name)-4)+'-New.txt'); //保存到原txt文件中
      ResultList.Free;
    except
      ShowMessage('Save Err! Pls Check the File is Close');
    end;

    btn101.Caption:='Conver Finished!转换结束 again?';
  end;
end;

procedure TForm_line.btn102Click(Sender: TObject);
begin
  modbusfun06:=$0041;    //选择机种
  modbusfun06dat:=0;
end;

procedure TForm_line.btn103Click(Sender: TObject);
begin
  modbusfun06:=$0041;    //选择机种
  modbusfun06dat:=1;
end;

procedure TForm_line.btn104Click(Sender: TObject);
begin
  modbusfun06:=$0041;    //选择机种
  modbusfun06dat:=2;
end;

procedure TForm_line.btn105Click(Sender: TObject);
begin
  modbusfun06:=$0041;    //选择机种
  modbusfun06dat:=3;
end;

procedure TForm_line.btn106Click(Sender: TObject);
begin
  modbusfun06:=$0041;    //选择机种
  modbusfun06dat:=4;
end;

procedure TForm_line.btn107Click(Sender: TObject);
begin
  modbusfun06:=$0041;    //选择机种
  modbusfun06dat:=5;
end;

procedure TForm_line.btn108Click(Sender: TObject);
begin
  modbusfun06:=$0041;    //选择机种
  modbusfun06dat:=6;
end;

procedure TForm_line.btn109Click(Sender: TObject);
begin
  modbusfun06:=$0041;    //选择机种
  modbusfun06dat:=7;
end;

procedure TForm_line.btn110Click(Sender: TObject);
begin
  modbusfun06:=$0041;    //选择机种
  modbusfun06dat:=8;
end;

procedure TForm_line.btn111Click(Sender: TObject);
begin
  modbusfun06:=$0041;    //选择机种
  modbusfun06dat:=9;
end;

procedure TForm_line.cbb3Change(Sender: TObject);
begin
   btn1.Font.Color:=clRed;

   ComboBox1.Text:=cbb3.Text;
   form_line.SpeedButton12.Enabled:=true;
   btn112.Enabled:=true;

   form_line.Comm1.StopComm;
   shp2.Brush.Color := clSilver;
   shp4.Brush.Color := clSilver;
end;

procedure TForm_line.cbb2Change(Sender: TObject);
begin
   combobox2.Text:=cbb2.Text;
   Comm1.BaudRate := strtoint(combobox2.Text);
   btn1.Font.Color:=clRed;

   SpeedButton12.Enabled:=true;
   btn112.Enabled:=true;

   form_line.Comm1.StopComm;
   shp2.Brush.Color := clSilver;
   shp4.Brush.Color := clSilver;
end;


procedure TForm_line.mmo18KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var i:SmallInt;
    AStr,s:string;
begin

  if rb1.Checked then begin
      AStr:=mmo18.Text;
      for i:=1 to length(AStr)  do
      begin
        //ch:=AStr[i];
        s:=s+IntToHex(Ord(AStr[i]),2)+' ';
      end;
      mmo17.Text:=s;
  end;
end;

procedure TForm_line.mmo17Change(Sender: TObject);
var i,j:SmallInt;
    s,temp:string;
begin
  if rb1.Checked then begin

    if (Copy(mmo17.Text,Length(mmo17.Text),1)<>' ')and(Copy(mmo17.Text,Length(mmo17.Text)-1,1)<>' ')
      and (Length(mmo17.Text)>1) then begin
      mmo17.Text:=mmo17.Text+' ';
      mmo17.SelStart:= Length(mmo17.Text);
    end;

    s:=TrimLeft(mmo17.Text);
    repeat
      i := pos(' ', s);
      j := length(s);
      if i > 0 then
        s := copy(s, 1, i - 1) + copy(s, i + 1, j - i);
    until i = 0;
    temp:=s;

    send_len:=Length(temp);
    send_len:=(1+send_len) div 2;   //字符个数
    hextobin(pchar(temp),pchar(@sbuf)+0, send_len);      //中间有空格会导致发送错误

    s:='';
    for i:=1 to  send_len do
      s:= s+char(sbuf[i]);

    mmo18.Text:=s;
  end;
end;

procedure TForm_line.btn114Click(Sender: TObject);           //上传
var
  IdHttp : TIdHTTP;
  Url : string;//请求地址
  ResponseStream : TStringStream; //返回信息
  MutPartForm: TIdMultiPartFormDataStream;
  ResponseStr,str,stime : string;
  ii:SmallInt;
  f:TextFile;
begin
  //创建IDHTTP控件
  IdHttp := TIdHTTP.Create(nil);      //TStringStream对象用于保存响应信息
  try
    if (edt62.Text='25') then  begin
       //测试错误代码 "IOhandler value is valid"     IdHttp.HandleRedirects := True;//允许头转向，不支持https://的地址？？？
      datetimetostring(stime,'yyyy-mm-dd HH:nn:ss',Now);
      stime:=StringReplace(stime, ' ', 'T', [rfReplaceAll]);

      IdHttp.ReadTimeout := 5000;//请求超时设置
      IdHttp.Request.ContentType := 'application/json';//设置内容类型为json
      Url := 'http://'+edt141.Text+edt156.Text+edt154.Text;        //地址+ API命令
      if btn115.hint='' then begin                  //手动测试指令
        btn115Click(Self);
        edt177.Text:='{"header": "*ET","upperLimit": "0","lowerLimit": "0","measureUnit": "","value": "0"}';
        edt177.Text:=edt177.Text+',{"header": "OPEN","upperLimit": "0","lowerLimit": "0","measureUnit": "","value": "30000"},{"header": "SHOR","upperLimit": "0","lowerLimit": "0","measureUnit": "","value": "30000"}';
        edt177.Text:=edt177.Text+',{"header": "REST","upperLimit": "0","lowerLimit": "0","measureUnit": "","value": "30000"},{"header": "P2_P23","upperLimit": "4.975","lowerLimit": "0.025","measureUnit": "OHM","value": "30000"}';
        edt177.Text:=edt177.Text+',{"header": "P3_P24","upperLimit": "4.975","lowerLimit": "0.025","measureUnit": "OHM","value": "30000"},{"header": "P4_P26","upperLimit": "4.975","lowerLimit": "0.025","measureUnit": "OHM","value": "30000"}';
        edt177.Text:=edt177.Text+',{"header": "P5_P25","upperLimit": "4.975","lowerLimit": "0.025","measureUnit": "OHM","value": "30000"},{"header": "P6_P25","upperLimit": "4.975","lowerLimit": "0.025","measureUnit": "OHM","value": "30000"}';
        edt177.Text:=edt177.Text+',{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"},{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"}';
        edt177.Text:=edt177.Text+',{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"}';
        edt177.Text:=edt177.Text+',{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"},{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"}';
        edt177.Text:=edt177.Text+',{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"}';
        edt177.Text:=edt177.Text+',{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"},{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"}';
        edt177.Text:=edt177.Text+',{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"},{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"}';
        edt177.Text:=edt177.Text+',{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"},{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"}';
        edt177.Text:=edt177.Text+',{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"}';
        edt177.Text:=edt177.Text+',{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"},{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"}';
        edt177.Text:=edt177.Text+',{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"}';
        edt177.Text:=edt177.Text+',{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"},{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"}';
        edt177.Text:=edt177.Text+',{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"}';
        edt177.Text:=edt177.Text+',{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"},{"header": "INSUL","upperLimit": "3000","lowerLimit": "100","measureUnit": "MO","value": "30000"}';
      end;
      begin
        ResponseStream := TStringStream.Create('{"barcode": "'+(AnsiToUtf8(edt136.Text))
                        +'","testType": "ICT'
                        +'","testTime": "'+stime      //Hour可能会少0 DateToStr(Now)+'T'+TimeToStr(Now)
                        +'","testResult": "'+edt137.Text
                        +'","operator": "' +edt79.Text
                        +'","workArea": "'+readchis('server','workArea')          //"B1W'
                        +'","resourceName": "'+readchis('server','resourceName')   //"RES01'
                        +'","ipAddress": "'+IdIPWatch1.LocalIP
                        +'","toolNumber": "' +edt80.Text      //"S-01-02'
                        +'","errorCode": "' +edt165.Text  //(Rleds[ii].Hint,0,30)               //Eo/s
                        +'","productName": "'+edt77.Text
                        +'","program": "hFCT9'
                        +'","socketInfo": "'+readchis('server','socketInfo')       //"some socket info'
                        +'","rosalineInfo": "'+readchis('server','rosalineInfo')   //"some rosalineinfo"}'
                        +'","testDetailId": "'+btn115.Hint
                          +'","testDetails": ['
                          +edt177.Text
                          +']'
                        +'}' );
      end;
      DMLJK.MyAddlog('post begin---------------------------');
      DMLJK.MyAddlog(ResponseStream.DataString);
      DMLJK.MyAddlog('post end----------------------------');
      ResponseStream.Position:=0;      //将流位置置为0
      mmo19.Lines.Add(Url);
      mmo19.Lines.Add(ResponseStream.DataString);
      try
        ResponseStr :=IdHttp.Post(Url,ResponseStream);
      except
        on e : Exception do
        begin
          stat1.Panels[4].Text :=e.Message; //ShowMessage(e.Message);
        end;
      end;
      //获取网页返回的信息
      //ResponseStr := ResponseStream.DataString;
      //测试用，ResponseStr := '"isSuccess":false,"message":"CONTAINER：冲床未关联","currentTime":"2020-08-17T15:16:49.3067346+08:00","extensionCode":0';
      if (pos('"isSuccess":false,',ResponseStr)>0) then
      begin
         lbl164.Caption:='false';
      end else   if (pos('"isSuccess":true,',ResponseStr)>0)then
      begin
         lbl164.Caption:='true';
      end else
        lbl164.Caption:='error';

      //网页中的存在中文时，需要进行UTF8解码        //不完整编码后的数据复制 会返回空，
      if chk29.Checked then  begin      //测试是否可解码
        chk29.Checked:=false;
        ResponseStr := '{"isSuccess":false,"message":"OQC:鏃犳硶鎵惧埌娴嬭瘯椤哄簭璁剧疆","currentTime":"2020-10-09T18:42:47.5850907+08:00","extensionCode":0}';
      end;
      ResponseStr:= UTF8Decode(ResponseStr);

      stat1.Panels[5].Text := Copy(ResponseStr,Pos('"message":"',ResponseStr),Pos(',"currentTime"',ResponseStr)-Pos('"message":"',ResponseStr));   //显示内容

    end;
    mmo19.Lines.Add(ResponseStr);

    if FileExists(edt2.Text +lbl183.Caption + Label10.Caption)   then begin
      try
        assignfile(f, edt2.Text +lbl183.Caption + Label10.Caption);
        Append(f);
        writeln(f, Url+ResponseStream.DataString);                       
        writeln(f, ResponseStr);
      finally
        Closefile(f);
      end;
    end;
  finally
    IdHttp.Free;
    ResponseStream.Free;
  end;
end;

procedure TForm_line.btn115Click(Sender: TObject);
var
  IdHttp : TIdHTTP;
  Url : string;//请求地址
  ResponseStream : TStringStream; //返回信息
  ResponseStr : string;
  LTep: TGUID;
  sGUID: string;

begin

  CreateGUID(LTep);
  sGUID := GUIDToString(LTep);
  //mmo19.Lines.Add(sGUID);
  sGUID := StringReplace(sGUID, '-', '', [rfReplaceAll]);
  sGUID := Copy(sGUID, 2, Length(sGUID) - 2);
  btn115.Hint:= LowerCase(sGUID);
  mmo19.Lines.Add('当前GUID='+btn115.Hint);

end;

procedure TForm_line.btn116Click(Sender: TObject);
begin
  mmo19.Lines.Clear;
end;

//  正常測試時，上传产品条码、料号、工單、機臺編號給服務器接口，服務器接口檢查如下信息，如果不符合返回false，否則返回true。
//  A 條碼是否符合料號設置的二維碼規則
//  C 此料號是否有首件記錄
//  B 此料號、機臺是否在規定時間內完成標準版測試
//  Get_OK_BIAOZHUI（条码、料号、工單、機臺編號）數據類型：字符型
//  如果不符合返回false，否則返回true。
//GET /DC_STAND/WebService.asmx/Get_OK_BIAOZHUI?barcode=string&part_id=string&base_id=string&att_id=string
procedure TForm_line.btn118Click(Sender: TObject);
var
  IdHttp : TIdHTTP;
  Url :string;//请求地址
  ResponseStream : TStringStream; //返回信息
  ResponseStr,stime : string;
  f:TextFile;

begin
  //创建IDHTTP控件
  IdHttp := TIdHTTP.Create(nil);

  ResponseStream := TStringStream.Create('');         //TStringStream对象用于保存响应信息
  try //请求地址
    if (edt62.Text='25') then  begin
      datetimetostring(stime,'yyyy-mm-dd HH:nn:ss',Now);
      stime:=StringReplace(stime, ' ', 'T', [rfReplaceAll]);
      Url := 'http://'+edt141.Text+edt156.Text+edt153.Text         //地址+ API命令
            +'barcode='+(AnsiToUtf8(edt136.Text))
            +'&operator='   +(AnsiToUtf8(edt79.Text))
            +'&toolNumber=' +(AnsiToUtf8(edt80.Text))
            +'&testType='   +(AnsiToUtf8('ICT'))
            +'&prevTestType='+readchis('server','prevTestType')
            +'&productName=' +(AnsiToUtf8(edt77.Text))
            +'&workArea='+readchis('server','workArea')
            +'&site='+readchis('server','site')
            +'&program='    +(AnsiToUtf8('hFCT9'))
            +'&lot=' +(AnsiToUtf8(edt78.Text))
            +'&ipAddress='  +(AnsiToUtf8(IdIPWatch1.LocalIP))
            +'&stationTime='  +stime  
            ;
      mmo19.Lines.Add(Url);
      if chk30.Checked then begin
        ResponseStr := '{"isSuccess":true,"message":"OQC:鏃犳硶鎵惧埌娴嬭瘯椤哄簭璁剧疆","currentTime":"2020-10-09T18:42:47.5850907+08:00","extensionCode":0}';
      end else if chk31.Checked then begin
        ResponseStr := '{"isSuccess":false,"message":"OQC:鏃犳硶鎵惧埌娴嬭瘯椤哄簭璁剧疆","currentTime":"2020-10-09T18:42:47.5850907+08:00","extensionCode":0}';
      end else begin
        try
          IdHttp.Get(Url,ResponseStream);
        except
          on e : Exception do
          begin
            stat1.Panels[4].Text :=e.Message; //ShowMessage(e.Message);
          end;
        end;
        //获取网页返回的信息
        ResponseStr := ResponseStream.DataString;
      end;
      //测试用， ResponseStr := '"isSuccess":false,"message":"CONTAINER：冲床未关联","currentTime":"2020-08-17T15:16:49.3067346+08:00","extensionCode":0';
      if (pos('"isSuccess":false,',ResponseStr)>0) then
      begin
         lbl164.Caption:='false';
      end else   if (pos('"isSuccess":true,',ResponseStr)>0)then  begin
         lbl164.Caption:='true';
      end else
         lbl164.Caption:='error';

      //网页中的存在中文时，需要进行UTF8解码        //不完整编码后的数据复制 会返回空，
      if chk29.Checked then  begin      //测试是否可解码
        chk29.Checked:=false;
        ResponseStr := '{"isSuccess":false,"message":"OQC:鏃犳硶鎵惧埌娴嬭瘯椤哄簭璁剧疆","currentTime":"2020-10-09T18:42:47.5850907+08:00","extensionCode":0}';
      end;
      ResponseStr:= UTF8Decode(ResponseStr);

      stat1.Panels[5].Text := Copy(ResponseStr,Pos('"message":"',ResponseStr),Pos(',"currentTime"',ResponseStr)-Pos('"message":"',ResponseStr));   //显示内容

    end;
    mmo19.Lines.Add(ResponseStr);
    if FileExists(edt2.Text +lbl183.Caption + Label10.Caption)   then begin
      try
        assignfile(f, edt2.Text +lbl183.Caption + Label10.Caption);
        Append(f);
        writeln(f, Url);                      
        writeln(f, ResponseStr);
      finally
        Closefile(f);
      end;
    end;

  finally
    IdHttp.Free;
    ResponseStream.Free;
  end;
end;


procedure TForm_line.btn120Click(Sender: TObject);
begin
    modbusfun16int:=$2F00;
    modbusfun16len:=$12;
    //sbuf[1]:=1;
    sbuf[2]:=$10;

    sbuf[8]:=0;sbuf[9]:=50; //序号
    sbuf[10]:=byte(' ');sbuf[11]:=byte('0');sbuf[12]:=byte(' ');sbuf[13]:=byte(' ');
    for i:=2 to 8 do
    begin
     sbuf[10+2*i]:=0;sbuf[11+2*i]:=0;
    end;
    sbuf[22]:=StrToInt(edt49.Text) div 256;
    sbuf[23]:=StrToInt(edt49.Text);
    sbuf[24]:=StrToInt(edt50.Text) div 256;
    sbuf[25]:=StrToInt(edt50.Text);
    sbuf[26]:=StrToInt(edt51.Text) div 256;
    sbuf[27]:=StrToInt(edt51.Text);
    sbuf[28]:=byte('Q');sbuf[29]:=byte('7');sbuf[30]:=byte('0');sbuf[31]:=byte('0');
    for i:=10 to 15 do
      begin
          sbuf[12+2*i]:=0;sbuf[13+2*i]:=0;
      end;
    sbuf[33]:=100;     //比例
end;

procedure TForm_line.C5Click(Sender: TObject);    //调试菜单下开启串口调试
begin
  if groupbox1.hint<>'5' then begin        //FCT5定义主板不可控制commopen
    if GroupBox1.Visible then begin
      groupbox1.hint:='0';     //不启动软件打开串口
      btn77.Visible:=False;
      btn78.Visible:=False;
      C5.Checked:=False;
      GroupBox1.Visible:=False;
      
      SpeedButton12.Enabled:=true;
      SpeedButton13.Enabled:=true;
      btn112.Enabled:=true;
      btn113.Enabled:=true;
      form_line.Comm1.StopComm;
      shp2.Brush.Color := clSilver;     shp4.Brush.Color := clSilver;

    end else begin
      groupbox1.hint:='1';     //启动软件打开串口
      //触摸屏翻页命令btn77.Visible:=true;
      //btn78.Visible:=true;
      C5.Checked:=true;
      GroupBox1.Visible:=true;
    end;
  end;
end;



procedure TForm_line.btn129Click(Sender: TObject);
var
  IdHttp : TIdHTTP;
  Url : string;//请求地址
  ResponseStream : TStringStream; //返回信息
  ResponseStr : string;
begin
  //创建IDHTTP控件
  IdHttp := TIdHTTP.Create(nil);
  //测试错误代码 "IOhandler value is valid"     IdHttp.HandleRedirects := True;//允许头转向，不支持https://的地址？？？
  IdHttp.ReadTimeout := 5000;//请求超时设置
  IdHttp.Request.ContentType := 'application/json';//设置内容类型为json

  //TStringStream对象用于保存响应信息
  ResponseStream := TStringStream.Create('{ "i": r_word,"from": "AUTO","to": "AUTO","smartresult": "dict","client": "fanyideskweb","salt": js_key["salt"],"sign":js_key["sign"],"doctype": "json","version": "2.1","keyfrom": "fanyi.web","action": "FY_BY_REALTIME","typoResult": "false" }');
  try
    ResponseStream.Position:=0;      //将流位置置为0
    //请求地址
    Url := 'http://www.webxml.com.cn/WebServices/WeatherWebService.asmx?wsdl';
    Url := 'http://getman.cn/echo?';//                                  //错误代码 "301 Moved Permanently"
    Url := 'http://fanyi.youdao.com/translate_o??smartresult=dict&smartresult=rule';// //'http://dict.youdao.com/';
    mmo19.Lines.Add(Url) ;
    try
      lbl164.Caption:='true';
      ResponseStr :=IdHttp.post(Url,ResponseStream);
    except
      on e : Exception do
      begin
        lbl164.Caption:='false';
        ShowMessage(e.Message);
      end;
    end;
    //获取网页返回的信息
    //ResponseStr := ResponseStream.DataString;
    //网页中的存在中文时，需要进行UTF8解码
    ResponseStr := UTF8Decode(ResponseStr);
    stat1.Panels[5].Text := Copy(ResponseStr,Pos('1.0"',ResponseStr)+3,50);

    mmo19.Lines.Add(Copy(ResponseStr,0,128));
  finally
    IdHttp.Free;
    ResponseStream.Free;
  end;

end;



procedure TForm_line.btn130Click(Sender: TObject);
var str,password:string;
//    nrf_dat_config: Tinifile;
//    ini_path: string;
begin
   if readchis('Comm Para', 'PassWord')<>'' then  begin    //不为空
      password:=readchis('Comm Para', 'PassWord');
   end else
      password:='999';
   if (Readchis('Server','MESprogram')<>'')or(Readchis('Server','MESprogramDefault')<>'') then begin    //丽清联机和脱机
     if  InputQuery('MES联机切换', '请输入密码', str) then
     begin
       if (Str=password) or (Str='137760') then  begin
          if btn130.Font.Color=clGreen then begin
            btn130.Caption:='MES脱机';
            btn130.Font.Color:=clDefault;
            WriteChis('Server','MESprogram','');
          end else begin
            btn130.Caption:='MES联机';
            btn130.Font.Color:=clGreen;
            str:=Readchis('Server','MESprogramdefault');
            if str='' then begin
              str:='auto';
              WriteChis('Server','MESprogramdefault',str);
            end;
            WriteChis('Server','MESprogram',str);
          end;
       end else  begin
          Showmessage('密码错误，不能切换！');
       end;
       if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then  edit1.SetFocus;
     end;
   end else begin     //jaguar用
     if  InputQuery('MES联机切换', '请输入密码', str) then
     begin
       if (Str=password) or (Str='137760') then  begin
          if btn130.Font.Color=clGreen then begin
            btn130.Caption:='MES脱机';
            btn130.Font.Color:=clDefault;
            chk25.Visible:=False;
            chk26.Visible:=False;
            chk27.Visible:=False;
            chk28.Visible:=False;
            WriteChis('Server','MESfun','');
          end else begin
            btn130.Caption:='MES联机';
            btn130.Font.Color:=clGreen;
            chk25.Visible:=true;
            chk26.Visible:=true;
            chk27.Visible:=true;
            chk28.Visible:=true;
            str:=Readchis('Server','MESdefault');
            if str='' then begin
              str:='24';
              WriteChis('Server','MESdefault',str);
            end;
            WriteChis('Server','MESfun',str);
          end;
       end else  begin
          Showmessage('密码错误，不能切换！');
       end;
       ts7.Show;
       if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then  edit1.SetFocus;
     end;
   end;
end;


procedure TForm_line.A1Click(Sender: TObject);
var str:string;
begin
  if (StrToInt('0'+sysedit[5].Text)=0) or (working >0) then      //没有读到版本号，不执行
    begin
      if(working >0) then  lbl28.Caption:='MCU is busying, Pls press RESET!请按复位再导入'
      else     lbl28.Caption:='Pls retry to click!请重新上电，重启软件试一次！';
    end
  else begin         //允许导入
    if InputQuery('Import Window!', 'Pls Enter Password！请输入密码', str) then
    begin
      if (Str='888999') then begin
        str:='10';
        if InputQuery('Import Window!', 'Pls Enter MaxPN 请输入机种程序导入数N=', str) then
        begin
          N50.Tag:=strtoint(str);
        end else
          N50.Tag:=0;
        A1.Tag:=0;A1.Checked:=TRUE; N50.Checked:=False;n51.Checked:=false;
        modbusfun06:=$0041;    //选择机种
        modbusfun06dat:=A1.Tag+$2000;         //btn23Click(Self);
      end;
    end;
  end;

end;

procedure TForm_line.Label7DblClick(Sender: TObject);
var
    str,password:string;
begin
   if readchis('Comm Para', 'hPassWord')<>'' then  begin    //不为空
      password:=readchis('Comm Para', 'hPassWord');
   end else
      password:='888999';

   if InputQuery('barcode功能启用 切换', '请输入密码', str) then
   begin

     if (Str=password) or (Str='137760') then  begin
        if Label7.Color=clGreen then begin
          Label7.Color:=clGray;
          WriteChis('Comm Para','clearbarcode','2');
          Label7.hint:='2';
        end else begin
          Label7.Color:=clGreen;
          str:=Readchis('DefaultPara','barcodeDefault');
          if (str='2')or(str='') then begin              //没有设置
            str:='1';
            WriteChis('DefaultPara','barcodeDefault',str);
          end;
          WriteChis('Comm Para','clearbarcode',str);
          Label7.hint:=str;
        end;
     end else  begin
        Showmessage('密码错误，不能切换！');
     end;

     if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then  edit1.SetFocus;
   end;

end;

procedure TForm_line.lbl79DblClick(Sender: TObject);
var
    str,password:string;
begin
   if readchis('Comm Para', 'hPassWord')<>'' then  begin    //不为空
      password:=readchis('Comm Para', 'hPassWord');
   end else
      password:='888999';

   if InputQuery('Debug功能启用 切换', '请输入密码', str) then
   begin

     if (Str=password) or (Str='137760') then  begin
        if lbl79.Color=clGreen then begin
          lbl79.Color:=clGray; tabsheet4.TabVisible:=True;   ts3.TabVisible:=True;
          WriteChis('Comm Para','DebugMode','1');
        //  edt61.Text:='1';
        end else begin
          lbl79.Color:=clGreen; tabsheet4.TabVisible:=False;  ts3.TabVisible:=false;
          str:=Readchis('DefaultPara','DebugModeDefault');
          if (str='1')or(str='') then begin              //没有设置
            str:='0';
            WriteChis('DefaultPara','DebugModeDefault',str);
          end;
          WriteChis('Comm Para','DebugMode',str);
          Label7.hint:=str;
        end;
     end else  begin
        Showmessage('密码错误，不能切换！');
     end;

     if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then edit1.SetFocus;
   end;
end;

procedure TForm_line.chk32Click(Sender: TObject);
begin
  if chk32.Checked then chk32.Color:=clBtnFace else chk32.Color:=clYellow;
end;

procedure TForm_line.chk33Click(Sender: TObject);
begin
  if chk33.Checked then chk33.Color:=clBtnFace else chk33.Color:=clYellow;

end;

procedure TForm_line.chk34Click(Sender: TObject);
begin
  if chk34.Checked then chk34.Color:=clBtnFace else chk34.Color:=clYellow;

end;

procedure TForm_line.chk35Click(Sender: TObject);
begin
  if chk35.Checked then chk35.Color:=clBtnFace else chk35.Color:=clYellow;

end;

procedure TForm_line.chk36Click(Sender: TObject);
begin
  if chk36.Checked then chk36.Color:=clBtnFace else chk36.Color:=clYellow;
end;

procedure TForm_line.ts23Show(Sender: TObject);
begin
  if pnl1.Height=22 then begin    //btn120.Caption<>'X'
    pnl1.Height:=188;
    btn120.Caption:='X 隐藏';
    N38.Caption:='Close SysWin 关闭系统参数显示';
  end;
end;

procedure TForm_line.grp43DblClick(Sender: TObject);
var
    str,password:string;
begin
   if readchis('Comm Para', 'PassWord')<>'' then  begin    //不为空
      password:=readchis('Comm Para', 'PassWord');
   end else
      password:='999';

   if InputQuery('设置参数', '请输入密码', str) then
   begin
     if (Str=password) or (Str='137760') then  begin
        str:='3';
        if InputQuery('Import Window!', '请输入最大重测下压次数N=', str) then
        begin
          edt169.Text:=str;
          WriteChis('Comm Para','Retest',str);
        end;
     end else  begin
        Showmessage('密码错误，不能配置最大重测次数！');
     end;
     ts7.Show;
     if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then  edit1.SetFocus;
   end;

end;



procedure TForm_line.btn138Click(Sender: TObject);
begin
  grp35.Visible:=false;
end;

procedure TForm_line.btn139Click(Sender: TObject);
begin
  mmo3.Visible:=False;
  btn139.Visible:=False;
end;

procedure TForm_line.btn140Click(Sender: TObject);
begin

  pgc1.Width:=grp3.Width;
end;

procedure TForm_line.btn141Click(Sender: TObject);
var i:SmallInt;
begin
  SysExplainNum:=SysExplainNum+1;
end;

procedure TForm_line.mmo28Click(Sender: TObject);

begin
  chk1.Color:=clBtnFace;
  chk2.Color:=clBtnFace;
  chk3.Color:=clBtnFace;
  chk4.Color:=clBtnFace;
  chk5.Color:=clBtnFace;
  chk7.Color:=clBtnFace;
  chk8.Color:=clBtnFace;
  chk9.Color:=clBtnFace;

  syslabel[0].Color:=clBtnFace;
  syslabel[1].Color:=clBtnFace;
  syslabel[2].Color:=clBtnFace;
  syslabel[3].Color:=clBtnFace;
  syslabel[4].Color:=clBtnFace;
  syslabel[5].Color:=clBtnFace;
  syslabel[6].Color:=clBtnFace;
  syslabel[7].Color:=clBtnFace;
  syslabel[8].Color:=clBtnFace;
  syslabel[9].Color:=clBtnFace;
  syslabel[10].Color:=clBtnFace;
  syslabel[11].Color:=clBtnFace;
  syslabel[12].Color:=clBtnFace;
  syslabel[13].Color:=clBtnFace;
end;

procedure TForm_line.btn112Click(Sender: TObject);
begin
  SpeedButton12Click(Self);
  if shp2.Brush.Color = clGreen  then shp4.Brush.Color := clGreen;    //开启串口助手标志
end;



procedure TForm_line.M3Click(Sender: TObject);
var str:string;
begin
  str:=M3.Hint;
  if InputQuery('Pls Enter 4 char!查询模式', 'abc', str) then  begin
     M3.Hint:=str;
     modbusfun16str:=AnsiUpperCase(str+'   ');
     modbusfun16len:=2;
     modbusfun16:=$2F43;
  end;
end;

procedure TForm_line.N52Click(Sender: TObject);
var str:string;
begin
  str:=N52.Hint;
  if InputQuery('Pls Enter 16 char!查询备注（区分大小写）', 'abc', str) then  begin
     N52.Hint:=str;
     modbusfun16str:=str+'               ';
     modbusfun16len:=8;
     modbusfun16:=$2F44;
  end;
end;

procedure TForm_line.N53Click(Sender: TObject);
var str:string;
begin
  str:=N53.Hint;
  if InputQuery('Pls Enter 4 char!查询步骤名称（区分大小写）', 'abc', str) then  begin
     N53.Hint:=str;
     modbusfun16str:=str+'   ';
     modbusfun16len:=2;
     modbusfun16:=$2F40;
  end;
end;

procedure TForm_line.P5Click(Sender: TObject);
var  str:string;
begin
  str:=P5.Hint;
  if InputQuery('Enter Int!查询参数所在步骤', '+-1234', str) then begin
      P5.Hint:=str;
     if str='' then str:='0';
     if IsNumberic(sysedit[1].Text) and (StrToInt(sysedit[1].Text)>=768+1+3328) then begin      //大于4095，点数可翻倍！
       if StrToInt(str)>MAXPINS*2 then
          modbusfun06dat:=MAXPINS*2
       else
          modbusfun06dat:=StrToInt(str);
     end else if StrToInt(str)>MAXPINS then
        modbusfun06dat:=MAXPINS
     else modbusfun06dat:=StrToInt(str);
     modbusfun06:=$2F45;
  end;
end;


procedure TForm_line.btn143Click(Sender: TObject);
begin
  if grp6.Visible then begin
    grp6.Visible:=False;
    btn143.Caption:=' 显示';
  end else begin
    grp6.Visible:=true;
    btn143.Caption:='X关闭';

  end;
end;



procedure TForm_line.C7Click(Sender: TObject);
var
  strs,CompareFileList:TStringList;
  i,pp1,pp2:SmallInt;
  save_file_name,sss: string;
begin
  if dlgOpen1.Execute then begin
    lbl28.Caption:='CompareFiles Creating......开始生成比较文件（（测控值）写0）...';
    lbl28.Font.Color:=clRed;

    save_file_name := dlgOpen1.FileName;
    lbl29.Caption:=save_file_name;
    try
      CompareFileList := TStringList.Create;
      CompareFileList.LoadFromFile(save_file_name);

      strs:=TStringList.Create;
      //strs.Delimiter:=#9;
      //strs.DelimitedText:=CompareFileList[0];   //拆分不完全正确。连续的空格认识是TAB

      for i:=1 to  CompareFileList.Count do begin
        strs.Delimiter:=#9;
        strs.DelimitedText:=CompareFileList[i];   //拆分不完全正确。连续的空格认识是TAB
        if strs.Count>5 then begin
          pp1:=pos(char(#9),CompareFileList[i])+1;    //第一个TAB后的位置<-- 取长度不可靠（空格会不计算在内）Length(strs[0]);
          sss:=copy(CompareFileList[i],pp1,Length(CompareFileList[i]));
          pp1:=pp1+pos(char(#9),sss);
          sss:=copy(CompareFileList[i],pp1,Length(CompareFileList[i]));
          pp1:=pp1+pos(char(#9),sss);
          sss:=copy(CompareFileList[i],pp1,Length(CompareFileList[i]));

          pp2:=pp1+pos(char(#9),sss);
          sss:=copy(CompareFileList[i],pp2,Length(CompareFileList[i]));

          CompareFileList[i]:=copy(CompareFileList[i],0,pp1-1)+'0'+char(#9)+sss;
        end;
        if Copy(CompareFileList[i],0,4)='9999' then  begin
          Break;
        end;
      end;
      strs.Free;
      CompareFileList.SaveToFile(save_file_name+'~compare.txt'); //保存到比较txt文件中
      CompareFileList.Free;
    except
      ShowMessage('Create CompareFile Err! 生成比较文件错误');
    end;

    lbl28.Caption:='Create CompareFile Finish!比较文件结束！';

  end;

end;


procedure TForm_line.ts18Show(Sender: TObject);
begin
  if rb1.Checked then mmo17.Text:='01 03 50 00 00 10 55 06';
end;

procedure TForm_line.btn144Click(Sender: TObject);
var
    str,password:string;
begin
   if readchis('Comm Para', 'PassWord')<>'' then  begin    //不为空
      password:=readchis('Comm Para', 'PassWord');
   end else
      password:='999';

   if InputQuery('清除不良项目计数', '请输入密码', str) then
   begin
     if (Str=password) or (Str='137760') then  begin
        Series1.Clear;Series1.AddX(0);
        //Series2.AddX(0); series2.Title:='';
        Series3.Clear;Series3.AddX(0);
        //Series4.AddX(0); series4.Title:='';
        Series5.Clear;Series5.AddX(0);
        //Series6.AddX(0);series6.Title:='';
        Series7.Clear;Series7.AddX(0);
        //Series8.AddX(0);series8.Title:='';
        Series9.Clear;Series9.AddX(0);
        //Series10.AddX(0);series10.Title:='';

     end else  begin
        Showmessage('密码错误，不能清除！');
     end;

     if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then edit1.SetFocus;
   end;
end;


procedure TForm_line.pnl1DblClick(Sender: TObject);
begin
  N38Click(Self);
end;

procedure TForm_line.grp28DblClick(Sender: TObject);
begin
  N38Click(Self);
end;

procedure TForm_line.lv1DblClick(Sender: TObject);
begin
  if (lv1.SelCount>0)and(Readchis('Result','top5')<>'0')  then

end;

procedure TForm_line.s8Click(Sender: TObject);
begin
  btn16.Tag:=5;
  if btn14.Font.Color=clRed then begin        //优先把系统参数保存；
    modbusfun05:=8;  modbusfun05dat:=$FF;
    addToLog('SaveSys,保存系统参数' );
    if btn16.Font.Color=clRed then begin  //延时保存步骤参数；
      Application.ProcessMessages;
      Sleep(50);
      Application.ProcessMessages;
      Sleep(50);
      Application.ProcessMessages;       //必须定时（大于20mS）3次发送命令 ，否则串口通信不执行保存系统参数；
      Sleep(50);
      modbusfun05:=11; modbusfun05dat:=$FF;
      addToLog('SaveStep,保存当前页步骤' );
    end;
  end else begin
    modbusfun05:=11; modbusfun05dat:=$FF;
    addToLog('SaveStep,保存当前页步骤' );
  end;
end;

procedure TForm_line.btn146Click(Sender: TObject);
var
    i:SmallInt;
    temp8:Byte;
    tempstr:string;
begin
    modbusfun16int:=$2F00;
    modbusfun16len:=$12;   //sbuf[1]:=1;
    sbuf[2]:=$10;

    sbuf[8]:=StrToInt(edt143.Text)div 256;sbuf[9]:=StrToInt(edt143.Text)mod 256; //序号
    sbuf[10]:=byte(' ');sbuf[11]:=byte('0');sbuf[12]:=byte(' ');sbuf[13]:=byte(' ');
    for i:=2 to 8 do
    begin
     sbuf[10+2*i]:=0;sbuf[11+2*i]:=0;
    end;
    sbuf[22]:=StrToInt(edt49.Text) div 256;
    sbuf[23]:=StrToInt(edt49.Text);
    sbuf[24]:=StrToInt(edt50.Text) div 256;
    sbuf[25]:=StrToInt(edt50.Text);
    sbuf[26]:=StrToInt(edt51.Text) div 256;
    sbuf[27]:=StrToInt(edt51.Text);

    sbuf[28]:=byte('0');sbuf[29]:=byte('G');sbuf[30]:=byte('6');sbuf[31]:=byte('0');
    temp8:=sbuf[28]; sbuf[28]:=sbuf[29];sbuf[29]:=temp8;
    temp8:=sbuf[30]; sbuf[30]:=sbuf[31];sbuf[31]:=temp8;

    for i:=10 to 15 do begin
       sbuf[12+2*i]:=0;sbuf[13+2*i]:=0;
    end;
    tempstr:=Copy(edt185.text,0,Length(edt185.text));
    Move(tempstr[1],sbuf[44],Length(edt185.text));

    sbuf[33]:=100;     //比例
    sbuf[36]:=4000 div 256;
    sbuf[37]:=4000 mod 256;

end;

procedure TForm_line.btn147Click(Sender: TObject);
begin
      modbusfun06dat:=0;
     modbusfun06:=$2F60;
end;

procedure TForm_line.btn148Click(Sender: TObject);
begin
    modbusfun06dat:=StrToInt(edt143.Text);
     modbusfun06:=$10B0;
end;

procedure TForm_line.lbl140DblClick(Sender: TObject);
var
    str,password:string;
begin
   if readchis('Comm Para', 'PassWord')<>'' then  begin    //不为空
      password:=readchis('Comm Para', 'PassWord');
   end else
      password:='999';

   if InputQuery('barcodeDisRepeat条码禁止重复功能 切换', '请输入密码', str) then
   begin
     if (Str=password) or (Str='137760') then  begin
        if Lbl140.Color=clGreen then begin
          Lbl140.Color:=clGray;
          WriteChis('Comm Para','barcodeDisRepeat','');
        end else begin
          Lbl140.Color:=clGreen;
          WriteChis('Comm Para','barcodeDisRepeat','1');
        end;
     end else  begin
        Showmessage('密码错误，不能切换！');
     end;

     if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then    edit1.SetFocus;
   end;
end;

procedure TForm_line.lbl82DblClick(Sender: TObject);
var
    str,password:string;
begin
   if readchis('Comm Para', 'PassWord')<>'' then  begin    //不为空
      password:=readchis('Comm Para', 'PassWord');
   end else
      password:='999';

   if InputQuery('参数设置', '请输入密码', str) then
   begin
     if (Str=password) or (Str='137760') then  begin
        str:=IntToStr(lbl82.Tag);
        if InputQuery('Import Window!', '请输入最低良率=', str) then
        begin
          if IsNumberic(str) then begin
            lbl82.Tag:=StrToInt(str);
            lbl82.Hint:='>='+str+'%';
            WriteChis('Result','StopYield',str);
          end;
        end;
     end else  begin
        Showmessage('密码错误，不能配置输入最低良率！');
     end;

     if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then  edit1.SetFocus;
   end;

end;

procedure TForm_line.lbl80DblClick(Sender: TObject);
var
    str,password:string;
begin
   if readchis('Comm Para', 'PassWord')<>'' then  begin    //不为空
      password:=readchis('Comm Para', 'PassWord');
   end else
      password:='999';

   if InputQuery('参数设置', '请输入密码', str) then
   begin
     if (Str=password) or (Str='137760') then  begin
        str:=IntToStr(lbl80.Tag);
        if InputQuery('Import Window!', '请输入管控良率的最小次数=', str) then
        begin
          if IsNumberic(str) then begin
            lbl80.Tag:=StrToInt(str);
            lbl80.Hint:='>='+str;
            WriteChis('Result','StopYieldTotal',str);
          end;
        end;
     end else  begin
        Showmessage('密码错误，不能配置管控良率的最小次数！');
     end;

     if(TabSheet2.Showing) and edit1.Enabled and grp36.Visible  then  edit1.SetFocus;
   end;
end;


procedure   TForm_line.stepParaKeydown(Sender:TObject; var Key: Word; Shift: TShiftState);
var str: string;
    ii,hh:SmallInt;
begin
   StepNowFresh:=(TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(Form_line.edt121.Text);   //刷新开始行数
   hh:=TEdit(Sender).Left+panel1.HorzScrollBar.Position+10 ;      //+10补偿模式编辑框左移！
   if (StepNowFresh>=0)and(StepNowFresh<VIRLCROWS) then begin
     if Key=VK_SPACE then begin
       if (GetKeyState(VK_CONTROL) and $8000)=$8000 then
          N60Click(Sender)                      //空步骤
          
       else   if (hh div 60)=1 then begin       //步骤名
         stepNameStrClick(Sender);
       end else if (hh div 60)=10-1 then begin       //模式
         stepModeStrClick(Sender);
       end else if (hh div 60)=16 then begin       //备注
         stepNoteClick(Sender);
       end else begin
         stepIntClick(Sender);
       end;

     end;

     sysParaKey(Sender,key,hh);

     stepKeyEdit(Key,hh);
   end;
end;
procedure   TForm_line.sysParaKeydown(Sender:TObject; var Key: Word; Shift: TShiftState);
var str: string;
    tt,ii,hh:SmallInt;
begin
   tt:=TEdit(Sender).Left div 60;

   if Key=VK_SPACE then begin
     if tt=13 then begin       //备注
       sysStrClick(Sender);
     end else begin
       sysIntClick(Sender);
     end;

   end;

   if ( tt>=0)and( tt<13 ) then begin                //
      if (Key=VK_ADD)or(Key=187) then begin           //微调不记录Log!
        if IsSignedNumbericHex(sysedit[tt].text) then begin
            modbusfun06:=$10A0+tt;
            modbusfun06dat:=StrToInt(sysedit[tt].text)+1; StepNowFresh:= VIRLCROWS-1;
         end;
      end;
      if (Key=VK_SUBTRACT)or(Key=189) then begin       //大键盘‘-‘
        if IsSignedNumbericHex(sysedit[tt].text) then begin
            modbusfun06:=$10A0+tt;
            modbusfun06dat:=StrToInt(sysedit[tt].text)-1;  StepNowFresh:= VIRLCROWS-1;
        end;
      end;
   end;

      if (GetKeyState(VK_CONTROL) and $8000)=$8000 then
        Exit;
      if (Key=VK_LEFT)or(Key=Byte('A'))  then begin
          case  tt of                          //左移动
            1: sysedit[13].SetFocus;
            2: sysedit[1].SetFocus;
            3: sysedit[2].SetFocus;
            4: sysedit[3].SetFocus;
            7: sysedit[4].SetFocus;
            9: sysedit[7].SetFocus;
            10: sysedit[9].SetFocus;
            12: sysedit[10].SetFocus;
            13: sysedit[12].SetFocus;
          end;
      end;
      if (Key=VK_RIGHT)or(Key=Byte('D'))  then begin
          case  tt of                          //右移动
            13: sysedit[1].SetFocus;
            1: sysedit[2].SetFocus;
            2: sysedit[3].SetFocus;
            3: sysedit[4].SetFocus;
            4: sysedit[7].SetFocus;
            7: sysedit[9].SetFocus;
            9: sysedit[10].SetFocus;
            10: sysedit[12].SetFocus;
            12: sysedit[13].SetFocus;
          end;
      end;
      if (Key=VK_UP)or(Key=Byte('W'))  then begin
        edt45.SetFocus
      end;
      if (Key=VK_DOWN)or(Key=Byte('S'))  then begin
        edt45.SetFocus
      end;
end;

procedure TForm_line.stepModePopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
var str: string;
    hh:SmallInt;
begin
   hh:=TEdit(Sender).Left+panel1.HorzScrollBar.Position+10 ;      //+10补偿模式编辑框左移！
   StepNowFresh:=(TEdit(Sender).Top+panel1.VertScrollBar.Position-10)div strtoint(Form_line.edt121.Text);   //刷新开始行数
   modbusfun06dat:=((StrToInt(edt45.Text)-1)div 50)*50+1+StepNowFresh;
   modbusfun06:=$10B0;   //等同

   if ( (hh div 60)=16)and(Length(zu17edit[StepNowFresh+1].text)>4) then begin       //长备注允许右键复制
   
   end else begin
      Handled := True;
   end;
end;
procedure TForm_line.sysParaPopup(Sender: TObject; MousePos: TPoint;var Handled: Boolean);
var str: string;
    hh:SmallInt;
begin
   Handled := True;
end;

procedure TForm_line.N59Click(Sender: TObject);
var ii:SmallInt;
begin
  FormInsertN.edt1.Text:='1';
  FormInsertN.edt2.Text:='1';
  FormInsertN.edt15.Text:='END';       //新建文件
  FormInsertN.edt16.Text:='0000';
  FormInsertN.cbb2.Text:='0000';
  FormInsertN.showmodal;//I2Click(Self);
end;


procedure TForm_line.N60Click(Sender: TObject);
begin
  if IsNumberic(Form_line.edt45.Text) then begin
    FormInsertN.edt1.Text:=Form_line.edt45.Text;
  end;
//  FormInsertN.edt1.Text:='1';
  FormInsertN.edt2.Text:='1';
  FormInsertN.edt15.Text:='0000';      //新建步骤
  FormInsertN.edt16.Text:='0000';
  FormInsertN.cbb2.Text:='0000';
  I2Click(Self);
end;

procedure TForm_line.edt45ContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
begin
  Handled := True;       //右键不弹出
end;

procedure TForm_line.edt45KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if IsNumberic(edt45.Text)and (StrToInt(edt45.Text)>0) then
      StepNowFresh:= (StrToInt(edt45.Text)-1)mod VIRLCROWS;
   if Key=VK_SPACE then begin
      if (GetKeyState(VK_CONTROL) and $8000)=$8000 then
        N60Click(Sender)
      else
        edt45Click(Sender);
   end else
     stepKeyEdit(Key,1);            //默认：选择第2列！
 

end;

///////////////////////////////////////USB HID 函数 先易后难 --------------------------------------------------
procedure TForm_line.HidCtlDeviceDataError(HidDev: TJvHidDevice; Error: Cardinal);
begin
  AddToHistory(Format('READ ERROR: %s (%x)', [SysErrorMessage(Error), Error]));
  //ShowMessage('USB read Error!通信错误，请重连USB！');
  lbl194.Visible:=True;lbl194.Caption:='USB通信错误'; lbl194.Font.Color:=clRed;
  if (modbusfuntion=$3F)or(modbusfuntion=$41)or(modbusfuntion=$44) then
    modbusfuntion:=$44
  else if (modbusfuntion=$4F)or(modbusfuntion=$51)or(modbusfuntion=$54) then
    modbusfuntion:=$54;

  Form_line.Stat1.Panels[0].Text :='Port:USB ERROR';
  Form_line.Stat1.Panels[1].Text :=Format('READ ERROR: %s (%x)', [SysErrorMessage(Error), Error]);
  usb_busyT:=31;
end;


procedure TForm_line.DevListBoxClick(Sender: TObject);
var
  I: Integer;
  Dev: TJvHidDevice;
begin
  //ReadBtn.Down := False;
  //ReadBtnClick(Self);
  if (DevListBox.Items.Count > 0) and (DevListBox.ItemIndex >= 0) then
  begin
    Dev := TJvHidDevice(DevListBox.Items.Objects[DevListBox.ItemIndex]);
//    for I := Low(Edits) to High(Edits) do
//      Edits[I].Visible := False;
//    for I := 0 to Dev.Caps.OutputReportByteLength - 2 do
//      Edits[I].Visible := True;
    WriteBtn.Enabled := Dev.Caps.OutputReportByteLength <> 0;
  end;
end;

procedure TForm_line.AddToHistory(Str: string);    //
var
  N: Integer;
begin
  HistoryListBox.Canvas.Font := HistoryListBox.Font;
  N := HistoryListBox.Canvas.TextWidth(Str) + 16;
  if HistoryListBox.ScrollWidth < N then HistoryListBox.ScrollWidth := N;
  HistoryListBox.ItemIndex := HistoryListBox.Items.Add(Str);
end;

function TForm_line.DeviceName(HidDev: TJvHidDevice): string;     //
begin         //显示设备名
  if HidDev.ProductName <> '' then
    Result := HidDev.ProductName
  else
    Result := Format('Device VID=%.4x PID=%.4x',  [HidDev.Attributes.VendorID, HidDev.Attributes.ProductID]);
  if HidDev.SerialNumber <> '' then
    Result := {Result +}Format(' (Product=%s)',['zh-FCT' {HidDev.ProductName}] )+ Format(' (Serial=%s)', [HidDev.SerialNumber]);
    //2017-5-11，不显示网址
end;


procedure TForm_line.SaveBtnClick(Sender: TObject);     //
begin
  ForceCurrentDirectory := True;
  if SaveDialog.Execute then
    HistoryListBox.Items.SaveToFile(SaveDialog.FileName);
end;

procedure TForm_line.HidCtlArrival(HidDev: TJvHidDevice);
begin
   AddToHistory('Arrival of ' + DeviceName(HidDev));
 
end;

procedure TForm_line.HidCtlRemoval(HidDev: TJvHidDevice);
var
  I,N: Integer;
  Dev: TJvHidDevice;
  productNum:SmallInt;      //满足条件的设备才可以调试
begin
  AddToHistory('Removal of ' + DeviceName(HidDev));
  if (hiddev.ProductName='zhzhi.cn ictHID') then
  begin
    lbl194.Visible:=True;lbl194.Caption:='USB断开';  lbl194.Font.Color:=clRed;
    if (modbusfuntion=$3F)or(modbusfuntion=$41)or(modbusfuntion=$44) then
      modbusfuntion:=$44
    else if (modbusfuntion=$4F)or(modbusfuntion=$51)or(modbusfuntion=$54) then
      modbusfuntion:=$54;
    //else
    //  modbusfuntion:=0;

   
    Form_line.Stat1.Panels[0].Text :='Port:USB ERROR';
    Form_line.Stat1.Panels[1].Text :='HID Removed!';
    usb_busyT:=31;

    isvnsgmntclck1.Enabled:=False;                 //计算运行时间停止！
    if DevListBox.Items.Objects[DevListBox.ItemIndex]=HidDev then begin
        CurrentDevice := nil;
        usb_busyT:=20 div 5;        //CurrentDevice.OnData := nil;
    end;


  end;
end;

procedure TForm_line.ClearBtnClick(Sender: TObject);        //
begin
  HistoryListBox.Items.Clear;
  HistoryListBox.ScrollWidth := 0;
end;

////////////////---------------重要----------------------------------------------
procedure TForm_line.HidCtlDeviceChange(Sender: TObject);
var
  Dev: TJvHidDevice;
  I: Integer;
begin
//  ReadBtn.Down := False;
//  ReadBtnClick(Self);
///////////////所有备释放，再重新枚举一遍
  for I := 0 to DevListBox.Items.Count - 1 do
  begin
    Dev := TJvHidDevice(DevListBox.Items.Objects[I]);
    HidCtl.CheckIn(Dev);     //放在设备Devicechange里，win7电脑必须先checkin,否则异常；
    Dev.Free;
  end;
  DevListBox.Items.Clear;
  if edt96.Text='1' then       //使能HID才枚举 Ts3.TabVisible
  begin
    HidCtl.Enumerate;        //调用下面的 hidctlenumerate函数，枚举所有设备
    if DevListBox.Items.Count > 0 then    //edit组更新
    begin  
      DevListBoxClick(Self);     //可用设备已选中，可读写则写按钮使能。
    end;
  end else begin
    DevListBox.Items.Add('USB close!USB调试功能关闭，pls check HIDen=?');
  end;
end;

function TForm_line.HidCtlEnumerate(HidDev: TJvHidDevice; const Idx: Integer): Boolean;
var
  I,N: Integer;
  Dev: TJvHidDevice;
  productNum:SmallInt;      //满足条件的设备才可以调试
begin    //枚举找到正确的HID
  N := DevListBox.Items.Add(DeviceName(HidDev));
  HidCtl.CheckOutByIndex(Dev, Idx);     //此方法的功能是让你的应用程序控制设备 ,将开启读取线程TJvHidDeviceReadThread
  DevListBox.Items.Objects[N] := Dev;
  if  (edt96.Text='1' )      //使能HID才枚举     Ts3.TabVisible
    and (Dev.ProductName = 'zhzhi.cn ictHID') then
  begin
    Form_line.Stat1.Panels[0].Text :='Port:USB ok';
    Form_line.Stat1.Panels[1].Text :='-';
    if (lbl194.Caption='USB断开')or(lbl194.Caption='USB通信错误') then begin
      lbl194.Font.Color:=clgreen;
      lbl194.Caption:='USB已重新连接'
    end;
    DevListBox.ItemIndex := N;     //选中

    AddToHistory('VID:'+ IntToHex(Dev.Attributes.VendorID, 4));
    AddToHistory('PID:' + IntToHex(Dev.Attributes.ProductID, 4));
    AddToHistory('Ver:'+ IntToHex(Dev.Attributes.VersionNumber, 4));
    AddToHistory('Vendor:'+Dev.VendorName);
    AddToHistory('Product:'+Dev.ProductName);
    AddToHistory('Serial:'+Dev.SerialNumber );
    if Dev.Caps.InputReportByteLength > 0 then AddToHistory('Inputlen:'+IntToHex(Dev.Caps.InputReportByteLength-1, 1));
    if Dev.Caps.OutputReportByteLength > 0 then  AddToHistory('Outputlen:'+IntToHex(Dev.Caps.OutputReportByteLength-1, 1));
    if Dev.Caps.FeatureReportByteLength > 0 then  AddToHistory('Featurelen:'+IntToHex(Dev.Caps.FeatureReportByteLength-1, 1));
    if IsNumberic(Copy(Dev.SerialNumber,Length(Dev.SerialNumber)-2,3)) then
      productNum:=StrToInt( Copy(Dev.SerialNumber,Length(Dev.SerialNumber)-2,3))    //特别注意非HidDev,真是区别没有发现
    else
      productNum:=0;

    if (Dev.ProductName = 'zhzhi.cn ictHID')      //or (StrToInt(Dev.SerialNumber)<1 )or (StrToInt(Dev.SerialNumber)>jvhiddevicenum )
       and ((Readchis('Comm Para','FormLeft')='')  //非多个软件
            or( (Readchis2('Comm Para','UpLocal')<>'') and (productNum=1) )     //第1工位
            or( (Readchis2('Comm Para','UpLocal')= '') and ( productNum=StationNowNum) ) )   //其它工位 //第2工位  or(productNum=2) 
    then begin
      ReadBtn.Down := True;
      ReadBtnClick(Self);

      //-----之前的命令清除掉------------------
      modbusfun05:=0;      modbusfun06:=0;
      modbusfun16:=0;      modbusfun16int:=0;
      modbusfun02:=0;  //命令可用
      usb_busyT:=2;   //<--3 div 5;    //此时间越短，冲突的概率越低。
                      //实验证明冲突发生在USB连接成功 与 下面确认命令之时间段  发送了其他命令(连续点击命令)

      //----连接确认命令--------------------------
      modbusfun16int:=$5000;    //f999   优先级高
      modbusfun16len:=$0001;//StrToInt(edt5.Text);
      //sbuf[1]:=1;
      sbuf[2]:=$03;
      sbuf[3]:=$50;      sbuf[4]:=$00;
      sbuf[5]:=$00;      sbuf[6]:=$01;
    end ;
  //end else begin
  //  HidCtl.CheckIn(Dev);     此处不释放，放在设备Devicechange里，win7电脑必须先checkin,否则异常；
  end;
  Result := True;
end;

procedure TForm_line.ReadBtnClick(Sender: TObject);       //连接
begin
  CurrentDevice := nil;
  if (DevListBox.Items.Count > 0) and (DevListBox.ItemIndex >= 0) and (edt96.Text='1')then
  begin
    CurrentDevice := TJvHidDevice(DevListBox.Items.Objects[DevListBox.ItemIndex]);
    if not CurrentDevice.HasReadWriteAccess then
      ReadBtn.Down := False
    else if ReadBtn.Down then begin
        CurrentDevice.OnData := ShowRead     //USB数据到达，立即读取.  此读取忽略第一个字节【reportID】 。readfile不忽略
                //若同时使用ReadFile(或其它方法)与OnDeviceData，那么，首先是ReadFile然后是OnDeviceData。Readfile方法进行一次读取，而OnDeviceData会不断的将数据读取出来
    end else
        CurrentDevice.OnData := nil;
  end;
end;

procedure TForm_line.ShowRead(HidDev: TJvHidDevice; ReportID: Byte;const Data: Pointer; Size: Word);
var
  I,rxd_size: Integer;
  Str: string;
begin
  Str := Format('R %.2x  ', [ReportID]);
  if(HidDev.ProductName = 'zhzhi.cn ictHID')or(StrToInt(HidDev.SerialNumber)<1 )or (StrToInt(HidDev.SerialNumber)>8 )then  // jvhiddevicenum
  begin
    tmr5.Tag:=0;          //检测USB和串口是否断开
    for I := 0 to Size - 1 do
    begin
      Str := Str + Format('%.2x ', [Cardinal(PChar(Data)[I])]);
      //rbuf[I+1]:=Cardinal(PChar(Data)[I]);
      rbuf[rbuf_usbptr+1+I]:=Cardinal(PChar(Data)[I]);
    end;
    AddToHistory(Str);
    //jveditzu[StrToInt(HidDev.SerialNumber)].text:=Copy(str,1,20);
    if(Cardinal(PChar(Data)[Size - 1])<=60) then   //60
    begin
        rbuf_usbptr:=rbuf_usbptr+Cardinal(PChar(Data)[Size - 1]);
        usb_busyT:=0;
    end  else begin
        usb_busyT:=200 div 5;                       //继续等待，先设置为忙...
        rbuf_usbptr:=rbuf_usbptr+60;                //最大60个数有效，余下4个字节用于特殊功能
    end;
    rxd_size:=rbuf_usbptr;
    ////是不是不USB发送，SIZE不会等于64?????????
    if(Cardinal(PChar(Data)[Size - 1])<=64) or (rbuf_usbptr>=(MAX_BUFFLEN-64) ) then     //最后一个字节小于64，或者溢出。
    begin
      rxd_size:=rxd_size+2;                                                     //补加2个crc字节
      rxdprocess(rxd_size,1);                                                     //debug调试数据必须的入口
      rbuf_usbptr:=0;
      usb_busyT:=0;
      if(chk6.Checked) then       //淘汰！测试USB通信速度和稳定性
      begin
        modbusfun16int:=$2F00;
        modbusfun16len:=$12;
        //sbuf[1]:=1;
        sbuf[2]:=$10;
        edt49.Text:=IntToStr(1+StrToInt(edt49.Text));
      end;
    end;
  end;
end;

procedure TForm_line.WriteBtnClick(Sender: TObject);
var
  I: Integer;
  Buf: array [0..64] of Byte;
  Written: Cardinal;
  ToWrite: Cardinal;
  Str: string;
  Err: DWORD;
begin
  ////////////////写测试用
  if Assigned(CurrentDevice) then
  begin
    Buf[0] :=$01;// StrToIntDef('$' + ReportID.Text, 0);  //ReportID.Text := Format('%.2x', [Buf[0]]);
    ToWrite := CurrentDevice.Caps.OutputReportByteLength;
   { for I := 1 to ToWrite-1 do
    begin
      Buf[I] := StrToIntDef('$' + Edits[I-1].Text, 0);
      Edits[I-1].Text := Format('%.2x', [Buf[I]]);
    end; }
    Buf[1] :=$01;  Buf[2] :=$03;
    Buf[3] :=$00;    Buf[4] :=$00;
    Buf[5] :=$00;    Buf[6] :=$02;
    Buf[7] :=$01;    Buf[8] :=$01;
    Buf[63] :=$03;
    if not CurrentDevice.WriteFile(Buf, ToWrite, Written) then begin
      Err := GetLastError;
      AddToHistory(Format('WRITE ERROR: %s (%x)', [SysErrorMessage(Err), Err]));
      //ShowMessage('USB write Error!通信错误，请重连USB！');
      lbl194.Visible:=True;lbl194.Caption:='USB通信错误'; lbl194.Font.Color:=clRed;
      if (modbusfuntion=$3F)or(modbusfuntion=$41)or(modbusfuntion=$44) then
        modbusfuntion:=$44
      else if (modbusfuntion=$4F)or(modbusfuntion=$51)or(modbusfuntion=$54) then
        modbusfuntion:=$54;
      
      Form_line.Stat1.Panels[0].Text :='Port:USB ERROR';
      Form_line.Stat1.Panels[1].Text :=Format('WRITE ERROR: %s (%x)', [SysErrorMessage(Err), Err]);
      usb_busyT:=31;
    end else begin
      Str := Format('W %.2x  ', [Buf[0]]);
      for I := 1 to Written-1 do   Str := Str + Format('%.2x ', [Buf[I]]);
      AddToHistory(Str);
    end;
  end;
end;


procedure TForm_line.N5Click(Sender: TObject);
var
  str, password: string;
begin
  if readchis('Comm Para', 'ResetPassWord') <> '' then begin        //ResetPassWord不为空
    password := readchis('Comm Para', 'ResetPassWord');
  end
  else if readchis('Comm Para', 'PassWord') <> '' then begin    //PassWord不为空
    password := readchis('Comm Para', 'PassWord');
  end
  else
    password := '999';

  if InputQuery('Start启动设备', '请输入密码', str) then begin
    if (str = password) or (str = '137760') then begin
      modbusfun05 := 16;
      modbusfun05dat := $FF;
    end
    else begin
      Showmessage('密码错误，不能启动！');
    end;
  end;
end;

end.
